(function($) {
  $( ".boton-modal-suscripcion" ).click(function() {
    var id_modal = $(this).attr('id');

    $("#modal" + id_modal).css('display', 'block');

    let windowPosition = $(window).scrollTop();
    if (windowPosition >= 0 && windowPosition < 100) {
      $('html, body').animate({ scrollTop: 100 }, 50);
    }

    $( "span.cerrar" ).click(function() {
      $('.modal-suscripcion').css('display', 'none');
    });

  });
})( jQuery );
  
  
