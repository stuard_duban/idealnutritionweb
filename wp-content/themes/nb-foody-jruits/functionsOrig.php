<?php
/**
 * nb_foody functions and definitions
 *
 * @package Netbase
 */
/**
 * Define theme constants
 */
if (!function_exists('nbfoody_default_options')) {

    function nbfoody_default_options($option) {
        $default = array(
            'nbcore_blog_archive_layout' => 'classic',
            'nbcore_blog_sidebar' => 'right-sidebar',
            'nbcore_excerpt_only' => false,
            'nbcore_excerpt_length' => '35',
            'nbcore_blog_single_sidebar' => 'right-sidebar',
            'nbcore_color_scheme' => 'scheme_1',
            'nbcore_primary_color' => '#6abf77',
            'nbcore_secondary_color' => '#6abf77',
            'nbcore_background_color' => '#ffffff',
            'nbcore_inner_background' => '#edf0f5',
            'nbcore_heading_color' => '#323232',
            'nbcore_body_color' => '#444444',
            'nbcore_link_color' => '#000000',
            'nbcore_link_hover_color' => '#6abf77',
            'nbcore_divider_color' => '#d7d7d7',
            'nbcore_header_style' => 'left-inline_1',
            'nbcore_logo_upload' => '',
            'nbcore_logo_width' => '210',
            'nbcore_header_fixed' => false,
            'header_bgcolor' => '#ffffff',
            'nbcore_blog_width' => '70',
            'nbcore_blog_meta_date' => true,
            'nbcore_blog_meta_read_time' => true,
            'nbcore_blog_meta_author' => true,
            'nbcore_blog_meta_category' => true,
            'nbcore_blog_meta_tag' => true,
            'nbcore_blog_sticky_sidebar' => false,
            'nbcore_blog_meta_align' => 'left',
            'show_title_section' => true,
            'nbcore_page_title_padding' => '75',
            'nbcore_page_title_color' => '#444444',
            'body_font_family' => 'google,Open Sans',
            'body_font_style' => '400',
            'body_font_size' => '16',
            'heading_font_family' => 'google,Open Sans',
            'heading_font_style' => '400',
            'heading_base_size' => '16',
            'subset_cyrillic' => false,
            'subset_greek' => false,
            'subset_vietnamese' => false,
            'nbcore_wc_breadcrumb' => true,
            'nbcore_wc_content_width' => '70',
            'nbcore_pa_swatch_style' => '',
            'nbcore_wc_attr' => false,
            'nbcore_shop_title' => esc_html__('Shop', 'nb-foody'),
            'nbcore_shop_action' => true,
            'nbcore_shop_sidebar' => 'right-sidebar',
            'nbcore_loop_columns' => 'three-columns',
            'nbcore_products_per_page' => '12',
            'nbcore_product_list' => 'grid-type_1',
            'nbcore_shop_content_width' => '70',
            'nbcore_grid_product_description' => true,
            'nbcore_pd_details_title' => true,
            'nbcore_pd_details_width' => '70',
            'nbcore_pd_details_sidebar' => 'right-sidebar',
            'nbcore_wc_sale' => 'style-1',
            'nbcore_pd_images_width' => '50',
            'nbcore_pd_thumb_pos' => 'bottom-thumb',
            'nbcore_pd_meta_layout' => 'left-images',
            'nbcore_pd_featured_autoplay' => false,
            'nbcore_info_style' => 'accordion-tabs',
            'nbcore_reviews_form' => 'full-width',
            'nbcore_reviews_round_avatar' => true,
            'nbcore_add_cart_style' => 'style-1',
            'nbcore_pd_show_social' => true,
            'nbcore_show_related' => true,
            'nbcore_pd_related_columns' => '3',
            'nbcore_show_upsells' => false,
            'nbcore_pd_upsells_columns' => '3',
            'nbcore_pb_background' => '#6abf77',
            'nbcore_pb_background_hover' => '#71c07f',
            'nbcore_pb_text' => '#ffffff',
            'nbcore_pb_text_hover' => '#ffffff',
            'nbcore_pb_border' => '#6abf77',
            'nbcore_pb_border_hover' => '#71c07f',
            'nbcore_sb_background' => 'transparent',
            'nbcore_sb_background_hover' => '#6abf77',
            'nbcore_sb_text' => '#6abf77',
            'nbcore_sb_text_hover' => '#ffffff',
            'nbcore_sb_border' => '#6abf77',
            'nbcore_sb_border_hover' => '#6abf77',
            'nbcore_button_padding' => '30',
            'nbcore_button_border_radius' => '0',
            'nbcore_button_border_width' => '2',
            'nbcore_cart_layout' => 'cart-layout-2',
            'nbcore_show_cross_sells' => true,
            'nbcore_cross_sells_per_row' => '4',
            'nbcore_cross_sells_limit' => '6',
            'home_page_title_section' => false,
            'nbcore_show_footer_top' => false,
            'nbcore_footer_top_layout' => 'layout-9',
            'nbcore_footer_top_color' => '#cccccc',
            'nbcore_footer_top_bg' => '#333333',
            'nbcore_show_footer_bot' => false,
            'nbcore_footer_bot_layout' => 'layout-9',
            'nbcore_footer_bot_color' => '#cccccc',
            'nbcore_footer_bot_bg' => '#333333',
            'nbcore_footer_abs_color' => '#cccccc',
            'nbcore_footer_abs_bg' => '#333333',
            'nbcore_top_section_padding' => '10',
            'nbcore_middle_section_padding' => '20',
            'nbcore_bot_section_padding' => '30',
            'nbcore_header_top_bg' => '#282725',
            'nbcore_header_top_color' => '#333333',
            'nbcore_header_middle_bg' => '#ffffff',
            'nbcore_header_middle_color' => '#333333',
            'nbcore_header_bot_bg' => '#fff',
            'nbcore_header_bot_color' => '#333333',
            'nbcore_footer_top_heading' => '#999999',
            'nbcore_footer_bot_heading' => '#323232',
            'nbcore_blog_archive_comments' => true,
            'nbcore_blog_archive_summary' => true,
            'nbcore_blog_archive_post_style' => 'style-1',
            'nbcore_blog_single_title_position' => 'position-1',
            'nbcore_blog_single_show_thumb' => true,
            'nbcore_blog_single_title_size' => '50',
            'nbcore_blog_single_show_social' => true,
            'nbcore_blog_single_show_author' => true,
            'nbcore_blog_single_show_nav' => true,
            'nbcore_blog_single_show_comments' => true,
            'nbcore_page_title_size' => '50',
            'nbcore_footer_abs_padding' => '10',
            'share_buttons_style' => 'style-1',
            'share_buttons_position' => 'inside-content',
            'pagination_style' => 'pagination-style-1',
            'show_back_top' => true,
            'back_top_shape' => 'square',
            'back_top_style' => 'light',
            'shop_sticky_sidebar' => false,
            'product_sticky_sidebar' => false,
            //meta
            'page_thumb' => 'no-thumb',
            'page_sidebar' => 'full-width',
            'page_content_width' => '70',
            'nbcore_blog_masonry_columns' => '2',
            'product_category_wishlist' => true,
            'product_category_quickview' => true,
        );

        if (!empty($default[$option])) {
            return $default[$option];
        }
    }

}
if (!function_exists('nbfoody_header_add_to_cart_fragment')) {

    function nbfoody_header_add_to_cart_fragment($fragments) {
        global $woocommerce;

        ob_start();
        ?>
        <a class="nb-cart-section" href="<?php echo wc_get_cart_url(); ?>"
           title="<?php esc_attr_e('View cart', 'nb-foody'); ?>">
            <i class="icon-shopping-cart"></i>
            <span class="count"><?php echo WC()->cart->get_cart_contents_count(); ?></span>
            <span class="total"><?php echo WC()->cart->get_cart_total(); ?></span>
        </a>
        <?php
        $fragments['a.nb-cart-section'] = ob_get_clean();

        return $fragments;
    }

    add_filter('woocommerce_add_to_cart_fragments', 'nbfoody_header_add_to_cart_fragment');
}
if (!function_exists('nbfoody_header_woo_section')) {

    function nbfoody_header_woo_section($account = TRUE) {
        $header_style = nbfoody_get_options('nbcore_header_style');

        if (in_array('woocommerce/woocommerce.php', apply_filters('active_plugins', get_option('active_plugins')))) {
            if ($account):
                ?>
                <div class="header-account-wrap">
                    <?php
                    if (is_user_logged_in()):
                        if ('left-inline' == $header_style):
                            ?>
                            <i class="icon-user-o"></i>
                        <?php else: ?>
                            <span class="account-text"><?php esc_html_e('My Account', 'nb-foody'); ?></span>
                        <?php endif; ?>
                        <div class="nb-account-dropdown">
                            <?php wc_get_template('myaccount/navigation.php'); ?>
                        </div>
                    <?php else: ?>
                        <a href="<?php echo get_permalink(get_option('woocommerce_myaccount_page_id')); ?>"
                           class="not-logged-in" title="<?php esc_attr_e('Login', 'nb-foody'); ?>">
                               <?php if ('left-inline' == $header_style): ?>
                                <i class="icon-text-height"></i>
                            <?php else: ?>
                                <span class="account-text"><?php esc_html_e('Login', 'nb-foody'); ?></span>
                            <?php endif; ?>
                        </a>
                    <?php endif; ?>
                </div>
            <?php endif; ?>
            <div class="header-cart-wrap">
                <a class="nb-cart-section" href="<?php echo wc_get_cart_url(); ?>"
                   title="<?php esc_attr_e('View cart', 'nb-foody'); ?>">
                    <i class="icon-shopping-cart"></i>
                    <span class="count"><?php echo WC()->cart->get_cart_contents_count(); ?></span>
                    <span class="total"><?php echo WC()->cart->get_cart_total(); ?></span>
                </a>
                <div class="mini-cart-section">
                    <div class="mini-cart-wrap">
                        <?php woocommerce_mini_cart(); ?>
                    </div>
                </div>
            </div>
            <?php
        }
    }

}

function my_enqueue($hook) {
    // Only add to the edit.php admin page.
    // See WP docs.
    /*if ('admin.php' !== $hook) {
        echo "funciona?";
        return;
    }*/
    wp_enqueue_script('my_custom_script', get_template_directory_uri() . '/assets/js/custom.js');
}

add_action('admin_enqueue_scripts', 'my_enqueue');

/*Funcion para remover campos*/
add_filter( 'woocommerce_checkout_fields' , 'custom_override_checkout_fields' );

function custom_override_checkout_fields( $fields ) {
    //unset($fields['billing']['billing_first_name']);
    //Si los platillos de suscripcion no estan, se elimina el delivery
    $_POST['orden_suscripcion'] = $_POST['orden_suscripcion'];
    if($_POST['Semana_Siguiente']) {


      //unset($fields['billing']['billing_myfield18']);
      //unset($fields['billing']['billing_myfield18c']);
      //$fields['billing']['billing_myfield18']['required'] = false;
      //$fields['billing']['billing_myfield18c']['required'] = false;
      $fields['billing']['billing_myfield18']['class'] = array(0 => "ocultar-campo",
                                                            1 => "campo-ciudad");
      $fields['billing']['billing_myfield18c']['class'] = array(0 => "ocultar-campo",
                                                                1 => "campo-fecha");
    }


    return $fields;
}



/**
 * Check if a specific product category is in the cart
 */
function wc_ninja_category_is_in_the_cart() {
	// Add your special category slugs here
	$categories = array( 'individual-meals-menu', 'custom-meal');
  //$categories = array( 'individual-meals-menu');

	// Products currently in the cart
	$cart_ids = array();

	// Categories currently in the cart
	$cart_categories = array();

	// Find each product in the cart and add it to the $cart_ids array
	foreach( WC()->cart->get_cart() as $cart_item_key => $values ) {
		$cart_product = $values['data'];
		$cart_ids[]   = $cart_product->id;
	}

	// Connect the products in the cart w/ their categories
	foreach( $cart_ids as $id ) {
		$products_categories = get_the_terms( $id, 'product_cat' );

		// Loop through each product category and add it to our $cart_categories array
		foreach ( $products_categories as $products_category ) {
			$cart_categories[] = $products_category->slug;
		}
	}

	// If one of the special categories are in the cart, return true.
	if ( ! empty( array_intersect( $categories, $cart_categories ) ) ) {
    //echo "hay menus</br>";
		return true;
	} else {
		return false;
	}
}


/*add_filter( 'woocommerce_checkout_get_value', 'populating_checkout_fields', 10, 2 );
function populating_checkout_fields ( $value, $input ) {

    $token = ( ! empty( $_GET['token'] ) ) ? $_GET['token'] : '';

    if( 'testtoken' == $token ) {
        // Define your checkout fields  values below in this array (keep the ones you need)
        $checkout_fields = array(
            'billing_myfield18'    => 'John',
            'billing_myfield18c'    => 'John'
        );
        foreach( $checkout_fields as $key_field => $field_value ){
            if( $input == $key_field && ! empty( $field_value ) ){
                $value = $field_value;
            }
        }
    }
    return $value;
}*/

function has_bought_items() {
    $bought = false;

    // Set HERE ine the array your specific target product IDs
    $prod_arr = array( '696' );

    // Get all customer orders
    /*$customer_orders = get_posts( array(
        'numberposts' => -1,
        'meta_key'    => '_customer_user',
        'meta_value'  => get_current_user_id(),
        'post_type'   => 'shop_order', // WC orders post type
        'post_status' => 'wc-completed' // Only orders with status "completed"
    ) );*/

    $customer_orders = get_posts( array(
        'numberposts' => -1,
        'meta_key'    => '_customer_user',
        'meta_value'  => get_current_user_id(),
        'post_type'   => 'shop_order', // WC orders post type
        'post_status' => 'wc-processing'
    ) );

    echo "customer orders: <pre>";
    var_dump($customer_orders);

    $customer_orders[] = get_posts( array(
        'numberposts' => -1,
        'meta_key'    => '_customer_user',
        'meta_value'  => get_current_user_id(),
        'post_type'   => 'shop_order', // WC orders post type
        'post_status' => 'wc-completed'
    ) );

    echo "customer orders: <pre>";
    var_dump($customer_orders);
    //exit;
    foreach ( $customer_orders as $customer_order ) {
        // Updated compatibility with WooCommerce 3+
        $order_id = method_exists( $order, 'get_id' ) ? $order->get_id() : $order->id;
        $order = wc_get_order( $customer_order );

        // Iterating through each current customer products bought in the order
        foreach ($order->get_items() as $item) {
            // WC 3+ compatibility
            if ( version_compare( WC_VERSION, '3.0', '<' ) ){
                echo "item:w3+ <pre>";
                //var_dump($item);
                exit;
                $product_id = $item['product_id'];
              }
            else {
              echo "item: <pre>";
                var_dump($item);
                $product_id = $item->get_product_id();

                $products_categories = get_the_terms( $id, 'product_cat' );
              }
            // Your condition related to your 2 specific products Ids
            if ( in_array( $product_id, $prod_arr ) )
                $bought = true;
        }
    }
    // return "true" if one the specifics products have been bought before by customer
    return $bought;
}

function add_theme_scripts() {

  wp_enqueue_script( 'custom-validation', get_template_directory_uri() . '/assets/js/custom-validation.js', array ( 'jquery' ), 1.1, true);

  /*Agregando validacion para dispositivos moviles*/
  wp_enqueue_script( 'mobile-links', get_stylesheet_directory_uri() . '/assets/js/custom.js', array ( 'jquery' ), 1.1, true);


}
add_action( 'wp_enqueue_scripts', 'add_theme_scripts' );


/*Adding section in the dashboard*/


/** Step 2 (from text above). */
add_action( 'admin_menu', 'my_plugin_menu' );

/** Step 1.*/
function my_plugin_menu() {
	add_options_page( 'Subscription Manager Options', 'Subscription Manager', 'manage_options', 'subscription-manager', 'subscription_manager' );
}

/** Step 3.*/
function subscription_manager() {
  include_once("wp-config.php");
  include_once("wp-includes/wp-db.php");

  global $wpdb;

	if ( !current_user_can( 'manage_options' ) )  {
		wp_die( __( 'You do not have sufficient permissions to access this page.' ) );
	}
  $blogusers = get_users( [ 'role__in' => [ 'author', 'subscriber' ] ] );

	echo '<div class="wrap">';
	echo '<h1>Subscription Manager</h1>';
	echo '<p>Here you could activate or deactivate the subscription of the members</p>';
  echo '</div>';



  $sql = "SELECT DISTINCT u.ID, u.user_login, s.subscription_state FROM wp_users u JOIN wp_orders_in s on u.ID = s.user";
  $results = $wpdb->get_results($sql);

  /*echo "Usuarios suscritos<pre>";
  print_r($results);*/


  $formularioActivar = "<form id='form-actualizar' action='". get_site_url() ."/wp-content/plugins/IdealNutrition/resources/actualizarSuscripcion.php'  method='post'>";

  $formularioDesactivar = "<form id='form-actualizar' action='". get_site_url() ."/wp-content/plugins/IdealNutrition/resources/actualizarSuscripcion.php'  method='post'>";
  foreach ($results as $key => $usuario) {
    //echo $usuario->user_login;
    //print_r($usuario);


    if($usuario->subscription_state == 0) {
      $formularioActivar .= "<input type='checkbox' name='".$usuario->ID."' value='".$usuario->user_login."' ";
      $formularioActivar .= ">";
      $formularioActivar .= $usuario->user_login . "<br>";
    }
    else {
      $formularioDesactivar .= "<input type='checkbox' name='".$usuario->ID."' value='".$usuario->user_login."' ";
      $formularioDesactivar .= ">";
      $formularioDesactivar .= $usuario->user_login . "<br>";
    }



  }
  $formularioDesactivar .= "<input type='hidden' name='accionSuscrip' value='desactivar'/>
                <input type='submit' value='Desactivar' />
              </form>";

  $formularioActivar .= "<input type='hidden' name='accionSuscrip' value='activar'/>
                <input type='submit' value='Activar' />
              </form>";

  $urlAdmin = admin_url('admin-ajax.php');
  //echo "url $urlAdmin";



echo $formularioActivar . "<br>" . $formularioDesactivar;

}

/*Add checkbox to disable or enable subscription*/

add_action( 'show_user_profile', 'my_extra_user_fields' );
add_action( 'edit_user_profile', 'my_extra_user_fields' );
function my_extra_user_fields( $user )
{
  global $wpdb;
  $sql = "SELECT subscription_state FROM wp_orders_in WHERE user = $user->ID";

  $result = $wpdb->get_results($sql);

  $checked = "";
  $disabled = "";
  if(count($result) > 0) {

    if($result[0]->subscription_state == 1) {
      $checked = "checked";
    }
  }
  else {
    $disabled = "disabled";
  }
  //if($result) {
  ?>
    <table class="form-table" id="subs-container">
        <tr class="user-subscription-wrap">
            <th><label for="user_subscription">Subscription</label></th>
            <td>
                <input id="user_subscription" name="user_subscription" type="checkbox" value="<?php echo $user->ID;?>" <?php echo $checked . " " . $disabled;?> >
                <span class="description"><?php _e("Disable or Enable Subscription"); ?></span>
            </td>
        </tr>
    </table>
<?php //}
 }


add_action( 'personal_options_update', 'save_my_extra_user_fields' );
add_action( 'edit_user_profile_update', 'save_my_extra_user_fields' );

function save_my_extra_user_fields( $user_id )
{
    if ( !current_user_can( 'edit_user', $user_id ) ) { return false; }else{

        if(isset($_POST['user_subscription']) && $_POST['user_subscription'] != ""){
            update_usermeta( $user_id, 'user_subscription', $_POST['user_subscription'] );
        }
    }
}

/*Ajax Callback*/

add_action( 'admin_footer', 'sub_action_javascript' ); // Write our JS below here

function sub_action_javascript() { ?>
  <script type="text/javascript" >

      var subs_field = jQuery('.user-subscription-wrap').html();
      jQuery('.user-subscription-wrap').html('');
      //var role_field = jQuery('.user-role-wrap').html();
      console.log(jQuery('.user-role-wrap').parent())
      jQuery('.user-role-wrap').after(subs_field);

  </script>
	<script type="text/javascript" >
	jQuery('#user_subscription').click(function () {
    console.log("Se clickeo el chekbox");

    var data = {
			'action': 'my_action',
			'id_user': jQuery(this).val(),
      'state' : jQuery(this).attr('checked')
		};

		// since 2.8 ajaxurl is always defined in the admin header and points to admin-ajax.php
		jQuery.post(ajaxurl, data, function(response) {
			alert(response);
		});
  });
	</script>

   <?php
}




add_action( 'wp_ajax_my_action', 'my_action' );

function my_action() {
	global $wpdb;
  $sql = "UPDATE wp_orders_in set subscription_state = ";

  if($_POST['state'] == "checked") {
    $sql .= "1 ";
  }
  else {
    $sql .= "0 ";
  }
  $sql .= "WHERE user = " . $_POST['id_user'];


  $result = $wpdb->get_results($sql);
  //var_dump($result);

  if($result == NULL) {
    echo "The subscription has been update";
  }
  else {
    echo "An error has occurred";
  }
  //echo $result;
	wp_die(); // this is required to terminate immediately and return a proper response
}

add_action( 'init', 'select_your_meals_redirect' );
add_action( 'template_redirect', 'select_your_meals_redirect' );
function select_your_meals_redirect($user) {
  global $wpdb;
  global $woocommerce;
  $sql = "SELECT subscription_state FROM wp_orders_in WHERE user = " . get_current_user_id();
  $user = $wpdb->get_results($sql);

  if ( is_user_logged_in() && is_page( 'select-your-meals' ) ) {

      if(!$user[0]->subscription_state && count($user)) {
        echo "<div style='text-align: center; font-size: 28px; margin-top: 20px;'>You can not access to this page.</div>";
        echo "<script type='text/javascript'>
              var url='https://ideal.luxlifeentertainment.com/my-account/';
              setTimeout('window.location=url',5000);
              </script>";
        //wp_redirect( home_url( '/subscription-needed/' ) );
        $woocommerce->cart->empty_cart();
        exit;
      }
  }
}

add_action( 'init', 'woocommerce_clear_cart_url' );
function woocommerce_clear_cart_url() {
  global $woocommerce;

    if ( is_front_page() && isset( $_GET['empty-cart'] ) ) {
        $woocommerce->cart->empty_cart();
    }
}

add_action('woocommerce_cart_coupon', function () {
    global $woocommerce;
    echo '<a style="background-color: #F44336 !important;border-color: #F44336 !important;margin-top: 10px !important;" class="bt-5 nb-wide-button button" href="' . $woocommerce->cart->get_cart_url() . '?empty-cart">Empty cart</a>';
});