<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package nb-foody
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
    <head>
        <meta charset="<?php bloginfo('charset'); ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <link rel="shortcut icon" type="image/x-icon" href="/wp-content/uploads/2018/06/fav-icon.png">
        <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,400i,700" rel="stylesheet">
        <link rel="profile" href="http://gmpg.org/xfn/11">
        <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
        <?php wp_head(); ?>
        <script>
            jQuery(document).ready(function ($) {
                // Altura de los reviews
                var superaltura = 0;
                $(".jw-review-bottom").each(function (index) {
                    if (superaltura < $(this).height()) {
                        superaltura = $(this).height();
                    }
                });
                superaltura = superaltura + 40;
                $(".jw-review-bottom").css("height", superaltura + "px");
                // Cambio de logo en el home
                //$(".home .main-logo.img-logo img").attr("src", "/wp-content/uploads/2018/03/Logo-ideal-Nutritions-2.png");

            });
        </script>
        <?php
        if (false !== strpos($_SERVER["REQUEST_URI"], 'suscription-plans') || false !== strpos($_SERVER["REQUEST_URI"], 'pay-as-go-you-plans')) {
            ?>
            <style>
                div.product-addtocart:hover {
                    background-color: rgb(248, 246, 244) !important;
                    text-align: center;
                }
                span.subscription-details{display:none !important;}
                div.variations h5.label{display:none;}
                div.variations{
                    position: absolute;
                    bottom: 15px;
                    margin-bottom: 20px;
                    margin-left: 15px;
                    width: 92%;
                }
                <?php if (false !== strpos($_SERVER["REQUEST_URI"], 'pay-as-go-you-plans')) { ?>
                    div.variations_button {
                        margin-top: 48px;
                    }
                <?php } ?>

                .delivery {
                    position: absolute;
                    width: fit-content;
                    left: 50%;
                    max-width: 50px;
                    margin: -54px -25px;
                }
                .delivery .title{
                    font-size: 12px;
                    white-space: nowrap;
                    margin-left: -12px;
                    font-weight: bold;
                    color: #e34700;
                }
                ul.woocommerce-error, #give-as-present, html #wpadminbar,.site-header, .nb-page-title-wrap,.single-product-wrap .product-image,table.variations td.label, .nb-quantity, .cart-url, .product .entry-summary > p.price, div.woocommerce-tabs, .related,footer, .woocommerce .woocommerce-message {display: none !important;}
                .single-product-wrap .entry-summary .entry-title {
                    margin-top: 0;
                    margin-bottom: 10px;
                    <?php if (strpos($_SERVER["REQUEST_URI"], 'suscription-plans') !== false) {
                        ?>background: url(/wp-content/uploads/2018/03/planes-1.png) no-repeat top center;<?php
                    } else {
                        ?>background: url(/wp-content/uploads/2018/06/planes-3.png) no-repeat top center;<?php
                    }
                    ?>
                    background-size: cover;
                    font-size: 27px;
                    color: #fff;
                    text-align: center;
                    padding-top: 10px;
                    padding-bottom: 65px;
                    font-family: 'Roboto';
                    margin: 0;
                }
                table.variations{    position: absolute;
                                     bottom: 0px;}
                .single-product-wrap .cart {
                    position: relative;height: 305px;
                }
                .cart-notice-wrap.active {
                    z-index: 9999999999999999999999999999999999  !important;
                }
                .shop-main.left-images .entry-summary {
                    background: #f8f6f4;
                    box-shadow: 2px 2px 9px rgba(0, 0, 0, 0.3);
                    width: 100% !important;
                    max-width: initial !important;
                    padding: 0px;
                    flex: initial !important;
                    overflow: hidden;
                    max-height: 420px;

                    position: fixed;
                    z-index: 999999999999999999999999 !important;
                    max-width: 398px !important;
                }
                html {
                    margin-top: 0px !important;
                }
                .single_add_to_cart_button{
                    <?php if (strpos($_SERVER["REQUEST_URI"], 'suscription-plans') !== false) {
                        ?>color: #29f5a3 !important;<?php
                    } else {
                        ?>color: #fdcd0b !important;<?php
                    }
                    ?>
                    padding: 0px 15px;
                    background: transparent;
                    text-align: center;
                    max-width: 150px;
                    font-size: 16px;
                    display: block;
                    border-radius: 20px;
                    background-clip: padding-box;
                    margin: 0 auto;
                    margin-top: 35px;
                    max-height: 36px;
                    border: none !important;
                    text-transform: none;
                }
                .single_add_to_cart_button:hover, .single_add_to_cart_button:focus{
                    <?php if (strpos($_SERVER["REQUEST_URI"], 'suscription-plans') !== false) {
                        ?>color: #29f5a3 !important;<?php
                    } else {
                        ?>color: #fdcd0b !important;<?php
                    }
                    ?>
                    background-color: transparent !important;
                }
                .single_add_to_cart_button:before{
                    content: "";
                    position: absolute;
                    margin-top: 5px;
                    margin-left: -15px;
                    border-radius: 25px;
                    min-height: 36px;
                    <?php if (strpos($_SERVER["REQUEST_URI"], 'suscription-plans') !== false) {
                        ?>border: 1px solid #29f5a3 !important;<?php
                    } else {
                        ?>border: 1px solid #fdcd0b !important;<?php
                    }
                    ?>
                    min-width: 114px;
                }
                a.reset_variations{
                    display: none !important;
                    font-size: 11px;
                    cursor: pointer;
                    color: white;
                    background: #FF5722;
                    padding: 2px 10px;
                    border-radius: 25px;
                    position: absolute;
                    right: 38px;
                    bottom: 20px;
                }

                .single-product-wrap .price > span.amount{
                    color: #49665b;
                    text-align: center;
                    font-size: 120px;
                    padding: 0;
                    margin: 0;
                    line-height: 0.7em;
                    font-weight: 100;
                }
                .single-product-wrap .price > span.amount > span.woocommerce-Price-currencySymbol{
                    font-size: 0.3em;
                    top: -1.7em;
                    vertical-align: super;
                }
                .single-product-wrap .woocommerce-variation-price {
                    text-align: center;
                }
                span.subscription-details{
                    display: block;
                    margin: 25px auto -30px;
                    text-align: center;
                    font-size: 12px;
                }
                table.variations td.value{border-top:none !important;}
                span.decimal{font-size: 36px !important;}
                @media screen and (max-width: 768px) {
                    .entry-summary{
                        position: relative !important;
                        margin: 0 auto;
                    }
                }
                @media screen and (max-width: 384px) {
                    .entry-summary{
                        position: relative !important;
                        margin: 0 auto;
                    }
                }
                @media screen and (max-width: 320px) {
                    .single-product-wrap .price > span.amount {
                        font-size: 90px;
                    }
                }

            </style>
            <script type="text/javascript">
                jQuery(document).ready(function () {
                    jQuery('#meals').each(function () {
                        jQuery(this).change(function () {
                            var amount = jQuery('.woocommerce-variation-price').find('.woocommerce-Price-amount');
                            var seg = 100;
                            if (amount.html() === undefined) {
                                seg = 1000;
                            }
                            setTimeout(function () {
                                var amount = jQuery('.woocommerce-variation-price').find('.woocommerce-Price-amount');
                                var el = amount.html();
                                el = el.split('.');
                                amount.html('');
                                amount.append(el[0] + '<span class="decimal">.' + el[1] + '</span>');
                            }, seg);
                        });
                        jQuery(this).change();
                    });
                    jQuery('select[name="attribute_meals"]').each(function () {
                        jQuery(this).change(function () {
                            var am = jQuery('.single_variation').find('.woocommerce-Price-amount');
                            var seg = 100;
                            if (am.html() === undefined) {
                                seg = 1000;
                            }
                            setTimeout(function () {
                                var am = jQuery('.single_variation').find('.woocommerce-Price-amount');
                                var ei = am.html();
                                ei = ei.split('.');
                                am.html('');
                                am.append(ei[0] + '<span class="decimal">.' + ei[1] + '</span>');
                            }, seg);
                        });
                        jQuery(this).change();
                    });
                    //

                });
            </script>
            <?php
        }
        if (false !== strpos($_SERVER["REQUEST_URI"], 'gift-card') && false !== strpos($_SERVER["REQUEST_URI"], 'product')) {
            ?>
            <style>
                #wpadminbar,header, footer, .woocommerce .woocommerce-message .button, div.nb-page-title-wrap, .related, .woocommerce-tabs, .product-image,
                .product_title, .summary > p.price, .product_meta, .nb-social-icons, .ywgc-template, .ywgc-editor-section-title, .ywgc-amount-label,.nb-quantity{display:none !important;}
                .woocommerce .woocommerce-message{
                    position: fixed;
                    top: 56%;
                    z-index: 100;
                    width: 600px;
                }
                .ywgc-amount-value{border: none !important;padding: 0px !important;}

                .entry-summary{width: 100% !important;flex:initial !important; max-width: initial !important;}
                #ywgc-sender-name {width: 100%;}html {
                    margin-top: -25px !important;
                }.wpb_content_element,.gift-cards-list{
                    margin-bottom: 0px !important;
                }
                .amount p{padding-top: 0px !important}
                #content .container{padding: 0px !important; margin: 0px !important}
                .single_add_to_cart_button{
                    margin: 0 auto;
                    padding: 0px 30px;
                    background-color: #fa4600 !important;
                    border: none !important;
                    border-radius: 25px;
                }

            </style>
            <?php
        }
        ?>
        <style>
<?php
if (false !== strpos($_SERVER["REQUEST_URI"], 'custom-meals-chicken-64') || false !== strpos($_SERVER["REQUEST_URI"], 'custom-meal-skirt-steak-60')) {
    ?>p.price{display:none;}<?php
}
?>
            div[data-original-title="Free delivery"] span.fui-select,
            div[data-original-title="Cost 5 $"] span.fui-select,
            div[data-original-title="Cost 10 $"] span.fui-select{
                top: -22px !important;
                right: 15px !important;
            }
            .fui-select {
                background-image: url(/wp-content/uploads/2018/04/Select.png) !important;
                background-repeat: no-repeat !important;
                background-color: #71ca08 !important;
                background-size: 35px 15px !important;
                background-position: 50% !important;
                border-radius: 5px !important;
                width: 50px !important;
                height: 24px !important;
            }
            #mainPanel .genSlide .genContent div.selectable.checked span.icon_select{
                color: #F44336 !important;
            }
            li.mini_cart_item img{max-height: 73px !important;}
            li.mini_cart_item .variation{opacity: 0;height: 0px;margin-bottom: -15px;}
            .fzbuk-login-form-wrap {
                -webkit-box-sizing: border-box !important;
                -moz-box-sizing: border-box !important;
                box-sizing: border-box !important;
                background: #02d839 !important;
                background: -moz-radial-gradient(center, ellipse cover, #5170ad 0%, #355493 100%) !important;
                background: -webkit-gradient(radial, center center, 0px, center center, 100%, color-stop(0%, #5170ad), color-stop(100%, #355493)) !important;
                background: -webkit-radial-gradient(center, ellipse cover, #4CAF50 0%, #02d839 100%) !important;
                background: -o-radial-gradient(center, ellipse cover, #5170ad 0%, #355493 100%) !important;
                background: -ms-radial-gradient(center, ellipse cover, #5170ad 0%, #355493 100%) !important;
                background: radial-gradient(ellipse at center, #4CAF50 0%, #02d839 100%) !important;
                filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#5170ad', endColorstr='#355493',GradientType=1 ) !important;
                border: 1px solid #8BC34A !important;
                box-shadow: 0 1px #AED581 inset, 0 0 10px 5px rgba(0, 0, 0, 0.1) !important;
                border-radius: 5px !important;
                position: relative !important;
                width: 360px !important;
                margin: 10px auto !important;
                padding: 50px 30px 30px 30px !important;
                text-align: center !important;
            }
            .ult-modal-input-wrapper.ult-adjust-bottom-margin{margin-bottom: 0px !important;}
            .ultoverlay{
                position: fixed;
                width: 100%;
                height: 100%;
                top: 0;
                bottom: 0;
                left: 0;
                right: 0;
                background: rgba(28,28,28,.9);
                z-index: 9999;
                z-index: 16777271;
            }
            .ultoverlay form {
                position: absolute;
                left: 50%;
                top: 50%;
            }
            .ultoverlay .close{    position: absolute;cursor:pointer;
                                   font-size: 36px;
                                   right: 30px;
                                   top: 30px;}
			
            .modallogin form {
                margin-top: -207px;
                margin-left: -180px;
            }
            .modalregister form {
                margin-top: -218px;
                margin-left: -180px;
            }
            .nb-navbar{max-width:700px;}
            .site-header.left-inline_1 .middle-section .icon-header-section {
                max-width: 20%;
            }
            @media screen and (max-width: 992px) {
                .nb-navbar .menu-item > a span {
                    color: #27ad2c !important;
                }
                .menu-user{
                    display: inline-block !important;
                }
                #menu-user{
                    display: none !important;
                }
            }
            @media screen and (min-width: 993px) {
                .menu-user{
                    display: none !important;
                }
                #menu-user{
                    display: inline-block !important;
                }
            }
            @media screen and (max-width: 768px) {
                .nb-navbar .menu-item > a span {
                    color: #27ad2c !important;
                }
                #form-wysija-2, p.presponsive{text-align: center !important;}
            }
            @media screen and (max-width: 680px) {
				
                .nb-navbar .menu-item > a span {
                    color: #27ad2c !important;
                }
                .woocommerce-MyAccount-navigation, .woocommerce-MyAccount-content {
                    width: 100%;
                }
                .widget .widget-title, .widget ul li {text-align: center;}
            }
            @media screen and (max-width: 384px) {
                .jw-login{min-width:200px !important}
                .nb-navbar .menu-item > a span {
                    color: #27ad2c !important;
                }
                .box_content_netbase_team_nb_post_type2_0 .img_post_netbase_team {
                    display: block;
                    width: 100%;
                }
                .content_post_netbase_team {
                    display: block;
                    width: 100%;
                }
                #form-wysija-2, p.presponsive{text-align: center !important;}
                div.vc_col-sm-6,div.vc_col-sm-4{width:100% !important;}
                .jr-about-us .aio-icon-component.contenidos_jw1 .aio-ibd-block .aio-icon-description {
                    flex-direction: initial;
                    width: 100%;
                    text-align: justify;
                    float: left;
                }
                .site-header.left-inline_1 .middle-section .icon-header-section {
                    display: none;
                    max-width: 70%;
                    margin-left: 20%;
                }
                .widget .widget-title, .widget ul li {text-align: center;}
                h1.size-42{font-size:42px !important}
            }
            @media screen and (max-width: 320px) {
                .jw-login{min-width:200px !important}
                .nb-navbar .menu-item > a span {
                    color: #27ad2c !important;
                }
                .box_content_netbase_team_nb_post_type2_0 .img_post_netbase_team {
                    display: block;
                    width: 100%;
                }
                .content_post_netbase_team {
                    display: block;
                    width: 100%;
                }
                h1.size-42{font-size:42px !important}
                .jr-about-us .aio-icon-component.contenidos_jw1 .aio-ibd-block .aio-icon-description {
                    flex-direction: initial;
                    width: 80%;
                    text-align: justify;
                    float: left;
                }
                .site-header.left-inline_1 .middle-section .icon-header-section {
                    max-width: 70%;
                    margin-left: 20%;
                    display: none;
                }
                .widget .widget-title, .widget ul li {text-align: center;}
            }
<?php if (false === strpos($_SERVER["REQUEST_URI"], 'suscription-plans') && false === strpos($_SERVER["REQUEST_URI"], 'pay-as-go-you-plans')) { ?>
                /*div.variations, .single_variation_wrap span.price{display:none !important;}*/
<?php } ?>
            i.icon-garbage{color:#333333 !important;}
            .socials-section{opacity: 0 !important;}
            .img-logo{
                margin-top: -20px;
            }
            @media screen and (min-width: 768px) {
                .main-logo img {
                    min-width: 200px;
                }
            }
            @media (max-width: 575px){
                .site-header.left-inline_1 .middle-section .main-logo {
                    -webkit-box-flex: initial !important;
                    -ms-flex: initial !important;
                    flex: initial !important;
                    margin: 0 auto !important;
                    text-align: center !important;
                }
            }
            #estimation_popup.wpe_bootstraped #mainPanel .genSlide .genContent .col-md-2 {
                margin-right: 20px !important;
            }
            
<?php if (false !== strpos($_SERVER["REQUEST_URI"], 'product')) { ?>
                .woocommerce-breadcrumb{display:none !important}
<?php } ?>
            button#give-as-present {
                margin: 10px 0;
                border-radius: 0px;
                min-height: 51px;
            }
            .nb-page-title-wrap {
                padding-top: 75px;
                padding-bottom: 75px;
                background-image: url(https://ideal.reallibertychange.com/wp-content/uploads/2018/05/shutterstock_326600915.jpg);
                background-repeat: no-repeat;
            }
            .nb-page-title-wrap h2 {
                font-size: 50px;
                color: #fff;
                font-weight: bold;
                text-shadow: 3px 3px black;
            }
            .nb-page-title-wrap nav {
                color: #444444;
                padding: 10px 15px;
                background-color: white;
            }
            a[data-product_id].product_type_simple>i:before, a.btn-special-add-cart >i:before {
                font-family: 'fontello';
                font-size: 23px;
                content: '\e909';
                font-style: normal !important;
                color: #9a9a9a !important;
            }
<?php if (false === strpos($_SERVER["REQUEST_URI"], 'suscription-plans') || false === strpos($_SERVER["REQUEST_URI"], 'pay-as-go-you-plans')) { ?>
                div.product-addtocart{text-align: center;min-height: 45px;padding-top: 5px;width: 100%;}
                div.product-addtocart:hover{background-color: rgba(106, 191, 119, 1);text-align: center;}
                div.product-addtocart:hover i:before{color:#fff !important;}
                .checked .quantityBtns{display:block !important}
<?php } ?>
            

<?php if (false !== strpos($_SERVER["REQUEST_URI"], 'suscription-plans') || false !== strpos($_SERVER["REQUEST_URI"], 'pay-as-go-you-plans')) { ?>
                /*.variations,.woocommerce-variation-price{display:initial !important}
                .single_variation_wrap{ text-align: center !important; }*/
                div.variations_button {
                    margin-top: -5px;
                }
<?php } else { ?>
                .variations,.woocommerce-variation-price{display:none !important}
                div.product-addtocart:hover {
                    background-color: rgb(255, 255, 255) !important;
                    text-align: center;
                }
<?php } ?>
            body{ overflow-x: hidden !important}
            .type_item_Suscription{
                color: #3f6d33;
                font-weight: bold;
            }
            .type_item_Bebidas{
                color: #313969;
                font-weight: bold;
            }
            .type_item_none{
                color: #1a1a1a;
                font-weight: bold;
            }
            .type_item_Suscription .picture:hover .more {
                border: 3px solid #3f6d33 !important;
            }
            .type_item_Bebidas .picture:hover .more {
                border: 3px solid #313969 !important;
            }
            .type_item_none .picture:hover .more {
                border: 3px solid #1a1a1a !important;
            }
            a.yith-wcqv-button{     float: right !important;
                                    margin-left: 140px !important;}
            .products .product .product-action .button {
                border-left: 1px solid #dedede !important;
                border-right: 1px solid #dedede !important;
                border-bottom: 1px solid #dedede !important;
            }
            #estimation_popup.wpe_bootstraped #mainPanel .genSlide .genContent .itemDes {
                margin-top: 16px!important;
                font-size: 12px;
                line-height: 14px;
                margin-left: -25px;
                margin-right: -25px;
                border-radius: 5px;
                padding: 10px;
                background-color: rgb(52, 73, 94);
                text-overflow: ellipsis;
                overflow: hidden;
                white-space: nowrap;
                color: #fff;
            }
            
        </style>
        <style>
            /** Generated by FG **/
            @font-face {
                font-family: 'MankSans';
                src: url('/wp-content/themes/nb-foody-jruits/fonts/MankSans.eot');
                src: local('☺'), url('/wp-content/themes/nb-foody-jruits/fonts/MankSans.woff') format('woff'), url('/wp-content/themes/nb-foody-jruits/fonts/MankSans.ttf') format('truetype'), url('/wp-content/themes/nb-foody-jruits/fonts/MankSans.svg') format('svg');
                font-weight: normal;
                font-style: normal;
            }
            body,.fs, .uvc-sub-heading{font-family: 'MankSans' !important;}
            body,.page #site-wrapper,#site-wrapper{ background: #ffffff !important;}
            .page-id-352 .jw-login .botonjw a, .jw-login .botonjw a {
                background: #1ce934;
                color: #fff !important;
                display: block;
                border-radius: 15px;
                padding: 4px 20px;
            }
            .page-id-352 .site-header .top-section-wrap,.site-header .top-section-wrap {
                background-color: #fff !important;
            }
            .socials-section {
                opacity: 1 !important;
            }
            .page-id-352 .top-section-wrap i, .page-id-352 .top-section-wrap div {
                color: rgba(73, 88, 90, 1) !important;
            }
            ul.social-section .social-item {
                font-size: 20px;
                margin-right: 20px;
            }
            .page-id-352 .jw-login .normaljw {
                border-left: 1px solid #49585a;
                margin-left: 6px;
            }
            .page-id-352 .jw-login .botonjw a, .page-id-352 .jw-login .normaljw a {
                color: #49585a;
                font-weight: bold;
                font-size: 14px;
                display: block;
                padding: 4px 10px;
            }
            .page-id-352 .site-header .top-section-wrap {
                border-bottom: 1.3px solid #49585a;
            }
            /*HEADER*/
            .page-id-352 .site-header .middle-section-wrap {
                background-color: rgb(255, 255, 255) !important;
            }

            .site-header.left-inline_1 .middle-section-wrap {
                padding-bottom: 0px;
            }
            .main-logo {
                padding-top: 10px;
                padding-bottom: 4px;
                width: 130px;
                min-width: 130px !important;
            }
            .main-logo img {
                min-width: 130px;
            }
            .site-header.left-inline_1 .middle-section .main-nav-wrap .menu-home-menu-container {
                float: none;
                width: fit-content;
                margin: 0 auto;
                padding-top: 0px;
            }
            .page-id-352 .middle-section-wrap span,.middle-section-wrap span{
                color: rgb(73, 88, 90) !important;
            }
            .page-id-352 .middle-section-wrap span:hover,.middle-section-wrap span:hover {
                color: rgb(28, 233, 52) !important;
            }
            .page-id-352 .site-header.left-inline_1 .middle-section .icon-header-section .icon-header-wrap .header-search-wrap .nb-input-group .search-field {
                padding: 10px 10px 0;
                height: 35px;
                border-bottom: 1px solid #49585a;
                background: transparent;
                box-shadow: none;
                color: #49585a;
            }
            .page-id-352 .site-header.left-inline_1 .middle-section .icon-header-section .icon-header-wrap .header-search-wrap .nb-input-group .search-button button i {
                color: #49585a !important;
            }
            ::placeholder { color: #49585a !important; }
            :-ms-input-placeholder { color: #49585a !important; } /* IE 10+ */
            ::-webkit-input-placeholder { color: #49585a !important; } /* WebKit */
            ::-moz-placeholder { color: #49585a !important; }
            .ult-easy-separator-wrapper.ult-bottom-easy-separator {
                bottom: -3px;
            }
            .senil:before {
                content: '';
                position: absolute;
                bottom: 45px;
                margin: 0px -100px;
                left: 0px;
                right: 0px;
                border-bottom: 1px solid #49585b;
            }
            .senil{
                height: 0px;
                padding: 0px;
                margin: 0px;
            }
            .img-home {
                margin-top: 70px !important;
                margin-bottom: 46px !important;
            }
            a:hover, a:focus, a:active, .widget ul li a:hover {
                color: #1ce934;
            }
            .footer-top-section {
                margin-top: -24px;
                background-color: #3a403f;
            }
            .footer-abs-section {
                border-top: 2px solid #d8d9d9;
            }
            .site-footer .footer-top-section {
                padding: 80px 160px 0px;
            }

            /*PAGINAS INTRENAS*/
            .products .product .price .amount {
                font-size: 24px;
                color: #ff4600;
            }

            a.yith-wcqv-button {
                float: right !important;
                margin-left: auto !important;
            }
            .title-parallax {
                background-size: 100%;
                background-position: right;
                padding: 30px 0px !important;
                background-color: #00ea00;
                background-repeat: no-repeat;
            }
            .products .product .jr-product-custom .product-content .product-title > a:hover {
                color: #1ce934;
            }
            .products .product .product-action .button:hover {
                background-color: #1ce934;
            }
            .products .product .product-action .button:hover i:before {
                color: #fff !important;
            }
            p {
                padding-top: 0px !important;
            }
            .mini-cart-wrap .cart_list {
                float: left;
                width: 100%;
                max-height: 290px;
                overflow-y: scroll;
                padding-left: 0;
                list-style-type: none;
            }
            .product_list_widget span.amount {
                font-size: 16px;
                font-weight: bold;
                color: #ff4600;
            }
            .mini-cart-wrap .buttons .button {
                height: 34px;
                line-height: 36px;
                border: none;
                border-radius: 0px;
                background-color: #1ce934;
            }
            .prod-destacado-3 a {
                margin-top: 10px;
            }
            .nb-page-title-wrap {
                display: none;
            }
            .is-active {
                background-color: #1ce934 !important;
            }
            .button:hover,.button, .nb-primary-button:hover,.nb-primary-button,
            .post-password-form input[type='submit'],.post-password-form input[type='submit']:hover, .button:focus, .nb-primary-button:focus,
            .nb-primary-button{
                color: #ffffff;
                background-color: rgb(28, 233, 52);
                border-color: rgb(28, 233, 52);
            }
            .site-header.left-inline_1 .middle-section .main-nav-wrap {
                bottom: 10px;
            }
            .page-id-352 .middle-section-wrap a{
                color: rgb(0, 234, 0) !important;
            }
            .woocommerce-MyAccount-navigation ul li a {
                font-weight: bold;
            }
            .jr-contact-us .uavc-list .uavc-list-content .uavc-list-icon {
                color: #1ce934;
                border: 2px solid #1ce934;
            }
            #estimation_popup.wpe_bootstraped {
                background-color: #ffffff !important;
            }
            #estimation_popup.wpe_bootstraped #mainPanel {
                background-color: #ffffff !important;
            }
            .btn-primary {
                color: #fff;
                background-color: #1ce934 !important;
            }
            <?php if (false !== strpos($_SERVER["REQUEST_URI"], 'suscription-plans')) { ?>
                .single_add_to_cart_button, .single_add_to_cart_button {
                    color: #29f5a3 !important;
                    background-color: transparent !important;
                }
                .single_add_to_cart_button:hover, .single_add_to_cart_button:focus {
                    color: #29f5a3 !important;
                    background-color: transparent !important;
                }
                .shop-main {
                    padding-left: 0px;
                    padding-right: 0px;
                    width: 100%;
                }
                .woocommerce-MyAccount-navigation ul li a {
                    color: #282828;
                    font-weight: bold;
                }
            <?php } if (false !== strpos($_SERVER["REQUEST_URI"], 'pay-as-go-you-plans')) { ?>
                .single_add_to_cart_button {
                    color: #fdcd0b !important;
                    background-color: transparent !important;
                }
                .single_add_to_cart_button:hover, .single_add_to_cart_button:focus {
                    color: #fdcd0b !important;
                    background-color: transparent !important;
                }
                .shop-main {
                    padding-left: 0px;
                    padding-right: 0px;
                    width: 100%;
                }
            <?php } ?>
            strong{}

            @media screen and (max-width: 768px){
                #Bandera{
                    position: relative !important;
                    margin-top: -35px !important;
                    right: 0px !important;
                    width: 200px !important;
                }
                #product-696,#product-705{float: left;}
                .shop-main.left-images .entry-summary {
                    min-width: 330px;
                    max-width: 332px !important;
                }
                .movil-center{
                    text-align: center !important;
                }
                .site-footer .footer-top-section {
                    padding: 80px 0px 0px;
                }
                .title-parallax {
                    background-image: none !important;
                }
                .fzbuk-login-form-wrap {
                    width: 335px !important;
                }

            }
            @media screen and (max-width: 400px) {
                #estimation_popup.wpe_bootstraped #mainPanel .genSlide .genContent .itemBloc.lfb_picRow {
                    max-width: 135px;
                }
                #estimation_popup.wpe_bootstraped #mainPanel .genSlide .genContent .col-md-2 {
                    padding: 0px !important;
                }
                #estimation_popup.wpe_bootstraped #mainPanel .genSlide .genContent div.selectable span.icon_select {
                    position: absolute;
                    left: 50%;
                    margin-left: -25px;
                    right: inherit;
                }
                #estimation_popup.wpe_bootstraped #mainPanel .genSlide .genContent .itemDes {
                    max-width: 150px;
                    min-width: 150px;
                    width: 100%;
                    margin: 16px auto !important;
                }
                .main-navigation .menu-main-menu-wrap {
                    width: 200px;
                }
                .site-header.left-inline_1 .middle-section .main-nav-wrap .menu-home-menu-container {
                    width: 100%;
                }
                .woocommerce-orders-table__cell-order-actions a, .payment-method-actions a{
                    display:block;
                    white-space: nowrap;
                    width: 100%;
                    text-align: center;
                }
                .woocommerce-MyAccount-content {
                    width: 100%;
                    overflow-x: scroll;
                }
                .nb-navbar .menu-item-has-children:after {
                    top: -10px !important;
                    font-size: 35px !important;
                    right: 20px  !important;
                    color: #36ff00 !important;
                }
                .formCSS a.movile, .formCSS input.movile{
                    width: 100%;
                    display:block;
                    white-space: nowrap;
                    margin-bottom: 10px !important;
                    text-align: center;
                }
                .tab-responsive{
                    width: 100%;
                    max-width:  345px;
                    overflow-x: scroll;
                }
            }
            ::-webkit-scrollbar {
                width: 6px;
                height: 6px;
            }
            ::-webkit-scrollbar-button {
                width: 5px;
                height: 5px;
            }
            ::-webkit-scrollbar-thumb {
                background: #e1e1e1;
                border: 0px none #ffffff;
                border-radius: 50px;
            }
            ::-webkit-scrollbar-thumb:hover {
                background: #ffffff;
            }
            ::-webkit-scrollbar-thumb:active {
                background: #c0c0c0;
            }
            ::-webkit-scrollbar-track {
                background: #49585b;
                border: 0px none #ffffff;
                border-radius: 50px;
            }
            ::-webkit-scrollbar-track:hover {
                background: #49585b;
            }
            ::-webkit-scrollbar-track:active {
                background: #333333;
            }
            ::-webkit-scrollbar-corner {
                background: transparent;
            }
            @media (max-width: 991px){
                .main-navigation .menu-main-menu-wrap {
                    background: #f2f2f3;
                    overflow: auto;
                    box-shadow: -2px -11px 3px 3px #0000008f;
                }
                .main-navigation .menu-main-menu-wrap .menu-main-menu-title {
                    border-bottom: 1px solid #d2c7c7;
                }
                .nb-navbar .menu-item > a {
                    border-bottom: 1px solid #d2c7c7 !important;
                }
                .nb-navbar .menu-item > a:hover{
                    background-color: #00e900 !important;
                    color: #fff !important;
                }
                .nb-navbar .menu-item > a:hover span {
                    color: #ffffff !important;
                }

                .nb-navbar .menu-item:hover:after {
                    color: #fff !important;
                }

                .nb-navbar .menu-item > a span {
                    color: #868686 !important;
                    font-weight: bold;
                }
            }
            @media (max-width: 768px){
                #colblackcol{
                    margin-right: 5px !important;
                    margin-left: 5px !important;
                }
                #colblackcol .vc_col-sm-3{
                    vertical-align: top;
                    display: inline-block;
                    width: 50% !important;
                }
                #rowmeals div.vc_col-sm-4 {
                    width: 50% !important;
                    display: inline-block;
                }
                #vencedor .upb_row_bg{
                    background-image: url(/wp-content/uploads/2018/07/RESPONSIVE-02.png) !important;
                    background-color: #f3eeed !important;
                    background-size: 100% !important;
                    background-repeat: no-repeat !important;
                    background-position: bottom;
                }
            }
            @media (max-width: 768px){
                .senil,.hide-col-400{display:none!important;}
            }
            .avatar_container [class^=icon-], .avatar_container [class*=" icon-"], .ci_controls [class*=" icon-"], .ci_controls [class^=icon-] {
                font-family: icomoon !important;
            }
            .avatar_container [class^="icon-"]:before, .avatar_container [class*=" icon-"]:before {
                font-family: icomoon !important;
            }
            .mini-cart-wrap .buttons .button{padding-top:0px}
            form.edit-account::before{content: "" !important;}
            tr.tax-total span.woocommerce-Price-amount{float: right !important;color: #009c18 !important;font-weight: bold !important;}
            .product-action{max-height: 46px;overflow: hidden;}
            button.single_add_to_cart_button.button.alt {
                padding-top: 0px !important;
                background-color: #1ce934 !important;
            }
        </style>
    </head>

    <body <?php body_class(); ?>>
        <script type="text/javascript">
            var day = '';
            jQuery(document).ready(function () {
                window.onscroll = function () {
                    _action_();
                };
                var header = jQuery('span#can').parent();
                var sticky = header.offset();
                function _action_() {
                    //console.log(window.pageYOffset + ' >=' + sticky.top + ' ' + jQuery(header).attr('class'));
                    if (sticky !== undefined) {
                        if (window.pageYOffset >= sticky.top) {
                            header.attr("style", 'transition: all 0.3s ease-out;z-index: 9999;position: fixed !important;top: 65px !important;font-size: 20px !important;left: initial !important;right: 20px !important;width:  fit-content;padding:  5px 20px !important;color:  #fff;border-radius:  5px;background-color: #71ca08;');
                        } else {
                            header.removeAttr("style");
                        }
                    }
                }
<?php if (false !== strpos($_SERVER["REQUEST_URI"], 'gift-card') || false !== strpos($_SERVER["REQUEST_URI"], 'suscription-plans') || false !== strpos($_SERVER["REQUEST_URI"], 'pay-as-go-you-plans')) { ?>
                    setTimeout(function () {
                        jQuery("#formilla-frame").remove();
                    }, 1000);

<?php } ?>
                /*setTimeout(function () {
                 
                 jQuery('select[data-originaltitle="Select your city"]').change(function () {
                 jQuery('a#lfb_btnNext_29').attr('style', 'display: inline-block;margin-top: 30px;');
                 var label = jQuery('[data-title="Delivery"]');
                 jQuery('.order-x2').each(function () {
                 var x2 = jQuery(this).html();
                 if (x2.indexOf('Sunday') > -1) {
                 var dx2s = jQuery('#DetailsX2Sunday');
                 if (dx2s.length > 0) {
                 dx2s.html(('First half of the meals will be prepared and delivered Sunday, second half will be prepared and delivered on Wednesday'));
                 } else {
                 label.append('<div id="DetailsX2Sunday" style="position:  relative;margin: 0 auto;max-width: 320px;line-height: 18px;font-weight: 600;">First half of the meals will be prepared and delivered Sunday, second half will be prepared and delivered on Wednesday</div>');
                 }
                 jQuery('#DetailsX2Monday').html('');
                 } else if (x2.indexOf('Monday') > -1) {
                 var dx2s = jQuery('#DetailsX2Monday');
                 if (dx2s.length > 0) {
                 dx2s.html(('First half of the meals will be prepared and delivered Monday, second half will be prepared and delivered on Thursday'));
                 } else {
                 label.append('<div id="DetailsX2Monday" style="position:  relative;margin: 0 auto;max-width: 320px;line-height: 18px;font-weight: 600;">First half of the meals will be prepared and delivered Monday, second half will be prepared and delivered on Thursday</div>');
                 }
                 jQuery('#DetailsX2Sunday').html('');
                 }
                 });
                 });
                 
                 }, 3500);*/

            });

        </script>
        <div class="modallogin ultoverlay" style="display:none"><span class="close vc_icon_element-icon fa fa-times-circle" style="color: #FE6C61;"></span><?= do_shortcode('[profilepress-login id="2"]'); ?> </div>
        <div class="modalregister ultoverlay" style="display:none"><span class="close vc_icon_element-icon fa fa-times-circle" style="color: #FE6C61;"></span><?= do_shortcode('[profilepress-registration id="2"]'); ?> </div>

        <div id="page" class="site">

            <div id="site-wrapper">

                <a class="skip-link screen-reader-text" href="#content"><?php esc_html_e('Skip to content', 'nb-foody'); ?></a>

                <header class="site-header <?php nbfoody_header_class(); ?>">

                    <?php
                    do_action('nb_core_before_header');

                    nbfoody_get_header();

                    do_action('nb_core_after_header');
                    ?>

                </header>

                <div id="content" class="site-content">