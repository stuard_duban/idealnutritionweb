<?php
$text_section_content = nbfoody_get_options('nbcore_header_text_section');
$social_section_content = (nbfoody_get_options('nbcore_header_facebook') || nbfoody_get_options('nbcore_header_twitter') || nbfoody_get_options('nbcore_header_linkedin') || nbfoody_get_options('nbcore_header_instagram') || nbfoody_get_options('nbcore_header_blog') || nbfoody_get_options('nbcore_header_pinterest') || nbfoody_get_options('nbcore_header_ggplus'));
?>
<?php if($text_section_content || has_nav_menu('header-sub') || $social_section_content): ?>
	<div class="top-section-wrap">
		<div class="container">
			<div class="top-section">
				<?php $text_section_content = nbfoody_get_options('nbcore_header_text_section'); 
				if($text_section_content):
				?>
					<div class="text-section">
						<?php echo esc_html($text_section_content); ?>
					</div>
				<?php endif;
				if($social_section_content): ?>
					<div class="socials-section">
						<?php //nbfoody_social_section(); ?>
						<ul class="social-section"><li class="social-item"><a target="_blank" href="https://facebook.com/idealnutritionnow"><i class="icon-facebook"></i></a></li><li class="social-item"><a target="_blank" href="https://www.instagram.com/idealnutritionnow/"><i class="icon-instagram"></i></a></li>
						    <li class="social-item"><a target="_blank" href="mailto:info@idealnutritionsofla.com?subject=Contact Web"><i class="icon-mail"></i></a></li>
						</ul>
					</div>
				<?php endif;
				if (has_nav_menu('header-sub')):
					nbfoody_sub_menu();
				endif; ?>
				<div class="jw-login">
    			    <?php
    			    if ((is_user_logged_in())){
    			        $current_user = wp_get_current_user();
						?>
					<a class="menu-user" href="/my-account/" style="font-size: 24px"><i class="icon-user"></i></a>
					<nav id="menu-user" class="main-navigation" style="display: inline-block;vertical-align: super;">
<a class="mobile-toggle-button icon-menu" target="_blank"></a>
<div class="menu-main-menu-wrap">
<div class="menu-main-menu-title">
<h3>Account</h3>
<span class="icon-cancel-circle"></span>
</div>
<div class="menu-home-menu-container">
<ul id="menu-home-menu" class="nb-navbar">
<li id="menu-item-712" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-712"><a href="javascript:void()"><span><?="Hi, ".$current_user->display_name;?></span></a>
<ul class="sub-menu" style="right: 0px;left:  initial;">
	<li id="menu-item-625" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-625"><a href="/my-account/"><span>Dashboard</span></a></li>
	<li id="menu-item-628" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-628"><a href="/my-account/edit-address/"><span>Addresses</span></a></li>
	<li id="menu-item-626" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-626"><a href="/my-account/orders/"><span>Orders history</span></a></li>
	<!--li id="menu-item-629" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-629"><a href="/my-account/gift-cards/"><span>Gift Card</span></a></li-->
	<li id="menu-item-630" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-630"><a href="/my-account/subscriptions/"><span>Subscriptions</span></a></li>
	<li id="menu-item-627" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-627"><a href="/my-account/payment-methods/"><span>Payment method</span></a></li>
	<li id="menu-item-628" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-628"><a href="<?=wc_logout_url();?>"><span>Logout</span></a></li>
</ul>
</li>
</ul>
</div>
</div>
</nav>
					<?php
    			    }else{
    			        ?>
    			        <div class="botonjw" style="display: inline-block;"><a id="btnlogin" href="/log-in/" style="cursor:pointer;">Login</a></div>
    			        <div class="normaljw" style="display: inline-block;"><a id="btnregister" href="/sign-up/" style="cursor:pointer;">Register</a></div>
					<script type="text/javascript">
				(function($){
					$(document).ready(function(){/*
						var selector = "#btnlogin";
						$(selector).click(function(){
							$('.modallogin').show();
						});
						selector = "#btnregister";
						$(selector).click(function(){
							$('.modalregister').show();
						});
						selector = ".close";
						$(selector).each(function(){
							$(this).click(function(){
							$('.ultoverlay').each(function(){
								$(this).hide();
							});
						});
						});*/
					});
				})(jQuery);
				</script>
    			        <?php
    			    }
    			    ?>
					
    			
				<div class="icon-header-section" style="display: inline-block;margin-left: 0px !important;">
                <div class="icon-header-wrap" style="padding-left: 0px !important;">
                <?php 
                nbfoody_header_woo_section(false);
                ?>
                </div>
            </div></div>
			</div>
		</div>
	</div>
<?php endif; ?>
<div class="middle-section-wrap">
    <div class="container-fluid">
        <div class="middle-section">
            <?php nbfoody_get_site_logo(); ?>
            <div class="main-nav-wrap">
                <?php nbfoody_main_nav(); ?>
            </div>
            <div class="icon-header-section">
                <div class="icon-header-wrap bar-search">
                <?php 
                nbfoody_search_section(false);
                //nbfoody_header_woo_section(false);
                ?>
                </div>
            </div>
        </div>
    </div>
</div>
