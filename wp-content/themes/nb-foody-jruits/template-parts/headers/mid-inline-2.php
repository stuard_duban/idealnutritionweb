<?php $text_section_content = nbfoody_get_options('nbcore_header_text_section'); ?>
<?php
$social_section_content = (nbfoody_get_options('nbcore_header_facebook') || nbfoody_get_options('nbcore_header_twitter') || nbfoody_get_options('nbcore_header_linkedin') || nbfoody_get_options('nbcore_header_instagram') || nbfoody_get_options('nbcore_header_blog') || nbfoody_get_options('nbcore_header_pinterest') || nbfoody_get_options('nbcore_header_ggplus'));
?>
<?php if($text_section_content || has_nav_menu('header-sub') || $social_section_content): ?>
	<div class="top-section-wrap">
		<div class="container">
			<div class="top-section">
				<?php if($text_section_content): ?>
					<div class="text-section">
						<?php printf(esc_html__('%s', 'nb-foody'), $text_section_content); ?>
					</div>
				<?php endif;
				if($social_section_content): ?>
					<div class="socials-section">
						<?php nbfoody_social_section(); ?>
					</div>
				<?php endif;
				if (has_nav_menu('header-sub')):
					nbfoody_sub_menu();
				endif; ?>
			</div>
		</div>
	</div>
<?php endif; ?>
<div class="middle-section-wrap">
    <div class="container">
        <div class="middle-section">
            <div class="flex-section equal-section menu-section">
                <?php nbfoody_main_nav(); ?>
            </div>
            <div class="flex-section logo-section">
                <?php nbfoody_get_site_logo(); ?>
            </div>
            <div class="flex-section equal-section icon-section flex-end">
                <?php nbfoody_search_section(false);
				nbfoody_header_woo_section(false);?>
            </div>
        </div>
    </div>
</div>