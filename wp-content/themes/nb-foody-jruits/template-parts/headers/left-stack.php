<div class="top-section-wrap">
    <div class="container">
        <div class="top-section">
            <?php $text_section_content = nbfoody_get_options('nbcore_header_text_section');
            if($text_section_content):
                ?>
                <div class="text-section">
                    <?php echo esc_html($text_section_content); ?>
                </div>
            <?php endif;?>
                <?php nbfoody_sub_menu(); ?>
        </div>
    </div>
</div>
<div class="middle-section-wrap">
    <div class="container">
        <div class="middle-section">
            <?php nbfoody_get_site_logo();
            nbfoody_search_section(false); ?>
        </div>
    </div>
</div>
<div class="bot-section-wrap">
    <div class="container">
        <div class="bot-section">
            <?php nbfoody_main_nav(); ?>
                <div class="icon-header-wrap">
                    <?php nbfoody_header_woo_section(); ?>
                </div>
        </div>
    </div>
</div>
