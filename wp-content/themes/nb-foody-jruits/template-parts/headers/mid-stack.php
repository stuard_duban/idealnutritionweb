<div class="middle-section-wrap">
    <div class="container">
        <div class="middle-section">
            <div class="flex-section equal-section">
                <?php nbfoody_sub_menu(); ?>
            </div>
            <div class="flex-section">
                <?php nbfoody_get_site_logo(); ?>
            </div>
            <div class="flex-section equal-section flex-end">
                <div class="icon-header-section">
                    <div class="icon-header-wrap">
                        <?php
                        nbfoody_header_woo_section();
                        nbfoody_search_section();
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="bot-section-wrap">
    <div class="container">
        <div class="bot-section">
            <?php nbfoody_main_nav(); ?>
        </div>
    </div>
</div>
<!--End site main navbar-->
