<!--Site main navbar-->
<div class="middle-section-wrap">
    <div class="container-fluid">
        <div class="middle-section">
            <div class="flex-section equal-section menu-section">
                <?php nbfoody_main_nav(); ?>
            </div>
            <div class="flex-section logo-section">
                <?php nbfoody_get_site_logo(); ?>
            </div>
            <div class="flex-section equal-section flex-end icon-section">
                <div class="icon-header-section">
                    <div class="icon-header-wrap">
                        <?php
                        $text_section_content = nbfoody_get_options('nbcore_header_text_section');
                        if ($text_section_content):
                            ?>
                            <div class="text-section">
                                <?php printf(esc_html__('%s', 'nb-foody'), $text_section_content); ?>
                            </div>
                        <?php
                        endif;
                        nbfoody_search_section(false);
                        nbfoody_header_woo_section(false);
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--End site main navbar-->
