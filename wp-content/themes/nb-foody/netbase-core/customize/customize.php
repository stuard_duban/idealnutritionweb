<?php

/**
 * Class that handle all the options in WordPress Theme Customize
 */
class NBFoody_Customize
{

    protected static $prefix = 'NBFoody_';

    protected static $initialized = false;
    /**
     * Hold all options values
     *
     * @var array
     */
    protected static $customize_options = array();


    public static function register($wp_customize)
    {

        $customize_options['header'] = NBFoody_Customize_Options_Header::options();

        $customize_options['typography'] = NBFoody_Customize_Options_Typography::options();

        $customize_options['layout'] = NBFoody_Customize_Options_Elements::options();

        $customize_options['color'] = NBFoody_Customize_Options_Color::options();

        $customize_options['blog'] = NBFoody_Customize_Options_Blog::options();

        $customize_options['woocommerce'] = NBFoody_Customize_Options_WooCommerce::options();

        $customize_options['footer'] = NBFoody_Customize_Options_Footer::options();

        if ($wp_customize) {
            if (self::$initialized) {
                return;
            }

            $wp_customize->remove_section('colors');
        }

        foreach ($customize_options as $section => $define) {
            if (isset($define['sections'])) {
                $sections = $define['sections'];

                unset($define['sections']);

                $panel = $section;

                if ($wp_customize) {
                    if ($wp_customize->get_panel($panel)) {
                        foreach ($define as $key => $value) {
                            $wp_customize->get_panel($panel)->$key = $value;
                        }
                    } else {
                        if (isset($define['type']) && class_exists($class = $define['type'])) {
                            unset($define['type']);

                            $section = new $class($wp_customize, $panel, $define);

                            $wp_customize->add_panel($section);
                        } else {
                            $wp_customize->add_panel($panel, $define);
                        }
                    }
                }

                foreach ($sections as $section => $define) {
                    if (!isset($define['panel'])) {
                        $define['panel'] = $panel;
                    }

                    $customize_options[$section] = $define;
                }

                unset($customize_options[$panel]);
            }
        }

        foreach ($customize_options as $section => $define) {
            $settings = isset($define['settings']) ? $define['settings'] : array();
            $controls = isset($define['controls']) ? $define['controls'] : array();

            unset($define['settings']);
            unset($define['controls']);

            if ($wp_customize) {
                //Check if section existed
                // TODO Reverse to if(!$wp_customize->get_section($section)) ??
                if ($wp_customize->get_section($section)) {
                    foreach ($define as $key => $value) {
                        $wp_customize->get_section($section)->$key = $value;
                    }
                } else {
                    if (isset($define['type']) && class_exists($class = $define['type'])) {
                        unset($define['type']);

                        $section = new $class($wp_customize, $section, $define);

                        $wp_customize->add_section($section);
                    } else {
                        $wp_customize->add_section($section, $define);
                    }
                }
            }

            foreach ($settings as $setting => $define) {
                if (!array_key_exists($setting, self::$customize_options)) {
                    self::$customize_options[$setting] = isset($define['default']) ? $define['default'] : null;
                }

                if ($wp_customize) {
                    $wp_customize->add_setting($setting, array_merge(array('sanitize_callback' => null), $define));
                }
            }

            if ($wp_customize) {
                foreach ($controls as $control => $define) {
                    if (isset($define['type']) && class_exists($class = $define['type'])) {
                        unset($define['type']);

                        $control = new $class($wp_customize, $control, $define);


                        $wp_customize->add_control($control);
                    } else {
                        $wp_customize->add_control($control, $define);
                    }
                }
            }
        }

        if ($wp_customize) {
            self::$initialized = true;
        }
    }

    // Todo change to minified version
    public static function customize_control_js()
    {
        //this is minified version
        wp_enqueue_script('nbcore-customizer-control', get_template_directory_uri() . '/assets/netbase/js/admin/control.min.js', array('customize-controls', 'jquery'), NBFoody_VER, true);
        wp_enqueue_script('nbcore-customize', get_template_directory_uri() . '/assets/netbase/js/admin/customize.min.js', array('jquery'), NBFoody_VER, true);

        wp_localize_script('nbcore-customizer-control', 'nbUpload', array(
            'google_fonts' => NBFoody_Helper::google_fonts(),
        ));
    }

    // Todo change to minified version
    public static function customize_preview_js()
    {
        //this is minified version
        wp_enqueue_script('nbcore_customizer_preview', get_template_directory_uri() . '/assets/netbase/js/admin/preview.min.js', array('customize-preview', 'jquery'), NBFoody_VER, true);
    }

    // Todo change to minified version
    public static function customize_style()
    {
        //this is minified version
        wp_enqueue_style('nbcore_customizer_style', get_template_directory_uri() . '/assets/netbase/css/admin/customizer/customizer.css', NULL, NBFoody_VER, 'all');
    }

    public static function get_options()
    {
        // Get theme options.
        static $prepared;

        if ( ! isset( $prepared ) ) {
            self::register( null );

            // Prepare theme option values.
            $theme_mods = get_theme_mods();

            if ( $theme_mods && is_array( $theme_mods ) ) {
                self::$customize_options = array_merge( self::$customize_options, $theme_mods );
            }

            // State that theme options is prepared.
            $prepared = true;
        }

        return self::$customize_options;
    }
}