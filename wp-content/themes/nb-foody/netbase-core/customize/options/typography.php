<?php
class NBFoody_Customize_Options_Typography {
    public static function options()
    {
        return array(
            'title' => esc_html__('Typography', 'nb-foody'),
            'priority' => 14,
            'settings' => array(
                'body_font_intro' => array(),
                'body_font_family' => array(
                    'default' => 'google,Noto Sans',
                    'sanitize_callback' => 'wp_filter_nohtml_kses',
                ),
                'body_font_style' => array(
                    'default' => '400',
                    'sanitize_callback' => 'wp_filter_nohtml_kses',
                ),
                'body_font_size' => array(
                    'default' => '14',
                    'sanitize_callback' => 'absint',
                ),
                'heading_font_intro' => array(),
                'heading_font_family' => array(
                    'default' => 'google,Vidaloka',
                    'sanitize_callback' => 'wp_filter_nohtml_kses'
                ),
                'heading_font_style' => array(
                    'default' => '400',
                    'sanitize_callback' => 'wp_filter_nohtml_kses',
                ),
                'heading_base_size' => array(
                    'default' => '16',
                    'sanitize_callback' => 'absint',
                ),
                'subset_intro' => array(),
                'subset_cyrillic' => array(
                    'default' => false,
                    'transport' => 'refresh',
                    'sanitize_callback' => array('NBFoody_Customize_Sanitize', 'sanitize_checkbox'),
                ),
                'subset_greek' => array(
                    'default' => false,
                    'transport' => 'refresh',
                    'sanitize_callback' => array('NBFoody_Customize_Sanitize', 'sanitize_checkbox'),
                ),
                'subset_vietnamese' => array(
                    'default' => false,
                    'transport' => 'refresh',
                    'sanitize_callback' => array('NBFoody_Customize_Sanitize', 'sanitize_checkbox'),
                ),
                'font_color_focus' => array(),
            ),
            'controls' => array(
                'body_font_intro' => array(
                    'label' => esc_html__('Body Font', 'nb-foody'),
                    'section' => 'typography',
                    'type' => 'NBFoody_Customize_Control_Heading',
                ),
                'body_font_family' => array(
                    'label'   => esc_html__( 'Font Family', 'nb-foody' ),
                    'section' => 'typography',
                    'dependency' => 'body_font_style',
                    'type'    => 'NBFoody_Customize_Control_Typography',
                ),
                'body_font_style' => array(
                    'label' => esc_html__('Font Styles', 'nb-foody'),
                    'section' => 'typography',
                    'type'    => 'NBFoody_Customize_Control_Font_Style',
                    'choices' => array(
                        'italic' => true,
                        'underline' => true,
                        'uppercase' => true,
                        'weight' => true,
                    ),
                ),
                'body_font_size' => array(
                    'label' => esc_html__('Font Size', 'nb-foody'),
                    'section' => 'typography',
                    'type' => 'NBFoody_Customize_Control_Slider',
                    'choices' => array(
                        'unit' => 'px',
                        'min' => '8',
                        'max' => '30',
                        'step' => '1',
                    ),
                ),
                'heading_font_intro' => array(
                    'label' => esc_html__('Heading Font', 'nb-foody'),
                    'section' => 'typography',
                    'type' => 'NBFoody_Customize_Control_Heading',
                ),
                'heading_font_family' => array(
                    'label'   => esc_html__( 'Heading font', 'nb-foody' ),
                    'section' => 'typography',
                    'dependency' => 'heading_font_style',
                    'type'    => 'NBFoody_Customize_Control_Typography',
                ),
                'heading_font_style' => array(
                    'label' => esc_html__('Font Styles', 'nb-foody'),
                    'section' => 'typography',
                    'type'    => 'NBFoody_Customize_Control_Font_Style',
                    'choices' => array(
                        'italic' => true,
                        'underline' => true,
                        'uppercase' => true,
                        'weight' => true,
                    ),
                ),
                'heading_base_size' => array(
                    'label' => esc_html__('Heading base size', 'nb-foody'),
                    'section' => 'typography',
                    'type' => 'NBFoody_Customize_Control_Slider',
                    'choices' => array(
                        'unit' => 'px',
                        'min' => '10',
                        'max' => '40',
                        'step' => '1',
                    ),
                ),
                'heading_font_upload' => array(
                    'section' => 'typography',
                    'type' => 'NBFoody_Customize_Control_Upload_Font',
                ),
                'subset_intro' => array(
                    'label' => esc_html__('Font subset', 'nb-foody'),
                    'description' => esc_html__('Turn these settings on if you have to support these scripts', 'nb-foody'),
                    'section' => 'typography',
                    'type' => 'NBFoody_Customize_Control_Heading',
                ),
                'subset_cyrillic' => array(
                    'label'   => esc_html__( 'Cyrillic subset', 'nb-foody' ),
                    'section' => 'typography',
                    'type'    => 'NBFoody_Customize_Control_Switch',
                ),
                'subset_greek' => array(
                    'label'   => esc_html__( 'Greek subset', 'nb-foody' ),
                    'section' => 'typography',
                    'type'    => 'NBFoody_Customize_Control_Switch',
                ),
                'subset_vietnamese' => array(
                    'label'   => esc_html__( 'Vietnamese subset', 'nb-foody' ),
                    'section' => 'typography',
                    'type'    => 'NBFoody_Customize_Control_Switch',
                ),
                'font_color_focus' => array(
                    'section' => 'typography',
                    'type'    => 'NBFoody_Customize_Control_Focus',
                    'choices' => array(
                        'type_color' => esc_html__('Edit font color', 'nb-foody'),
                    ),
                ),
            ),
        );
    }
}