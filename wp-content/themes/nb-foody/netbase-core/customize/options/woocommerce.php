<?php
class NBFoody_Customize_Options_WooCommerce
{
    public static function options()
    {
		$attributes = array();
        if ( class_exists( 'WooCommerce' ) ) {
            $attribute_taxonomies = wc_get_attribute_taxonomies();
            $attributes = array();
            foreach($attribute_taxonomies as $attr)
            {
                if($attr->attribute_type == 'select')
                {
                    $key = 'pa_'.$attr->attribute_name;
                    $attributes[$key] = $attr->attribute_label;
                }
            }
        }
        $arr = array(
            'title' => esc_html__('WooCommerce', 'nb-foody'),
            'priority' => 15,
            'sections' => apply_filters('nbt_woocommerce_array', array(
				'product_att' => array(
					'title' => esc_html__('Product attributes', 'nb-foody'),
					'settings' => array(						
                        'nbcore_pa_swatch_intro' => array(),
						'nbcore_pa_swatch_style' => array(
                            'default' => '',
                            'sanitize_callback' => array('NBFoody_Customize_Sanitize', 'sanitize_checkbox')
                        ),
                        'nbcore_wc_attr' => array(
                            'default' => false,
                            'sanitize_callback' => array('NBFoody_Customize_Sanitize', 'sanitize_checkbox')
                        ),
					),
					'controls' => array(
						'nbcore_pa_swatch_intro' => array(
                            'label' => esc_html__('Swatch style Attributes', 'nb-foody'),
                            'section' => 'product_att',
                            'type' => 'NBFoody_Customize_Control_Heading',
                        ),
						'nbcore_pa_swatch_style' => array(
                            'label' => esc_html__('Swatch style Attributes', 'nb-foody'),
                            'description' => esc_html__('This options also effect for product attributes', 'nb-foody'),
                            'section' => 'product_att',
                            'type' => 'NBFoody_Customize_Control_Checkbox_List',
                            'choices' => $attributes,
                        ),
                        'nbcore_wc_attr' => array(
                            'label' => esc_html__('Show Attribute ?', 'nb-foody'),
							'description' => esc_html__('This options also effect for product attributes in product archive', 'nb-foody'),
                            'section' => 'product_att',
                            'type' => 'NBFoody_Customize_Control_Switch',
                        ),
					),
				),
                'product_category' => array(
                    'title' => esc_html__('Product category', 'nb-foody'),
                    'settings' => array(
                        'nbcore_pa_title_intro' => array(),
                        'nbcore_shop_title' => array(
                            'default' => esc_html__('Shop', 'nb-foody'),
                            'transport' => 'postMessage',
                            'sanitize_callback' => 'wp_filter_nohtml_kses'
                        ),
                        'nbcore_wc_breadcrumb' => array(
                            'default' => true,
                            'sanitize_callback' => array('NBFoody_Customize_Sanitize', 'sanitize_checkbox')
                        ),
                        'nbcore_pa_layout_intro' => array(),
                        'nbcore_shop_content_width' => array(
                            'default' => '70',
                            'transport' => 'postMessage',
                            'sanitize_callback' => 'absint'
                        ),
                        'nbcore_shop_sidebar' => array(
                            'default' => 'right-sidebar',
                            'sanitize_callback' => array('NBFoody_Customize_Sanitize', 'sanitize_selection')
                        ),
                        'shop_sticky_sidebar' => array(
                            'default' => false,
                            'sanitize_callback' => array('NBFoody_Customize_Sanitize', 'sanitize_checkbox')
                        ),
                        'nbcore_product_list' => array(
                            'default' => 'grid-type',
                            'sanitize_callback' => array('NBFoody_Customize_Sanitize', 'sanitize_selection')
                        ),
                        'nbcore_loop_columns' => array(
                            'default' => 'three-columns',
                            'sanitize_callback' => array('NBFoody_Customize_Sanitize', 'sanitize_selection')
                        ),
                        'nbcore_pa_other_intro' => array(),
                        'nbcore_shop_banner' => array(
                            'default' => '',
                        ),
                        'nbcore_shop_action' => array(
                            'default' => true,
                            'sanitize_callback' => array('NBFoody_Customize_Sanitize', 'sanitize_checkbox')
                        ),
                        'nbcore_products_per_page' => array(
                            'default' => '12',
                            'sanitize_callback' => 'absint'
                        ),
                        'nbcore_wc_sale' => array(
                            'default' => 'style-1',
                            'sanitize_callback' => array('NBFoody_Customize_Sanitize', 'sanitize_selection')
                        ),
                        'nbcore_grid_product_description' => array(
                            'default' => false,
                            'sanitize_callback' => array('NBFoody_Customize_Sanitize', 'sanitize_checkbox')
                        ),
						'product_category_wishlist' => array(
							'default' => false,
                            'sanitize_callback' => array('NBFoody_Customize_Sanitize', 'sanitize_checkbox')
						),
                        'product_category_quickview' => array(
                            'default' => false,
                            'sanitize_callback' => array('NBFoody_Customize_Sanitize', 'sanitize_checkbox')
                        ),
                    ),
                    'controls' => array(
                        'nbcore_pa_title_intro' => array(
                            'label' => esc_html__('Product category title', 'nb-foody'),
                            'section' => 'product_category',
                            'type' => 'NBFoody_Customize_Control_Heading',
                        ),
                        'nbcore_shop_title' => array(
                            'label' => esc_html__('Shop page title', 'nb-foody'),
                            'section' => 'product_category',
                            'type' => 'text',
                        ),
                        'nbcore_wc_breadcrumb' => array(
                            'label' => esc_html__('Show breadcrumb ?', 'nb-foody'),
                            'section' => 'product_category',
                            'type' => 'NBFoody_Customize_Control_Switch',
                        ),
                        'nbcore_pa_layout_intro' => array(
                            'label' => esc_html__('Product category layout', 'nb-foody'),
                            'section' => 'product_category',
                            'type' => 'NBFoody_Customize_Control_Heading',
                        ),
                        'nbcore_shop_content_width' => array(
                            'label' => esc_html__('WooCommerce content width', 'nb-foody'),
                            'description' => esc_html__('This options also effect Cart page', 'nb-foody'),
                            'section' => 'product_category',
                            'type' => 'NBFoody_Customize_Control_Slider',
                            'choices' => array(
                                'unit' => '%',
                                'min' => '60',
                                'max' => '80',
                                'step' => '1'
                            ),
                        ),
                        'nbcore_shop_sidebar' => array(
                            'label' => esc_html__('Sidebar Layout', 'nb-foody'),
                            'section' => 'product_category',
                            'description' => esc_html__('Sidebar Position for product category and shop page', 'nb-foody'),
                            'type' => 'NBFoody_Customize_Control_Radio_Image',
                            'choices' => array(
                                'left-sidebar' => get_template_directory_uri() . '/assets/images/options/2cl.png',
                                'no-sidebar' => get_template_directory_uri() . '/assets/images/options/1c.png',
                                'right-sidebar' => get_template_directory_uri() . '/assets/images/options/2cr.png',
                            ),
                        ),
                        'shop_sticky_sidebar' => array(
                            'label' => esc_html__('Sticky Sidebar', 'nb-foody'),
                            'section' => 'product_category',
                            'type' => 'NBFoody_Customize_Control_Switch',
                        ),
                        'nbcore_product_list' => array(
                            'label' => esc_html__('Product List', 'nb-foody'),
                            'section' => 'product_category',
                            'type' => 'NBFoody_Customize_Control_Radio_Image',
                            'choices' => array(
                                'grid-type' => get_template_directory_uri() . '/assets/images/options/grid.png',
                                'list-type' => get_template_directory_uri() . '/assets/images/options/list.png',
                                'menu' => get_template_directory_uri() . '/assets/images/options/productmenu.png',
                                'grid-type1' => get_template_directory_uri() . '/assets/images/options/product2.png',
                                'grid-type_1' => get_template_directory_uri() . '/assets/images/options/product1.png',
                            ),
                        ),
                        'nbcore_loop_columns' => array(
                            'label' => esc_html__('Products per row', 'nb-foody'),
                            'section' => 'product_category',
                            'type' => 'NBFoody_Customize_Control_Radio_Image',
                            'choices' => array(
                                'two-columns' => get_template_directory_uri() . '/assets/images/options/2-columns.png',
                                'three-columns' => get_template_directory_uri() . '/assets/images/options/3-columns.png',
                                'four-columns' => get_template_directory_uri() . '/assets/images/options/4-columns.png',
                            ),
                        ),
                        'nbcore_pa_other_intro' => array(
                            'label' => esc_html__('Other', 'nb-foody'),
                            'section' => 'product_category',
                            'type' => 'NBFoody_Customize_Control_Heading',
                        ),
                        'nbcore_shop_banner' => array(
                            'label' => esc_html__('Shop Banner', 'nb-foody'),
                            'section' => 'product_category',
                            'type' => 'WP_Customize_Cropped_Image_Control',
                            'flex_width'  => true,
                            'flex_height' => true,
                            'width' => 2000,
                            'height' => 1000,
                        ),
                        'nbcore_shop_action' => array(
                            'label' => esc_html__('Show shop action', 'nb-foody'),
                            'section' => 'product_category',
                            'type' => 'NBFoody_Customize_Control_Switch'
                        ),
                        'nbcore_products_per_page' => array(
                            'label' => esc_html__('Products per Page', 'nb-foody'),
                            'section' => 'product_category',
                            'type' => 'number',
                            'input_attrs' => array(
                                'min'   => 1,
                                'step'  => 1,
                            ),
                        ),
                        'nbcore_wc_sale' => array(
                            'label' => esc_html__('Choose sale tag style', 'nb-foody'),
                            'section' => 'product_category',
                            'type' => 'select',
                            'choices' => array(
                                'style-1' => esc_html__('Style 1', 'nb-foody'),
                                'style-2' => esc_html__('Style 2', 'nb-foody'),
                            ),
                        ),
                        //TODO remove this
                        'nbcore_grid_product_description' => array(
                            'label' => esc_html__('Product Description', 'nb-foody'),
                            'section' => 'product_category',
                            'type' => 'NBFoody_Customize_Control_Switch',
                        ),
                        'product_category_wishlist' => array(
                            'label' => esc_html__('Wishlist button', 'nb-foody'),
                            'description' => esc_html__('This feature need YITH WooCommerce Wishlist plugin to be installed and activated', 'nb-foody'),
                            'section' => 'product_category',
                            'type' => 'NBFoody_Customize_Control_Switch',
                        ),
                        'product_category_quickview' => array(
                            'label' => esc_html__('Quickview button', 'nb-foody'),
                            'description' => esc_html__('This feature need YITH WooCommerce Quick View plugin to be installed and activated', 'nb-foody'),
                            'section' => 'product_category',
                            'type' => 'NBFoody_Customize_Control_Switch',
                        ),
                    ),
                ),
                'product_details' => array(
                    'title' => esc_html__('Product details', 'nb-foody'),
                    'settings' => array(
                        'nbcore_pd_layout_intro' => array(),
                        'nbcore_pd_details_title' => array(
                            'default' => true,
                            'sanitize_callback' => array('NBFoody_Customize_Sanitize', 'sanitize_checkbox')
                        ),
                        'nbcore_pd_details_sidebar' => array(
                            'default' => 'right-sidebar',
                            'sanitize_callback' => array('NBFoody_Customize_Sanitize', 'sanitize_selection')
                        ),
                        'nbcore_pd_details_width' => array(
                            'default' => '70',
                            'transport' => 'postMessage',
                            'sanitize_callback' => 'absint'
                        ),
                        'product_sticky_sidebar' => array(
                            'default' => false,
                            'sanitize_callback' => array('NBFoody_Customize_Sanitize', 'sanitize_checkbox')
                        ),
                        'nbcore_pd_meta_layout' => array(
                            'default' => 'left-images',
                            'sanitize_callback' => array('NBFoody_Customize_Sanitize', 'sanitize_selection')
                        ),
                        'nbcore_add_cart_style' => array(
                            'default' => 'style-1',
                            'sanitize_callback' => array('NBFoody_Customize_Sanitize', 'sanitize_selection')
                        ),
                        'nbcore_pd_show_social' => array(
                            'default' => true,
                            'sanitize_callback' => array('NBFoody_Customize_Sanitize', 'sanitize_checkbox')
                        ),
                        'nbcore_pd_gallery_intro' => array(),
                        'nbcore_pd_images_width' => array(
                            'default' => '50',
                            'transport' => 'postMessage',
                            'sanitize_callback' => 'absint'
                        ),
                        'nbcore_pd_featured_autoplay' => array(
                            'default' => false,
                            'sanitize_callback' => array('NBFoody_Customize_Sanitize', 'sanitize_checkbox')
                        ),
                        'nbcore_pd_thumb_pos' => array(
                            'default' => 'bottom-thumb',
                            'sanitize_callback' => array('NBFoody_Customize_Sanitize', 'sanitize_selection')
                        ),
                        'nbcore_pd_info_tab_intro' => array(),
                        'nbcore_info_style' => array(
                            'default' => 'accordion-tabs',
                            'sanitize_callback' => array('NBFoody_Customize_Sanitize', 'sanitize_selection')
                        ),
                        'nbcore_reviews_form' => array(
                            'default' => 'split',
                            'transport' => 'postMessage',
                            'sanitize_callback' => array('NBFoody_Customize_Sanitize', 'sanitize_selection')
                        ),
                        'nbcore_reviews_round_avatar' => array(
                            'default' => false,
                            'transport' => 'postMessage',
                            'sanitize_callback' => array('NBFoody_Customize_Sanitize', 'sanitize_checkbox')
                        ),
                        'nbcore_other_products_intro' => array(),
                        'nbcore_show_upsells' => array(
                            'default' => true,
                            'sanitize_callback' => array('NBFoody_Customize_Sanitize', 'sanitize_checkbox')
                        ),
                        'nbcore_pd_upsells_columns' => array(
                            'default' => '3',
                            'sanitize_callback' => array('NBFoody_Customize_Sanitize', 'sanitize_selection')
                        ),
                        'nbcore_upsells_limit' => array(
                            'default' => '6',
                            'sanitize_callback' => 'absint'
                        ),
                        'nbcore_show_related' => array(
                            'default' => true,
                            'sanitize_callback' => array('NBFoody_Customize_Sanitize', 'sanitize_checkbox')
                        ),
                        'nbcore_pd_related_columns' => array(
                            'default' => '3',
                            'sanitize_callback' => array('NBFoody_Customize_Sanitize', 'sanitize_selection')
                        ),
                    ),
                    'controls' => array(
                        'nbcore_pd_layout_intro' => array(
                            'label' => esc_html__('Layout', 'nb-foody'),
                            'section' => 'product_details',
                            'type' => 'NBFoody_Customize_Control_Heading',
                        ),
                        'nbcore_pd_details_title' => array(
                            'label' => esc_html__('Enable Product title', 'nb-foody'),
                            'description' => esc_html__('Default product title is not display if the Page title is showing. Enable this to displaying both.', 'nb-foody'),
                            'section' => 'product_details',
                            'type' => 'NBFoody_Customize_Control_Switch',
                        ),
                        'nbcore_pd_details_sidebar' => array(
                            'label' => esc_html__('Product details sidebar', 'nb-foody'),
                            'section' => 'product_details',
                            'type' => 'NBFoody_Customize_Control_Radio_Image',
                            'choices' => array(
                                'left-sidebar' => get_template_directory_uri() . '/assets/images/options/2cl.png',
                                'no-sidebar' => get_template_directory_uri() . '/assets/images/options/1c.png',
                                'right-sidebar' => get_template_directory_uri() . '/assets/images/options/2cr.png',
                            ),
                        ),
                        'nbcore_pd_details_width' => array(
                            'label' => esc_html__('Product details content width', 'nb-foody'),
                            'section' => 'product_details',
                            'type' => 'NBFoody_Customize_Control_Slider',
                            'choices' => array(
                                'unit' => '%',
                                'min' => '60',
                                'max' => '80',
                                'step' => '1'
                            ),
                        ),
                        'product_sticky_sidebar' => array(
                            'label' => esc_html__('Sticky sidebar', 'nb-foody'),
                            'section' => 'product_details',
                            'type' => 'NBFoody_Customize_Control_Switch',
                        ),
                        'nbcore_pd_meta_layout' => array(
                            'label' => esc_html__('Product meta layout', 'nb-foody'),
                            'section' => 'product_details',
                            'type' => 'NBFoody_Customize_Control_Radio_Image',
                            'choices' => array(
                                'left-images' => get_template_directory_uri() . '/assets/images/options/left-image.png',
                                'right-images' => get_template_directory_uri() . '/assets/images/options/right-image.png',
                                'wide' => get_template_directory_uri() . '/assets/images/options/wide.png',
                            ),
                        ),
                        'nbcore_add_cart_style' => array(
                            'label' => esc_html__('Add to cart input style', 'nb-foody'),
                            'section' => 'product_details',
                            'type' => 'select',
                            'choices' => array(
                                'style-1' => esc_html__('Style 1', 'nb-foody'),
                                'style-2' => esc_html__('Style 2', 'nb-foody'),
                            ),
                        ),
                        'nbcore_pd_show_social' => array(
                            'label' => esc_html__('Show social share?', 'nb-foody'),
                            'section' => 'product_details',
                            'type' => 'NBFoody_Customize_Control_Switch',
                        ),
                        'nbcore_pd_gallery_intro' => array(
                            'label' => esc_html__('Product Gallery', 'nb-foody'),
                            'section' => 'product_details',
                            'type' => 'NBFoody_Customize_Control_Heading',
                        ),
                        'nbcore_pd_images_width' => array(
                            'label' => esc_html__('Product images width', 'nb-foody'),
                            'section' => 'product_details',
                            'type' => 'NBFoody_Customize_Control_Slider',
                            'choices' => array(
                                'unit' => '%',
                                'min' => '30',
                                'max' => '60',
                                'step' => '1'
                            ),
                        ),
                        'nbcore_pd_featured_autoplay' => array(
                            'label' => esc_html__('Featured Images Autoplay', 'nb-foody'),
                            'section' => 'product_details',
                            'type' => 'NBFoody_Customize_Control_Switch',
                        ),
                        'nbcore_pd_thumb_pos' => array(
                            'label' => esc_html__('Small thumb position', 'nb-foody'),
                            'section' => 'product_details',
                            'type' => 'NBFoody_Customize_Control_Radio_Image',
                            'choices' => array(
                                'bottom-thumb' => get_template_directory_uri() . '/assets/images/options/bottom-thumb.png',
                                'left-thumb' => get_template_directory_uri() . '/assets/images/options/left-thumb.png',
                                'inside-thumb' => get_template_directory_uri() . '/assets/images/options/inside-thumb.png',
                            ),
                        ),
                        'nbcore_pd_info_tab_intro' => array(
                            'label' => esc_html__('Information tab', 'nb-foody'),
                            'section' => 'product_details',
                            'type' => 'NBFoody_Customize_Control_Heading',
                        ),
                        'nbcore_info_style' => array(
                            'label' => esc_html__('Tab style', 'nb-foody'),
                            'section' => 'product_details',
                            'type' => 'select',
                            'choices' => array(
                                'horizontal-tabs' => esc_html__('Horizontal', 'nb-foody'),
                                'accordion-tabs' => esc_html__('Accordion', 'nb-foody'),
                            ),
                        ),
                        'nbcore_reviews_form' => array(
                            'label' => esc_html__('Reviews form style', 'nb-foody'),
                            'section' => 'product_details',
                            'type' => 'select',
                            'choices' => array(
                                'split' => esc_html__('Split', 'nb-foody'),
                                'full-width' => esc_html__('Full Width', 'nb-foody'),
                            ),
                        ),
                        'nbcore_reviews_round_avatar' => array(
                            'label' => esc_html__('Round reviewer avatar', 'nb-foody'),
                            'section' => 'product_details',
                            'type' => 'NBFoody_Customize_Control_Switch',
                        ),
                        'nbcore_other_products_intro' => array(
                            'label' => esc_html__('Related & Cross-sells products', 'nb-foody'),
                            'section' => 'product_details',
                            'type' => 'NBFoody_Customize_Control_Heading',
                        ),
                        'nbcore_show_upsells' => array(
                            'label' => esc_html__('Show upsells products?', 'nb-foody'),
                            'section' => 'product_details',
                            'type' => 'NBFoody_Customize_Control_Switch',
                        ),
                        'nbcore_pd_upsells_columns' => array(
                            'label' => esc_html__('Upsells Products per row', 'nb-foody'),
                            'section' => 'product_details',
                            'type' => 'select',
                            'choices' => array(
                                '2' => esc_html__('2 Products', 'nb-foody'),
                                '3' => esc_html__('3 Products', 'nb-foody'),
                                '4' => esc_html__('4 Products', 'nb-foody'),
                            ),
                        ),
                        'nbcore_upsells_limit' => array(
                            'label' => esc_html__('Upsells Products limit', 'nb-foody'),
                            'section' => 'product_details',
                            'type' => 'number',
                            'input_attrs' => array(
                                'min' => '2',
                                'step' => '1'
                            ),
                        ),
                        'nbcore_show_related' => array(
                            'label' => esc_html__('Show related product?', 'nb-foody'),
                            'section' => 'product_details',
                            'type' => 'NBFoody_Customize_Control_Switch',
                        ),
                        'nbcore_pd_related_columns' => array(
                            'label' => esc_html__('Related Products per row', 'nb-foody'),
                            'section' => 'product_details',
                            'type' => 'select',
                            'choices' => array(
                                '2' => esc_html__('2 Products', 'nb-foody'),
                                '3' => esc_html__('3 Products', 'nb-foody'),
                                '4' => esc_html__('4 Products', 'nb-foody'),
                            ),
                        ),
                    ),
                ),
                'other_wc_pages' => array(
                    'title' => esc_html__('Other Pages', 'nb-foody'),
                    'settings' => array(
                        'nbcore_cart_intro' => array(),
                        'nbcore_cart_layout' => array(
                            'default' => 'cart-layout-1',
                            'sanitize_callback' => array('NBFoody_Customize_Sanitize', 'sanitize_selection')
                        ),
                        'nbcore_show_to_shop' => array(
                            'default' => true,
                            'sanitize_callback' => array('NBFoody_Customize_Sanitize', 'sanitize_checkbox')
                        ),
                        'nbcore_show_cross_sells' => array(
                            'default' => true,
                            'sanitize_callback' => array('NBFoody_Customize_Sanitize', 'sanitize_checkbox')
                        ),
                        'nbcore_cross_sells_per_row' => array(
                            'default' => '4',
                            'sanitize_callback' => array('NBFoody_Customize_Sanitize', 'sanitize_selection')
                        ),
                        'nbcore_cross_sells_limit' => array(
                            'default' => '6',
                            'sanitize_callback' => 'absint'
                        ),
                    ),
                    'controls' => array(
                        'nbcore_cart_intro' => array(
                            'label' => esc_html__('Cart', 'nb-foody'),
                            'section' => 'other_wc_pages',
                            'type' => 'NBFoody_Customize_Control_Heading'
                        ),
                        'nbcore_cart_layout' => array(
                            'label' => esc_html__('Cart page layout', 'nb-foody'),
                            'section' => 'other_wc_pages',
                            'type' => 'NBFoody_Customize_Control_Radio_Image',
                            'choices' => array(
                                'cart-layout-1' => get_template_directory_uri() . '/assets/images/options/cart-style-1.png',
                                'cart-layout-2' => get_template_directory_uri() . '/assets/images/options/cart-style-2.png',
                            ),
                        ),
                        'nbcore_show_to_shop' => array(
                            'label' => esc_html__('Show Continue shopping button', 'nb-foody'),
                            'section' => 'other_wc_pages',
                            'type' => 'NBFoody_Customize_Control_Switch',
                        ),
                        'nbcore_show_cross_sells' => array(
                            'label' => esc_html__('Show cross sells', 'nb-foody'),
                            'section' => 'other_wc_pages',
                            'type' => 'NBFoody_Customize_Control_Switch'
                        ),
                        'nbcore_cross_sells_per_row' => array(
                            'label' => esc_html__('Products per row', 'nb-foody'),
                            'section' => 'other_wc_pages',
                            'type' => 'select',
                            'choices' => array(
                                '3' => esc_html__('3 products', 'nb-foody'),
                                '4' => esc_html__('4 products', 'nb-foody'),
                                '5' => esc_html__('5 products', 'nb-foody'),
                            ),
                        ),
                        'nbcore_cross_sells_limit' => array(
                            'label' => esc_html__('Cross sells Products limit', 'nb-foody'),
                            'section' => 'other_wc_pages',
                            'type' => 'number',
                            'input_attrs' => array(
                                'min' => '3',
                                'step' => '1'
                            ),
                        ),
                    ),
                ),
            )),
        );

        return $arr;
    }
}