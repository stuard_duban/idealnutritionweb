<?php

class NBFoody_Customize_Options_Color
{
    public static function options()
    {
        return array(
            'title' => esc_html__('Color', 'nb-foody'),
            'priority' => 13,
            'sections' => apply_filters('nbt_color_array', array(
                'general_color' => array(
                    'title' => esc_html__('General', 'nb-foody'),
                    'settings' => array(
                        'nbcore_main_colors_intro' => array(),
                        'nbcore_primary_color' => array(
                            'default' => '#1e88e5',
                            'transport' => 'postMessage',
                            'sanitize_callback' => 'wp_filter_nohtml_kses'
                        ),
                        'nbcore_secondary_color' => array(
                            'default' => '#fdd835',
                            'transport' => 'postMessage',
                            'sanitize_callback' => 'wp_filter_nohtml_kses'
                        ),
                        'nbcore_background_colors_intro' => array(),
                        'nbcore_background_color' => array(
                            'default' => '#fff',
                            'transport' => 'postMessage',
                            'sanitize_callback' => 'wp_filter_nohtml_kses'
                        ),
                        'nbcore_inner_background' => array(
                            'default' => '#edf0f5',
                            'transport' => 'postMessage',
                            'sanitize_callback' => 'wp_filter_nohtml_kses'
                        ),
                    ),
                    'controls' => array(
                        'nbcore_main_colors_intro' => array(
                            'label' => esc_html__('Main Colors', 'nb-foody'),
                            'section' => 'general_color',
                            'type' => 'NBFoody_Customize_Control_Heading',
                        ),
                        'nbcore_primary_color' => array(
                            'label' => esc_html__('Primary Color', 'nb-foody'),
                            'section' => 'general_color',
                            'type' => 'NBFoody_Customize_Control_Color',
                        ),
                        'nbcore_secondary_color' => array(
                            'label' => esc_html__('Secondary Color', 'nb-foody'),
                            'section' => 'general_color',
                            'type' => 'NBFoody_Customize_Control_Color',
                        ),
                        'nbcore_background_colors_intro' => array(
                            'label' => esc_html__('Background', 'nb-foody'),
                            'section' => 'general_color',
                            'type' => 'NBFoody_Customize_Control_Heading',
                        ),
                        'nbcore_background_color' => array(
                            'label' => esc_html__('Site Background Color', 'nb-foody'),
                            'section' => 'general_color',
                            'type' => 'NBFoody_Customize_Control_Color',
                        ),
                        'nbcore_inner_background' => array(
                            'label' => esc_html__('Inner Background Color', 'nb-foody'),
                            'section' => 'general_color',
                            'type' => 'NBFoody_Customize_Control_Color',
                        ),
                    ),
                ),
                'type_color' => array(
                    'title' => esc_html__('Type', 'nb-foody'),
                    'settings' => array(
                        'nbcore_text_colors_intro' => array(),
                        'nbcore_heading_color' => array(
                            'default' => '#323232',
                            'transport' => 'postMessage',
                            'sanitize_callback' => 'wp_filter_nohtml_kses'
                        ),
                        'nbcore_body_color' => array(
                            'default' => '#777777',
                            'transport' => 'postMessage',
                            'sanitize_callback' => 'wp_filter_nohtml_kses'
                        ),
                        'nbcore_link_colors_intro' => array(),
                        'nbcore_link_color' => array(
                            'default' => '#000000',
                            'sanitize_callback' => 'wp_filter_nohtml_kses'
                        ),
                        'nbcore_link_hover_color' => array(
                            'default' => '#444444',
                            'sanitize_callback' => 'wp_filter_nohtml_kses'
                        ),
                        'nbcore_divider_colors_intro' => array(),
                        'nbcore_divider_color' => array(
                            'default' => '#e4e4e4',
                            'transport' => 'postMessage',
                            'sanitize_callback' => 'wp_filter_nohtml_kses'
                        ),
                    ),
                    'controls' => array(
                        'nbcore_text_colors_intro' => array(
                            'label' => esc_html__('Text', 'nb-foody'),
                            'section' => 'type_color',
                            'type' => 'NBFoody_Customize_Control_Heading',
                        ),
                        'nbcore_heading_color' => array(
                            'label' => esc_html__('Heading Color', 'nb-foody'),
                            'section' => 'type_color',
                            'type' => 'NBFoody_Customize_Control_Color',
                        ),
                        'nbcore_body_color' => array(
                            'label' => esc_html__('Body Color', 'nb-foody'),
                            'section' => 'type_color',
                            'type' => 'NBFoody_Customize_Control_Color',
                        ),
                        'nbcore_link_colors_intro' => array(
                            'label' => esc_html__('Link', 'nb-foody'),
                            'section' => 'type_color',
                            'type' => 'NBFoody_Customize_Control_Heading',
                        ),
                        'nbcore_link_color' => array(
                            'label' => esc_html__('Link Color', 'nb-foody'),
                            'section' => 'type_color',
                            'type' => 'NBFoody_Customize_Control_Color',
                        ),
                        'nbcore_link_hover_color' => array(
                            'label' => esc_html__('Link Hover Color', 'nb-foody'),
                            'section' => 'type_color',
                            'type' => 'NBFoody_Customize_Control_Color',
                        ),
                        'nbcore_divider_colors_intro' => array(
                            'label' => esc_html__('Divider', 'nb-foody'),
                            'section' => 'type_color',
                            'type' => 'NBFoody_Customize_Control_Heading',
                        ),
                        'nbcore_divider_color' => array(
                            'label' => esc_html__('Divider Color', 'nb-foody'),
                            'section' => 'type_color',
                            'type' => 'NBFoody_Customize_Control_Color',
                        ),
                    ),
                ),
                'header_colors' => array(
                    'title' => esc_html__('Header', 'nb-foody'),
                    'settings' => array(
                        'nbcore_header_top_colors_intro' => array(),
                        'nbcore_header_top_bg' => array(
                            'default' => '#282725',
                            'transport' => 'postMessage',
                            'sanitize_callback' => 'wp_filter_nohtml_kses',
                        ),
                        'nbcore_header_top_color' => array(
                            'default' => '#e4e4e4',
                            'transport' => 'postMessage',
                            'sanitize_callback' => 'wp_filter_nohtml_kses',
                        ),
                        'nbcore_header_middle_colors_intro' => array(),
                        'nbcore_header_middle_bg' => array(
                            'default' => '#ffffff',
                            'transport' => 'postMessage',
                            'sanitize_callback' => 'wp_filter_nohtml_kses',
                        ),
                        'nbcore_header_middle_color' => array(
                            'default' => '#646464',
                            'transport' => 'postMessage',
                            'sanitize_callback' => 'wp_filter_nohtml_kses',
                        ),
                        'nbcore_header_bottom_colors_intro' => array(),
                        'nbcore_header_bot_bg' => array(
                            'default' => '#fff',
                            'transport' => 'postMessage',
                            'sanitize_callback' => 'wp_filter_nohtml_kses',
                        ),
                        'nbcore_header_bot_color' => array(
                            'default' => '#646464',
                            'transport' => 'postMessage',
                            'sanitize_callback' => 'wp_filter_nohtml_kses',
                        ),
                    ),
                    'controls' => array(
                        'nbcore_header_top_colors_intro' => array(
                            'label' => esc_html__('Header Top', 'nb-foody'),
                            'section' => 'header_colors',
                            'type' => 'NBFoody_Customize_Control_Heading',
                        ),
                        'nbcore_header_top_bg' => array(
                            'label' => esc_html__('background color', 'nb-foody'),
                            'section' => 'header_colors',
                            'type' => 'NBFoody_Customize_Control_Color',
                        ),
                        'nbcore_header_top_color' => array(
                            'label' => esc_html__('Text color', 'nb-foody'),
                            'section' => 'header_colors',
                            'type' => 'NBFoody_Customize_Control_Color',
                        ),
                        'nbcore_header_middle_colors_intro' => array(
                            'label' => esc_html__('Header Middle', 'nb-foody'),
                            'section' => 'header_colors',
                            'type' => 'NBFoody_Customize_Control_Heading',
                        ),
                        'nbcore_header_middle_bg' => array(
                            'label' => esc_html__('background color', 'nb-foody'),
                            'section' => 'header_colors',
                            'type' => 'NBFoody_Customize_Control_Color',
                        ),
                        'nbcore_header_middle_color' => array(
                            'label' => esc_html__('Text color', 'nb-foody'),
                            'section' => 'header_colors',
                            'type' => 'NBFoody_Customize_Control_Color',
                        ),
                        'nbcore_header_bottom_colors_intro' => array(
                            'label' => esc_html__('Header Bottom', 'nb-foody'),
                            'section' => 'header_colors',
                            'type' => 'NBFoody_Customize_Control_Heading',
                        ),
                        'nbcore_header_bot_bg' => array(
                            'label' => esc_html__('background color', 'nb-foody'),
                            'section' => 'header_colors',
                            'type' => 'NBFoody_Customize_Control_Color',
                        ),
                        'nbcore_header_bot_color' => array(
                            'label' => esc_html__('Text color', 'nb-foody'),
                            'section' => 'header_colors',
                            'type' => 'NBFoody_Customize_Control_Color',
                        ),
                    ),
                ),
                'footer_colors' => array(
                    'title' => esc_html__('Footer', 'nb-foody'),
                    'settings' => array(
                        'nbcore_footer_top_color_intro' => array(),
                        'nbcore_footer_top_heading' => array(
                            'default' => '#323232',
                            'transport' => 'postMessage',
                            'sanitize_callback' => 'wp_filter_nohtml_kses'
                        ),
                        'nbcore_footer_top_color' => array(
                            'default' => '#777777',
                            'transport' => 'postMessage',
                            'sanitize_callback' => 'wp_filter_nohtml_kses'
                        ),
                        'nbcore_footer_top_bg' => array(
                            'default' => '#edf0f5',
                            'transport' => 'postMessage',
                            'sanitize_callback' => 'wp_filter_nohtml_kses'
                        ),
                        'nbcore_footer_bot_color_intro' => array(),
                        'nbcore_footer_bot_heading' => array(
                            'default' => '#323232',
                            'transport' => 'postMessage',
                            'sanitize_callback' => 'wp_filter_nohtml_kses'
                        ),
                        'nbcore_footer_bot_color' => array(
                            'default' => '#777777',
                            'transport' => 'postMessage',
                            'sanitize_callback' => 'wp_filter_nohtml_kses'
                        ),
                        'nbcore_footer_bot_bg' => array(
                            'default' => '#edf0f5',
                            'transport' => 'postMessage',
                            'sanitize_callback' => 'wp_filter_nohtml_kses'
                        ),
                        'nbcore_footer_abs_color_intro' => array(),
                        'nbcore_footer_abs_color' => array(
                            'default' => '#edf0f5',
                            'transport' => 'postMessage',
                            'sanitize_callback' => 'wp_filter_nohtml_kses'
                        ),
                        'nbcore_footer_abs_bg' => array(
                            'default' => '#1f1f1f',
                            'transport' => 'postMessage',
                            'sanitize_callback' => 'wp_filter_nohtml_kses'
                        ),
                    ),
                    'controls' => array(
                        'nbcore_footer_top_color_intro' => array(
                            'label' => esc_html__('Footer top', 'nb-foody'),
                            'section' => 'footer_colors',
                            'type' => 'NBFoody_Customize_Control_Heading',
                        ),
                        'nbcore_footer_top_heading' => array(
                            'label' => esc_html__('Heading color', 'nb-foody'),
                            'section' => 'footer_colors',
                            'type' => 'NBFoody_Customize_Control_Color',
                        ),
                        'nbcore_footer_top_color' => array(
                            'label' => esc_html__('Text color', 'nb-foody'),
                            'section' => 'footer_colors',
                            'type' => 'NBFoody_Customize_Control_Color',
                        ),
                        'nbcore_footer_top_bg' => array(
                            'label' => esc_html__('Background color', 'nb-foody'),
                            'section' => 'footer_colors',
                            'type' => 'NBFoody_Customize_Control_Color',
                        ),
                        'nbcore_footer_bot_color_intro' => array(
                            'label' => esc_html__('Footer bottom', 'nb-foody'),
                            'section' => 'footer_colors',
                            'type' => 'NBFoody_Customize_Control_Heading',
                        ),
                        'nbcore_footer_bot_heading' => array(
                            'label' => esc_html__('Heading color', 'nb-foody'),
                            'section' => 'footer_colors',
                            'type' => 'NBFoody_Customize_Control_Color',
                        ),
                        'nbcore_footer_bot_color' => array(
                            'label' => esc_html__('Text color', 'nb-foody'),
                            'section' => 'footer_colors',
                            'type' => 'NBFoody_Customize_Control_Color',
                        ),
                        'nbcore_footer_bot_bg' => array(
                            'label' => esc_html__('Background color', 'nb-foody'),
                            'section' => 'footer_colors',
                            'type' => 'NBFoody_Customize_Control_Color',
                        ),
                        'nbcore_footer_abs_color_intro' => array(
                            'label' => esc_html__('Footer Absolute Bottom', 'nb-foody'),
                            'section' => 'footer_colors',
                            'type' => 'NBFoody_Customize_Control_Heading',
                        ),
                        'nbcore_footer_abs_color' => array(
                            'label' => esc_html__('Text color', 'nb-foody'),
                            'section' => 'footer_colors',
                            'type' => 'NBFoody_Customize_Control_Color',
                        ),
                        'nbcore_footer_abs_bg' => array(
                            'label' => esc_html__('Background color', 'nb-foody'),
                            'section' => 'footer_colors',
                            'type' => 'NBFoody_Customize_Control_Color',
                        ),
                    ),
                ),
                'button_colors' => array(
                    'title' => esc_html__('Buttons', 'nb-foody'),
                    'settings' => array(
                        'nbcore_pb_intro' => array(),
                        'nbcore_pb_background' => array(
                            'default' => '#1e88e5',
                            'transport' => 'postMessage',
                            'sanitize_callback' => 'wp_filter_nohtml_kses'
                        ),
                        'nbcore_pb_background_hover' => array(
                            'default' => '#1565C0',
                            'transport' => 'postMessage',
                            'sanitize_callback' => 'wp_filter_nohtml_kses'
                        ),
                        'nbcore_pb_text' => array(
                            'default' => '#ffffff',
                            'transport' => 'postMessage',
                            'sanitize_callback' => 'wp_filter_nohtml_kses'
                        ),
                        'nbcore_pb_text_hover' => array(
                            'default' => '#ffffff',
                            'transport' => 'postMessage',
                            'sanitize_callback' => 'wp_filter_nohtml_kses'
                        ),
                        'nbcore_pb_border' => array(
                            'default' => '#1e88e5',
                            'transport' => 'postMessage',
                            'sanitize_callback' => 'wp_filter_nohtml_kses'
                        ),
                        'nbcore_pb_border_hover' => array(
                            'default' => '#1565C0',
                            'transport' => 'postMessage',
                            'sanitize_callback' => 'wp_filter_nohtml_kses'
                        ),
                        'nbcore_sb_intro' => array(),
                        'nbcore_sb_background' => array(
                            'default' => 'transparent',
                            'transport' => 'postMessage',
                            'sanitize_callback' => 'wp_filter_nohtml_kses'
                        ),
                        'nbcore_sb_background_hover' => array(
                            'default' => '#1e88e5',
                            'transport' => 'postMessage',
                            'sanitize_callback' => 'wp_filter_nohtml_kses'
                        ),
                        'nbcore_sb_text' => array(
                            'default' => '#1e88e5',
                            'transport' => 'postMessage',
                            'sanitize_callback' => 'wp_filter_nohtml_kses'
                        ),
                        'nbcore_sb_text_hover' => array(
                            'default' => '#ffffff',
                            'transport' => 'postMessage',
                            'sanitize_callback' => 'wp_filter_nohtml_kses'
                        ),
                        'nbcore_sb_border' => array(
                            'default' => '#1e88e5',
                            'transport' => 'postMessage',
                            'sanitize_callback' => 'wp_filter_nohtml_kses'
                        ),
                        'nbcore_sb_border_hover' => array(
                            'default' => '#1e88e5',
                            'transport' => 'postMessage',
                            'sanitize_callback' => 'wp_filter_nohtml_kses'
                        ),
                    ),
                    'controls' => array(
                        'nbcore_pb_intro' => array(
                            'label' => esc_html__('Primary button', 'nb-foody'),
                            'section' => 'button_colors',
                            'type' => 'NBFoody_Customize_Control_Heading',
                        ),
                        'nbcore_pb_background' => array(
                            'label' => esc_html__('Background', 'nb-foody'),
                            'section' => 'button_colors',
                            'type' => 'NBFoody_Customize_Control_Color',
                        ),
                        'nbcore_pb_background_hover' => array(
                            'label' => esc_html__('Background Hover', 'nb-foody'),
                            'section' => 'button_colors',
                            'type' => 'NBFoody_Customize_Control_Color',
                        ),
                        'nbcore_pb_text' => array(
                            'label' => esc_html__('Text', 'nb-foody'),
                            'section' => 'button_colors',
                            'type' => 'NBFoody_Customize_Control_Color',
                        ),
                        'nbcore_pb_text_hover' => array(
                            'label' => esc_html__('Text hover', 'nb-foody'),
                            'section' => 'button_colors',
                            'type' => 'NBFoody_Customize_Control_Color',
                        ),
                        'nbcore_pb_border' => array(
                            'label' => esc_html__('Border', 'nb-foody'),
                            'section' => 'button_colors',
                            'type' => 'NBFoody_Customize_Control_Color',
                        ),
                        'nbcore_pb_border_hover' => array(
                            'label' => esc_html__('Border hover', 'nb-foody'),
                            'section' => 'button_colors',
                            'type' => 'NBFoody_Customize_Control_Color',
                        ),
                        'nbcore_sb_intro' => array(
                            'label' => esc_html__('Secondary button', 'nb-foody'),
                            'section' => 'button_colors',
                            'type' => 'NBFoody_Customize_Control_Heading',
                        ),
                        'nbcore_sb_background' => array(
                            'label' => esc_html__('Background', 'nb-foody'),
                            'section' => 'button_colors',
                            'type' => 'NBFoody_Customize_Control_Color',
                        ),
                        'nbcore_sb_background_hover' => array(
                            'label' => esc_html__('Background Hover', 'nb-foody'),
                            'section' => 'button_colors',
                            'type' => 'NBFoody_Customize_Control_Color',
                        ),
                        'nbcore_sb_text' => array(
                            'label' => esc_html__('Text', 'nb-foody'),
                            'section' => 'button_colors',
                            'type' => 'NBFoody_Customize_Control_Color',
                        ),
                        'nbcore_sb_text_hover' => array(
                            'label' => esc_html__('Text hover', 'nb-foody'),
                            'section' => 'button_colors',
                            'type' => 'NBFoody_Customize_Control_Color',
                        ),
                        'nbcore_sb_border' => array(
                            'label' => esc_html__('Border', 'nb-foody'),
                            'section' => 'button_colors',
                            'type' => 'NBFoody_Customize_Control_Color',
                        ),
                        'nbcore_sb_border_hover' => array(
                            'label' => esc_html__('Border hover', 'nb-foody'),
                            'section' => 'button_colors',
                            'type' => 'NBFoody_Customize_Control_Color',
                        ),
                    ),
                ),
                'other_colors' => array(
                    'title' => esc_html__('Other', 'nb-foody'),
                    'settings' => array(
                        'nbcore_page_title_color_intro' => array(),
                        'nbcore_page_title_color' => array(
                            'default' => '#323232',
                            'transport' => 'postMessage',
                            'sanitize_callback' => 'wp_filter_nohtml_kses'
                        ),
                    ),
                    'controls' => array(
                        'nbcore_page_title_color_intro' => array(
                            'label' => esc_html__('Page title', 'nb-foody'),
                            'section' => 'other_colors',
                            'type' => 'NBFoody_Customize_Control_Heading'
                        ),
                        'nbcore_page_title_color' => array(
                            'label' => esc_html__('Text color', 'nb-foody'),
                            'section' => 'other_colors',
                            'type' => 'NBFoody_Customize_Control_Color'
                        ),
                    ),
                ),
            )),
        );
    }
}