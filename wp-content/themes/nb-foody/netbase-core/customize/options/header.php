<?php

class NBFoody_Customize_Options_Header
{
    public static function options()
    {
        return array(
            'title' => esc_html__('Header Options', 'nb-foody'),
            'description' => esc_html__('header description', 'nb-foody'),
            'priority' => 11,
            'sections' => apply_filters('nbt_header_array', array(
                'header_presets' => array(
                    'title' => esc_html__('Presets', 'nb-foody'),
                    'settings' => array(
                        'header_heading' => array(),
                        'nbcore_header_style' => array(
                            'default' => 'mid-stack',
                            'transport' => 'refresh',
                            'sanitize_callback' => array('NBFoody_Customize_Sanitize', 'sanitize_selection')
                        ),
                    ),
                    'controls' => array(
                        'header_heading' => array(
                            'label' => esc_html__('Header style', 'nb-foody'),
                            'description' => esc_html__('Quickly select a preset to change your header layout.', 'nb-foody'),
                            'type' => 'NBFoody_Customize_Control_Heading',
                            'section' => 'header_presets',
                        ),
                        'nbcore_header_style' => array(
                            'section' => 'header_presets',
                            'type' => 'NBFoody_Customize_Control_Radio_Image',
                            'choices' => array(
                                'mid-stack' => get_template_directory_uri() . '/assets/images/options/headers/mid-stack.png',
                                'mid-stack-2' => get_template_directory_uri() . '/assets/images/options/headers/mid-stack-2.png',
                                'mid-inline' => get_template_directory_uri() . '/assets/images/options/headers/mid-inline.png',
                                'mid-inline-2' => get_template_directory_uri() . '/assets/images/options/headers/mid-inline-2.png',
                                'left-inline' => get_template_directory_uri() . '/assets/images/options/headers/left-inline.png',
                                'left-inline_1' => get_template_directory_uri() . '/assets/images/options/headers/left-inline_1.png',
                                'left-stack' => get_template_directory_uri() . '/assets/images/options/headers/left-stack.png',
                                'left-stack-2' => get_template_directory_uri() . '/assets/images/options/headers/left-stack-2.png',
                            ),
                        ),
                    ),
                ),
                'header_general' => array(
                    'title' => esc_html__('Sections', 'nb-foody'),
                    'settings' => array(
                        'nbcore_general_intro' => array(),
                        'nbcore_logo_upload' => array(
                            'default' => '',
                            'sanitize_callback' => array('NBFoody_Customize_Sanitize', 'sanitize_file_image')
                        ),
                        'nbcore_logo_width' => array(
                            'default' => '120',
                            'transport' => 'postMessage',
                            'sanitize_callback' => 'absint'
                        ),
                        'nbcore_header_fixed' => array(
                            'default' => false,
                            'sanitize_callback' => array('NBFoody_Customize_Sanitize', 'sanitize_checkbox')
                        ),
                        'nbcore_header_text_section' => array(
                            'default' => '',
                            'transport' => 'postMessage',
                            'sanitize_callback' => 'wp_filter_nohtml_kses',
                        ),
                        'nbcore_header_top_intro' => array(),
                        'nbcore_top_section_padding' => array(
                            'default' => '10',
                            'transport' => 'postMessage',
                            'sanitize_callback' => 'absint',
                        ),
                        'nbcore_header_middle_intro' => array(),
                        'nbcore_middle_section_padding' => array(
                            'default' => '20',
                            'transport' => 'postMessage',
                            'sanitize_callback' => 'absint',
                        ),
                        'nbcore_header_bot_intro' => array(),
                        'nbcore_bot_section_padding' => array(
                            'default' => '30',
                            'transport' => 'postMessage',
                            'sanitize_callback' => 'absint',
                        ),
                        'nbcore_header_color_focus' => array(),
                    ),
                    'controls' => array(
                        'nbcore_general_intro' => array(
                            'label' => esc_html__('General', 'nb-foody'),
                            'section' => 'header_general',
                            'type' => 'NBFoody_Customize_Control_Heading',
                        ),
                        'nbcore_logo_upload' => array(
                            'label' => esc_html__('Site Logo', 'nb-foody'),
                            'section' => 'header_general',
                            'description' => esc_html__('If you don\'t upload logo image, your site\'s logo will be the Site Title ', 'nb-foody'),
                            'type' => 'WP_Customize_Upload_Control'
                        ),
                        'nbcore_logo_width' => array(
                            'label' => esc_html__('Logo Area Width', 'nb-foody'),
                            'section' => 'header_general',
                            'type' => 'NBFoody_Customize_Control_Slider',
                            'choices' => array(
                                'unit' => 'px',
                                'min' => '100',
                                'max' => '600',
                                'step' => '10',
                            ),
                        ),
                        'nbcore_header_fixed' => array(
                            'label' => esc_html__('Fixed header', 'nb-foody'),
                            'section' => 'header_general',
                            'type' => 'NBFoody_Customize_Control_Switch',
                        ),
                        'nbcore_header_text_section' => array(
                            'label' => esc_html__('Text section', 'nb-foody'),
                            'section' => 'header_general',
                            'type' => 'textarea',
                        ),
                        'nbcore_header_top_intro' => array(
                            'label' => esc_html__('Header topbar', 'nb-foody'),
                            'section' => 'header_general',
                            'type' => 'NBFoody_Customize_Control_Heading',
                        ),
                        'nbcore_top_section_padding' => array(
                            'label' => esc_html__('Top & bottom padding', 'nb-foody'),
                            'section' => 'header_general',
                            'type' => 'NBFoody_Customize_Control_Slider',
                            'choices' => array(
                                'unit' => 'px',
                                'min' => '0',
                                'max' => '45',
                                'step' => '1'
                            ),
                        ),
                        'nbcore_header_middle_intro' => array(
                            'label' => esc_html__('Header Middle', 'nb-foody'),
                            'section' => 'header_general',
                            'type' => 'NBFoody_Customize_Control_Heading',
                        ),
                        'nbcore_middle_section_padding' => array(
                            'label' => esc_html__('Top & bottom padding', 'nb-foody'),
                            'section' => 'header_general',
                            'type' => 'NBFoody_Customize_Control_Slider',
                            'choices' => array(
                                'unit' => 'px',
                                'min' => '0',
                                'max' => '45',
                                'step' => '1'
                            ),
                        ),

                        'nbcore_header_bot_intro' => array(
                            'label' => esc_html__('Header bottom', 'nb-foody'),
                            'section' => 'header_general',
                            'type' => 'NBFoody_Customize_Control_Heading',
                        ),
                        'nbcore_bot_section_padding' => array(
                            'label' => esc_html__('Top & bottom padding', 'nb-foody'),
                            'section' => 'header_general',
                            'type' => 'NBFoody_Customize_Control_Slider',
                            'choices' => array(
                                'unit' => 'px',
                                'min' => '0',
                                'max' => '45',
                                'step' => '1'
                            ),
                        ),
                        'nbcore_header_color_focus' => array(
                            'section' => 'header_general',
                            'type' => 'NBFoody_Customize_Control_Focus',
                            'choices' => array(
                                'header_colors' => esc_html__('Edit color', 'nb-foody'),
                            ),
                        ),
                    ),
                ),
                'header_social' => array(
                    'title' => esc_html__('Socials', 'nb-foody'),
                    'settings' => array(
                        'header_socials' => array(),
                        'nbcore_header_facebook' => array(
                            'default' => '',
                            'transport' => 'refresh',
                            'sanitize_callback' => 'esc_url_raw'
                        ),
                        'nbcore_header_twitter' => array(
                            'default' => '',
                            'transport' => 'refresh',
                            'sanitize_callback' => 'esc_url_raw'
                        ),
                        'nbcore_header_linkedin' => array(
                            'default' => '',
                            'transport' => 'refresh',
                            'sanitize_callback' => 'esc_url_raw'
                        ),
                        'nbcore_header_instagram' => array(
                            'default' => '',
                            'transport' => 'refresh',
                            'sanitize_callback' => 'esc_url_raw'
                        ),
                        'nbcore_header_blog' => array(
                            'default' => '',
                            'transport' => 'refresh',
                            'sanitize_callback' => 'esc_url_raw'
                        ),
                        'nbcore_header_pinterest' => array(
                            'default' => '',
                            'transport' => 'refresh',
                            'sanitize_callback' => 'esc_url_raw'
                        ),
                        'nbcore_header_ggplus' => array(
                            'default' => '',
                            'transport' => 'refresh',
                            'sanitize_callback' => 'esc_url_raw'
                        ),
                    ),
                    'controls' => array(
                        'header_socials' => array(
                            'label' => esc_html__('Socials', 'nb-foody'),
                            'description' => esc_html__('Your social links', 'nb-foody'),
                            'type' => 'NBFoody_Customize_Control_Heading',
                            'section' => 'header_social',
                        ),
                        'nbcore_header_facebook' => array(
                            'label' => esc_html__('Facebook', 'nb-foody'),
                            'section' => 'header_social',
                            'type' => 'url',
                        ),
                        'nbcore_header_twitter' => array(
                            'label' => esc_html__('Twitter', 'nb-foody'),
                            'section' => 'header_social',
                            'type' => 'url',
                        ),
                        'nbcore_header_linkedin' => array(
                            'label' => esc_html__('Linkedin', 'nb-foody'),
                            'section' => 'header_social',
                            'type' => 'url',
                        ),
                        'nbcore_header_instagram' => array(
                            'label' => esc_html__('Instagram', 'nb-foody'),
                            'section' => 'header_social',
                            'type' => 'url',
                        ),
                        'nbcore_header_blog' => array(
                            'label' => esc_html__('Blog', 'nb-foody'),
                            'section' => 'header_social',
                            'type' => 'url',
                        ),
                        'nbcore_header_pinterest' => array(
                            'label' => esc_html__('Pinterest', 'nb-foody'),
                            'section' => 'header_social',
                            'type' => 'url',
                        ),
                        'nbcore_header_ggplus' => array(
                            'label' => esc_html__('Google Plus', 'nb-foody'),
                            'section' => 'header_social',
                            'type' => 'url',
                        ),
                    ),
                ),
            )),
        );
    }
}