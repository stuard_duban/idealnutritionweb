<?php
/**
 * Template part for displaying posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package nb-foody
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="entry-content">
        <?php
        nbfoody_get_categories();
        the_title( '<h3 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h3>' );
        nbfoody_posted_on();
        if(nbfoody_get_options('nbcore_blog_archive_comments')):?>
            <?php if ( ! post_password_required() && ( comments_open() || '0' != get_comments_number() ) ) : ?>
                <span class="comments-link"><i class="icon-speech-bubble"></i><?php comments_popup_link( esc_html__( 'Leave a comment', 'nb-foody' ), esc_html__( 'One Comment', 'nb-foody' ), esc_html__( '% Comments', 'nb-foody' ) ); ?></span>
            <?php endif; ?>
        <?php endif;
        nbfoody_featured_thumb();
		if(nbfoody_get_options('nbcore_blog_archive_summary')):
		?>
			<div class="entry-text">
				<?php
				if(nbfoody_get_options('nbcore_excerpt_only')) :
					nbfoody_get_excerpt();
					echo '<div class="read-more-link"><a class="bt-4 nb-secondary-button" href="' . get_permalink() . '">' . esc_html__('View post', 'nb-foody') . '<span>&rarr;</span></a></div>';
				else :
					the_content( sprintf(
					/* translators: %s: Name of current post. */
						wp_kses( esc_html__( 'Continue reading %s ', 'nb-foody' ) . '<span class="meta-nav">&rarr;</span>', array( 'span' => array( 'class' => array() ) ) ),
						the_title( '<span class="screen-reader-text">"', '"</span>', false )
					) );

					wp_link_pages( array(
						'before' => '<div class="page-links ' . nbfoody_get_options('pagination_style') . '">' . esc_html__( 'Pages:', 'nb-foody' ),
						'after'  => '</div>',
						'link_before' => '<span>',
						'link_after' => '</span>',
					) );
				endif; ?>
			</div>
		<?php endif; ?>
        <div class="entry-footer">
		    <?php nbfoody_get_tags(); ?>
        </div>
	</div>
	
</article><!-- #post-## -->
