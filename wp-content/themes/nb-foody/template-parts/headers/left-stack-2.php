<div class="middle-section-wrap">
    <div class="container">
        <div class="middle-section">
            <?php nbfoody_get_site_logo(); ?>
            <div class="main-nav-wrap">
                <?php nbfoody_sub_menu(); ?>
                <div class="icon-header-wrap">
                    <?php nbfoody_header_woo_section(); ?>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="bot-section-wrap">
    <div class="container">
        <div class="bot-section">
            <?php nbfoody_main_nav(); ?>
        </div>
    </div>
</div>
