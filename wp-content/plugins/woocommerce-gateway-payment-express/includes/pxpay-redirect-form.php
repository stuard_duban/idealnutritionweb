<form action="<?php echo esc_url( $dps_adr ); ?>" method="post" id="dps_payment_form" >
	<input type="submit" class="button-alt button alt" id="submit_Payment_Express_payment_form" value="<?php echo __('Pay via Payment_Express', 'woocommerce-gateway-payment-express-pxpay'); ?>" />
	<a class="button cancel" href="<?php echo esc_url( $order->get_cancel_order_url() ); ?>"><?php echo __('Cancel order &amp; restore cart', 'woocommerce-gateway-payment-express-pxpay'); ?></a>

	<script type="text/javascript">
		jQuery(function(){
			jQuery("body").block(
				{
					message: "<img src='<?php echo $img_loader; ?>' alt='&#x21bb;' style='float:left; margin-right: 10px;' /><?php echo __('Thank you for your order. We are now redirecting you to Payment Express to make payment.', 'woocommerce-gateway-payment-express-pxpay'); ?>",
					overlayCSS:
					{
						background: "#fff",
						opacity: 0.6
					},
					css: {
						padding:        20,
						textAlign:      "center",
						color:          "#555",
						border:         "3px solid #aaa",
						backgroundColor:"#fff",
						cursor:         "wait",
						lineHeight:		"32px"
					}
				});
			jQuery("#submit_Payment_Express_payment_form").click();
		});
	</script>
</form>