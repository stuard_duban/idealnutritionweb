<?php

if ( ! defined( 'ABSPATH' ) ) { exit; }

spl_autoload_register( 'WC_Gateway_Payment_Express_PxPay_Notification_Handler::autoload' );

/**
 * Handles notifications sent back from DPS Payment Express
 */

class WC_Gateway_Payment_Express_PxPay_Notification_Handler {


	/**
	 * Constructor.
	 */

	public function __construct( $pxpay_userid, $pxpay_key, $pxpay_url, $success_url = '', $fail_url = '' ) {

		$this->access_userid = $pxpay_userid;
		$this->access_key = $pxpay_key;
		$this->access_url = $pxpay_url;
		
		$this->success_url = $success_url;
		$this->fail_url = $fail_url;

		add_action( 'woocommerce_api_wc_gateway_payment_express_pxpay', array( $this, 'pxpay_check_dps_callback' ) );
					 
		add_action( 'pxpay_valid_dps_callback', array($this, 'successful_request') );

		/* initiation of logging instance */
		$this->log = new WC_Logger();
		
	}


	/**
	 * Check for valid server callback	 
	 */	
	function pxpay_check_dps_callback() {
		$this->log->add( 'pxpay', '====== callback function has been accessed' );
		if ( isset($_REQUEST["userid"]) ) :
			$uri  = explode('result=', $_SERVER['REQUEST_URI']);
			$uri1 = $uri[1];
			$uri2  = explode('&', $uri1);
			$enc_hex = $uri2[0];

			do_action("pxpay_valid_dps_callback", $enc_hex);
		endif;
		
	}

	function successful_request ($enc_hex) {
		$this->log->add( 'pxpay', print_r( array( 'enc_hex' => $enc_hex ), true ) );
		$this->Payment_Express_success_result($enc_hex);
	}
	
	public function Payment_Express_success_result($enc_hex) {
		global $woocommerce;
		
		if ( isset( $enc_hex ) ) {
			$resultReq = new DpsPxPayResult( $this->access_userid, $this->access_key, $this->access_url );
			$resultReq->result = wp_unslash( $enc_hex );

			try {
				$this->log->add( 'pxpay', '========= requesting transaction result' );
				$response = $resultReq->processResult();
				
				/* $response['option1'] => Order number : 392 */
				$orderNumber = explode( ':', $response->option1 );
				$orderNumber = trim( $orderNumber[1] );
				
				$order_id = apply_filters( 'filter_pxpay_notification_orderID', $orderNumber );
				
				$order =  wc_get_order( $order_id );
				
				$return_url = '';
				if ( $order ) {
				  $return_url = $order->get_checkout_order_received_url();
				} else {
				  $return_url = wc_get_endpoint_url( 'order-received', '', wc_get_page_permalink( 'checkout' ) );
				}

				if ( is_ssl() || get_option('woocommerce_force_ssl_checkout') == 'yes' ) {
				  $return_url = str_replace( 'http:', 'https:', $return_url );
				}
				
				$return_url = apply_filters( 'filter_pxpay_notification_return_url', $return_url, $order );

				$this->log->add( 'pxpay', print_r( array( 'response' => $response, 'orderId' => $orderNumber, 'order' => $order, 'return_url' => $return_url ), true ) );

				do_action( 'dpspxpay_process_return' );

				if ( $response->isValid ) {
					if ( $response->success ) {
						$order->payment_complete();
						//$order->reduce_order_stock();
						$woocommerce->cart->empty_cart();
						
						$userId = explode( '-', $response->billingID );
						if( isset( $userId[1] ) && !empty($userId[1]) ){
							$userId = trim( $userId[1] );
				
							$this->log->add( 'pxpay', print_r( array( 'current_user->ID' => $userId, 'BillingId' => $response->billingID ), true ) );
						
							/*new supplier details (dpsbillingid) being saved */
							update_user_meta( $userId, '_supDpsBillingID', $response->billingID );
							update_post_meta( $order_id, 'dpsTxnRef', $response->txnRef );

						}

						if( $this->success_url != '' || $this->success_url != null ){ $return_url = $this->success_url; }
						wp_redirect( $return_url );
						exit();

					} else {
					
						$this->log->add( 'pxpay', sprintf( 'failed; %s', $response->statusText ) );
						$order->update_status('failed', sprintf(__('Payment %s via Payment Express.', 'woocommerce-gateway-payment-express-pxpay'), strtolower( $response->statusText ) ) );
						wc_add_notice( sprintf(__('Payment %s via Payment Express.', 'woocommerce-gateway-payment-express-pxpay'), strtolower( $response->statusText ) ), $notice_type = 'error' );

						if( $this->success_url != '' || $this->success_url != null ){ $return_url = $this->fail_url; }
						wp_redirect( $return_url );
						exit();
						
					}

				}

			}
			catch (DpsPxPayException $e) {
				$this->log->add( 'pxpay', print_r( $e->getMessage(), true ) );
				exit;
			}
		}

	}

	/**
	* autoload classes as/when needed
	*
	* @param string $class_name name of class to attempt to load
	*/
	public static function autoload($class_name) {

		static $classMap = array (
			'DpsPxPayResult'	=> 'class.DpsPxPayResult.php',
		);

		if (isset($classMap[$class_name])) {
			require DPSPXPAY_PLUGIN_ROOT . $classMap[$class_name];
		}
	}
	
}