<?php
/**
 * Plugin Name: WooCommerce Payment Express Gateway
 * Plugin URI: http://woothemes.com/woocommerce
 * Description: A payment gateway for Payment Express. Uses PX-Pay method.
 * Version Date: 30 November 2017
 * Version: 2.8
 * Author: OPMC
 * Author URI: http://www.opmc.com.au/
 * Text Domain: woocommerce-gateway-payment-express-pxpay
 * Domain Path: /lang/
 */
if ( ! defined( 'ABSPATH' ) ) {exit;} /* Exit if accessed directly */

/**
 * Required functions
 */
if ( ! function_exists( 'woothemes_queue_update' ) ) {
	require_once( 'woo-includes/woo-functions.php' );
}

/**
 * Plugin updates
 */
woothemes_queue_update( plugin_basename( __FILE__ ), '698f37b20ad1d121c3f14fe2b2f8c104', '18640' );


/**
 *  Define all constant values
 */
define('DPSPXPAY_PLUGIN_ROOT', dirname(__FILE__) . '/');
register_activation_hook(__FILE__, 'activation' );

function activation() {
    global $wpdb;

    if(get_option('woocommerce_payment_express_settings')){
        $cst_setting = get_option('woocommerce_payment_express_settings');
        $cst_setting['success_url'] = '';
        $cst_setting['fail_url'] = '';
        update_option('woocommerce_payment_express_settings', $cst_setting);
    }
}

if ( in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ) {

	if ( ! class_exists( 'Woocommerce_paymentexpress_pxpay_init' ) ) {

		load_plugin_textdomain( 'woocommerce-gateway-payment-express', false, dirname( plugin_basename( __FILE__ ) ) . '/lang' );
		
		final class Woocommerce_paymentexpress_pxpay_init {
			
			private static $instance = null;
			public static function initialize() {
				if ( is_null( self::$instance ) ){
					self::$instance = new self();
				}

				return self::$instance;
			}
			
			public function __construct() {
				
				// called after all plugins have loaded
				add_action( 'plugins_loaded', array( $this, 'plugins_loaded' ) );
				
			}
		
			/**
			 * Take care of anything that needs all plugins to be loaded
			 */
			public function plugins_loaded() {

				if ( ! class_exists( 'WC_Payment_Gateway' ) ) {
					return;
				}

				/**
				 * Add the gateway to WooCommerce
				 */
				require_once( plugin_basename( 'class-woocommerce-gateway-payment-express-pxpay.php' ) );
				add_filter( 'woocommerce_payment_gateways', array( $this, 'add_payment_express_gateway') );

			}
			
			public function add_payment_express_gateway( $methods ) {

				$methods[] = 'WC_Gateway_Payment_Express_PxPay'; 
				return $methods;
			}
			
		}

		/* finally instantiate our plugin class and add it to the set of globals */
		$GLOBALS['Woocommerce_paymentexpress_pxpay_init'] = Woocommerce_paymentexpress_pxpay_init::initialize();

	}
	
}
