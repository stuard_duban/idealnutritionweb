<?php
/**
 * My Account Dashboard
 *
 * Shows the first intro screen on the account dashboard.
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/dashboard.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see         https://docs.woocommerce.com/document/template-structure/
 * @author      WooThemes
 * @package     WooCommerce/Templates
 * @version     2.6.0
 */
if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

$User = get_currentuserinfo();
$subscriptions = wcs_get_users_subscriptions();

?>
<h2 style="font-size: calc(2vw + 1.1vh + 1.1vmin);">
    My Account
</h2>
<?php

if ($subscriptions) {

    foreach ($subscriptions as $subscription) {

        $subStatus = esc_attr(wcs_get_subscription_status_name($subscription->get_status()));

        $order_id = false;
        /*Posible Validacion transacciones on-hold*/
        if ($subStatus == 'Active') {
            $order = wc_get_order($subscription->get_data()['parent_id']);
            $order_id = $subscription->get_data()['parent_id'];
            $title = '';
            foreach ($subscription->get_items() as $line_item) {
                $title = $line_item['name'];
            }
            break;
        } else if ($subStatus == 'Cancelled' ) {
            $order_id = false;
        }
    }

    if ($order_id) {

      $MealSelect = false;

      if (SelectMealsbyOrder($order_id)) {
          ?>
          <div class="woocommerce-Message woocommerce-Message--info woocommerce-info" style="padding: 3px 45px;">
              <?php esc_html_e('Your active subscription plan is ', 'woocommerce'); ?>
              <span <!--href="/your-order-of-the-week/?order=<//?= $order_id; ?-->" style="font-weight: bold;"><?php esc_html_e($title, 'woocommerce'); ?></span>
              <br>
              <?php esc_html_e('Meals Selected for the week?', 'woocommerce'); ?>
              <strong style="color: #70a83b;font-weight:  bold;"><a href="/my-account/downloads/"><?php esc_html_e('YES', 'woocommerce'); ?></a></strong>
          </div>
          <div id="Bandera" style="position:  absolute;margin-top: -110px;right: 0px;width: 200px;"><img src="/wp-content/uploads/2018/05/1.jpg" style="width: 100%;"></div>
          <?php
          $MealSelect = false; //true;
      } else {

          ?>
            <div class="woocommerce-Message woocommerce-Message--info woocommerce-info">
                <?php esc_html_e('Meals Selected for the week?', 'woocommerce'); ?>
                <strong style="color: #e34700;font-weight: bold;">NO</strong>
                <a class="btn btn-danger btn-xs" href="/select-your-meals/?order=<?= $order_id; ?>&plan">
                    <?php esc_html_e('See Meals', 'woocommerce') ?>
                </a>
                <a class="btn btn-warning btn-xs" href="<?php get_site_url(); ?>/change-order-subscription/">
                  <?php esc_html_e('Change Quantity', 'woocommerce') ?>
                </a>
            </div>
            <div id="Bandera" style="position:  absolute;margin-top: -110px;right: 0px;width: 200px;"><img src="/wp-content/uploads/2018/05/1.jpg" style="width: 100%;"></div>
          <?php
      }

    } else {
        ?>
        <div class="woocommerce-Message woocommerce-Message--info woocommerce-info">
            <?php ($subStatus == "On hold" ? esc_html_e('Your subscription has been disabled.', 'woocommerce')
            : esc_html_e('You do not have any active subscription plan.', 'woocommerce') );
             ?>
        </div>
        <div id="Bandera" style="position:  absolute;margin-top: -110px;right: 0px;width: 200px;"><img src="/wp-content/uploads/2018/05/2.jpg" style="width: 100%;"></div>
        <?php
    }
} else {
    ?>
    <div class="woocommerce-Message woocommerce-Message--info woocommerce-info">
        <?php esc_html_e('You do not have any active subscription plan.', 'woocommerce'); ?>
    </div>
    <div id="Bandera" style="position:  absolute;margin-top: -110px;right: 0px;width: 200px;"><img src="/wp-content/uploads/2018/05/2.jpg" style="width: 100%;"></div>
<?php } ?>
<?php if ($_REQUEST['change']) { ?>
    <div class="woocommerce-Message woocommerce-Message--info woocommerce-info" style="border-top: 3px solid #70a83b;background: #e8fbd6;">
        <?php
        esc_html_e('Thank You');
        ?><a href="/your-order-of-the-week/?order=<?= $_REQUEST['change']; ?>"><strong> <?php esc_html_e('Click Here'); ?> </strong></a><?php
        if (isset($_REQUEST['yes'])) {
            esc_html_e('To View Your Meals Selected.', 'woocommerce');
        } else {
            esc_html_e('To View Your Meals Selected.', 'woocommerce');
        }
        ?>
    </div>
<?php } ?>

<div class="row"> <!--style="border-bottom: 1px solid #6666;border-top: 1px solid #6666;"-->
    <?php
      //Comprobar estado de suscripcion
      /*global $wpdb;

      $table_name = $wpdb->prefix . "orders_in";
      */

    if (!isset($MealSelect)) {

        ?>
        <div class="col-sm-6">
            <?php echo do_shortcode('[shortcode-suscription-plans]'); ?>
        </div>
        <div class="col-sm-6">
            <!--ul>
            <?php /* foreach (wc_get_account_menu_items() as $endpoint => $label) : ?>
              <?php
              if ($down && $endpoint === 'downloads' && $down ||
              !$down && $endpoint !== 'downloads' ||
              $down && $endpoint !== 'downloads') {
              ?>
              <li class="<?php echo wc_get_account_menu_item_classes($endpoint); ?>" data-data="<?php echo esc_html($label); ?>">
              <a href="<?php echo esc_url(wc_get_account_endpoint_url($endpoint)); ?>"><?php echo ( $label ); ?></a>
              </li>
              <?php } ?>
              <?php endforeach; */ ?>
            </ul-->

            <!--iframe id="green-see" name="greensee" style="min-height: 430px;width: 100%;" scrolling="no" name="frame" frameborder="0" src="https://ideal.reallibertychange.com/product/suscription-plans/"></iframe-->

            <?php echo do_shortcode('[shortcode-pay-as-go-you-plans]'); ?>
            <!--iframe id="green-red" style="min-height: 430px;width: 100%;" scrolling="no" name="frame" frameborder="0" src="https://ideal.reallibertychange.com/product/pay-as-go-you-plan/"></iframe-->
        </div>
        <?php
    } else {
        ?><div class="col-sm-12">
            <img src="/wp-content/uploads/2018/09/fondo.png" style="width: 100%;">
            <?php
            if (!$MealSelect) {
                ?>
                <div style="<?php echo ($DB[0]->subscription_state == 1) ? 'display:inline-block;' : "display:none;" ?> position: absolute;width: 200px;background-color: #01ea03;color: #fff;font-size: 2em;font-weight: bold;right: 10%;bottom: 20%;">
                    <a class="btn btn-danger btn-xs" href="<?php echo "/select-your-meals/?order=$order_id&plan"; ?>" style="margin-top: -8px;background-color: #fd440b;width: 100%;font-size: 1em;" >
                        <?php esc_html_e('Click here', 'woocommerce') ?>
                    </a>
                    <div style="margin-top: -10px;padding: 0px 20px;white-space: nowrap;">to see meals</div>
                </div>

                <?php
            }
            ?>
        </div><?php
    }
    ?>
    <?php ?>
</div>

<?php
/**
 * My Account dashboard.
 *
 * @since 2.6.0
 */
do_action('woocommerce_account_dashboard');

/**
 * Deprecated woocommerce_before_my_account action.
 *
 * @deprecated 2.6.0
 */
do_action('woocommerce_before_my_account');

/**
 * Deprecated woocommerce_after_my_account action.
 *
 * @deprecated 2.6.0
 */
do_action('woocommerce_after_my_account');

/* Omit closing PHP tag at the end of PHP files to avoid "headers already sent" issues. */
