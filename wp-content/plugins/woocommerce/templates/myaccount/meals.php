<?php
/**
 * Downloads
 *
 * Shows downloads on the account page.
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/downloads.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 3.2.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

$downloads     = WC()->customer->get_downloadable_products();
$has_downloads = (bool) $downloads;
?><h1>Plan Suscription</h1><?php
$User = get_currentuserinfo();
$subscriptions=wcs_get_users_subscriptions();
if($subscriptions){
    ?>
    <table cellspacing="0" cellpadding="0"  style="position:relative;width: 100%; margin: 0 auto;">
        <thead>
            <tr>
                <th class="woocommerce-orders-table__header woocommerce-orders-table__header-order-number">
                    #Orden
                </th>
                <th class="woocommerce-orders-table__header woocommerce-orders-table__header-order-status">
                    Plan
                </th>
                <th class="woocommerce-orders-table__header woocommerce-orders-table__header-order-status">
                    Selected Meals
                </th>
                <th class="woocommerce-orders-table__header woocommerce-orders-table__header-order-actions">
                    Action
                </th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($subscriptions as $subscription) {
				if(	esc_attr(wcs_get_subscription_status_name($subscription->get_status()))=='Active'){ ?>
				    <tr>
				        <?php $order_id = $subscription->get_data()['parent_id']; ?>
                <td>
                    <a href="<?='/my-account/view-order/'.$order_id;?>"><?=$order_id;?></a>
                </td><?php
                $order = wc_get_order($order_id);
                $url ='';
				$pos = '';
					foreach ($subscription->get_items() as $line_item) {
					    ?>
					    <td>
                    <?php
						echo $line_item['name'];
	if('Subscription Plans - 12 meals per week'===$line_item['name']){
		$pos = '12/';
	}
	if('Subscription Plans - 14 meals per week'===$line_item['name']){
		$pos = '14/';
	}
	if('Subscription Plans - 16 meals per week'===$line_item['name']){
		$pos = '16/';
	}
	if('Pay As Go You Plans - 12 meals per week'===$line_item['name']){
		$pos = '12-ii/';
	}
	if('Pay As Go You Plans - 14 meals per week'===$line_item['name']){
		$pos = '14-ii/';
	}
	if('Pay As Go You Plans - 16 meals per week'===$line_item['name']){
		$pos = '16-ii/';
	}

							?>
                </td><?php
						$res = get_post_meta(intval($line_item['product_id']));
						$res=(unserialize($res['_product_attributes'][0]));
					    if(is_array($res)){
							if(array_key_exists('see_meals', $res)){
								$url=$res['see_meals']['value'];
							}
						}
					}

$product_name=[];
foreach ($order->get_items() as $item_key => $item_values){
    $item_data = $item_values->get_data();
    $product_name[] = $item_data['name'];
}
?>
<td style="text-align:  center;">
                    <?php if(count($product_name)>1){
                    echo 'YES';
                    }else{ echo 'NO'; };?>
                </td>
                <td style="text-align:  right;">
                    <?php if(count($product_name)>1){
                    ?>-<?php
                    }else{ ?><a class="woocommerce-button button view" href="<?=$url.$pos;?>">SEE MEALS</a><?php } ;?>
                </td>
                </tr><?php

				} ?>

		    <?php } ?>
		    </tbody>
    </table>
    <?php

}else{?>
	<div class="woocommerce-Message woocommerce-Message--info woocommerce-info">
		<a class="woocommerce-Button button" href="<?php echo esc_url( apply_filters( 'woocommerce_return_to_shop_redirect', wc_get_page_permalink( 'shop' ) ) ); ?>">
			<?php esc_html_e( 'Go shop', 'woocommerce' ) ?>
		</a>
		<?php esc_html_e( 'You do not have any active subscription plan.', 'woocommerce' ); ?>
	</div>
<?php } ?>

<?php do_action( 'woocommerce_after_account_downloads', $has_downloads ); ?>
