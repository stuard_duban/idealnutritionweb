<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

if ( ! class_exists( 'WC_Email_Notify_Changes', false ) ) :

/**
 * Notify Changues to Admins.
 *
 * An email sent to the customer via admin.
 *
 * @class       WC_Email_Notify_Changes
 * @version     2.3.0
 * @package     WooCommerce/Classes/Emails
 * @author      WooThemes
 * @extends     WC_Email
 * 
 * Desarrollo EME - Luis Cortés
 * 
 */
class WC_Email_Notify_Changes extends WC_Email {

	/**
	 * Constructor.
	 */
	public function __construct() {
		$this->id             = 'notify_changes';
// 		$this->customer_email = true;
		$this->title          = __( 'Notify changes / Order details', 'woocommerce' );
		$this->description    = __( 'Notification is sent to the administrators with the detail of the order.', 'woocommerce' );
		$this->template_html  = 'emails/notify-changes.php';
		$this->template_plain = 'emails/plain/notify-changes.php';
		$this->placeholders   = array(
			'{site_title}'   => $this->get_blogname(),
			'{order_date}'   => '',
			'{order_number}' => '',
		);

		// Call parent constructor
		parent::__construct();

// 		$this->manual = true;
        $this->recipient = $this->get_option( 'recipient', get_option( 'admin_email' ) );
	}

	/**
	 * Get email subject.
	 *
	 * @since  3.1.0
	 * @return string
	 */
	public function get_default_subject() {
		return __( 'Changes for order {order_number}', 'woocommerce' );
	}

	/**
	 * Get email heading.
	 *
	 * @since  3.1.0
	 * @return string
	 */
	public function get_default_heading() {
		return __( 'Changes for order {order_number}', 'woocommerce' );
	}

	/**
	 * Get email subject.
	 *
	 * @access public
	 * @return string
	 */
// 	public function get_subject() {
// 		if ( $this->object->has_status( array( 'completed', 'processing' ) ) ) {
// 			$subject = $this->get_option( 'subject_paid', $this->get_default_subject( true ) );
// 			$action  = 'woocommerce_email_subject_customer_invoice_paid';
// 		} else {
// 			$subject = $this->get_option( 'subject', $this->get_default_subject() );
// 			$action  = 'woocommerce_email_subject_customer_invoice';
// 		}
// 		return apply_filters( $action, $this->format_string( $subject ), $this->object );
// 	}

	/**
	 * Get email heading.
	 *
	 * @access public
	 * @return string
	 */
// 	public function get_heading() {
// 		if ( $this->object->has_status( wc_get_is_paid_statuses() ) ) {
// 			$heading = $this->get_option( 'heading_paid', $this->get_default_heading( true ) );
// 			$action  = 'woocommerce_email_heading_customer_invoice_paid';
// 		} else {
// 			$heading = $this->get_option( 'heading', $this->get_default_heading() );
// 			$action  = 'woocommerce_email_heading_customer_invoice';
// 		}
// 		return apply_filters( $action, $this->format_string( $heading ), $this->object );
// 	}

	/**
	 * Trigger the sending of this email.
	 *
	 * @param int $order_id The order ID.
	 * @param WC_Order $order Order object.
	 */
	public function trigger( $order_id, $order = false ) {
		$this->setup_locale();

		if ( $order_id && ! is_a( $order, 'WC_Order' ) ) {
			$order = wc_get_order( $order_id );
		}

		if ( is_a( $order, 'WC_Order' ) ) {
			$this->object                         = $order;
// 			$this->recipient                      = $this->object->get_billing_email();
			$this->placeholders['{order_date}']   = wc_format_datetime( $this->object->get_date_created() );
			$this->placeholders['{order_number}'] = $this->object->get_order_number();
		}

		if ( $this->is_enabled() && $this->get_recipient() ) {
			$this->send( $this->get_recipient(), $this->get_subject(), $this->get_content(), $this->get_headers(), $this->get_attachments() );
		}

		$this->restore_locale();
	}

	/**
	 * Get content html.
	 *
	 * @access public
	 * @return string
	 */
	public function get_content_html() {
		return wc_get_template_html( $this->template_html, array(
			'order'         => $this->object,
			'email_heading' => $this->get_heading(),
			'sent_to_admin' => true,
			'plain_text'    => false,
			'email'			=> $this,
		) );
	}

	/**
	 * Get content plain.
	 *
	 * @access public
	 * @return string
	 */
	public function get_content_plain() {
		return wc_get_template_html( $this->template_plain, array(
			'order'         => $this->object,
			'email_heading' => $this->get_heading(),
			'sent_to_admin' => true,
			'plain_text'    => true,
			'email'			=> $this,
		) );
	}

	/**
	 * Initialise settings form fields.
	 */
	public function init_form_fields() {
		$this->form_fields = array(
			'enabled' => array(
				'title'         => __( 'Enable/Disable', 'woocommerce' ),
				'type'          => 'checkbox',
				'label'         => __( 'Enable this email notification', 'woocommerce' ),
				'default'       => 'yes',
			),
			'recipient' => array(
				'title'         => __( 'Recipient(s)', 'woocommerce' ),
				'type'          => 'text',
				'description'   => sprintf( __( 'Enter recipients (comma separated) for this email. Defaults to %s.', 'woocommerce' ), '<code>' . esc_attr( get_option( 'admin_email' ) ) . '</code>' ),
				'placeholder'   => '',
				'default'       => '',
				'desc_tip'      => true,
			),
			'subject' => array(
				'title'         => __( 'Subject', 'woocommerce' ),
				'type'          => 'text',
				'desc_tip'      => true,
				/* translators: %s: list of placeholders */
				'description'   => sprintf( __( 'Available placeholders: %s', 'woocommerce' ), '<code>{site_title}, {order_date}, {order_number}</code>' ),
				'placeholder'   => $this->get_default_subject(),
				'default'       => '',
			),
			'heading' => array(
				'title'         => __( 'Email heading', 'woocommerce' ),
				'type'          => 'text',
				'desc_tip'      => true,
				/* translators: %s: list of placeholders */
				'description'   => sprintf( __( 'Available placeholders: %s', 'woocommerce' ), '<code>{site_title}, {order_date}, {order_number}</code>' ),
				'placeholder'   => $this->get_default_heading(),
				'default'       => '',
			),
			'email_type' => array(
				'title'         => __( 'Email type', 'woocommerce' ),
				'type'          => 'select',
				'description'   => __( 'Choose which format of email to send.', 'woocommerce' ),
				'default'       => 'html',
				'class'         => 'email_type wc-enhanced-select',
				'options'       => $this->get_email_type_options(),
				'desc_tip'      => true,
			),
		);
	}
}

endif;

return new WC_Email_Notify_Changes();
