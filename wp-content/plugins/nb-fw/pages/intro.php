<?php
/*
 * Admin Intro section
 */
$active_tab = isset($_GET['tab']) ? $_GET['tab'] : 'page=foody-panel';
$url = admin_url() . 'admin.php?page=foody-panel';
?>
    <h1><?php esc_html_e('Foody', 'core-wp'); ?> <span><?php echo NBT_VER; ?></span></h1>
    <div class="intro-wrap">
        <p class="intro-text">
            <?php esc_html_e('Thank you for choosing our Foody theme. Those pages below will help you setup our theme in your site.', 'core-wp'); ?>
        </p>
    </div>
    <h2 class="nav-tab-wrapper">
<!--        <a href="--><?php //echo esc_url($url); ?><!--"-->
<!--           class="nav-tab --><?php //if ($_GET['page'] == 'foody-panel') echo 'nav-tab-active'; ?><!--">-->
<!--            --><?php //esc_html_e('Plugins', 'core-wp'); ?>
<!--        </a>-->
        <a href="<?php echo admin_url('themes.php?page=pt-one-click-demo-import'); ?>" class="nav-tab">
            <?php esc_html_e('Sample data', 'core-wp'); ?>
        </a>
        <a href="<?php admin_url('customize.php'); ?>" class="nav-tab">
            <?php esc_html_e('Theme options', 'core-wp'); ?>
        </a>
<!--        <a href=""-->
<!--           class="nav-tab --><?php //if ($_GET['page'] == 'flatsome-panel-support') echo 'nav-tab-active'; ?><!--">-->
<!--            --><?php //esc_html_e('Support & Guide', 'core-wp'); ?>
<!--        </a>-->
    </h2>
<?php

