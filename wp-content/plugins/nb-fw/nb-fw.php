<?php
/*
Plugin Name: Netbase Framework
Plugin URI: http://netbaseteam.com/
Description: Portfolio Plugin for NBCORE theme.
Version: 1.0.0
Author: NBTeam
Author URI: http://themeforest.net/user/netbaseteam?ref=pencidesign
*/

define('NB_FW_PATH', plugin_dir_path(__FILE__));
define('NB_FW_NAME', plugin_basename(__FILE__));

require_once(NB_FW_PATH . 'metaboxes/metaboxes.php');
require_once(NB_FW_PATH . 'widgets/widget.php');
require_once(NB_FW_PATH . 'inc/custom-template-tags.php');
NBT_Widget::init();
class NBT_Panel
{

    protected static $initialized = false;

    public static function init()
    {
        if (self::$initialized) {
            return;
        }

//        add_action('admin_enqueue_scripts', array(__CLASS__, 'enqueue_scripts'));
        // add_action('admin_menu', array(__CLASS__, 'register_menus'));
        add_filter( 'pt-ocdi/import_files', array(__CLASS__, 'ocdi_import_files') );
        add_action( 'pt-ocdi/after_import', array(__CLASS__, 'after_import') );
    }

    public static function enqueue_scripts()
    {
        global $pagenow;

        if ($pagenow == 'admin.php') {
            wp_enqueue_style('nb-admin-panel', NB_FW_PATH_uri() . '/assets/netbase/css/admin/admin-panel.min.css');
//            wp_enqueue_script('nb-admin-panel', NB_FW_PATH_uri(). '/assets/netbase/js/admin/admin.min.js', array('jquery'), NBT_VER, true);
//            wp_enqueue_script('nb-admin-panel', NB_FW_PATH_uri(). '/assets/src/js/admin/admin.js', array('jquery'), NBT_VER, true);
        }
    }

    public static function register_menus()
    {
        add_menu_page(
            esc_html__('Foody', 'core-wp'),
            esc_html__('Foody', 'core-wp'),
            'manage_options', 'foody-panel',
            array(__CLASS__, 'welcome_page'),
            'dashicons-welcome-widgets-menus',
            2
        );

//        add_submenu_page(
//            'foody-panel',
//            'Plugins',
//            'Plugins',
//            'manage_options',
//            'foody-panel-plugins',
//            array(__CLASS__, 'foody_panel_plugins')
//        );

        add_submenu_page(
            'foody-panel',
            '',
            'Sample Data',
            'manage_options',
            'themes.php?page=pt-one-click-demo-import'
        );

        add_submenu_page(
            'foody-panel',
            '',
            'Theme options',
            'manage_options',
            'customize.php'
        );

//        add_submenu_page(
//            'foody-panel',
//            'Support & Guide',
//            'Support & Guide',
//            'manage_options',
//            'foody-panel-support',
//            array(__CLASS__, 'foody_panel_support')
//        );
    }

    public static function welcome_page()
    {
        $plugins = NBT_Core::$plugins;
        ?>
        <div class="nb-panel">
            <div class="wrap nb-wrap">
                <?php require_once(NB_FW_PATH . 'pages/intro.php'); ?>
                <div class="welcome-panel">
                    <p class="intro-text"><?php esc_html_e('First you need to install some of our theme\'s plugin. Some plugins will be required and some will not, but we recommend you to install all of our plugins to make Foody theme run smoothly', 'core-wp') ?></p>
                    <a href="<?php admin_url('themes.php?page=tgmpa-install-plugins'); ?>"
                       class="button button-primary button-hero">
                        <?php esc_html_e('Install Plugins', 'core-wp'); ?>
                    </a>
                </div>

                <div id="nb-plugins" class="tab-pane" role="tabpanel">
                    <?php foreach ($plugins as $plugin): ?>
                        <?php $plugin['thumb']; ?>
                        <div class="item-wrap">
                            <div class="item">
                                <div class="plugin-screenshot">
                                    <img src="<?php echo esc_attr($plugin['thumb']) ?>" alt="">
                                </div>
                                <div class="plugin-meta">
                                    <h3 class="plugin-name"><?php echo esc_html($plugin['name']) ?></h3>
                                    <span class="plugin-version"><?php echo esc_html($plugin['version']) ?></span>
                                </div>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
        <?php
    }

    public static function foody_panel_support()
    {
        ?>
        <div class="nb-panel">
            <div class="wrap nb-wrap">
                <?php require_once(NB_FW_PATH . '/netbase-core/panel/pages/intro.php'); ?>
                <?php require_once(NB_FW_PATH . '/netbase-core/panel/pages/support.php'); ?>
            </div>
        </div>
        <?php
    }

    public static function foody_panel_plugins()
    {
        $plugins = NBT_Core::$plugins;
//        var_dump($plugins);die;
        ?>
        <div class="nb-panel">
            <div class="wrap nb-wrap">
                <?php require_once(NB_FW_PATH . '/netbase-core/panel/pages/intro.php'); ?>
                <div id="nb-plugins" class="tab-pane" role="tabpanel">
                    <?php foreach ($plugins as $plugin): ?>
                        <?php $plugin['thumb']; ?>
                        <div class="item">
                            <div class="plugin-screenshot">
                                <img src="<?php echo esc_attr($plugin['thumb']) ?>" alt="">
                            </div>
                            <div class="plugin-meta">
                                <h3><?php echo esc_html($plugin['name']) ?></h3>
                                <a class="plugin-action button button-primary">install</a>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
        <?php
    }

    public static function ocdi_import_files() {
        return array(
            array(
                'import_file_name'           => 'Foodlife Pizza',
                'local_import_file'            => NB_FW_PATH . 'ocdi/pizza/content.xml',
                'local_import_widget_file'     => NB_FW_PATH . 'ocdi/pizza/widgets.wie',
                'local_import_customizer_file' => NB_FW_PATH . 'ocdi/pizza/customizer.dat',
                'import_preview_image_url'     => 'http://netbaseteam.com/wordpress/theme/import_preview_img/pizza.png',
            ),
            array(
                'import_file_name'           => 'Foodlife Jruits',
                'local_import_file'            => NB_FW_PATH . 'ocdi/jruits/content.xml',
                'local_import_widget_file'     => NB_FW_PATH . 'ocdi/jruits/widgets.wie',
                'local_import_customizer_file' => NB_FW_PATH . 'ocdi/jruits/customizer.dat',
                'import_preview_image_url'     => 'http://netbaseteam.com/wordpress/theme/import_preview_img/jruits.png',
            ),
            array(
                'import_file_name'           => 'Foodlife Bakery',
                'local_import_file'            => NB_FW_PATH . 'ocdi/bakery/content.xml',
                'local_import_widget_file'     => NB_FW_PATH . 'ocdi/bakery/widgets.wie',
                'local_import_customizer_file' => NB_FW_PATH . 'ocdi/bakery/customizer.dat',
                'import_preview_image_url'     => 'http://netbaseteam.com/wordpress/theme/import_preview_img/bakery.png',
            ),
        );
    }

    public static function after_import( $selected_import ) {

        if ( 'Foodlife Pizza' === $selected_import['import_file_name'] ) {
            $page = get_page_by_title( 'Home');
            if ( isset( $page->ID ) ) {
                update_option( 'page_on_front', $page->ID );
                update_option( 'show_on_front', 'page' );
            }
            $primary_menu = get_term_by('name', 'Main Menu', 'nav_menu');
            $sub_menu = get_term_by('name', 'Top Menu', 'nav_menu');
            set_theme_mod( 'nav_menu_locations', array(
                    'primary' => $primary_menu->term_id,
                    'header-sub' => $sub_menu->term_id,
                )
            );

            if ( class_exists( 'RevSlider' ) ) {
                $slider_array = array(
                    NB_FW_PATH .'ocdi/pizza/pizza-1.zip'
                );

                $slider = new RevSlider();

                foreach($slider_array as $filepath){
                    $slider->importSliderFromPost(true,true,$filepath);
                }

                echo 'Slider processed';
            }
        }

        if ( 'Foodlife Jruits' === $selected_import['import_file_name'] ) {
            $page = get_page_by_title( 'Home-page');
            if ( isset( $page->ID ) ) {
                update_option( 'page_on_front', $page->ID );
                update_option( 'show_on_front', 'page' );
            }
            $primary_menu = get_term_by('name', 'Home-menu', 'nav_menu');
            set_theme_mod( 'nav_menu_locations', array(
                    'primary' => $primary_menu->term_id,
                )
            );

            if ( class_exists( 'RevSlider' ) ) {
                $slider_array = array(
                    NB_FW_PATH .'ocdi/jruits/slider-fruits.zip'
                );

                $slider = new RevSlider();

                foreach($slider_array as $filepath){
                    $slider->importSliderFromPost(true,true,$filepath);
                }

                echo 'Slider processed';
            }
        }

        if ( 'Foodlife Bakery' === $selected_import['import_file_name'] ) {
            $page = get_page_by_title( 'Home');
            if ( isset( $page->ID ) ) {
                update_option( 'page_on_front', $page->ID );
                update_option( 'show_on_front', 'page' );
            }
            $primary_menu = get_term_by('name', 'Main Menu', 'nav_menu');
            set_theme_mod( 'nav_menu_locations', array(
                    'primary' => $primary_menu->term_id,
                )
            );

            if ( class_exists( 'RevSlider' ) ) {
                $slider_array = array(
                    NB_FW_PATH .'ocdi/bakery/bakery-1.zip'
                );

                $slider = new RevSlider();

                foreach($slider_array as $filepath){
                    $slider->importSliderFromPost(true,true,$filepath);
                }

                echo 'Slider processed';
            }
        }





    }
}
NBT_Panel::init();
