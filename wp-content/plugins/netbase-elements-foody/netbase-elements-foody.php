<?php

/*
 * Plugin Name: Netbase Element Foody
 * Description: Element For WP Theme Foody
 * Author: Netbaseteam
 * Version: 1.0.0
 */

if (!defined('ABSPATH')) {
    die(); // not follow direct
}
// Check vc active or no active
if (class_exists("WPBakeryShortCode")) {

    // Begin Element Testimonial
    class netbase_element_testimonial extends WPBakeryShortCode {

        // Element Init
        function __construct() {
            add_action('init', array($this, 'netbase_element_testimonial_map'));
            add_shortcode('netbase_element_testimonial', array($this, 'netbase_element_testimonial_html'));

            add_action('wp_enqueue_scripts', 'load_style_element');

            function load_style_element() {
                wp_enqueue_style('netbase_element_testimonial_css', plugins_url() . '/netbase-elements-foody/assets/css/css-fontend.css');
            }

            function shortcodeScripts() {
                wp_register_script('vc_nb_owl-carousel', vc_asset_url('lib/owl-carousel2-dist/owl.carousel.min.js'), array(
                    'jquery',
                        ), WPB_VC_VERSION, true);
                wp_register_style('vc_nb_owl-carousel-css', vc_asset_url('lib/owl-carousel2-dist/assets/owl.min.css'), array(), WPB_VC_VERSION);
                wp_enqueue_script('vc_nb_owl-carousel');
                wp_enqueue_style('vc_nb_owl-carousel-css');
            }

            add_action('wp_enqueue_scripts', 'shortcodeScripts');
        }

        // Element Mapping
        public function netbase_element_testimonial_map() {

            vc_map(
                    array(
                        'name' => __('Netbase Testimonials', 'netbase_element_testimonial'),
                        'base' => 'netbase_element_testimonial',
                        'category' => __('Netbase Element Foody', 'netbase_element_testimonial'),
                        'description' => __('Testimonial Element Foody Netbase', 'netbase_element_testimonial'),
                        'icon' => plugins_url() . '/netbase-elements-foody/assets/images/testimonial.png',
                        'params' => array(
                            array(
                                'type' => 'param_group',
                                'value' => __('Value', 'netbase_element_testimonial'),
                                'param_name' => 'data',
                                'group' => 'Custom Group',
                                'params' => array(
                                    array(
                                        'type' => 'attach_image',
                                        'holder' => 'img',
                                        'class' => 'images_test',
                                        'heading' => __('Upload Avatar', 'netbase_element_testimonial'),
                                        'param_name' => 'nb_image_test',
                                        'description' => __('Follow upload image', 'netbase_element_testimonial'),
                                        'admin_label' => false,
                                        'group' => 'Custom Group',
                                    ),
                                    array(
                                        'type' => 'textfield',
                                        'holder' => 'h3',
                                        'class' => 'names_test',
                                        'heading' => __('Name', 'netbase_element_testimonial'),
                                        'param_name' => 'nb_name_test',
                                        'value' => __('Default Name', 'netbase_element_testimonial'),
                                        'description' => __('Add name yoursefl', 'netbase_element_testimonial'),
                                        'admin_label' => false,
                                        'group' => 'Custom Group',
                                    ),
                                    array(
                                        'type' => 'textfield',
                                        'holder' => 'p',
                                        'class' => 'positions_test',
                                        'heading' => __('Position', 'netbase_element_testimonial'),
                                        'param_name' => 'nb_position_test',
                                        'value' => __('Default Position', 'netbase_element_testimonial'),
                                        'group' => 'Custom Group',
                                        'description' => __('Add position yoursefl', 'netbase_element_testimonial'),
                                        'admin_label' => false
                                    ),
                                    array(
                                        'type' => 'textfield',
                                        'holder' => 'div',
                                        'class' => 'contents_test',
                                        'heading' => __('Content', 'netbase_element_testimonial'),
                                        'param_name' => 'nb_content_test',
                                        'value' => __('Default Content', 'netbase_element_testimonial'),
                                        'group' => 'Custom Group',
                                        'description' => __('Add contents', 'netbase_element_testimonial'),
                                        'admin_label' => false
                                    ),
                                )
                            ),
                            array(
                                'type' => 'dropdown',
                                'holder' => 'div',
                                'class' => 'style_nav_test',
                                'heading' => __('Show/Hide Button Navs', 'netbase_element_testimonial'),
                                'param_name' => 'nb_nav_show_hide',
                                'value' => array(
                                    __('Select Styles', 'netbase_element_testimonial') => '',
                                    __('Show', 'netbase_element_testimonial') => 'true',
                                    __('Hide', 'netbase_element_testimonial') => 'false',
                                ),
                                'group' => 'Slide Setting',
                                'description' => __('Show hide button next, previourt slide', 'netbase_element_testimonial'),
                                'admin_label' => false
                            ),
                            array(
                                'type' => 'dropdown',
                                'holder' => 'div',
                                'class' => 'style_dost_test',
                                'heading' => __('Show/Hide Button Doted', 'netbase_element_testimonial'),
                                'param_name' => 'nb_dosts_show_hide',
                                'value' => array(
                                    __('Select Styles', 'netbase_element_testimonial') => '',
                                    __('Show', 'netbase_element_testimonial') => 'true',
                                    __('Hide', 'netbase_element_testimonial') => 'false',
                                ),
                                'group' => 'Slide Setting',
                                'description' => __('Show hide dosted slide', 'netbase_element_testimonial'),
                                'admin_label' => false
                            ),
                            array(
                                'type' => 'dropdown',
                                'holder' => 'div',
                                'class' => 'style_auto_test',
                                'heading' => __('Set Auto', 'netbase_element_testimonial'),
                                'param_name' => 'nb_auto_slide',
                                'value' => array(
                                    __('Select Styles', 'netbase_element_testimonial') => '',
                                    __('Auto', 'netbase_element_testimonial') => 'true',
                                    __('No Auto', 'netbase_element_testimonial') => 'false',
                                ),
                                'group' => 'Slide Setting',
                                'description' => __('Set slide run auto or not auto', 'netbase_element_testimonial'),
                                'admin_label' => false
                            ),
                            array(
                                'type' => 'textfield',
                                'holder' => 'p',
                                'class' => 'nb_time_test',
                                'heading' => __('Set Time', 'netbase_element_testimonial'),
                                'param_name' => 'nb_time_run_test',
                                'value' => __('', 'netbase_element_testimonial'),
                                'description' => __('Set time run slide, time set is seconds', 'netbase_element_testimonial'),
                                'admin_label' => false,
                                'group' => 'Slide Setting',
                            ),
                            array(
                                'type' => 'css_editor',
                                'heading' => __('Css', 'netbase_element_testimonial'),
                                'param_name' => 'css',
                                'group' => __('Design options', 'netbase_element_testimonial'),
                            ),
                        )
                    )
            );
        }

        // Element HTML
        public function netbase_element_testimonial_html($atts) {

            // Params extraction
            extract(
                    shortcode_atts(
                            array(
                'nb_nav_show_hide' => '',
                'nb_dosts_show_hide' => '',
                'nb_auto_slide' => '',
                'nb_time_run_test' => '',
                'css' => '',
                            ), $atts
                    )
            );

            // Fill $html var with data
            $data = vc_param_group_parse_atts($atts['data']);

            $css_class = 'nb_testimonial';
            $css_class = apply_filters(VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, $css_class . vc_shortcode_custom_css_class($css, ' '), 'netbase_element_testimonial', $atts);
            $css_class = trim(preg_replace('/\s+/', ' ', $css_class));
            $html = '<div class="' . $css_class . ' owl-carousel box_data_testimonial">';
            foreach ($data as $key => $value) {
                $nb_content_test[$key] = $value['nb_content_test'];
                $nb_name_test[$key] = $value['nb_name_test'];
                $nb_position_test[$key] = $value['nb_position_test'];
                $getimg[$key] = wp_get_attachment_image_src($value['nb_image_test'], "large");
                $img[$key] = $getimg[$key][0];
                $html .= '<div>
                            <div class="box_data_testimonial_content">' . $nb_content_test[$key] . '</div>
                            <div class="box_data_testimonial_imgdata">
                                <div class="box_data_testimonial_img"><img src="' . $img[$key] . '" class="img_test" alt="' . $img[$key] . '"/></div>
                                <div class="box_data_testimonial_data">
                                    <p class="box_data_testimonial_name">' . $nb_name_test[$key] . '</p>
                                    <p class="box_data_testimonial_positon">' . $nb_position_test[$key] . '</p>
                                </div>
                            </div>';
                $html .= '</div>';
            }
            $html .= '</div><script type="text/javascript">
                    jQuery(document).ready(function () {
                        jQuery(".owl-carousel").vcOwlCarousel({
                            loop: true,
                            autoplay: ' . $nb_auto_slide . ',
                            slideSpeed: ' . $nb_time_run_test . ',
                            nav: ' . $nb_nav_show_hide . ',
                            navText: ["",""],
                            dots: ' . $nb_dosts_show_hide . ',
                            items: 1,
                        });
                    });</script>';

            return $html;
        }

    }

    // End Element Testimonial
    new netbase_element_testimonial();

    // Begin Element Blog
    class netbase_element_blog extends WPBakeryShortCode {

        // Element Init
        function __construct() {
            add_action('init', array($this, 'netbase_element_blog_map'));
            add_shortcode('netbase_element_blog', array($this, 'netbase_element_blog_html'));

            add_action('wp_enqueue_scripts', 'load_style_element_blogpost');

            function load_style_element_blogpost() {
                wp_enqueue_style('netbase_element_blogpost_css', plugins_url() . '/netbase-elements-foody/assets/css/css-fontend.css');
            }

            function timeago() {
                return human_time_diff(get_the_time('U'), current_time('timestamp')) . ' ago';
            }

        }

        // Element Mapping
        public function netbase_element_blog_map() {

            vc_map(
                    array(
                        'name' => __('Netbase Recent Blog Posts', 'netbase_element_blog'),
                        'base' => 'netbase_element_blog',
                        'category' => __('Netbase Element Foody', 'netbase_element_blog'),
                        'description' => __('Blogs Posts Element Foody Netbase', 'netbase_element_blog'),
                        'icon' => plugins_url() . '/netbase-elements-foody/assets/images/news.png',
                        'params' => array(
                            array(
                                'type' => 'textfield',
                                'holder' => 'p',
                                'class' => 'quantity_blog',
                                'heading' => __('Posts Number', 'netbase_element_blog'),
                                'param_name' => 'nb_number_post_blog',
                                'value' => __('', 'netbase_element_blog'),
                                'group' => 'Custom Group',
                                'description' => __('Number posts display', 'netbase_element_blog'),
                                'admin_label' => false
                            ),
                            array(
                                'type' => 'dropdown',
                                'holder' => 'div',
                                'class' => 'style_blog',
                                'heading' => __('Select Styles', 'netbase_element_blog'),
                                'param_name' => 'nb_style_post_blog',
                                'value' => array(
                                    __('Select Styles', 'netbase_element_blog') => '',
                                    __('Styles 2', 'netbase_element_blog') => 'nb_post_type2',
                                ),
                                'group' => 'Custom Group',
                                'description' => __('Style display posts', 'netbase_element_blog'),
                                'admin_label' => false
                            ),
                            array(
                                'type' => 'textfield',
                                'holder' => 'p',
                                'class' => 'quantity_words_blog',
                                'heading' => __('Words Limit', 'netbase_element_blog'),
                                'param_name' => 'nb_number_words_blog',
                                'value' => __('', 'netbase_element_blog'),
                                'group' => 'Data Setting',
                                'description' => __('Number words display', 'netbase_element_blog'),
                                'admin_label' => false
                            ),
                            array(
                                'type' => 'dropdown',
                                'holder' => 'div',
                                'class' => 'orderby_posts_blog',
                                'heading' => __('Order by', 'netbase_element_blog'),
                                'param_name' => 'nb_orderby_posts_blog',
                                'value' => array(
                                    __('Select Order', 'netbase_element_blog') => '',
                                    __('Posts ID', 'netbase_element_blog') => 'ID',
                                    __('Posts Name', 'netbase_element_blog') => 'title',
                                    __('Author', 'netbase_element_blog') => 'author',
                                    __('Date', 'netbase_element_blog') => 'date',
                                ),
                                'group' => 'Data Setting',
                                'description' => __('Style order', 'netbase_element_blog'),
                                'admin_label' => false
                            ),
                            array(
                                'type' => 'dropdown',
                                'holder' => 'p',
                                'class' => 'sorted_posts_blog',
                                'heading' => __('Sort Order', 'netbase_element_blog'),
                                'param_name' => 'nb_sort_order_blog',
                                'value' => array(
                                    __('Select Sort', 'netbase_element_blog') => '',
                                    __('Descending', 'netbase_element_blog') => 'DESC',
                                    __('Ascending', 'netbase_element_blog') => 'ASC',
                                ),
                                'group' => 'Data Setting',
                                'description' => __('Style sort', 'netbase_element_blog'),
                                'admin_label' => false
                            ),
                            array(
                                'type' => 'css_editor',
                                'heading' => __('Css', 'netbase_element_blog'),
                                'param_name' => 'css',
                                'group' => __('Design options', 'netbase_element_blog'),
                            ),
                        )
                    )
            );
        }

        // Element HTML
        public function netbase_element_blog_html($atts) {
            // Params extraction
            extract(
                    shortcode_atts(
                            array(
                'nb_number_post_blog' => '',
                'nb_style_post_blog' => '',
                'nb_number_words_blog' => '',
                'nb_orderby_posts_blog' => '',
                'nb_sort_order_blog' => '',
                'css' => '',
                            ), $atts
                    )
            );

            $args = array(
                'numberposts' => $nb_number_post_blog,
                //'category' => 0,
                'orderby' => $nb_orderby_posts_blog,
                'order' => $nb_sort_order_blog,
                'post_type' => 'post',
                'post_status' => ' publish',
            );

            $css_class = 'nb_recent_blog';
            $css_class = apply_filters(VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, $css_class . vc_shortcode_custom_css_class($css, ' '), 'netbase_element_blog', $atts);
            $css_class = trim(preg_replace('/\s+/', ' ', $css_class));
            $html = '<div  class="' . $css_class . '">';

            $recent_posts = wp_get_recent_posts($args);
            foreach ($recent_posts as $key => $recent) {
                $html .= '<div class="box_content_netbase_team_' . $nb_style_post_blog . '_' . $key . '">'
                        . '<a class="img_post_netbase_team" href="' . get_permalink($recent["ID"]) . '">' . get_the_post_thumbnail($recent['ID'], 'full') . '</a>
                            <div class="content_post_netbase_team">
                                <div class="info_post_netbase_team">
                                    <span class="author_post_netbase_team">' . get_the_author_meta('display_name', $recent["post_author"]) . '</span>
                                    <span class="clock_post_netbase_team">' . human_time_diff(strtotime($recent['post_date']), current_time('timestamp')) . '</span>
                                    <span class="comment_post_netbase_team">' . $recent['comment_count'] . ' Comments</span>
                                </div>
                                <a class="title_post_netbase_team" href="' . get_permalink($recent["ID"]) . '">' . ( __($recent["post_title"])) . '
                                </a>

                                <p class="except_post_netbase_team">' . wp_trim_words($recent["post_content"], $nb_number_words_blog, ' ...') . '</p>

                                <a class="read_more_post_netbase_team" href="' . get_permalink($recent["ID"]) . '">Read more</a>
                            </div>';
                $html .= '</div>';
            }
            wp_reset_query();

            '</div>';

            return $html;
        }

    }

    // End Element blog
    new netbase_element_blog();
}
?>