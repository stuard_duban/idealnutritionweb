<?php
/*
Plugin Name: Elfsight Testimonials Slider CC
Description: Level up your brand trust with bright testimonials on your website
Plugin URI: https://elfsight.com/testimonials-slider-widget/wordpress/?utm_source=markets&utm_medium=codecanyon&utm_campaign=testimonials-slider&utm_content=plugin-site
Version: 1.1.0
Author: Elfsight
Author URI: https://elfsight.com/?utm_source=markets&utm_medium=codecanyon&utm_campaign=testimonials-slider&utm_content=plugins-list
*/

if (!defined('ABSPATH')) exit;


require_once('core/elfsight-plugin.php');

$elfsight_testimonials_slider_config_path = plugin_dir_path(__FILE__) . 'config.json';
$elfsight_testimonials_slider_config = json_decode(file_get_contents($elfsight_testimonials_slider_config_path), true);

new ElfsightPlugin(
	array(
		'name' => 'Testimonials Slider',
		'description' => 'Level up your brand trust with bright testimonials on your website',
		'slug' => 'elfsight-testimonials-slider',
		'version' => '1.1.0',
		'text_domain' => 'elfsight-testimonials-slider',
		'editor_settings' => $elfsight_testimonials_slider_config['settings'],
		'editor_preferences' => $elfsight_testimonials_slider_config['preferences'],
		'script_url' => plugins_url('assets/elfsight-testimonials-slider.js', __FILE__),

		'plugin_name' => 'Elfsight Testimonials Slider',
		'plugin_file' => __FILE__,
		'plugin_slug' => plugin_basename(__FILE__),

		'vc_icon' => plugins_url('assets/img/vc-icon.png', __FILE__),

		'menu_icon' => plugins_url('assets/img/menu-icon.png', __FILE__),
		'update_url' => 'https://a.elfsight.com/updates/v1/',

		'preview_url' => plugins_url('preview/index.html', __FILE__),
		'observer_url' => plugins_url('preview/testimonials-slider-observer.js', __FILE__),

		'product_url' => 'https://codecanyon.net/item/testimonials-slider-wordpress-testimonials-plugin/21640651?ref=Elfsight',
		'support_url' => 'https://elfsight.ticksy.com/submit/#100012252'
	)
);

?>
