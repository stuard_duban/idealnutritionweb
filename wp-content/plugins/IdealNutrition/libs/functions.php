<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of functions
 *
 * @author JB
 */
/*
error_reporting(E_ALL);
ini_set('display_errors', 1);
*/
function subs_supend() {
    $customer_user = get_post_meta($_REQUEST['post'], '_customer_user', true );

    $E = new zEmail();
    $E->wp_send(get_userdata(intval($customer_user)));
    sleep(1);
}

function clear_cart() {

    if (!isset($_SESSION['EmptyCart'])) {
        global $woocommerce;
        $_SESSION['EmptyCart'] = true;
    }
}

function zUriMyAccount() {
    $m = get_option('woocommerce_myaccount_page_id');
    if ($m) {
        $m = get_permalink($m);
    }
    return $m;
}

function WC_Order_in($arg = '') {
    global $wpdb;
    $table_name = $wpdb->prefix . "orders_in";
    if (is_array($arg)) {
        $where = '';
        $count = 0;
        foreach ($arg as $col => $value) {
            $count++;
            if (!is_array($value)) {
                $where .= " `" . $col . "`='" . $value . "'";
                if ($count < count($arg)) {
                    $where .= " AND";
                }
            } else {
                $args = $value;
                $j = 0;
                foreach ($args as $cols => $vals) {
                    $j++;
                    $where .= " `" . $cols . "`='" . $vals . "'";
                    if ($j < count($args)) {
                        $where .= " AND";
                    }
                }
                if ($count < count($value)) {
                    $where .= " OR";
                }
            }
        }
        $where = ' WHERE ' . $where;
    } else {
        $where = $arg;
    }//var_dump("SELECT * FROM " . $table_name . $where . "  ORDER BY wp_orders_in.first_delivery_date DESC");    exit();
    return $wpdb->get_results("SELECT * FROM " . $table_name . $where);
}

function WC_Order_in_last_item($arg = '') {
    global $wpdb;
    $table_name = $wpdb->prefix . "orders_in";
    if (is_array($arg)) {
        $where = '';
        $count = 0;
        foreach ($arg as $col => $value) {
            $count++;
            if (!is_array($value)) {
                $where .= " `" . $col . "`='" . $value . "'";
                if ($count < count($arg)) {
                    $where .= " AND";
                }
            } else {
                $args = $value;
                $j = 0;
                foreach ($args as $cols => $vals) {
                    $j++;
                    $where .= " `" . $cols . "`='" . $vals . "'";
                    if ($j < count($args)) {
                        $where .= " AND";
                    }
                }
                if ($count < count($value)) {
                    $where .= " OR";
                }
            }
        }
        $where = ' WHERE ' . $where;
    } else {
        $where = $arg;
    }//var_dump("SELECT * FROM " . $table_name . $where);    exit();
    return $wpdb->get_results("SELECT * FROM " . $table_name . $where . "  ORDER BY wp_orders_in.first_delivery_date DESC");
}

function zWhatPlans($Order) {
    global $wpdb;
    $id = 0;
    $variable = 0;
    $_KEY = false;
    $category = false;
    $jOrder = new WC_Order($Order);
    $items = $jOrder->get_items();
    $table_name = $wpdb->prefix . 'termmeta';
    foreach ($items as $key => $product) {
        if ($id === 0 && $variable === 0) {
            $res = get_post_meta($product->get_product_id());
            $attr = (unserialize($res['_product_attributes'][0]));
            if (count($attr) > 0) {
                foreach ($attr as $key => $value) {
                    if ($id === 0 && $variable === 0) {
                        if (strlen(trim($key)) > 8) {
                            $RD = $wpdb->get_results("SELECT * FROM $table_name WHERE meta_key='plans' AND meta_value = '" . $key . "'");
                            if (count($RD) !== 0) {
                                $id = $product->get_product_id();
                                $variable = $product->get_variation_id();
                                $_KEY = $key;
                                $category = $RD[0]->term_id;
                            }
                        }
                    } else {
                        break;
                    }
                }
            }
        } else {
            break;
        }
    }
    if ($id !== 0) {
        $Plans = wc_get_product($variable);
        if (class_exists('WC_Subscriptions_Product') && WC_Subscriptions_Product::is_subscription($id)) {
            return array(
                'id' => $id,
                'variable' => $variable,
                'title' => $Plans->name,
                'price' => $Plans->price,
                'meals' => floatval(str_replace('Subscription Plans - ', '', str_replace(' meals per week', '', $Plans->name)))
            );
        } else {
            return array(
                'id' => $id,
                'variable' => $variable,
                'title' => $Plans->name,
                'price' => $Plans->price,
                'meals' => floatval(str_replace('Pay As Go You Plans - ', '', str_replace(' meals per week', '', $Plans->name)))
            );
        }
    } else {
        return false;
    }
}

function zpage_by_title($title) {
    global $wpdb;
    return basename(get_permalink($wpdb->get_var("SELECT * FROM $wpdb->posts WHERE post_title = '" . $title . "' AND post_type = 'page'")));
}

function zAddressIPs() {
    if (isset($_SERVER["HTTP_CLIENT_IP"])) {
        return $_SERVER["HTTP_CLIENT_IP"];
    } elseif (isset($_SERVER["HTTP_X_FORWARDED_FOR"])) {
        return $_SERVER["HTTP_X_FORWARDED_FOR"];
    } elseif (isset($_SERVER["HTTP_X_FORWARDED"])) {
        return $_SERVER["HTTP_X_FORWARDED"];
    } elseif (isset($_SERVER["HTTP_FORWARDED_FOR"])) {
        return $_SERVER["HTTP_FORWARDED_FOR"];
    } elseif (isset($_SERVER["HTTP_FORWARDED"])) {
        return $_SERVER["HTTP_FORWARDED"];
    } else {
        return $_SERVER["REMOTE_ADDR"];
    }
}

function zRequestMove() {

    switch ($_REQUEST['subscription_action']) {
        case 'addsubscription': addsubscription_request();
            break;
        case 'updatesubscription': updatesubscription_request();
            break;
        case 'zSeeMeals_Proccess': zSeeMeals_Proccess();
            break;
        case 'zCheckOut_Proccess': zCheckOut_Proccess();
            break;
        case 'zEditOut_Proccess': zEditOut_Proccess();
            break;
        case 'UpdateDeliveryDate': UpdateDeliveryDate();
            break;
        case 'zcoupon': $C = new zCuponez();
            $C->save();
            break;
        case 'save_order_limit':
            global $wpdb;
            $Week = (object) $_POST;
            $option = $_POST[$Week->option];
            $wpdb->update(
                $wpdb->prefix .'orders_max', 
                array( 
                    $Week->option => $option
                ), 
                array(
                    "id" => $Week->id
                )
            );
            break;
        default:
            break;
    }
}
/*Funcion para verificar platos de la siguiente semana*/
function SelectMealsbyOrder($id) {

    global $wpdb;

    //date_default_timezone_set('America/Los_Angeles');
    $table_name = $wpdb->prefix . "orders_in";
    $User = wp_get_current_user();
    $ID_User = $User->ID;
    $DB = $wpdb->get_results("SELECT * FROM $table_name WHERE orders=$id AND user=$ID_User ORDER BY first_delivery_date DESC");

    //echo "trae de base de datos antes <pre>" . var_dump($DB);
    if (count($DB) === 0) {
        return false;
    } else {
        $DB = $DB[0];
    }

    $tz = 'America/Bogota';
    $timestamp = time();
    $dt = new DateTime("now", new DateTimeZone($tz)); //first argument "must" be a string
    $dt->setTimestamp($timestamp); //adjust the object to correct timestamp
    //echo $dt->format('d-m-Y');
    //$date = date('d-m-Y'); //echo $date;exit();

    $date = $dt->format('d-m-Y');//echo $date;exit();

    /*echo "trae de base de datos <pre>" . var_dump($DB);
    $date = date('Y-m-d');
    echo "fecha de hoy: " . $date;*/

    $siguienteSabado = date('Y-m-d', strtotime('next Saturday', strtotime(date('Y-m-j', strtotime($date)))));
    //echo "sabado siguiente: " . $siguienteSabado;

    $first_delivery = $DB->first_delivery_date;
    //echo "siguiente sabado" . $siguienteSabado;
    //echo "Primera fecha de entrega: ". $first_delivery;
    if($siguienteSabado < $first_delivery) {
        //echo "ya tienes una orden pendiente";
        return true;
    }
    else{
        //echo "necesitaas pedir una orden pendiente";
        return false;
    }

    exit;
    //echo "Datos obtenidos: bd<pre>" .  var_dump($DB);

}

function Page_Exist($title, $name = false) {
    $post = get_page_by_title($title, 'OBJECT', 'page');
    if (!$post) {
        return false;
    } else {
        if (!$name) {
            return true;
        }
        $post_data = get_page($post->ID);
        return ($post_data->post_name);
    }
}

function _zCheckCart() {
    global $wpdb;
    $r = (WC()->cart->get_cart());
    $product_id = 0;
    $Key = '';
    $Category = 0;
    foreach ($r as $key => $product) {
        if ($product_id === 0) {
            $res = get_post_meta($product['product_id']);
            $attr = (unserialize($res['_product_attributes'][0]));
            $table_name = $wpdb->prefix . 'termmeta';

            foreach ($attr as $key => $att) {
                if (strlen(trim($key)) > 8) {
                    $RD = $wpdb->get_results("SELECT * FROM $table_name WHERE meta_key='plans' AND meta_value = '" . $key . "'");
                    if (count($RD) !== 0) {
                        $product_id = $product['product_id'];
                        $Key = $key;
                        $Category = $RD[0]->term_id;
                    }
                }
            }
        }
    }
    if (class_exists('WC_Subscriptions_Product') && WC_Subscriptions_Product::is_subscription($product_id)) {
        $isSubscriptions = TRUE;
    } else {
        $isSubscriptions = FALSE;
    }
    return strlen(trim($Key)) === 0 ? false : array(
        'product_id' => $product_id,
        'unikey' => $Key,
        'category' => $Category,
        'isSubscriptions' => $isSubscriptions
    );
}
