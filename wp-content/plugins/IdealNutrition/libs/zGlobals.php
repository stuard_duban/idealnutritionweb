<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of global
 *
 * @author Windows
 */
class zGlobals {

    private $require = null;

    public function __construct($arg) {
        
        define('_zPid', uniqid());
        define('_zDid', uniqid());
        define('z_SITE_URL', get_settings('siteurl'));
        define('z_PATH', $arg[1]);
        define('z_PLUGIN_URL', DIRECTORY_SEPARATOR . 'wp-content' . DIRECTORY_SEPARATOR . basename(dirname($arg[0])) .
                DIRECTORY_SEPARATOR . plugin_basename($arg[0]) . DIRECTORY_SEPARATOR);
        $this->require = serialize($arg[2]);
        if (!session_id()) {
            @session_start();
        }
        $_SESSION[_zPid] = '';
    }

    public function init() {
        $path = array('libs', 'resources', 'plans', 'cupon', 'dashboard', 'report');
        define('z_VERSION', '1.7.0.1');
        define('WP_TB_Coupon','coupon');
        //define('zSymbol', get_woocommerce_currency_symbol());
        define('PAGE_SELECT_PRODUCTS', 'Select Your Meals');
        define('PAGE_VIEW_ORDER_OF_WEEK', 'Your order of the week');
        define('PAGE_VIEW_MEALS_OF_DELIVERY', 'View meals of delivery');
        
        foreach (unserialize($this->require) as $req) {
            foreach ($path as $root) {       
                if (file_exists(z_PATH . $root . DIRECTORY_SEPARATOR . $req . '.php')) {
                    if (!defined('z_' . strtoupper($root))) {
                        define('z_' . strtoupper($root), z_PATH . $root . DIRECTORY_SEPARATOR);
                    }
                    require_once z_PATH . $root . DIRECTORY_SEPARATOR . $req . '.php';   
                }
            }
        }
        $this->loadALLShortcode();
        __zload_CSS();
        __zload_JS();
        $this->__CheckDeliverys();
        $this->__zload_script_footer();
    }

    public function __zload_script_footer() {
        add_action('wp_footer', '__zload_enabled_empty_cart');
    }

    public function loadALLShortcode() {
        add_shortcode('select-your-meals', 'select_your_meals');
        add_shortcode('meals-orders-view', 'meals_orders_view');
        add_shortcode('view-meals-delivery', 'view_meals_delivery');
    }

    public function loadAjax($ajaxs) {
        foreach ($ajaxs as $ajax) {
            add_action('wp_ajax_nopriv_'.$ajax, $ajax);
            add_action('wp_ajax_'.$ajax, $ajax);
        }
    }

    public function __CheckDeliverys() {
        add_action('woocommerce_cart_calculate_fees', 'update_fee');
        //add_action('woocommerce_cart_calculate_fees', 'update_tax');
    }

}
