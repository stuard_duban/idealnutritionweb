<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class zinstaller {

    function __construct() {
        if (!Page_Exist(PAGE_SELECT_PRODUCTS)) {
            $post_data = array(
                'post_title' => wp_strip_all_tags(PAGE_SELECT_PRODUCTS),
                'post_content' => '[select-your-meals]',
                'post_status' => 'publish',
                'post_type' => 'page',
                'post_author' => 1,
                'post_category' => array(1, 2)
            );
            wp_insert_post($post_data);
        }//
        if (!Page_Exist(PAGE_VIEW_ORDER_OF_WEEK)){
            $post_data = array(
                'post_title' => wp_strip_all_tags(PAGE_VIEW_ORDER_OF_WEEK),
                'post_content' => '[meals-orders-view]',
                'post_status' => 'publish',
                'post_type' => 'page',
                'post_author' => 1,
                'post_category' => array(1, 2)
            );
            wp_insert_post($post_data);
        }
        if (!Page_Exist(PAGE_VIEW_MEALS_OF_DELIVERY)){
            $post_data = array(
                'post_title' => wp_strip_all_tags(PAGE_VIEW_MEALS_OF_DELIVERY),
                'post_content' => '[view-meals-delivery]',
                'post_status' => 'publish',
                'post_type' => 'page',
                'post_author' => 1,
                'post_category' => array(1, 2)
            );
            wp_insert_post($post_data);
        }
        $this->__installDB();
    }

    function __SQL_($name_table) {
        $r = "CREATE TABLE `" . $name_table . "`(
                `id` INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,
                `users` INTEGER UNSIGNED NOT NULL DEFAULT 0,
                `ip` VARCHAR(45) NOT NULL DEFAULT '',
                `coupon` VARCHAR(65) NOT NULL DEFAULT '',
                `meals` INTEGER UNSIGNED NOT NULL DEFAULT 0,
                `status` ENUM('Available','Disabled','Locked','Used')  NOT NULL DEFAULT 'Available',
                `dates` DATE NOT NULL DEFAULT 0,
                `dateused` DATE NOT NULL DEFAULT 0,
                `dateexpire` DATE NOT NULL DEFAULT 0,
                PRIMARY KEY(`id`)
              )
          ENGINE = InnoDB";
        return $r;
    }

    function __installDB() {
        require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
        global $wpdb;
        $name_table = $wpdb->prefix . WP_TB_Coupon;
        if(count($wpdb->get_results("SELECT * FROM " . $name_table))===0){
            return dbDelta($this->__SQL_($name_table));
        }
        
    }

}
