<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of zMenu
 *
 * @author Windows
 */
class zMenu {

    //put your code here
    private $shortcode = 'z_add_new_tab_';
    private $Tab = array(1, 2, 3, 4, 5, 6, 7);

    public function _Tab() {
        foreach ($this->Tab as $tab) {
            add_action('admin_menu', $this->shortcode . $tab);
        }
    }

}

function z_add_new_tab_1() {//, 'shop_manager')
    add_menu_page(
            'Product Subscription', 'Subscription', 'administrator', 'product_subscription', 'z_add_new_tab_content_1', 'dashicons-screenoptions'); //z_SITE_URL.z_PLUGIN_URL.'resources/icon/product_subscription.svg'
    add_menu_page(
            'Product Subscription', 'Subscription', 'shop_manager', 'admin_product_subscription', 'z_add_new_tab_content_1', 'dashicons-screenoptions'); //z_SITE_URL.z_PLUGIN_URL.'resources/icon/product_subscription.svg'
}

function z_add_new_tab_2() {//, 'shop_manager')
    add_menu_page(
            'Coupon Code', 'Coupon', 'administrator', 'coupon_code', 'z_add_new_tab_content_2', 'dashicons-tickets-alt'); //z_SITE_URL.z_PLUGIN_URL.'resources/icon/product_subscription.svg'
    add_menu_page(
            'Coupon Code', 'Coupon', 'shop_manager', 'coupon_code', 'admin_z_add_new_tab_content_2', 'dashicons-tickets-alt'); //z_SITE_URL.z_PLUGIN_URL.'resources/icon/product_subscription.svg'
}

function z_add_new_tab_3() {
    add_menu_page('Delivery Report'
            , 'Delivery Report', 'administrator', 'delivery_report', 'z_add_new_tab_content_3', 'dashicons-format-aside');
    add_menu_page('Delivery Report'
            , 'Delivery Report', 'shop_manager', 'admin_delivery_report', 'z_add_new_tab_content_3', 'dashicons-format-aside');
}

function z_add_new_tab_4() {
    add_menu_page('Kitchen Report'
            , 'Kitchen Report', 'administrator', 'kitchen_report', 'z_add_new_tab_content_4', 'dashicons-format-aside');
    add_menu_page('Kitchen Report'
            , 'Kitchen Report', 'shop_manager', 'admin_kitchen_report', 'z_add_new_tab_content_4', 'dashicons-format-aside');
}

function z_add_new_tab_5() {
    add_menu_page('Sales Report'
            , 'Sales Report', 'administrator', 'sales_report', 'z_add_new_tab_content_5', 'dashicons-format-aside');
    add_menu_page('Sales Report'
            , 'Sales Report', 'shop_manager', 'admin_sales_report', 'z_add_new_tab_content_5', 'dashicons-format-aside');
}

function z_add_new_tab_6() {
    add_menu_page('See Meals Users'
            , 'See Meals Users', 'administrator', 'see_meals_users', 'z_add_new_tab_content_6', 'dashicons-format-aside');
    add_menu_page('See Meals Users'
            , 'See Meals Users', 'shop_manager', 'admin_see_meals_users', 'z_add_new_tab_content_6', 'dashicons-format-aside');
}

function z_add_new_tab_7() {
    add_menu_page('Order Limits'
            , 'Order Limits', 'administrator', 'order_limits', 'z_add_new_tab_content_7', 'dashicons-format-aside');
    add_menu_page('Order Limits'
            , 'Order Limits', 'shop_manager', 'admin_order_limits', 'z_add_new_tab_content_7', 'dashicons-format-aside');
}

function z_add_new_tab_content_1() {
    ?>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.0-rc.1/css/select2.min.css" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.0-rc.1/js/select2.min.js"></script>
    <style>
        span.select2{width: 100% !important;}
    </style>
    <h1>Product Subscription</h1>
    <?php addSubscription() ?>
    <hr>
    <?php zALLPlans(); ?>
    <?php
}

function z_add_new_tab_content_2() {
    $C = new zCuponez();
    ?>
    <h1>Coupon Code</h1>
    <?php $C->addCoupon(); ?>
    <hr>
    <?php $C->AllList(); ?>
    <?php
}

function z_add_new_tab_content_3() {
    $r = new report();
    $r->report_delivery();
}

function z_add_new_tab_content_4() {
    $r = new report();
    $r->report_kitchen();
}

function z_add_new_tab_content_5() {
    $r = new report();
    $r->report_sales();
}

function z_add_new_tab_content_6() {
    $r = new report();
    $r->report_see_meals();
}
//generamos las vistas de administrador
function z_add_new_tab_content_7() {
    //$r = new report();
    //$r->report_see_meals();
    global $wpdb;
    $week = $wpdb->get_results( "SELECT * FROM {$wpdb->prefix}orders_max WHERE id = 1");
    $week = isset($week[0]) ? $week[0]: (object) array(
        'id'=>'0',
        'dd'=>'0',
        'll'=>'0',
        'mm'=>'0',
        'mx'=>'0',
        'jj'=>'0',
        'vv'=>'0',
        'ss'=>'0',
    );
    ob_start();
    ?>
        <div class="wrap">
            <h1>Order Limits</h1>
            <div class="card" style="max-width: initial">
                <h2 class="title">Order limits per days</h2>
                
                <table style="width:100%">
                    <tr style="height: 33px;background: #1fcb26;">
                        <th>Sunday</th>
                        <th>Monday</th> 
                        <th>Tuesday</th>
                        <th>Wednesday</th>
                        <th>Thursday</th>
                        <th>Friday</th>
                        <th>Saturday</th>
                    </tr>
                    <tr>
                        <td style="text-align: center;border: 1px solid #1fcb26;height: 68px;">
                            <div id="dd"><?=$week->dd;?></div>
                            <div>available</div>
                            <div>
                                <a class="btn btn-primary" href="javascript:void(0)" onclick="jQuery('#Sunday').show()">Change</a>
                            </div>
                        </td>
                        <td style="text-align: center;border: 1px solid #1fcb26;height: 68px;">
                            <div id="ll"><?=$week->ll;?></div>
                            <div>available</div>
                            <div>
                                <a class="btn btn-primary" href="javascript:void(0)" onclick="jQuery('#Monday').show()">Change</a>
                            </div>
                        </td> 
                        <td style="text-align: center;border: 1px solid #1fcb26;height: 68px;">
                            <div id="mm"><?=$week->mm;?></div>
                            <div>available</div>
                            <div>
                                <a class="btn btn-primary" href="javascript:void(0)" onclick="jQuery('#Tuesday').show()">Change</a>
                            </div>
                        </td>
                        <td style="text-align: center;border: 1px solid #1fcb26;height: 68px;">
                            <div id="mx"><?=$week->mx;?></div>
                            <div>available</div>
                            <div>
                                <a class="btn btn-primary" href="javascript:void(0)" onclick="jQuery('#Wednesday').show()">Change</a>
                            </div>
                        </td>
                        <td style="text-align: center;border: 1px solid #1fcb26;height: 68px;">
                            <div id="jj" style="width: 100%"><?=$week->jj;?></div>
                            <div>available</div>
                            <div>
                                <a class="btn btn-primary" href="javascript:void(0)" onclick="jQuery('#Thursday').show()">Change</a>
                            </div>
                        </td> 
                        <td style="text-align: center;border: 1px solid #1fcb26;height: 68px;">
                            <div id="vv"><?=$week->vv;?></div>
                            <div>available</div>
                            <div>
                                <a class="btn btn-primary" href="javascript:void(0)" onclick="jQuery('#Friday').show()">Change</a>
                            </div>
                        </td>
                        <td style="text-align: center;border: 1px solid #1fcb26;height: 68px;">
                            <div id="ss"><?=$week->ss;?></div>
                            <div>available</div>
                            <div>
                                <a class="btn btn-primary" href="javascript:void(0)" onclick="jQuery('#Saturday').show()">Change</a>
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
			
        </div>
    <?php
    echo my_modale('Sunday', $week->dd, 'dd');
    echo my_modale('Monday', $week->ll, 'll');
    echo my_modale('Tuesday', $week->mm, 'mm');
    echo my_modale('Wednesday', $week->mx, 'mx');
    echo my_modale('Thursday', $week->jj, 'jj');
    echo my_modale('Friday', $week->vv, 'vv');
    echo my_modale('Saturday', $week->ss, 'ss');
    echo ob_get_clean();
}

function my_modale($day, $current, $name){
    ob_start();
    ?>
    <div id="<?=$day?>" style="display:none;position: fixed;top: 0px;left: 0px;right: 0px;bottom: 0px;background-color: #aaa6a6d6;padding-top: 7%;">
        <div style="border: 2px solid #d7d7d7;margin: 40px auto;width: fit-content;width: -moz-fit-content;background-color: white;border-radius: 4px;overflow: hidden;">
            <div style="display: inline-block;text-align:  center;max-width: 200px;"></div>
            <div style="padding: 30px 30px;display: inline-block;vertical-align: top;">
                <div style="float: right;cursor: pointer;font-weight: 700;">
                    <a onclick="jQuery('#<?=$day?>').hide()" style="color: #ff4600">X</a>
                </div>
                <div style="margin: -30px -30px 7px;padding: 7px 0px;">
                    <h2 style="margin: 7px 30px 7px 7px;text-align: left;">Update the order limit for <?=$day;?></h2>
                    <hr>
                </div>
                <form action="" method="post">
                    <input type="hidden" name="subscription_action" value="save_order_limit">
                    <input type="hidden" name="id" value="1">
                    <input type="hidden" name="option" value="<?=$name; ?>">
                    <div style="font-size: 18px;font-weight:  bold;text-transform: uppercase;">
                        
                        <label for="zMneu" style="display:block" style="display: block;font-size: 13px;font-weight: 600;"><?=$day;?></label>
                        <input type="number" name="<?=$name; ?>" min="0" id="zMneu" style="width:100%" required>
                    </div>
                    
                    <br>
                    <span style="text-align: center">Current available orders </span><span style="font-size: 18px;font-weight: bold;color: #ff4600;text-align: center">
                        <?=$current;?> 
                    </span>
                    <div style="margin-top:20px;text-align: right;">
                        <button type="submit" style="padding: 5px 10px;background: #ff4600;color: #fff;border: none;border-radius: 3px;">Update</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <?php
    return ob_get_clean();
}