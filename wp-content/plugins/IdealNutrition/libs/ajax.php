<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function notify_button_click() {
    // Check parameters
    $C = new zCuponez();
    $R = $C->getcoupon();
    $meals = 0;
    if ($R) {

        switch ($R->status) {
            case 'Available': $message = 'Coupon code ' . $R->status;
                $meals = $R->meals;
                break;
            case 'Disabled': $message = 'Coupon code ' . $R->status;
                break;
            case 'Locked': $message = 'Coupon code ' . $R->status;
                break;
            case 'Used': $message = 'Coupon code ' . $R->status;
                break;
            default: $message = 'Coupon code not available';
                break;
        }
    } else {
        $message = 'Coupon code not available';
    }
    wp_send_json(array('message' => $message, 'meals' => $meals));
}

function update_tax($data) {
    global $woocommerce;
    $percentage = 0.07;
    $taxes = array_sum($woocommerce->cart->taxes);
    $surcharge = ( $woocommerce->cart->cart_contents_total + $woocommerce->cart->shipping_total + $taxes ) * $percentage;

    WC()->session->set('ship_tax', '7%');//WC()->session->get('ship_tax')
    WC()->cart->add_fee('Tax ', $surcharge, false, '');
}

function get_limit_orders($data){
    //obtenemos la fecha que viene desde el checkout
    $cj = isset($_POST['datecj']) ? $_POST['datecj'] : '';
    $day_check_db = '';
    switch (explode(',',$cj)[0]) {
        case 'Sunday': $day_check_db = 'dd'; break;
        case 'Monday': $day_check_db = 'll'; break;
        case 'Tuesday': $day_check_db = 'mm'; break;
        case 'Wednesday': $day_check_db = 'mx'; break;
        case 'Thursday': $day_check_db = 'jj'; break;
        case 'Friday': $day_check_db = 'vv'; break;
        case 'Saturday': $day_check_db = 'ss'; break;
        default:
            $day_check_db = '';
            break;
    }
    if($day_check_db!==''){
        global $wpdb;
        $week = $wpdb->get_results( "SELECT {$day_check_db} FROM {$wpdb->prefix}orders_max WHERE id = 1");
        $week = isset($week[0]) ? (array) $week[0]: array(
            $day_check_db=>'0'
        );
        //obtenemos el registro de la base de datos
        $reg = $wpdb->get_results( "SELECT * FROM {$wpdb->prefix}orders_reg WHERE dates = '{$cj}'");
        $reg = isset($reg[0]) ? (array) $reg[0]: false;
        //si no existe lo insertamos en la base de datos
        if(!$reg){
            $reg_insert = array(
                'dates' => $cj,
                'counters' => $week[$day_check_db],
                'initial'=>$week[$day_check_db]
            );
            $wpdb->insert(
                $wpdb->prefix .'orders_reg',
                $reg_insert
            );
            $reg = array_merge(
                array('id' => $wpdb->insert_id),
                $reg_insert
            );
        }else{
            if(intval($week[$day_check_db])!==intval($reg['initial'])){
                //current serian los intentos actuales
                //                   12                               12                         10
                $current = (intval($reg['initial'])-intval($reg['counters']));
                //
                $counters = intval($week[$day_check_db])-intval($current);
                $wpdb->update($wpdb->prefix.'orders_reg', array(
                    'counters'=>$counters,
                    'initial'=>$week[$day_check_db],
                ), array(
                    'id' => $reg['id']
                ));
                $reg['counters'] = $counters;
                $reg['initial'] = $week[$day_check_db];
            }
            //initial
        }
        wp_send_json(array_merge($reg, array('day'=> explode(',',$cj)[0])));
    }else{
        wp_send_json(array(
            'id'=>0,
            'dates'=>'',
            'counters'=>'0',
            'day'=> explode(',',$cj)[0]
        ));
    }
}

function update_fee($data) {
    $Checked = _zCheckCart();

    

    if ($Checked) {
        $isSub = $Checked['isSubscriptions'];

    } else {
        $isSub = false;
        if($_POST['Semana_Siguiente'] != 0){
          $isSub = true;
          //echo "Es segunda semana se cobrara 0";
        }
    }
    if (!isset($_POST['time'])) {

        $chosen = WC()->session->get('ship_val');
        $chosen = empty($chosen) ? true : false;

        if ($chosen) {
            if ($isSub) {
                WC()->session->set('ship_val', '0');
            } else {
                WC()->session->set('ship_val', '0.00');
            }
        }
        WC()->cart->add_fee('Delivery ', WC()->session->get('ship_val'));
    }

    else {
      if($_POST['county'] == "Store Pick Up" && !$isSub)
      {


        WC()->session->set('ship_val', '2');
        WC()->cart->add_fee('Delivery ', WC()->session->get('ship_val'));

      }
      else {
        if ($isSub) {
            WC()->session->set('ship_val', '0');
            $r = 'price 0';
        } else {
            $time = isset($_POST['time']) ? $_POST['time'] : '';
            if (strlen(trim($time)) > 0) {
                $time = explode(' And ', $time);
            }
            if (count($time) > 1) {
                WC()->session->set('ship_val', '13.95');
                $r = 'price 13.95';
            } else {
                WC()->session->set('ship_val', '6.95');
                $r = 'price 6.95';
            }
        }
        WC()->cart->add_fee('Delivery ', WC()->session->get('ship_val'));
        wp_send_json(array('message' => __(time(), 'wpduf')));
      }
    }

    //Se verifica si lo recoge en tienda

}

function actualizarSuscripcion($data) {
  echo "Alopaso";
}
