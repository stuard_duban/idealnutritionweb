<?php 
$data = file_get_contents('../json/sales.json');
$data = json_decode($data, true);

?>
<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" href="<?= '/wp-content/plugins' . DIRECTORY_SEPARATOR . 'IdealNutrition/resources/'; ?>css/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="<?= '/wp-content/plugins' . DIRECTORY_SEPARATOR . 'IdealNutrition/resources/'; ?>css/bootstrap-table/src/bootstrap-table.css">
        <link rel="stylesheet" href="//rawgit.com/vitalets/x-editable/master/dist/bootstrap3-editable/css/bootstrap-editable.css">
        <link rel="stylesheet" href="<?= '/wp-content/plugins' . DIRECTORY_SEPARATOR . 'IdealNutrition/resources/'; ?>css/examples.css">
        <script src="<?= '/wp-content/plugins' . DIRECTORY_SEPARATOR . 'IdealNutrition/resources/'; ?>css/jquery.min.js"></script>
        <script src="<?= '/wp-content/plugins' . DIRECTORY_SEPARATOR . 'IdealNutrition/resources/'; ?>css/bootstrap/js/bootstrap.min.js"></script>
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="//cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.min.js"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/json2/20140204/json2.min.js"></script>
        <![endif]-->
        <style>.fixed-table-toolbar{display: none !important;}</style>
    </head>
    <body style="overflow: hidden ">
        <div class="container" style="margin-top:60px">
            <h1>Sales Report</h1>
            <div style="position: absolute;top: 85px;right: 112px;z-index: 1;"><a style="margin-right:5px" class="btn btn-danger" href="/wp-content/plugins/IdealNutrition/report/output.php?t=pdf&f=sales">Export PDF</a><a style="margin-right:5px" class="btn btn-info" href="/wp-content/plugins/IdealNutrition/report/output.php?t=word&f=sales">Export Word</a><a class="btn btn-success" href="/wp-content/plugins/IdealNutrition/report/output.php?t=excel&f=sales">Export Excel</a></div>
            <table id="table"
                   data-toolbar="#toolbar"
                   data-search="true"
                   data-show-refresh="true"
                   data-show-toggle="true"
                   data-show-columns="true"
                   data-show-export="true"
                   data-detail-view="true"
                   data-detail-formatter="detailFormatter"
                   data-minimum-count-columns="2"
                   data-show-pagination-switch="true"
                   data-pagination="true"
                   data-id-field="id"
                   data-page-list="[10, 25, 50, 100, ALL]"
                   data-show-footer="false"
                   data-side-pagination="server"
                   data-response-handler="responseHandler">
                <thead>
                    <tr>
                        <th>Nº</th>
                        <th>Username</th>
                        <th>Mail</th>
                        <th>Phone</th>
                        <th>City</th>
                        <th>Date</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($data as $key => $e) {
                        ?>
                    <tr>
                        <td><?=$e['id'];?></td>
                        <td>
                        <?=$e['Name'];?>
                        </td>
                        <td><?=$e['mail'];?></td>
                        <td><?=$e['phone'];?></td>
                        <td><?=$e['city'];?></td>
                        <td><?=$e['date'];?></td>
                    </tr>  
                        <?php
                    } ?>
                </tbody>
            </table>
        </div>
        <script>
            <?php
            $js_array = json_encode($data);

            echo "var _datas = ". $js_array . ";\n";
            echo "console.log(_datas);\n";

            ?>
            var $table = $('#table'),
                    $remove = $('#remove'),
                    selections = [];

            function initTable() {
                $table.bootstrapTable({
                    height: getHeight()/*,
                    /*columns: [
                        [
                            {
                                field: 'state',
                                checkbox: true,
                                rowspan: 2,
                                align: 'center',
                                valign: 'middle'
                            }, {
                                title: 'Username',
                                field: 'user',
                                rowspan: 2,
                                align: 'center',
                                valign: 'middle',
                                sortable: true,
                                footerFormatter: totalTextFormatter
                            }, {
                                title: 'Details',
                                colspan: 3,
                                align: 'center'
                            }
                        ],
                        [
                            {
                                field: 'meal',
                                title: 'Meals',
                                sortable: true,
                                editable: true,
                                footerFormatter: totalNameFormatter,
                                align: 'center'
                            }, {
                                field: 'qty',
                                title: 'Catidad',
                                sortable: true,
                                align: 'center',
                                editable: {
                                    type: 'text',
                                    title: 'Catidad',
                                    validate: function (value) {
                                        value = $.trim(value);
                                        if (!value) {
                                            return 'This field is required';
                                        }
                                        if (!/^\$/.test(value)) {
                                            return 'This field needs to start width $.'
                                        }
                                        var data = $table.bootstrapTable('getData'),
                                                index = $(this).parents('tr').data('index');
                                        console.log(data[index]);
                                        return '';
                                    }
                                },
                                footerFormatter: totalPriceFormatter
                            }, {
                                field: 'city',
                                title: 'City',
                                align: 'center',
                                events: operateEvents,
                                formatter: operateFormatter
                            }, {
                                field: 'dates',
                                title: 'date',
                                align: 'center',
                                events: operateEvents,
                                formatter: operateFormatter
                            }
                        ]
                    ]*/
                });
                // sometimes footer render error.
                setTimeout(function () {
                    $table.bootstrapTable('resetView');
                }, 200);
                $table.on('check.bs.table uncheck.bs.table ' +
                        'check-all.bs.table uncheck-all.bs.table', function () {
                            $remove.prop('disabled', !$table.bootstrapTable('getSelections').length);

                            // save your data, here just save the current page
                            selections = getIdSelections();
                            // push or splice the selections if you want to save all data selections
                        });
                
                $table.on('all.bs.table', function (e, name, args) {
                    console.log(name, args);
                    //var iframeHeight = jQuery('iframe').height()
                    jQuery( ".detail-icon" ).one( "click", function() {
                      parent.$(parent.document).click();
                    });
                });
                $remove.click(function () {
                    var ids = getIdSelections();
                    $table.bootstrapTable('remove', {
                        field: 'id',
                        values: ids
                    });
                    $remove.prop('disabled', true);
                });
                $(window).resize(function () {
                    $table.bootstrapTable('resetView', {
                        height: getHeight()
                    });
                });
            }

            function getIdSelections() {
                return $.map($table.bootstrapTable('getSelections'), function (row) {
                    return row.id
                });
            }

            function responseHandler(res) {
                $.each(res.rows, function (i, row) {
                    row.state = $.inArray(row.id, selections) !== -1;
                });
                return res;
            }

            function detailFormatter(index, row) {
                var html = [];
                $.each(row, function (key, value) {
                    //console.log(key);
                    if(key==='0'){
                        console.log(_datas[value]);
                        html.push('<p><b>Nº Order:</b> ' + _datas[value]['id'] + '</p>');
                        html.push('<p><b>Name:</b> ' + _datas[value]['Name'] + '</p>');
                        html.push('<p><b>Mail:</b> ' + _datas[value]['mail'] + '</p>');
                        html.push('<p><b>Phone:</b> ' + _datas[value]['phone'] + '</p>');
                        html.push('<p><b>City:</b> ' + _datas[value]['city'] + '</p>');
                        html.push('<p><b>Delivery Date:</b> ' + _datas[value]['date'] + '</p>');
                        html.push('<p><b>Address:</b> ' + _datas[value]['Address'] + '</p>');
                        html.push('<p><b>Delivery Instructions/Special Requests:</b> ' + _datas[value]['notes'] + '</p>');
                        var meal = _datas[value]['Meals'];
                        console.log(meal);
                        
                        var tr = '';
                        for(var i in meal){
                            var e = meal[i];
                            var c = '';
                            if(e['custom']!==undefined){
                                c = e['custom'];
                            }
                            tr = tr + '<tr><td style="text-align: center;">'+e['sku']+'</td><td style="text-align: center;">'+e['image']+'</td><td style="padding-left: 10px;">'+e['name']+'<br>'+c+'</td><td style="text-align: center;">'+e['qty']+'</td><td style="text-align: center;">'+e['day']+'</td><td style="text-align: center;">'+e['date']+'</td></tr>';
                        };
                        html.push('<table><thead><tr><th>REF</th><th>IMAGE</th><th>MEAL</th><th>QUANTITY</th><th>DAY</th><th>DATE</th></tr></thead><tbody>'+tr+'</tbody></table>');
                        //
                    }
                });
                return html.join('');
            }

            function operateFormatter(value, row, index) {
                return [
                    '<a class="like" href="javascript:void(0)" title="Like">',
                    '<i class="glyphicon glyphicon-heart"></i>',
                    '</a>  ',
                    '<a class="remove" href="javascript:void(0)" title="Remove">',
                    '<i class="glyphicon glyphicon-remove"></i>',
                    '</a>'
                ].join('');
            }

            window.operateEvents = {
                'click .like': function (e, value, row, index) {
                    alert('You click like action, row: ' + JSON.stringify(row));
                },
                'click .remove': function (e, value, row, index) {
                    $table.bootstrapTable('remove', {
                        field: 'id',
                        values: [row.id]
                    });
                }
            };

            function totalTextFormatter(data) {
                return 'Total';
            }

            function totalNameFormatter(data) {
                return data.length;
            }

            function totalPriceFormatter(data) {
                var total = 0;
                $.each(data, function (i, row) {
                    total += +(row.price.substring(1));
                });
                return '$' + total;
            }

            function getHeight() {
                return $(window).height() - $('h1').outerHeight(true);
            }

            $(function () {
                var scripts = [
                    location.search.substring(1) || '<?= '/wp-content/plugins' . DIRECTORY_SEPARATOR . 'IdealNutrition/resources/'; ?>css/bootstrap-table/src/bootstrap-table.js',
                    '<?= '/wp-content/plugins' . DIRECTORY_SEPARATOR . 'IdealNutrition/resources/'; ?>css/bootstrap-table/src/extensions/export/bootstrap-table-export.js',
                    '//rawgit.com/hhurz/tableExport.jquery.plugin/master/tableExport.js',
                    '<?= '/wp-content/plugins' . DIRECTORY_SEPARATOR . 'IdealNutrition/resources/'; ?>css/bootstrap-table/src/extensions/editable/bootstrap-table-editable.js',
                    '//rawgit.com/vitalets/x-editable/master/dist/bootstrap3-editable/js/bootstrap-editable.js'
                ],
                        eachSeries = function (arr, iterator, callback) {
                            callback = callback || function () {};
                            if (!arr.length) {
                                return callback();
                            }
                            var completed = 0;
                            var iterate = function () {
                                iterator(arr[completed], function (err) {
                                    if (err) {
                                        callback(err);
                                        callback = function () {};
                                    } else {
                                        completed += 1;
                                        if (completed >= arr.length) {
                                            callback(null);
                                        } else {
                                            iterate();
                                        }
                                    }
                                });
                            };
                            iterate();
                        };

                eachSeries(scripts, getScript, initTable);
            });

            function getScript(url, callback) {
                var head = document.getElementsByTagName('head')[0];
                var script = document.createElement('script');
                script.src = url;

                var done = false;
                // Attach handlers for all browsers
                script.onload = script.onreadystatechange = function () {
                    if (!done && (!this.readyState ||
                            this.readyState == 'loaded' || this.readyState == 'complete')) {
                        done = true;
                        if (callback)
                            callback();

                        // Handle memory leak in IE
                        script.onload = script.onreadystatechange = null;
                    }
                };

                head.appendChild(script);

                // We handle everything using the script element injection
                return undefined;
            }
        </script>
    </body>
</html>