<?php

$data = file_get_contents('../json/kitchen.json');
$data = json_decode($data, true);
?>
<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" href="<?= '/wp-content/plugins' . DIRECTORY_SEPARATOR . 'IdealNutrition/resources/'; ?>css/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="<?= '/wp-content/plugins' . DIRECTORY_SEPARATOR . 'IdealNutrition/resources/'; ?>css/bootstrap-table/src/bootstrap-table.css">
        <link rel="stylesheet" href="//rawgit.com/vitalets/x-editable/master/dist/bootstrap3-editable/css/bootstrap-editable.css">
        <link rel="stylesheet" href="<?= '/wp-content/plugins' . DIRECTORY_SEPARATOR . 'IdealNutrition/resources/'; ?>css/examples.css">
        <script src="<?= '/wp-content/plugins' . DIRECTORY_SEPARATOR . 'IdealNutrition/resources/'; ?>css/jquery.min.js"></script>
        <script src="<?= '/wp-content/plugins' . DIRECTORY_SEPARATOR . 'IdealNutrition/resources/'; ?>css/bootstrap/js/bootstrap.min.js"></script>
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="//cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.min.js"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/json2/20140204/json2.min.js"></script>
        <![endif]-->
        <style>.fixed-table-toolbar{display: none !important;}</style>
    </head>
    <body style="overflow: hidden ">
        <div class="container" style="margin-top:60px">
            <h1>Kitchen Report</h1>
            <div style="position: absolute;top: 85px;right: 112px;z-index: 1;"><a style="margin-right:5px" class="btn btn-danger" href="/wp-content/plugins/IdealNutrition/report/output.php?t=pdf&f=kitchen">Export PDF</a><a style="margin-right:5px" class="btn btn-info" href="/wp-content/plugins/IdealNutrition/report/output.php?t=word&f=kitchen">Export Word</a><a class="btn btn-success" href="/wp-content/plugins/IdealNutrition/report/output.php?t=excel&f=kitchen">Export Excel</a></div>
            <table id="table"
                   data-toolbar="#toolbar"
                   data-search="true"
                   data-show-refresh="true"
                   data-show-toggle="true"
                   data-show-columns="true"
                   data-show-export="true"
                   data-detail-view="true"
                   data-detail-formatter="detailFormatter"
                   data-minimum-count-columns="2"
                   data-show-pagination-switch="true"
                   data-pagination="true"
                   data-id-field="id"
                   data-page-list="[10, 25, 50, 100, ALL]"
                   data-show-footer="false"
                   data-side-pagination="server"
                   data-response-handler="responseHandler">
                <thead>
                    <tr>
                        <th>REF</th>
                        <th>IMAGEN</th>
                        <th>NAME</th>
                        <th>QUANTITY</th>
                        <th>DAY</th>
                        <th>DELIVERY DATE</th>
                    </tr>
                </thead>
                <tbody>
                    <?php //echo "Datos: <pre>" . var_dump($data);
                    foreach ($data as $key => $e) {
                        ?>
                    <tr>
                        <td><?=$e['sku'];?></td>
                        <td><?=$e['image'];?></td>
                        <td><?=$e['name'];?>
                        <?php
                            if(isset($e['custom'])){
                                echo '<br>'.$e['custom'];
                            }
                        ?>
                        </td>
                        <td><?=$e['qty'];?></td>
                        <td><?=$e['day'];?></td>
                        <td><?=$e['date'];?></td>
                    </tr>
                        <?php
                    } ?>
                </tbody>
            </table>
        </div>

        <script>
            <?php
            $js_array = json_encode($data);
            echo "var _datas = ". $js_array . ";\n";
            ?>
            var $table = $('#table'),
                    $remove = $('#remove'),
                    selections = [];

            function initTable() {
                $table.bootstrapTable({
                    height: getHeight()/*,
                    /*columns: [
                        [
                            {
                                field: 'state',
                                checkbox: true,
                                rowspan: 2,
                                align: 'center',
                                valign: 'middle'
                            }, {
                                title: 'Username',
                                field: 'user',
                                rowspan: 2,
                                align: 'center',
                                valign: 'middle',
                                sortable: true,
                                footerFormatter: totalTextFormatter
                            }, {
                                title: 'Details',
                                colspan: 3,
                                align: 'center'
                            }
                        ],
                        [
                            {
                                field: 'meal',
                                title: 'Meals',
                                sortable: true,
                                editable: true,
                                footerFormatter: totalNameFormatter,
                                align: 'center'
                            }, {
                                field: 'qty',
                                title: 'Catidad',
                                sortable: true,
                                align: 'center',
                                editable: {
                                    type: 'text',
                                    title: 'Catidad',
                                    validate: function (value) {
                                        value = $.trim(value);
                                        if (!value) {
                                            return 'This field is required';
                                        }
                                        if (!/^\$/.test(value)) {
                                            return 'This field needs to start width $.'
                                        }
                                        var data = $table.bootstrapTable('getData'),
                                                index = $(this).parents('tr').data('index');
                                        console.log(data[index]);
                                        return '';
                                    }
                                },
                                footerFormatter: totalPriceFormatter
                            }, {
                                field: 'city',
                                title: 'City',
                                align: 'center',
                                events: operateEvents,
                                formatter: operateFormatter
                            }, {
                                field: 'dates',
                                title: 'date',
                                align: 'center',
                                events: operateEvents,
                                formatter: operateFormatter
                            }
                        ]
                    ]*/
                });
                // sometimes footer render error.
                setTimeout(function () {
                    $table.bootstrapTable('resetView');
                }, 200);
                $table.on('check.bs.table uncheck.bs.table ' +
                        'check-all.bs.table uncheck-all.bs.table', function () {
                            $remove.prop('disabled', !$table.bootstrapTable('getSelections').length);

                            // save your data, here just save the current page
                            selections = getIdSelections();
                            // push or splice the selections if you want to save all data selections
                        });

                $table.on('all.bs.table', function (e, name, args) {
                    console.log(name, args);

                    jQuery( ".detail-icon" ).one( "click", function() {
                      parent.$(parent.document).click();
                    });
                });
                $remove.click(function () {
                    var ids = getIdSelections();
                    $table.bootstrapTable('remove', {
                        field: 'id',
                        values: ids
                    });
                    $remove.prop('disabled', true);
                });
                $(window).resize(function () {
                    $table.bootstrapTable('resetView', {
                        height: getHeight()
                    });
                });
            }

            function getIdSelections() {
                return $.map($table.bootstrapTable('getSelections'), function (row) {
                    return row.id
                });
            }

            function responseHandler(res) {
                $.each(res.rows, function (i, row) {
                    row.state = $.inArray(row.id, selections) !== -1;
                });
                return res;
            }

            function detailFormatter(index, row) {
                var html = [];
                $.each(row, function (key, value) {
                    //console.log(key);
                    if(key==='0'){
                        console.log(_datas[value]);
                        html.push('<p><b>Resumen:</b> ' + _datas[value]['resumen'] + '</p>');
                    }
                });
                return html.join('');
            }

            function operateFormatter(value, row, index) {
                return [
                    '<a class="like" href="javascript:void(0)" title="Like">',
                    '<i class="glyphicon glyphicon-heart"></i>',
                    '</a>  ',
                    '<a class="remove" href="javascript:void(0)" title="Remove">',
                    '<i class="glyphicon glyphicon-remove"></i>',
                    '</a>'
                ].join('');
            }

            window.operateEvents = {
                'click .like': function (e, value, row, index) {
                    alert('You click like action, row: ' + JSON.stringify(row));
                },
                'click .remove': function (e, value, row, index) {
                    $table.bootstrapTable('remove', {
                        field: 'id',
                        values: [row.id]
                    });
                }
            };

            function totalTextFormatter(data) {
                return 'Total';
            }

            function totalNameFormatter(data) {
                return data.length;
            }

            function totalPriceFormatter(data) {
                var total = 0;
                $.each(data, function (i, row) {
                    total += +(row.price.substring(1));
                });
                return '$' + total;
            }

            function getHeight() {
                return $(window).height() - $('h1').outerHeight(true);
            }

            $(function () {
                var scripts = [
                    location.search.substring(1) || '//cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.4/jspdf.min.js','<?= '/wp-content/plugins' . DIRECTORY_SEPARATOR . 'IdealNutrition/resources/'; ?>css/bootstrap-table/src/bootstrap-table.js',
                    '<?= '/wp-content/plugins' . DIRECTORY_SEPARATOR . 'IdealNutrition/resources/'; ?>css/bootstrap-table/src/extensions/export/bootstrap-table-export.js',
                    '<?= '/wp-content/plugins' . DIRECTORY_SEPARATOR . 'IdealNutrition/resources/'; ?>css/bootstrap-table/src/extensions/export/tableExport.js',
                    '<?= '/wp-content/plugins' . DIRECTORY_SEPARATOR . 'IdealNutrition/resources/'; ?>css/bootstrap-table/src/extensions/editable/bootstrap-table-editable.js',
                    '//rawgit.com/vitalets/x-editable/master/dist/bootstrap3-editable/js/bootstrap-editable.js'
                ],
                        eachSeries = function (arr, iterator, callback) {
                            callback = callback || function () {};
                            if (!arr.length) {
                                return callback();
                            }
                            var completed = 0;
                            var iterate = function () {
                                iterator(arr[completed], function (err) {
                                    if (err) {
                                        callback(err);
                                        callback = function () {};
                                    } else {
                                        completed += 1;
                                        if (completed >= arr.length) {
                                            callback(null);
                                        } else {
                                            iterate();
                                        }
                                    }
                                });
                            };
                            iterate();
                        };

                eachSeries(scripts, getScript, initTable);
            });

            function getScript(url, callback) {
                var head = document.getElementsByTagName('head')[0];
                var script = document.createElement('script');
                script.src = url;

                var done = false;
                // Attach handlers for all browsers
                script.onload = script.onreadystatechange = function () {
                    if (!done && (!this.readyState ||
                            this.readyState == 'loaded' || this.readyState == 'complete')) {
                        done = true;
                        if (callback)
                            callback();

                        // Handle memory leak in IE
                        script.onload = script.onreadystatechange = null;
                    }
                };

                head.appendChild(script);

                // We handle everything using the script element injection
                return undefined;
            }
        </script>
    </body>
</html>
