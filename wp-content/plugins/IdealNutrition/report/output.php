<?php

use Dompdf\Dompdf;

$dir = __DIR__ . DIRECTORY_SEPARATOR;
$tipo = isset($_REQUEST['t']) ? $_REQUEST['t'] : 'excel';
$file = isset($_REQUEST['f']) ? $_REQUEST['f'] : 'users';
$extension = '.xls';
$left = 'portrait';
if($file==='delivery'){
    $left = 'landscape';
}

if ($tipo == 'word')
    $extension = '.doc';
require_once $dir . 'pdf/'. $file . '_pdf.php';

// Si queremos exportar a PDF
if ($tipo == 'pdf') {
    require_once $dir . 'dompdf/autoload.inc.php';

    $dompdf = new DOMPDF(array('enable_remote' => true));
    $dompdf->set_option('isHtml5ParserEnabled', true);
    $dompdf->load_html($_HTML_);

    $dompdf->setPaper("letter", $left);
    $dompdf->render();
    $dompdf->stream(uniqid(date('Y-m-d')) . ".pdf");
} else {


    header("Pragma: public");
    header("Expires: 0");
    $filename = uniqid(date('Y-m-d')) . $extension;
    header("Content-type: application/x-msdownload");
    header("Content-Disposition: attachment; filename=$filename");
    header("Pragma: no-cache");
    header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
    echo $_HTML_;
}
