<?php

/*

 * To change this license header, choose License Headers in Project Properties.

 * To change this template file, choose Tools | Templates

 * and open the template in the editor.

 */



/**

 * Description of report

 * @author JB

 */

class report {

    private function form_header($day = false) {

        if (count($_POST) === 0) {

            ?>

            <div>

                <h1>Input the following information to generate the report</h1>

                <form method="POST" enctype="multipart/form-data">

                    <label for="desde">FROM</label>

                    <input required id="desde" name="desde" type='date' placeholder="Desde">

                    <label for="hasta">TO</label>

                    <input required id="hasta" name="hasta" type='date' placeholder="Hasta">

                    <label for="hasta">ORDER</label>

                    <input id="order" name="order" type='text' placeholder="NOT REQUIRED">

                    <input class="btn btn-primary" type="submit" value="Generate">

                    <?php if ($day) { ?>

                        <div style="display: block;margin-top: 10px;">

                            <label style="margin-right: 10px" for="Sunday"><input id="Sunday" type="checkbox" name="Sunday" checked> Sunday</label>

                            <label style="margin-right: 10px" for="Monday"><input id="Monday" type="checkbox" name="Monday" checked> Monday</label>

                            <label style="margin-right: 10px" for="Wednesday"><input id="Wednesday" type="checkbox" name="Wednesday" checked> Wednesday</label>

                            <label style="margin-right: 10px" for="Thursday"><input id="Thursday" type="checkbox" name="Thursday" checked> Thursday</label>

                            <label style="margin-right: 10px" for="drinks"><input id="drinks" type="checkbox" name="drinks"> Only Meals (Without drinks)</label>

                            <label style="margin-right: 10px" for="notes"><input id="notes" type="checkbox" name="notes">Only instructions of customer</label>

                        </div>

                    <?php } ?>

                </form>

            </div>

            <?php

            exit();

        }

    }



    private function form_footer($type) {

        $top = '32';

        if ($type === 'Delivery' || $type === 'Kitchen' || $type === 'Sales') {

            $top = '10';

        }

        ?>

        <form method="POST" enctype="multipart/form-data" style="position: absolute;top: <?= $top; ?>px;right: 110px;z-index: 1;">

            <?php if ($top === '10') { ?>

                <div style="display: block;margin-bottom: 10px;">

                    <label style="margin-right: 10px" for="Sunday"><input id="Sunday" type="checkbox" name="Sunday" checked> Sunday</label>

                    <label style="margin-right: 10px" for="Monday"><input id="Monday" type="checkbox" name="Monday" checked> Monday</label>

                    <label style="margin-right: 10px" for="Wednesday"><input id="Wednesday" type="checkbox" name="Wednesday" checked> Wednesday</label>

                    <label style="margin-right: 10px" for="Thursday"><input id="Thursday" type="checkbox" name="Thursday" checked> Thursday</label>

                    <label style="margin-right: 10px" for="drinks"><input id="drinks" type="checkbox" name="drinks"> Only Meals (Without drinks)</label>

                    <label style="margin-right: 10px" for="notes"><input id="notes" type="checkbox" name="notes">Only instructions of customer</label>

                </div>

            <?php } ?>

            <label for="desde">FROM</label>

            <input required id="desde" name="desde" type='date' placeholder="Desde">

            <label for="hasta">TO</label>

            <input required id="hasta" name="hasta" type='date' placeholder="Hasta">

            <input class="btn btn-primary" type="submit" value="Generate">

        </form>

        <iframe id="reporte-iframe" style="position:  absolute;top: 1px;min-height: 1050px;width: 100%;" src="<?= plugins_url() . DIRECTORY_SEPARATOR . 'IdealNutrition/report/type/' . $type; ?>.php" onload="iframeLoaded()"></iframe>

        <?php

    }



    private function SaveJson($type, $data) {
        file_put_contents(__DIR__ . '/json/' . strtolower($type) . '.json', json_encode($data));
    }



    public function report_meals() {

        $this->form_header();



        global $wpdb;

        $table_name = $wpdb->prefix . "orders_in";



        $date_from = $_POST['desde'];

        $date_to = $_POST['hasta'];

        $post_status = implode("','", array('wc-processing', 'wc-completed', 'wc-cancelled'));

        if (isset($_POST['order'])) {

            if (strlen(trim(($_POST['order']))) === 0) {

                unset($_POST['order']);

            }

        }

        $FILTER = isset($_POST['order']) ? 'ID = ' . $_POST['order'] . ' AND ' : '';

        $result = $wpdb->get_results("SELECT * FROM $wpdb->posts

            WHERE " . $FILTER . "post_type = 'shop_order'

            AND post_status IN ('{$post_status}')

            AND post_date BETWEEN '{$date_from}  00:00:00' AND '{$date_to} 23:59:59'

        ");



        $Orders = [];

        foreach ($result as $key => $order) {

            $ord = wc_get_order($order->ID);

            $WC = new WC_Product_Factory();

            $order = new WC_Order($order->ID);

            $order_items = $order->get_items();

            $DB = $wpdb->get_results("SELECT * FROM $table_name WHERE orders=" . $order->ID);

            $city = '';

            $time = '';

            if ($DB) {

                $city = $DB[0]->city;

                $time = $DB[0]->tim;

            }

            $item = [];

            foreach ($order_items as $items_key => $items_value) {

                $product = $WC->get_product($items_value['product_id']);

                $item[$items_value['product_id']] = [

                    'id' => $items_value['product_id'],

                    'name' => $items_value['name'],

                    'price' => $product->get_price_html(),

                    'qty' => $items_value['qty'],

                    'total' => '$' . number_format(floatval($items_value['line_total']), 2, '.', ','),

                    'image' => $product->get_image('shop_thumbnail'),

                    'resumen' => $product->get_image('shop_thumbnail') . '|<a target="_blank" href="' . get_permalink($items_value['product_id']) . '">' . $items_value['name'] . '</a> ' . $product->get_price_html() . ' <strong>x ' . $items_value['qty'] . '</strong> $' . number_format(floatval($items_value['line_total']), 2, '.', ',')

                ];

            }

            $order_meta = get_post_meta($order->ID);

            $Address = $order_meta['_shipping_address_index'][0];

            if (strlen(trim($Address)) === 0) {

                $Address = $order_meta['_billing_address_index'][0];

            }

            if (strlen(trim($city)) === 0) {

                $city = $order_meta['_billing_myfield18'][0];

            }if (strlen(trim($time)) === 0) {

                $time = $order_meta['_billing_myfield18c'][0];

            }

            if ($time === 'free') {

                $time = '-';

            }

            $Orders[$order->ID] = [

                'id' => $order->ID,

                'Username' => $ord->get_user_id(),

                'Name' => $order_meta['_billing_first_name'][0] . ' ' . $order_meta['_billing_last_name'][0],

                'mail' => $order_meta['_billing_email'][0],

                'phone' => $order_meta['_billing_phone'][0],

                'Meals' => $item,

                'Catidad' => '',

                'Address' => $Address,

                'city' => $city,

                'date' => $time,

            ];

        }

        $this->SaveJson('Orders', $Orders);

        $this->form_footer('Orders');

    }



    public function report_delivery() {

        $this->form_header(true);

        global $wpdb;

        $table_name = $wpdb->prefix . "orders_in";

        $date_from = date('Y-m-j', strtotime('now', strtotime(date('Y-m-j', strtotime($_POST['desde'])))));

        $date_to = date('Y-m-j', strtotime('now', strtotime(date('Y-m-j', strtotime($_POST['hasta'])))));

        $post_status = implode("','", array('wc-processing', 'wc-completed', 'wc-cancelled'));

        if (isset($_POST['order'])) {

            if (strlen(trim(($_POST['order']))) === 0) {

                unset($_POST['order']);

            }

        }

        $FILTER = isset($_POST['order']) ? 'ID = ' . $_POST['order'] . ' AND ' : '';

        $today = date("Y-m-d");

        /*Original*/
        $result = $wpdb->get_results("SELECT * FROM $wpdb->posts

            WHERE " . $FILTER . "post_type = 'shop_order'

            AND post_status IN ('{$post_status}')

            AND post_date BETWEEN '{$date_from}  00:00:00' AND '{$today} 23:59:59'

        ");

        $Orders = [];

        foreach ($result as $key => $order) {

            $ord = wc_get_order($order->ID);



            $WC = new WC_Product_Factory();



            $order = new WC_Order($order->ID);

            $Orders_Items = $order->get_items();



            $Custom = [];

            $Str = '';

            foreach ($Orders_Items as $key => $value) {

                $Str = '';

                foreach ($value->get_formatted_meta_data() as $meta_id => $meta) {

                    if (wp_kses_post($meta->display_key) !== 'Ref') {

                        $Str .= '<strong class="wc-item-meta-label">' . wp_kses_post($meta->display_key) . ':</strong> ' . wp_kses_post($meta->display_value) . '<br>';

                    }

                }

                if (strlen(trim($Str)) > 0) {

                    $Str = '<hr style="margin: 3px;border: 1px solid #12e61a;"><strong>' . $value['name'] . ' x ' . $value['qty'] . '</strong><br>' . $Str;

                }

                if (isset($Custom[$value['product_id']])) {

                    if (!is_array($Custom[$value['product_id']])) {

                        $cont = $Custom[$value['product_id']];

                        $Custom[$value['product_id']] = array();



                        $Custom[$value['product_id']][] = $cont;

                        $Custom[$value['product_id']][] = $Str;

                    } else {

                        $Custom[$value['product_id']][] = $Str;

                    }

                } else {

                    $Custom[$value['product_id']] = $Str;

                }

            }

            foreach ($Custom as $idproduct => $elem) {

                if (is_array($elem)) {

                    $str = '';

                    foreach ($elem as $ele) {

                        $str .= $ele;

                    }

                    $Custom[$idproduct] = $str;

                }

            }

            $fecha_inicio = $date_from;
            $fecha_fin = $date_to;
            $date_from = date('Y-m-j', strtotime('next Saturday', strtotime(date('Y-m-j', strtotime($_POST['desde'])))));
            if($date_from > $date_to) {
               $date_to = date('Y-m-j', strtotime('+7 day', strtotime(date('Y-m-j', strtotime($_POST['hasta'])))));
            }

            $DB = $wpdb->get_results("SELECT * FROM $table_name WHERE orders=" . $order->ID . " AND first_delivery_date BETWEEN '{$date_from}  00:00:00' AND '{$date_to} 23:59:59'");

            $primera_suscripcion = $wpdb->get_results("SELECT * FROM $table_name WHERE orders=" . $order->ID . " AND first_delivery_date ORDER BY first_delivery_date ASC");

            $FirstDay = '';

            $FirstDate = '';



            $city = '';

            $time = '';



            $order_meta = get_post_meta($order->ID);

            $Notex = ($order->customer_message);

            //Validacion Shipping address si existe*/

            $Address = $order_meta['_shipping_address_index'][0];

            if($order_meta['_billing_myfield12'][0] == 0){
              $Address =  $order_meta['_billing_first_name'][0] . " " . $order_meta['_billing_last_name'][0] . " " . $order_meta['_billing_myfield13'][0];
              $Address .= " " . $order_meta['_billing_myfield16'][0] . " " . $order_meta['_billing_myfield16c'][0];
              $Address .= " " . $order_meta['_billing_myfield17'][0];
            }


            if (strlen(trim($Address)) === 0) {

                $Address = $order_meta['_billing_address_index'][0];

            }

            if ($DB) {
              $lengthDB = count($DB);
              $order_items = [];
              for($countDB = 0; $countDB < $lengthDB; $countDB++){
                $Ddb = '';

                $SecondDay = '';

                $SecondDate = '';

                $city = $DB[$countDB]->city;

                $time = $DB[$countDB]->tim;

                /*Campo delivery*/
                $Ddb = $DB[$countDB]->delivery;

                $Ddb = explode(',', $Ddb);

                foreach ($Ddb as $key => $value) {

                    unset($Ddb[$key]);

                    $value = explode('=', $value);

                    if (isset($value[0]) && isset($value[1])) {

                        $Ddb[$value[0]] = $value[1];

                    }

                }
                /*Campo delivery*/

                /*Desfragmentando campo tim*/
                $rcox = (explode('And', $time));

                if (count($rcox) > 1) {

                    $rcox[0] = str_replace('You will receive half of your meals on: ', '', $rcox[0]);

                    $FirstDate = str_replace('<br></strong> ', '', $rcox[0]);

                    $FirstDay = explode(',', $FirstDate)[0];



                    $rcox[1] = str_replace(' the other half on: ', '', $rcox[1]);

                    $SecondDate = str_replace('<br></strong>', '', $rcox[1]);

                    $SecondDay = explode(',', $SecondDate)[0];

                } else {

                    $FirstDate = $rcox[0];

                    $FirstDay = explode(',', $FirstDate)[0];
                }

                $Crono = [$FirstDay => FormatDate($FirstDate)];

                if ($SecondDay !== '') {

                    $Crono[$SecondDay] = FormatDate($SecondDate);

                }

                $j = 1;

                foreach (explode('*', $DB[$countDB]->products) as $val) {
                    $arg = explode(',', $val);

                    if (isset($arg[0]) && isset($arg[1]) && isset($arg[2])) {
                        $product = $WC->get_product($arg[0]);
                        /*Validacion para que no se imprima adicionales de tabla orders_in*/
                        if(strpos($arg[1], "Subscription") ) {

                       

                          $IA = isAdd($Crono, $Ddb, $j, $arg[0], $arg[2], $date_from, $date_to, $order_items, [

                              'sku' => $product->sku,

                              'name' => $arg[1],

                              'price' => $product->get_price_html(),

                              'total' => floatval(0),

                              'image' => $product->get_image('shop_thumbnail'),

                              'resumen' => $product->get_image('shop_thumbnail') . '|<a target="_blank" href="' . get_permalink($arg[0]) . '">' . $arg[1] . '</a> ' . $product->get_price_html()

                          ]);

                          $order_items = $IA[0];

                          $j = $IA[1];
                        }
                        else if($DB[$countDB]->id == $primera_suscripcion[0]->id) {//Solo imprime si es primera semana suscripcion
                          $IA = isAdd($Crono, $Ddb, $j, $arg[0], $arg[2], $date_from, $date_to, $order_items, [

                              'sku' => $product->sku,

                              'name' => $arg[1],

                              'price' => $product->get_price_html(),

                              'total' => floatval(0),

                              'image' => $product->get_image('shop_thumbnail'),

                              'resumen' => $product->get_image('shop_thumbnail') . '|<a target="_blank" href="' . get_permalink($arg[0]) . '">' . $arg[1] . '</a> ' . $product->get_price_html()

                          ]);

                          $order_items = $IA[0];

                          $j = $IA[1];
                        }

                    }

                }
              
                if (isset($_POST['notes']) && strlen(trim($Notex)) > 0 || !isset($_POST['notes'])) {

                    foreach ($order_items as $key => $value) {

                        if (isset($Custom[$value['id']])) {

                            $order_items[$key]['custom'] = $Custom[$value['id']];

                        }

                    }
         
                    $Orders[] = [

                        'id' => $order->ID,

                        'Username' => $ord->get_user_id(),

                        'Name' => $order_meta['_billing_first_name'][0] . ' ' . $order_meta['_billing_last_name'][0],

                        'mail' => $order_meta['_billing_email'][0],

                        'phone' => $order_meta['_billing_phone'][0],

                        'Meals' => $order_items,

                        'notes' => $Notex,

                        'Address' => $Address,

                        'city' => $city,

                        'date' => $time,

                    ];
                }

              }
              /*Fin ciclo*/
            }
            
            else {
               
                if (strlen(trim($city)) === 0) {

                    $city = $order_meta['_billing_myfield18'][0];

                }if (strlen(trim($time)) === 0) {

                    $time = $order_meta['_billing_myfield18c'][0];

                }

                if ($time !== 'free') {

                    $Ddb = [];

                    $rcox = (explode('And', $time));

                    $SecondDay = '';

                      if (count($rcox) > 1) {

                          $rcox[0] = str_replace('You will receive half of your meals on: ', '', $rcox[0]);

                          $FirstDate = str_replace('<br></strong> ', '', $rcox[0]);

                          $FirstDay = explode(',', $FirstDate)[0];



                          $rcox[1] = str_replace(' the other half on: ', '', $rcox[1]);

                          $SecondDate = str_replace('<br></strong>', '', $rcox[1]);

                          $SecondDay = explode(',', $SecondDate)[0];

                      } else {

                          $FirstDate = $rcox[0];

                          $FirstDay = explode(',', $FirstDate)[0];

                      }


                      //$date_from
                      $Crono = [$FirstDay => FormatDate($FirstDate)];

                      if ($SecondDay !== '') {

                          $Crono[$SecondDay] = FormatDate($SecondDate);

                      }


                  

                    $j = 1;

                    $order_items = [];

                    foreach ($Orders_Items as $items_key => $items_value) {

                        $product = $WC->get_product($items_value['product_id']);

                        if (intval($items_value['qty']) > 1) {

                            $int = $j;

                            if (count($Crono) >= 2) {



                                $mitad = intval($items_value['qty']) / 2;



                                if (intval($items_value['qty']) % 2 == 0) {

                                    $fistqty = $mitad;

                                    $secodqty = $mitad;

                                } else {

                                    $fistqty = intval($mitad) + 1;

                                    $secodqty = intval($mitad);

                                }

                                for ($index = 1; $index <= intval($fistqty); $index++) {

                                    $Ddb[$int . $items_value['product_id']] = $FirstDay;

                                    $int++;

                                }

                                for ($index = 1; $index <= intval($secodqty); $index++) {

                                    $Ddb[$int . $items_value['product_id']] = $SecondDay;

                                    $int++;

                                }

                            } else {

                                for ($index = 1; $index <= intval($items_value['qty']); $index++) {

                                    $Ddb[$int . $items_value['product_id']] = $FirstDay;

                                    $int++;

                                }

                            }

                        } else {

                            $Ddb[$j . $items_value['product_id']] = $FirstDay;

                        }



                        if (count($Crono) < 2) {

                            $IA = isAdd($Crono, $Ddb, $j, $items_value['product_id'], $items_value['qty'], $date_from, $date_to, $order_items, [

                                'sku' => $product->sku,

                                'name' => $items_value['name'],

                                'price' => $product->get_price_html(),

                                'total' => floatval($items_value['line_total']),

                                'image' => $product->get_image('shop_thumbnail'),

                                'resumen' => $product->get_image('shop_thumbnail') . '|<a target="_blank" href="' . get_permalink($items_value['product_id']) . '">' . $items_value['name'] . '</a> ' . $product->get_price_html()

                            ]);

                            $order_items = $IA[0];

                            $j = $IA[1];

                        } else {

                            $IA = isAdd($Crono, $Ddb, $j, $items_value['product_id'], $fistqty, $date_from, $date_to, $order_items, [

                                'sku' => $product->sku,

                                'name' => $items_value['name'],

                                'price' => $product->get_price_html(),

                                'total' => floatval($items_value['line_total']),

                                'image' => $product->get_image('shop_thumbnail'),

                                'resumen' => $product->get_image('shop_thumbnail') . '|<a target="_blank" href="' . get_permalink($items_value['product_id']) . '">' . $items_value['name'] . '</a> ' . $product->get_price_html()

                            ]);

                            $order_items = $IA[0];

                            $j = $IA[1];

                            $IA = isAdd($Crono, $Ddb, $j, $items_value['product_id'], $secodqty, $date_from, $date_to, $order_items, [

                                'sku' => $product->sku,

                                'name' => $items_value['name'],

                                'price' => $product->get_price_html(),

                                'total' => floatval($items_value['line_total']),

                                'image' => $product->get_image('shop_thumbnail'),

                                'resumen' => $product->get_image('shop_thumbnail') . '|<a target="_blank" href="' . get_permalink($items_value['product_id']) . '">' . $items_value['name'] . '</a> ' . $product->get_price_html()

                            ]);

                            $order_items = $IA[0];

                            $j = $IA[1];

                        }

                    }

                }
                if (isset($_POST['notes']) && strlen(trim($Notex)) > 0 || !isset($_POST['notes'])) {

                    foreach ($order_items as $key => $value) {

                        if (isset($Custom[$value['id']])) {

                            $order_items[$key]['custom'] = $Custom[$value['id']];

                        }

                    }

                    //original
                    $Orders[] = [

                        'id' => $order->ID,

                        'Username' => $ord->get_user_id(),

                        'Name' => $order_meta['_billing_first_name'][0] . ' ' . $order_meta['_billing_last_name'][0],

                        'mail' => $order_meta['_billing_email'][0],

                        'phone' => $order_meta['_billing_phone'][0],

                        'Meals' => $order_items,

                        'notes' => $Notex,

                        'Address' => $Address,

                        'city' => $city,

                        'date' => $time,

                    ];

               

                }

            }

        }

        /*Si es diferente no hay ordenes
        Original*/
        $Second = $wpdb->get_results("SELECT * FROM $table_name WHERE orders != '-'");
        $wpdb->get_results("SELECT * FROM $table_name WHERE orders=" . $order->ID . " AND first_delivery_date BETWEEN '{$date_from}  00:00:00' AND '{$date_to} 23:59:59'");
        
        foreach ($Second as $ItemOrder) {

            if (!wc_get_order($ItemOrder->orders)) {
                continue;
            }

            if (date('d-m-Y', strtotime($ItemOrder->date)) >= date('d-m-Y', strtotime($date_from)) && date('d-m-Y', strtotime($ItemOrder->date)) <= date('d-m-Y', strtotime($date_to))) {
                $FirstDay = '';

                $FirstDate = '';

                $city = '';

                $time = '';

                $DBOrder = new WC_Order($ItemOrder->orders);

                $ord = wc_get_order($ItemOrder->orders);

                $WC = new WC_Product_Factory();

                $order_meta = get_post_meta($DBOrder->id);

                $Notex = $DBOrder->customer_message;

                $Address = $order_meta['_shipping_address_index'][0];

                //Validacion Shipping address si existe
                if($order_meta['_billing_myfield12'][0] == 0){
                  $Address =  $order_meta['_billing_first_name'][0] . " " . $order_meta['_billing_last_name'][0] . " " . $order_meta['_billing_myfield13'][0];
                  $Address .= " " . $order_meta['_billing_myfield16'][0] . " " . $order_meta['_billing_myfield16c'][0];
                  $Address .= " " . $order_meta['_billing_myfield17'][0];
                }

                if (strlen(trim($Address)) === 0) {

                    $Address = $order_meta['_billing_address_index'][0];

                }

                $Ddb = '';

                $SecondDay = '';

                $SecondDate = '';

                $city = $ItemOrder->city;

                $time = $ItemOrder->tim;

                $Ddb = $ItemOrder->delivery;

                $Ddb = explode(',', $Ddb);

                foreach ($Ddb as $key => $value) {

                    unset($Ddb[$key]);

                    $value = explode('=', $value);

                    if (isset($value[0]) && isset($value[1])) {

                        $Ddb[$value[0]] = $value[1];

                    }

                }

                $rcox = (explode('And', $time));

                if (count($rcox) > 1) {

                    $rcox[0] = str_replace('You will receive half of your meals on: ', '', $rcox[0]);

                    $FirstDate = str_replace('<br></strong> ', '', $rcox[0]);

                    $FirstDay = explode(',', $FirstDate)[0];



                    $rcox[1] = str_replace(' the other half on: ', '', $rcox[1]);

                    $SecondDate = str_replace('<br></strong>', '', $rcox[1]);

                    $SecondDay = explode(',', $SecondDate)[0];

                } else {

                    $FirstDate = $rcox[0];

                    $FirstDay = explode(',', $FirstDate)[0];

                }

                $Crono = [$FirstDay => FormatDate($FirstDate)];

                if ($SecondDay !== '') {

                    $Crono[$SecondDay] = FormatDate($SecondDate);

                }

                $order_items_second = [];

                $j = 1;

                foreach (explode('*', $ItemOrder->products) as $val) {
                    $arg = explode(',', $val);

                    if (!in_array("", $arg) & isset($arg[0]) && isset($arg[1]) && isset($arg[2])) {
                        $product = $WC->get_product($arg[0]);

                        $IA = isAdd($Crono, $Ddb, $j, $arg[0], $arg[2], $date_from, $date_to, $order_items_second, [

                            'sku' => $product->get_sku(),

                            'name' => $arg[1],

                            'price' => $product->get_price_html(),

                            'total' => floatval(0),

                            'image' => $product->get_image('shop_thumbnail'),

                            'resumen' => $product->get_image('shop_thumbnail') . '|<a target="_blank" href="' . get_permalink($arg[0]) . '">' . $arg[1] . '</a> ' . $product->get_price_html()

                        ]);

                        $order_items_second = $IA[0];

                        $j = $IA[1];

                    }

                }



                if (isset($_POST['notes']) && strlen(trim($Notex)) > 0 || !isset($_POST['notes'])) {

                    foreach ($order_items_second as $key => $value) {

                        if (isset($Custom[$value['id']])) {

                            $order_items_second[$key]['custom'] = $Custom[$value['id']];

                        }

                    }

                    // $SecondWeek[$DBOrder->ID] = [
                    $SecondWeek[] = [

                        'id' => $DBOrder->get_id(),

                        'Username' =>  $ItemOrder->user,

                        'Name' => $order_meta['_billing_first_name'][0] . ' ' . $order_meta['_billing_last_name'][0],

                        'mail' => $order_meta['_billing_email'][0],

                        'phone' => $order_meta['_billing_phone'][0],

                        'Meals' => $order_items_second,

                        'notes' => $Notex,

                        'Address' => $Address,

                        'city' => $city,

                        'date' => $time,

                    ];

                }

            }

        }




        /*Si no hay ordenes*/



        $My_Order = ['Sunday' => 'Sunday', 'Monday' => 'Monday', 'Wednesday' => 'Wednesday', 'Thursday' => 'Thursday'];



        if (!isset($_POST['Sunday'])) {

            unset($My_Order['Sunday']);

        }

        if (!isset($_POST['Monday'])) {

            unset($My_Order['Monday']);

        }

        if (!isset($_POST['Wednesday'])) {

            unset($My_Order['Wednesday']);

        }

        if (!isset($_POST['Thursday'])) {

            unset($My_Order['Thursday']);

        }

        foreach ($Orders as $key => $item) {

            $meales = [];

            foreach ($item['Meals'] as $kei => $meal) {

                if (strpos($meal['name'], 'meals per week') === false &&

                        strpos($meal['name'], 'dispatch') === false &&

                        $meal['name'] !== 'Delivery - 2 dispatch') {

                    $Add = true;

                    if (!isset($My_Order[$meal['day']])) {

                        $Add = false;

                    }

                    if (getCategoryName($meal['id']) === 'SUGAR FREE ENERGY DRINKS' && isset($_POST['drinks'])) {

                        $Add = false;

                    }

                    if ($Add) {

                        $meales[$kei] = $meal;

                    }

                }

            }




            if (count($meales) > 0) {

                $Orders[$key]['Meals'] = $meales;

            } else {

                unset($Orders[$key]);

            }



        }

        foreach ($SecondWeek as $key => $item) {

            $meales = [];

            foreach ($item['Meals'] as $kei => $meal) {

                if (strpos($meal['name'], 'meals per week') === false &&

                        strpos($meal['name'], 'dispatch') === false &&

                        $meal['name'] !== 'Delivery - 2 dispatch') {

                    $Add = true;

                    if (!isset($My_Order[$meal['day']])) {

                        $Add = false;

                    }

                    if (getCategoryName($meal['id']) === 'SUGAR FREE ENERGY DRINKS' && isset($_POST['drinks'])) {

                        $Add = false;

                    }

                    if ($Add) {

                        $meales[$kei] = $meal;

                    }

                }

            }



            //VAlidacion de orden
            $countOrd = count($Orders);

            for($i = 0; $i < $countOrd; $i++) {

              $existeRep = count(array_intersect($Orders[$i], $item));
            
              if($existeRep > 0) {
                break;
              }
              /*Si recorrio todas las posiciones y no hay repeticion se inserta*/
              if($i == $countOrd - 1) {
                $Orders[] = $item;

              }
            }

            if (count($meales) > 0) {

                $Orders['Meals'] = $meales;

            } else {
                /*Elimina ciertos pedidos*/
                //Orginal
                //unset($Orders[$key]);
            }

        }



        foreach($Orders as $key => $val){
            if(strlen(trim($val['id']))===0){
                unset($Orders[$key]);
            }
        }

        $Orders = array_values($Orders);
        $this->SaveJson('Delivery', $Orders);
        $this->form_footer('Delivery');
    }

    public function report_kitchen() {

        $this->form_header(true);

        global $wpdb;

        $table_name = $wpdb->prefix . "orders_in";

        $date_from = date('Y-m-j', strtotime('now', strtotime(date('Y-m-j', strtotime($_POST['desde'])))));

        $date_to = date('Y-m-j', strtotime('now', strtotime(date('Y-m-j', strtotime($_POST['hasta'])))));
        //fechas

        $post_status = implode("','", array('wc-processing', 'wc-completed', 'wc-cancelled'));

        if (isset($_POST['order'])) {

            if (strlen(trim(($_POST['order']))) === 0) {

                unset($_POST['order']);

            }

        }

        $FILTER = isset($_POST['order']) ? 'ID = ' . $_POST['order'] . ' AND ' : '';

        //Buscar posts desde el principio
        $today = date("Y-m-d");

        $result = $wpdb->get_results("SELECT * FROM $wpdb->posts

            WHERE " . $FILTER . "post_type = 'shop_order'

            AND post_status IN ('{$post_status}')

            AND post_date BETWEEN '{$date_from}  00:00:00' AND '{$today} 23:59:59'

        ");

        $Orders = [];

        foreach ($result as $key => $order) {

            $ord = wc_get_order($order->ID);

            $WC = new WC_Product_Factory();

            $order = new WC_Order($order->ID);

            $Orders_Items = $order->get_items();

            $Custom = [];

            $Str = '';

            foreach ($Orders_Items as $key => $value) {



                $Str = '';

                foreach ($value->get_formatted_meta_data() as $meta_id => $meta) {

                    if (wp_kses_post($meta->display_key) !== 'Ref') {

                        $Str .= '<strong class="wc-item-meta-label">' . wp_kses_post($meta->display_key) . ':</strong> ' . wp_kses_post($meta->display_value) . '<br>';

                    }

                }

                if (strlen(trim($Str)) > 0) {

                    $Str = '<hr style="margin: 3px;border: 1px solid #12e61a;"><strong>' . $value['name'] . ' x ' . $value['qty'] . '</strong><br>' . $Str;

                }

                if (isset($Custom[$value['product_id']])) {

                    if (!is_array($Custom[$value['product_id']])) {

                        $cont = $Custom[$value['product_id']];

                        $Custom[$value['product_id']] = array();



                        $Custom[$value['product_id']][] = $cont;

                        $Custom[$value['product_id']][] = $Str;

                    } else {

                        $Custom[$value['product_id']][] = $Str;

                    }

                } else {

                    $Custom[$value['product_id']] = $Str;

                }

            }

            foreach ($Custom as $idproduct => $elem) {

                if (is_array($elem)) {

                    $str = '';

                    foreach ($elem as $ele) {

                        $str .= $ele;

                    }

                    $Custom[$idproduct] = $str;

                }

            }

            /* End Update */

            //Original
            $date_from = date('Y-m-j', strtotime('next Saturday', strtotime(date('Y-m-j', strtotime($_POST['desde'])))));
            if($date_from > $date_to) {
                $date_to = date('Y-m-j', strtotime('+7 day', strtotime(date('Y-m-j', strtotime($_POST['hasta'])))));

            }
            $DB = $wpdb->get_results("SELECT * FROM $table_name WHERE orders=" . $order->ID . " AND first_delivery_date BETWEEN '{$date_from}  00:00:00' AND '{$date_to} 23:59:59'");

            $primera_suscripcion = $wpdb->get_results("SELECT * FROM $table_name WHERE orders=" . $order->ID . " AND first_delivery_date ORDER BY first_delivery_date ASC");

            $FirstDay = '';

            $FirstDate = '';

            $city = '';

            $time = '';

            $order_meta = get_post_meta($order->ID);

            $Address = $order_meta['_shipping_address_index'][0];

            if (strlen(trim($Address)) === 0) {

                $Address = $order_meta['_billing_address_index'][0];

            }

            if ($DB) {
              $lengthDB = count($DB);
              $order_items = [];
              for($countDB = 0; $countDB < $lengthDB; $countDB++){
                $Ddb = '';

                $SecondDay = '';

                $SecondDate = '';

                $city = $DB[$countDB]->city;

                $time = $DB[$countDB]->tim;

                $Ddb = $DB[$countDB]->delivery;

                $Ddb = explode(',', $Ddb);

                foreach ($Ddb as $key => $value) {

                    unset($Ddb[$key]);

                    $value = explode('=', $value);

                    if (isset($value[0]) && isset($value[1])) {

                        $Ddb[$value[0]] = $value[1];

                    }

                }

                $rcox = (explode('And', $time));

                if (count($rcox) > 1) {

                    $rcox[0] = str_replace('You will receive half of your meals on: ', '', $rcox[0]);

                    $FirstDate = str_replace('<br></strong> ', '', $rcox[0]);



                    $FirstDay = explode(',', $FirstDate)[0];



                    $rcox[1] = str_replace(' the other half on: ', '', $rcox[1]);

                    $SecondDate = str_replace('<br></strong>', '', $rcox[1]);

                    $SecondDay = explode(',', $SecondDate)[0];

                } else {

                    $FirstDate = $rcox[0];

                    $FirstDay = explode(',', $FirstDate)[0];

                }

                $Crono = [$FirstDay => FormatDate($FirstDate)];



                if ($SecondDay !== '') {

                    $Crono[$SecondDay] = FormatDate($SecondDate);

                }

                $j = 1;

                foreach (explode('*', $DB[$countDB]->products) as $val) {

                    $arg = explode(',', $val);

                    if (isset($arg[0]) && isset($arg[1]) && isset($arg[2])) {

                        $product = $WC->get_product($arg[0]);
                        /*Validacion para que no se imprima adicionales de tabla orders_in*/
                        if(strpos($arg[1], "Subscription") ) {


                            $IA = isAdd($Crono, $Ddb, $j, $arg[0], $arg[2], $date_from, $date_to, $order_items, [

                                'sku' => $product->sku,

                                'name' => $arg[1],

                                'price' => $product->get_price_html(),

                                'total' => floatval(0),

                                'image' => $product->get_image('shop_thumbnail'),

                                'resumen' => $product->get_image('shop_thumbnail') . '|<a target="_blank" href="' . get_permalink($arg[0]) . '">' . $arg[1] . '</a> ' . $product->get_price_html()

                            ]);

                            $order_items = $IA[0];

                            $j = $IA[1];
                        }
                        else if($DB[$countDB]->id == $primera_suscripcion[0]->id) {//Solo imprime si es primera semana suscripcion
                          $IA = isAdd($Crono, $Ddb, $j, $arg[0], $arg[2], $date_from, $date_to, $order_items, [

                              'sku' => $product->sku,

                              'name' => $arg[1],

                              'price' => $product->get_price_html(),

                              'total' => floatval(0),

                              'image' => $product->get_image('shop_thumbnail'),

                              'resumen' => $product->get_image('shop_thumbnail') . '|<a target="_blank" href="' . get_permalink($arg[0]) . '">' . $arg[1] . '</a> ' . $product->get_price_html()

                          ]);

                          $order_items = $IA[0];

                          $j = $IA[1];
                        }
                      }

               }
              }
            } else {

                if (strlen(trim($city)) === 0) {

                    $city = $order_meta['_billing_myfield18'][0];

                }if (strlen(trim($time)) === 0) {

                    $time = $order_meta['_billing_myfield18c'][0];

                }

                if ($time !== 'free') {

                    $Ddb = [];

                    $rcox = (explode('And', $time));

                    $SecondDay = '';

                    if (count($rcox) > 1) {

                        $rcox[0] = str_replace('You will receive half of your meals on: ', '', $rcox[0]);

                        $FirstDate = str_replace('<br></strong> ', '', $rcox[0]);

                        $FirstDay = explode(',', $FirstDate)[0];



                        $rcox[1] = str_replace(' the other half on: ', '', $rcox[1]);

                        $SecondDate = str_replace('<br></strong>', '', $rcox[1]);

                        $SecondDay = explode(',', $SecondDate)[0];

                    } else {

                        $FirstDate = $rcox[0];

                        $FirstDay = explode(',', $FirstDate)[0];

                    }



                    $Crono = [$FirstDay => FormatDate($FirstDate)];

                    if ($SecondDay !== '') {

                        $Crono[$SecondDay] = FormatDate($SecondDate);

                    }

                    $order_items = [];

                    $j = 1;

                    foreach ($Orders_Items as $items_key => $items_value) {



                        $product = $WC->get_product($items_value['product_id']);



                        if (intval($items_value['qty']) > 1) {

                            $int = $j;



                            if (count($Crono) >= 2) {



                                $mitad = intval($items_value['qty']) / 2;

                                if (intval($items_value['qty']) % 2 == 0) {

                                    $fistqty = $mitad;

                                    $secodqty = $mitad;

                                } else {

                                    $fistqty = intval($mitad) + 1;

                                    $secodqty = intval($mitad);

                                }

                                for ($index = 1; $index <= intval($fistqty); $index++) {

                                    $Ddb[$int . $items_value['product_id']] = $FirstDay;

                                    $int++;

                                }

                                for ($index = 1; $index <= intval($secodqty); $index++) {

                                    $Ddb[$int . $items_value['product_id']] = $SecondDay;

                                    $int++;

                                }

                            } else {

                                for ($index = 1; $index <= intval($items_value['qty']); $index++) {

                                    $Ddb[$int . $items_value['product_id']] = $FirstDay;

                                    $int++;

                                }

                            }

                        } else {

                            $Ddb[$j . $items_value['product_id']] = $FirstDay;

                        }





                        if (count($Crono) < 2) {



                            $IA = isAdd($Crono, $Ddb, $j, $items_value['product_id'], $items_value['qty'], $date_from, $date_to, $order_items, [

                                'sku' => $product->sku,

                                'name' => $items_value['name'],

                                'price' => $product->get_price_html(),

                                'total' => floatval($items_value['line_total']),

                                'image' => $product->get_image('shop_thumbnail'),

                                'resumen' => $product->get_image('shop_thumbnail') . '|<a target="_blank" href="' . get_permalink($items_value['product_id']) . '">' . $items_value['name'] . '</a> ' . $product->get_price_html()

                            ]);

                            $order_items = $IA[0];

                            $j = $IA[1];

                        } else {



                            $IA = isAdd($Crono, $Ddb, $j, $items_value['product_id'], $fistqty, $date_from, $date_to, $order_items, [

                                'sku' => $product->sku,

                                'name' => $items_value['name'],

                                'price' => $product->get_price_html(),

                                'total' => floatval($items_value['line_total']),

                                'image' => $product->get_image('shop_thumbnail'),

                                'resumen' => $product->get_image('shop_thumbnail') . '|<a target="_blank" href="' . get_permalink($items_value['product_id']) . '">' . $items_value['name'] . '</a> ' . $product->get_price_html()

                            ]);

                            $order_items = $IA[0];

                            $j = $IA[1];

                            $IA = isAdd($Crono, $Ddb, $j, $items_value['product_id'], $secodqty, $date_from, $date_to, $order_items, [

                                'sku' => $product->sku,

                                'name' => $items_value['name'],

                                'price' => $product->get_price_html(),

                                'total' => floatval($items_value['line_total']),

                                'image' => $product->get_image('shop_thumbnail'),

                                'resumen' => $product->get_image('shop_thumbnail') . '|<a target="_blank" href="' . get_permalink($items_value['product_id']) . '">' . $items_value['name'] . '</a> ' . $product->get_price_html()

                            ]);

                            $order_items = $IA[0];

                            $j = $IA[1];

                        }

                    }

                }

            } 

            foreach ($order_items as $key => $value) {
                if (isset($Custom[$value['id']])) {
                    $order_items[$key]['custom'] = $Custom[$value['id']];
                }
            }

            $Orders[] = [

                'id' => $order->ID,

                'Username' => $ord->get_user_id(),

                'Name' => $order_meta['_billing_first_name'][0] . ' ' . $order_meta['_billing_last_name'][0],

                'mail' => $order_meta['_billing_email'][0],

                'phone' => $order_meta['_billing_phone'][0],

                'Meals' => $order_items,

                'Catidad' => '',

                'Address' => $Address,

                'city' => $city,

                'date' => $time,

            ];
        }

        foreach ($Second as $ItemOrder) {
            if (date('d-m-Y', strtotime($ItemOrder->date)) >= date('d-m-Y', strtotime($date_from)) && date('d-m-Y', strtotime($ItemOrder->date)) <= date('d-m-Y', strtotime($date_to))) {
                $FirstDay = '';

                $FirstDate = '';

                $city = '';

                $time = '';

                $DBOrder = new WC_Order($ItemOrder->orders);

                $ord = wc_get_order($ItemOrder->orders);

                $WC = new WC_Product_Factory();

                $order_meta = get_post_meta($DBOrder->orders);

                $Notex = $DBOrder->customer_message;

                $Address = $order_meta['_shipping_address_index'][0];

                if (strlen(trim($Address)) === 0) {

                    $Address = $order_meta['_billing_address_index'][0];

                }

                $Ddb = '';

                $SecondDay = '';

                $SecondDate = '';

                $city = $ItemOrder->city;

                $time = $ItemOrder->tim;

                $Ddb = $ItemOrder->delivery;

                $Ddb = explode(',', $Ddb);

                foreach ($Ddb as $key => $value) {

                    unset($Ddb[$key]);

                    $value = explode('=', $value);

                    if (isset($value[0]) && isset($value[1])) {

                        $Ddb[$value[0]] = $value[1];

                    }

                }

                $rcox = (explode('And', $time));

                if (count($rcox) > 1) {

                    $rcox[0] = str_replace('You will receive half of your meals on: ', '', $rcox[0]);

                    $FirstDate = str_replace('<br></strong> ', '', $rcox[0]);

                    $FirstDay = explode(',', $FirstDate)[0];



                    $rcox[1] = str_replace(' the other half on: ', '', $rcox[1]);

                    $SecondDate = str_replace('<br></strong>', '', $rcox[1]);

                    $SecondDay = explode(',', $SecondDate)[0];

                } else {

                    $FirstDate = $rcox[0];

                    $FirstDay = explode(',', $FirstDate)[0];

                }

                $Crono = [$FirstDay => FormatDate($FirstDate)];

                if ($SecondDay !== '') {

                    $Crono[$SecondDay] = FormatDate($SecondDate);

                }

                $order_items_second = [];

                $j = 1;

                foreach (explode('*', $ItemOrder->products) as $val) {

                    $arg = explode(',', $val);

                    if (isset($arg[0]) && isset($arg[1]) && isset($arg[2])) {

                        $product = $WC->get_product($arg[0]);

                        $IA = isAdd($Crono, $Ddb, $j, $arg[0], $arg[2], $date_from, $date_to, $order_items_second, [

                            'sku' => $product->sku,

                            'name' => $arg[1],

                            'price' => $product->get_price_html(),

                            'total' => floatval(0),

                            'image' => $product->get_image('shop_thumbnail'),

                            'resumen' => $product->get_image('shop_thumbnail') . '|<a target="_blank" href="' . get_permalink($arg[0]) . '">' . $arg[1] . '</a> ' . $product->get_price_html()

                        ]);

                        $order_items_second = $IA[0];

                        $j = $IA[1];

                    }

                }

                if (isset($_POST['notes']) && strlen(trim($Notex)) > 0 || !isset($_POST['notes'])) {

                    foreach ($order_items_second as $key => $value) {

                        if (isset($Custom[$value['id']])) {

                            $order_items_second[$key]['custom'] = $Custom[$value['id']];

                        }

                    }

                    $SecondWeek[] = [

                        'id' => $DBOrder->ID,

                        'Username' => $ord->get_user_id(),

                        'Name' => $order_meta['_billing_first_name'][0] . ' ' . $order_meta['_billing_last_name'][0],

                        'mail' => $order_meta['_billing_email'][0],

                        'phone' => $order_meta['_billing_phone'][0],

                        'Meals' => $order_items_second,

                        'notes' => $Notex,

                        'Address' => $Address,

                        'city' => $city,

                        'date' => $time,

                    ];

                }

            }

        }



        $My_Meals = [];

        foreach ($Orders as $keyORD => $item) {

            foreach ($item['Meals'] as $key => $meal) {

                if (strpos($meal['name'], 'meals per week') === false &&

                        strpos($meal['name'], 'dispatch') === false &&

                        $meal['name'] !== 'Delivery - 2 dispatch') {


                  /*Validacion para que se visualicen custom meals diferentes en misma fecha*/
                  if($meal['custom'] != ""){
                      $My_Meals[] = $meal;

                  }
                  else {
                    if (isset($My_Meals[$key])) {

                        $_ = $My_Meals[$key];

                        $_['qty'] = intval($_['qty']) + intval($meal['qty']);

                        $_['total'] = floatval($_['total']) + floatval($meal['total']);

                        $My_Meals[$key] = $_;

                    } else {
                      $My_Meals[$key] = $meal;
                    }
                  }

                }

            }

        }

        foreach ($SecondWeek as $key => $item) {
            $meales = [];

            foreach ($item['Meals'] as $kei => $meal) {

                if (strpos($meal['name'], 'meals per week') === false &&

                        strpos($meal['name'], 'dispatch') === false &&

                        $meal['name'] !== 'Delivery - 2 dispatch') {

                    $Add = true;

                    if (!isset($My_Order[$meal['day']])) {

                        $Add = false;

                    }

                    if (getCategoryName($meal['id']) === 'SUGAR FREE ENERGY DRINKS' && isset($_POST['drinks'])) {

                        $Add = false;

                    }

                    if ($Add) {

                        $meales[$kei] = $meal;

                    }

                }

            }

            $Orders[$key] = $item;

            if (count($meales) > 0) {

                $Orders['Meals'] = $meales;

            } else {

                unset($Orders[$key]);

            }

        }

        foreach($Orders as $key => $val){

            if(strlen(trim($val['id']))===0){

                unset($Orders[$key]);

            }

        }

        $My_Order = ['Sunday', 'Monday', 'Wednesday', 'Thursday'];

        if (!isset($_POST['Sunday'])) {

            unset($My_Order[0]);

        }

        if (!isset($_POST['Monday'])) {

            unset($My_Order[1]);

        }

        if (!isset($_POST['Wednesday'])) {

            unset($My_Order[2]);

        }

        if (!isset($_POST['Thursday'])) {

            unset($My_Order[3]);

        }

        $MyMealsOrder = [];

        foreach ($My_Order as $day) {
            $PlatilloCustom =  [];
            foreach ($My_Meals as $key => $item) {

                if ($day === $item['day']) {

                    $Add = true;

                    if (getCategoryName($item['id']) === 'SUGAR FREE ENERGY DRINKS' && isset($_POST['drinks'])) {

                        $Add = false;

                    }

                    if ($Add) {

                        if(strpos($item['name'], "CUSTOM MEAL") !== false) {

                          $PlatilloCustom[$key] = $item;
                        }
                        else {
                          $MyMealsOrder[$key] = $item;
                        }

                    }

                }

            }

            $MyMealsOrder = array_merge($MyMealsOrder, $PlatilloCustom);
            unset($PlatilloCustom);

        }



        /**Ordenar**/

        /*Juntar Ordenes pay as you go con subscriptions
        Stuard Romero
        */
        //Ciclo para juntar comidas del mismo tipo
        foreach ($MyMealsOrder as $cod_fecha_actual => $orden_actual) {
          /*Ya existe el valor*/
          if(empty($auxMeals)) {
            $auxMeals[$cod_fecha_actual] = $orden_actual;
          }else {

            $cantidadAux = count($auxMeals);
            $contAux = 0;
            foreach ($auxMeals as $indexAux => $valorAux) {

              if($orden_actual['name'] == $valorAux['name'] &&
              $orden_actual['date'] == $valorAux['date']) {

                $auxMeals[$indexAux]['qty'] += $orden_actual['qty'];
                break;
              }



              if($contAux == $cantidadAux - 1) {
                $auxMeals[$cod_fecha_actual] = $orden_actual;
                break;
              }
              $contAux++;

            }
          }

        }
        $MyMealsOrder = $auxMeals;

        $this->SaveJson('Kitchen', $auxMeals);

        $this->form_footer('Kitchen');

    }


    public function report_sales() {

        $this->form_header(true);

        global $wpdb;

        $table_name = $wpdb->prefix . "orders_in";

        $date_from = $_POST['desde'];

        $date_to = $_POST['hasta'];

        $post_status = implode("','", array('wc-processing', 'wc-completed', 'wc-cancelled'));

        if (isset($_POST['order'])) {

            if (strlen(trim(($_POST['order']))) === 0) {

                unset($_POST['order']);

            }

        }

        $FILTER = isset($_POST['order']) ? 'ID = ' . $_POST['order'] . ' AND ' : '';

        $result = $wpdb->get_results("SELECT * FROM $wpdb->posts

            WHERE " . $FILTER . "post_type = 'shop_order'

            AND post_status IN ('{$post_status}')

            AND post_date BETWEEN '{$date_from}  00:00:00' AND '{$date_to} 23:59:59'

        ");

        $Orders = [];

        foreach ($result as $key => $order) {

            $ord = wc_get_order($order->ID);

            $WC = new WC_Product_Factory();



            $order = new WC_Order($order->ID);

            $Orders_Items = $order->get_items();

            /* Start Updated */

            $Custom = [];

            $Str = '';

            foreach ($Orders_Items as $key => $value) {

                $Str = '';
                foreach ($value->get_formatted_meta_data() as $meta_id => $meta) {

                    if (wp_kses_post($meta->display_key) !== 'Ref') {

                        $Str .= '<strong class="wc-item-meta-label">' . wp_kses_post($meta->display_key) . ':</strong> ' . wp_kses_post($meta->display_value) . '<br>';

                    }

                }

                if (strlen(trim($Str)) > 0) {

                    $Str = '<hr style="margin: 3px;border: 1px solid #12e61a;"><strong>' . $value['name'] . ' x ' . $value['qty'] . '</strong><br>' . $Str;

                }

                if (isset($Custom[$value['product_id']])) {

                    if (!is_array($Custom[$value['product_id']])) {

                        $cont = $Custom[$value['product_id']];

                        $Custom[$value['product_id']] = array();



                        $Custom[$value['product_id']][] = $cont;

                        $Custom[$value['product_id']][] = $Str;

                    } else {

                        $Custom[$value['product_id']][] = $Str;

                    }

                } else {

                    $Custom[$value['product_id']] = $Str;

                }

            }

            foreach ($Custom as $idproduct => $elem) {

                if (is_array($elem)) {

                    $str = '';

                    foreach ($elem as $ele) {

                        $str .= $ele;

                    }

                    $Custom[$idproduct] = $str;

                }

            }

            /* End Update */

            $DB = $wpdb->get_results("SELECT * FROM $table_name WHERE orders=" . $order->ID);

            $FirstDay = '';

            $FirstDate = '';

            $city = '';

            $time = '';

            $order_meta = get_post_meta($order->ID);

            $Notex = ($order->customer_message);

            $Address = $order_meta['_shipping_address_index'][0];

            if($order_meta['_billing_myfield12'][0] == 0){
                $Address =  $order_meta['_billing_first_name'][0] . " " . $order_meta['_billing_last_name'][0] . " " . $order_meta['_billing_myfield13'][0];
                $Address .= " " . $order_meta['_billing_myfield16'][0] . " " . $order_meta['_billing_myfield16c'][0];
                $Address .= " " . $order_meta['_billing_myfield17'][0];
            }

            if (strlen(trim($Address)) === 0) {
                $Address = $order_meta['_billing_address_index'][0];
            }

            if ($DB) {

                $Ddb = '';

                $SecondDay = '';

                $SecondDate = '';

                $city = $DB[0]->city;

                $time = $DB[0]->tim;

                $Ddb = $DB[0]->delivery;

                $Ddb = explode(',', $Ddb);

                foreach ($Ddb as $key => $value) {

                    unset($Ddb[$key]);

                    $value = explode('=', $value);

                    if (isset($value[0]) && isset($value[1])) {

                        $Ddb[$value[0]] = $value[1];

                    }

                }

                $rcox = (explode('And', $time));

                if (count($rcox) > 1) {

                    $rcox[0] = str_replace('You will receive half of your meals on: ', '', $rcox[0]);

                    $FirstDate = str_replace('<br></strong> ', '', $rcox[0]);

                    $FirstDay = explode(',', $FirstDate)[0];



                    $rcox[1] = str_replace(' the other half on: ', '', $rcox[1]);

                    $SecondDate = str_replace('<br></strong>', '', $rcox[1]);

                    $SecondDay = explode(',', $SecondDate)[0];

                } else {

                    $FirstDate = $rcox[0];

                    $FirstDay = explode(',', $FirstDate)[0];

                }

                $Crono = [$FirstDay => FormatDate($FirstDate)];

                if ($SecondDay !== '') {

                    $Crono[$SecondDay] = FormatDate($SecondDate);

                }

                $order_items = [];

                $j = 1;

                foreach (explode('*', $DB[0]->products) as $val) {

                    $arg = explode(',', $val);

                    if (isset($arg[0]) && isset($arg[1]) && isset($arg[2])) {

                        $product = $WC->get_product($arg[0]);

                        $IA = isAdd($Crono, $Ddb, $j, $arg[0], $arg[2], $date_from, $date_to, $order_items, [

                            'sku' => $product->sku,

                            'name' => $arg[1],

                            'price' => $product->get_price_html(),

                            'total' => floatval(0),

                            'image' => $product->get_image('shop_thumbnail'),

                            'resumen' => $product->get_image('shop_thumbnail') . '|<a target="_blank" href="' . get_permalink($arg[0]) . '">' . $arg[1] . '</a> ' . $product->get_price_html()

                                ], true);

                        $order_items = $IA[0];

                        $j = $IA[1];

                    }

                }

            } else {

                if (strlen(trim($city)) === 0) {

                    $city = $order_meta['_billing_myfield18'][0];

                }if (strlen(trim($time)) === 0) {

                    $time = $order_meta['_billing_myfield18c'][0];

                }

                if ($time !== 'free') {

                    $Ddb = [];

                    $rcox = (explode('And', $time));

                    $SecondDay = '';

                    if (count($rcox) > 1) {

                        $rcox[0] = str_replace('You will receive half of your meals on: ', '', $rcox[0]);

                        $FirstDate = str_replace('<br></strong> ', '', $rcox[0]);

                        $FirstDay = explode(',', $FirstDate)[0];


                        $rcox[1] = str_replace(' the other half on: ', '', $rcox[1]);

                        $SecondDate = str_replace('<br></strong>', '', $rcox[1]);

                        $SecondDay = explode(',', $SecondDate)[0];

                    } else {

                        $FirstDate = $rcox[0];

                        $FirstDay = explode(',', $FirstDate)[0];

                    }

                    $Crono = [$FirstDay => FormatDate($FirstDate)];

                    if ($SecondDay !== '') {

                        $Crono[$SecondDay] = FormatDate($SecondDate);

                    }

                    $j = 1;

                    $order_items = [];

                    foreach ($Orders_Items as $items_key => $items_value) {



                        $product = $WC->get_product($items_value['product_id']);



                        if (intval($items_value['qty']) > 1) {

                            $int = $j;

                            if (count($Crono) >= 2) {

                                $mitad = intval($items_value['qty']) / 2;

                                if (intval($items_value['qty']) % 2 == 0) {

                                    $fistqty = $mitad;

                                    $secodqty = $mitad;

                                } else {

                                    $fistqty = intval($mitad) + 1;

                                    $secodqty = intval($mitad);

                                }

                                for ($index = 1; $index <= intval($fistqty); $index++) {

                                    $Ddb[$int . $items_value['product_id']] = $FirstDay;

                                    $int++;

                                }

                                for ($index = 1; $index <= intval($secodqty); $index++) {

                                    $Ddb[$int . $items_value['product_id']] = $SecondDay;

                                    $int++;

                                }

                            } else {

                                for ($index = 1; $index <= intval($items_value['qty']); $index++) {

                                    $Ddb[$int . $items_value['product_id']] = $FirstDay;

                                    $int++;

                                }

                            }

                        } else {

                            $Ddb[$j . $items_value['product_id']] = $FirstDay;

                        }

                        if (count($Crono) < 2) {

                            $IA = isAdd($Crono, $Ddb, $j, $items_value['product_id'], $items_value['qty'], $date_from, $date_to, $order_items, [

                                'sku' => $product->sku,

                                'name' => $items_value['name'],

                                'price' => $product->get_price_html(),

                                'total' => floatval($items_value['line_total']),

                                'image' => $product->get_image('shop_thumbnail'),

                                'resumen' => $product->get_image('shop_thumbnail') . '|<a target="_blank" href="' . get_permalink($items_value['product_id']) . '">' . $items_value['name'] . '</a> ' . $product->get_price_html()

                                    ], true);
                                    
                            $order_items = $IA[0];

                            $j = $IA[1];

                        } else {

                            $IA = isAdd($Crono, $Ddb, $j, $items_value['product_id'], $fistqty, $date_from, $date_to, $order_items, [

                                'sku' => $product->sku,

                                'name' => $items_value['name'],

                                'price' => $product->get_price_html(),

                                'total' => floatval($items_value['line_total']),

                                'image' => $product->get_image('shop_thumbnail'),

                                'resumen' => $product->get_image('shop_thumbnail') . '|<a target="_blank" href="' . get_permalink($items_value['product_id']) . '">' . $items_value['name'] . '</a> ' . $product->get_price_html()

                                    ], true);

                            $order_items = $IA[0];

                            $j = $IA[1];

                            $IA = isAdd($Crono, $Ddb, $j, $items_value['product_id'], $secodqty, $date_from, $date_to, $order_items, [

                                'sku' => $product->sku,

                                'name' => $items_value['name'],

                                'price' => $product->get_price_html(),

                                'total' => floatval($items_value['line_total']),

                                'image' => $product->get_image('shop_thumbnail'),

                                'resumen' => $product->get_image('shop_thumbnail') . '|<a target="_blank" href="' . get_permalink($items_value['product_id']) . '">' . $items_value['name'] . '</a> ' . $product->get_price_html()

                                    ], true);

                            $order_items = $IA[0];

                            $j = $IA[1];

                        }

                    }

                }

            }

            foreach ($order_items as $key => $value) {

                if (isset($Custom[$value['id']])) {

                    $order_items[$key]['custom'] = $Custom[$value['id']];

                }

            }

            if (isset($_POST['notes']) && strlen(trim($Notex)) > 0 || !isset($_POST['notes'])) {

                foreach ($order_items as $key => $value) {

                    if (isset($Custom[$value['id']])) {

                        $order_items[$key]['custom'] = $Custom[$value['id']];

                    }

                }

                //original
                $Orders[$order->ID] = [

                    'id' => $order->ID,

                    'Username' => $ord->get_user_id(),

                    'Name' => $order_meta['_billing_first_name'][0] . ' ' . $order_meta['_billing_last_name'][0],

                    'mail' => $order_meta['_billing_email'][0],

                    'phone' => $order_meta['_billing_phone'][0],

                    'Meals' => $order_items,

                    'notes' => $Notex,

                    'Address' => $Address,

                    'city' => $city,

                    'date' => $time,

                ];


            }

        }

        $Second = $wpdb->get_results("SELECT * FROM $table_name WHERE orders != '-'");

        foreach ($Second as $ItemOrder) {

            if (!wc_get_order($ItemOrder->orders)) {
                continue;
            }

            if (date('d-m-Y', strtotime($ItemOrder->date)) >= date('d-m-Y', strtotime($date_from)) && date('d-m-Y', strtotime($ItemOrder->date)) <= date('d-m-Y', strtotime($date_to))) {

                $FirstDay = '';

                $FirstDate = '';

                $city = '';

                $time = '';

                $DBOrder = new WC_Order($ItemOrder->orders);

                $ord = wc_get_order($ItemOrder->orders);

                $WC = new WC_Product_Factory();

                $order_meta = get_post_meta($DBOrder->id);

                $Notex = $DBOrder->customer_message;

                $Address = $order_meta['_shipping_address_index'][0];

                //Validacion Shipping address si existe
                if($order_meta['_billing_myfield12'][0] == 0){
                    $Address =  $order_meta['_billing_first_name'][0] . " " . $order_meta['_billing_last_name'][0] . " " . $order_meta['_billing_myfield13'][0];
                    $Address .= " " . $order_meta['_billing_myfield16'][0] . " " . $order_meta['_billing_myfield16c'][0];
                    $Address .= " " . $order_meta['_billing_myfield17'][0];
                }

                if (strlen(trim($Address)) === 0) {

                    $Address = $order_meta['_billing_address_index'][0];

                }

                $Ddb = '';

                $SecondDay = '';

                $SecondDate = '';

                $city = $ItemOrder->city;

                $time = $ItemOrder->tim;

                $Ddb = $ItemOrder->delivery;

                $Ddb = explode(',', $Ddb);

                foreach ($Ddb as $key => $value) {

                    unset($Ddb[$key]);

                    $value = explode('=', $value);

                    if (isset($value[0]) && isset($value[1])) {

                        $Ddb[$value[0]] = $value[1];

                    }

                }

                $rcox = (explode('And', $time));

                if (count($rcox) > 1) {

                    $rcox[0] = str_replace('You will receive half of your meals on: ', '', $rcox[0]);

                    $FirstDate = str_replace('<br></strong> ', '', $rcox[0]);

                    $FirstDay = explode(',', $FirstDate)[0];



                    $rcox[1] = str_replace(' the other half on: ', '', $rcox[1]);

                    $SecondDate = str_replace('<br></strong>', '', $rcox[1]);

                    $SecondDay = explode(',', $SecondDate)[0];

                } else {

                    $FirstDate = $rcox[0];

                    $FirstDay = explode(',', $FirstDate)[0];

                }

                $Crono = [$FirstDay => FormatDate($FirstDate)];

                if ($SecondDay !== '') {

                    $Crono[$SecondDay] = FormatDate($SecondDate);

                }

                $order_items_second = [];

                $j = 1;

                foreach (explode('*', $ItemOrder->products) as $val) {

                    $arg = explode(',', $val);

                    if (isset($arg[0]) && isset($arg[1]) && isset($arg[2])) {

                        $product = $WC->get_product($arg[0]);

                        $IA = isAdd($Crono, $Ddb, $j, $arg[0], $arg[2], $date_from, $date_to, $order_items_second, [

                            'sku' => $product->sku,

                            'name' => $arg[1],

                            'price' => $product->get_price_html(),

                            'total' => floatval(0),

                            'image' => $product->get_image('shop_thumbnail'),

                            'resumen' => $product->get_image('shop_thumbnail') . '|<a target="_blank" href="' . get_permalink($arg[0]) . '">' . $arg[1] . '</a> ' . $product->get_price_html()

                        ]);

                        $order_items_second = $IA[0];

                        $j = $IA[1];

                    }

                }

                if (isset($_POST['notes']) && strlen(trim($Notex)) > 0 || !isset($_POST['notes'])) {

                    foreach ($order_items_second as $key => $value) {

                        if (isset($Custom[$value['id']])) {

                            $order_items_second[$key]['custom'] = $Custom[$value['id']];

                        }

                    }

                    $SecondWeek[$DBOrder->ID] = [

                        'id' => $DBOrder->ID,

                        'Username' => $ord->get_user_id(),

                        'Name' => $order_meta['_billing_first_name'][0] . ' ' . $order_meta['_billing_last_name'][0],

                        'mail' => $order_meta['_billing_email'][0],

                        'phone' => $order_meta['_billing_phone'][0],

                        'Meals' => $order_items_second,

                        'notes' => $Notex,

                        'Address' => $Address,

                        'city' => $city,

                        'date' => $time,

                    ];

                }

            }

        }

        $My_Order = ['Sunday' => 'Sunday', 'Monday' => 'Monday', 'Wednesday' => 'Wednesday', 'Thursday' => 'Thursday'];

        if (!isset($_POST['Sunday'])) {

            unset($My_Order['Sunday']);

        }

        if (!isset($_POST['Monday'])) {

            unset($My_Order['Monday']);

        }

        if (!isset($_POST['Wednesday'])) {

            unset($My_Order['Wednesday']);

        }

        if (!isset($_POST['Thursday'])) {

            unset($My_Order['Thursday']);

        }

        foreach ($Orders as $key => $item) {

            $meales = [];

            foreach ($item['Meals'] as $kei => $meal) {

                if (strpos($meal['name'], 'meals per week') === false &&

                        strpos($meal['name'], 'dispatch') === false &&

                        $meal['name'] !== 'Delivery - 2 dispatch') {

                    $Add = true;

                    if (!isset($My_Order[$meal['day']])) {

                        $Add = false;

                    }

                    if (getCategoryName($meal['id']) === 'SUGAR FREE ENERGY DRINKS' && isset($_POST['drinks'])) {

                        $Add = false;

                    }

                    if ($Add) {

                        $meales[$kei] = $meal;

                    }

                }

            }

            if (count($meales) > 0) {

                $Orders[$key]['Meals'] = $meales;

            } else {

                unset($Orders[$key]);

            }

        }

        foreach ($SecondWeek as $key => $item) {

            $meales = [];

            foreach ($item['Meals'] as $kei => $meal) {

                if (strpos($meal['name'], 'meals per week') === false &&

                        strpos($meal['name'], 'dispatch') === false &&

                        $meal['name'] !== 'Delivery - 2 dispatch') {

                    $Add = true;

                    if (!isset($My_Order[$meal['day']])) {

                        $Add = false;

                    }

                    if (getCategoryName($meal['id']) === 'SUGAR FREE ENERGY DRINKS' && isset($_POST['drinks'])) {

                        $Add = false;

                    }

                    if ($Add) {

                        $meales[$kei] = $meal;

                    }

                }

            }

            $Orders[$key] = $item;

            if (count($meales) > 0) {

                $Orders['Meals'] = $meales;

            } else {

                unset($Orders[$key]);

            }

        }

        foreach($Orders as $key => $val){

            if(strlen(trim($val['id']))===0){

                unset($Orders[$key]);

            }

        }

        $this->SaveJson('Sales', $Orders);

        $this->form_footer('Sales');

    }



    public function report_see_meals() {

        $this->form_header();

        global $wpdb;

        $table_name = $wpdb->prefix . "orders_in";

        $date_from = date('Y-m-j', strtotime('-7 days', strtotime(date('Y-m-j', strtotime($_POST['desde'])))));

        $date_to = $_POST['hasta'];   //Original

        $post_status = implode("','", array('wc-processing', 'wc-completed', 'wc-cancelled'));

        if (isset($_POST['order'])) {

            if (strlen(trim(($_POST['order']))) === 0) {

                unset($_POST['order']);

            }

        }

        $FILTER = isset($_POST['order']) ? 'ID = ' . $_POST['order'] . ' AND ' : '';

        $result = $wpdb->get_results("SELECT * FROM $wpdb->posts

            WHERE " . $FILTER . "post_type = 'shop_order'

            AND post_status IN ('{$post_status}')

            AND post_date BETWEEN '{$date_from}  00:00:00' AND '{$date_to} 23:59:59'

        ");

        foreach ($result as $key => $order) {
            
            $ord = wc_get_order($order->ID);

            $WC = new WC_Product_Factory();

            $order = new WC_Order($order->ID);

            $Orders_Items = $order->get_items();

            $Custom = [];

            $Str = '';

            foreach ($Orders_Items as $key => $value) {

                $Str = '';

                foreach ($value->get_formatted_meta_data() as $meta_id => $meta) {

                    if (wp_kses_post($meta->display_key) !== 'Ref') {

                        $Str .= '<strong class="wc-item-meta-label">' . wp_kses_post($meta->display_key) . ':</strong> ' . wp_kses_post($meta->display_value) . '<br>';

                    }

                }

                if (strlen(trim($Str)) > 0) {

                    $Str = '<hr style="margin: 3px;border: 1px solid #12e61a;"><strong>' . $value['name'] . ' x ' . $value['qty'] . '</strong><br>' . $Str;

                }

                if (isset($Custom[$value['product_id']])) {

                    if (!is_array($Custom[$value['product_id']])) {

                        $cont = $Custom[$value['product_id']];

                        $Custom[$value['product_id']] = array();



                        $Custom[$value['product_id']][] = $cont;

                        $Custom[$value['product_id']][] = $Str;

                    } else {

                        $Custom[$value['product_id']][] = $Str;

                    }

                } else {

                    $Custom[$value['product_id']] = $Str;

                }

            }

            foreach ($Custom as $idproduct => $elem) {

                if (is_array($elem)) {

                    $str = '';

                    foreach ($elem as $ele) {

                        $str .= $ele;

                    }

                    $Custom[$idproduct] = $str;

                }

            }



            $DB = $wpdb->get_results("SELECT * FROM $table_name WHERE orders=" . $order->ID . " AND first_delivery_date BETWEEN '{$date_from}  00:00:00' AND '{$date_to} 23:59:59'");

            $FirstDay = '';

            $FirstDate = '';

            $city = '';

            $time = '';

            $order_meta = get_post_meta($order->ID);

            $Notex = ($order->customer_message);

            $Address = $order_meta['_shipping_address_index'][0];

            if($order_meta['_billing_myfield12'][0] == 0){
              $Address =  $order_meta['_billing_first_name'][0] . " " . $order_meta['_billing_last_name'][0] . " " . $order_meta['_billing_myfield13'][0];
              $Address .= " " . $order_meta['_billing_myfield16'][0] . " " . $order_meta['_billing_myfield16c'][0];
              $Address .= " " . $order_meta['_billing_myfield17'][0];
            }


            if (strlen(trim($Address)) === 0) {

                $Address = $order_meta['_billing_address_index'][0];

            }

            if ($DB) {
              $lengthDB = count($DB);

              $order_items = [];
              for($countDB = 0; $countDB < $lengthDB; $countDB++){
                $Ddb = '';

                $SecondDay = '';

                $SecondDate = '';

                $city = $DB[$countDB]->city;

                $time = $DB[$countDB]->tim;


                $Ddb = $DB[$countDB]->delivery;

                $Ddb = explode(',', $Ddb);

                foreach ($Ddb as $key => $value) {

                    unset($Ddb[$key]);

                    $value = explode('=', $value);

                    if (isset($value[0]) && isset($value[1])) {

                        $Ddb[$value[0]] = $value[1];

                    }

                }
                /*Campo delivery*/

                /*Desfragmentando campo tim*/
                $rcox = (explode('And', $time));


                if (count($rcox) > 1) {

                    $rcox[0] = str_replace('You will receive half of your meals on: ', '', $rcox[0]);

                    $FirstDate = str_replace('<br></strong> ', '', $rcox[0]);

                    $FirstDay = explode(',', $FirstDate)[0];



                    $rcox[1] = str_replace(' the other half on: ', '', $rcox[1]);

                    $SecondDate = str_replace('<br></strong>', '', $rcox[1]);

                    $SecondDay = explode(',', $SecondDate)[0];

                } else {

                    $FirstDate = $rcox[0];

                    $FirstDay = explode(',', $FirstDate)[0];
                }



                $Crono = [$FirstDay => FormatDate($FirstDate)];

                if ($SecondDay !== '') {

                    $Crono[$SecondDay] = FormatDate($SecondDate);

                }


                $j = 1;

                foreach (explode('*', $DB[$countDB]->products) as $val) {


                    $arg = explode(',', $val);

                    if (isset($arg[0]) && isset($arg[1]) && isset($arg[2])) {


                        $product = $WC->get_product($arg[0]);


                          $IA = isAdd($Crono, $Ddb, $j, $arg[0], $arg[2], $date_from, $date_to, $order_items, [

                              'sku' => $product->sku,

                              'name' => $arg[1],

                              'price' => $product->get_price_html(),

                              'total' => floatval(0),

                              'image' => $product->get_image('shop_thumbnail'),

                              'resumen' => $product->get_image('shop_thumbnail') . '|<a target="_blank" href="' . get_permalink($arg[0]) . '">' . $arg[1] . '</a> ' . $product->get_price_html()

                          ]);


                          $order_items = $IA[0];



                          $j = $IA[1];


                    }

                }

                if (isset($_POST['notes']) && strlen(trim($Notex)) > 0 || !isset($_POST['notes'])) {

                    foreach ($order_items as $key => $value) {

                        if (isset($Custom[$value['id']])) {

                            $order_items[$key]['custom'] = $Custom[$value['id']];

                        }

                    }
                    
                    $ideal = new IdealNutrition();

                    $status_sub = $ideal->getSubscriptionStatus($ord->get_user_id(), 'active');
                    
                    if ($status_sub) {
                        $Orders[$order->ID] = [

                            'id' => $order->ID,
    
                            'Username' => $ord->get_user_id(),
    
                            'Name' => $order_meta['_billing_first_name'][0] . ' ' . $order_meta['_billing_last_name'][0],
    
                            'mail' => $order_meta['_billing_email'][0],
    
                            'phone' => $order_meta['_billing_phone'][0],
    
                            'Meals' => $order_items,
    
                            'notes' => $Notex,
    
                            'Address' => $Address,
    
                            'city' => $city,
    
                            'date' => $time,
    
                        ];
                    }

                }

              }
              /*Fin ciclo*/
            }



          }

        $My_Meals = [];

        foreach ($Orders as $key => $item) {

            $DB = $wpdb->get_results("SELECT * FROM $table_name WHERE orders=" . $key . " AND first_delivery_date ORDER BY first_delivery_date DESC");

            $city = '';

            $time = '';

            if (count($DB) > 0) {


                if (!isSelectMealsbyOrder($DB[0]->orders, $DB[0]->user)) {


                    $item['plan'] = '';

                    foreach ($item['Meals'] as $val) {

                        if (strlen(trim($item['plan'])) === 0) {

                            $item['plan'] = $val['name'];

                        }

                    }

                    if (strpos($item['plan'], 'Pay As Go You Plans') === false) {

                        if($DB[0]->first_delivery_date <= $date_to){
                          $item['DB'] = $DB[0];

                          $My_Meals[$key] = $item;
                        }

                    }

                }

            }

        }

        $this->SaveJson('Users', $My_Meals);

        $this->form_footer('Users');

    }
}
