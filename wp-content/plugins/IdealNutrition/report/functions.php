<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of functions
 *
 * @author yuber solution
 */
function getCategoryName($id) {
    global $post;
    $r = '';
    $terms = get_the_terms($id, 'product_cat');
    if (is_array($terms)) {
        foreach ($terms as $term) {
            $product_cat_id = $term->name;
            $r = $product_cat_id;
            break;
        }
    }
    return $r;
}

function StringtoDate($p) {
    return date('Y-m-d', strtotime($p));
}

function Crono($Crono, $date, $default) {
    if (isset($Crono[$date])) {
        return $Crono[$date];
    } else {
        return $default;
    }
}

function isAdd($Crono, $Ddb, $j, $id, $qty, $from, $to, $Cart, $Arg, $sales = false) {
    $cart = $Cart;
    $y = $j;
    //echo 'filter: ';
    //var_dump($j . $id);echo '<br>';
    if (isset($Ddb[$j . $id])) {

        if (intval($qty) > 1) {
            //
            for ($index = 1; $index <= intval($qty); $index++) {

                if (isset($Ddb[$y . $id])) {
                    $Day = $Ddb[$y . $id];
                    if (StringtoDate(Crono($Crono, $Day, $from)) >= StringtoDate($from) && StringtoDate(Crono($Crono, $Day, $from)) <= StringtoDate($to)) {
                        if (isset($cart[$Day . '_' . $id])) {
                            $aqty = $cart[$Day . '_' . $id]['qty'];
                            $aqty++;
                            $cart[$Day . '_' . $id]['qty'] = $aqty;
                        } else {
                            $cart[$Day . '_' . $id] = [
                                'id' => $id,
                                'sku' => $Arg['sku'],
                                'name' => $Arg['name'],
                                'price' => $Arg['price'],
                                'qty' => 1,
                                'total' => $Arg['total'],
                                'image' => $Arg['image'],
                                'day' => $Day,
                                'date' => Crono($Crono, $Day, $from),
                                'resumen' => $Arg['resumen']
                            ];
                        }
                    } else {
                        if ($sales) {
                            if (isset($cart[$Day . '_' . $id])) {
                                $aqty = $cart[$Day . '_' . $id]['qty'];
                                $aqty++;
                                $cart[$Day . '_' . $id]['qty'] = $aqty;
                            } else {
                                $cart[$Day . '_' . $id] = [
                                    'id' => $id,
                                    'sku' => $Arg['sku'],
                                    'name' => $Arg['name'],
                                    'price' => $Arg['price'],
                                    'qty' => 1,
                                    'total' => $Arg['total'],
                                    'image' => $Arg['image'],
                                    'day' => $Day,
                                    'date' => Crono($Crono, $Day, $from),
                                    'resumen' => $Arg['resumen']
                                ];
                            }
                        }
                    }
                    $y++;
                }
            }
        } else {
            $Day = $Ddb[$y . $id];
            if (StringtoDate(Crono($Crono, $Day, $from)) >= StringtoDate($from) && StringtoDate(Crono($Crono, $Day, $from)) <= StringtoDate($to)) {
                if (isset($cart[$Day . '_' . $id])) {
                    $aqty = $cart[$Day . '_' . $id]['qty'];
                    $aqty++;
                    $cart[$Day . '_' . $id]['qty'] = $aqty;
                } else {
                    $cart[$Day . '_' . $id] = [
                        'id' => $id,
                        'sku' => $Arg['sku'],
                        'name' => $Arg['name'],
                        'price' => $Arg['price'],
                        'qty' => 1,
                        'total' => $Arg['total'],
                        'image' => $Arg['image'],
                        'day' => $Day,
                        'date' => Crono($Crono, $Day, $from),
                        'resumen' => $Arg['resumen']
                    ];
                }
            } else {
                if ($sales) {
                    if (isset($cart[$Day . '_' . $id])) {
                        $aqty = $cart[$Day . '_' . $id]['qty'];
                        $aqty++;
                        $cart[$Day . '_' . $id]['qty'] = $aqty;
                    } else {
                        $cart[$Day . '_' . $id] = [
                            'id' => $id,
                            'sku' => $Arg['sku'],
                            'name' => $Arg['name'],
                            'price' => $Arg['price'],
                            'qty' => 1,
                            'total' => $Arg['total'],
                            'image' => $Arg['image'],
                            'day' => $Day,
                            'date' => Crono($Crono, $Day, $from),
                            'resumen' => $Arg['resumen']
                        ];
                    }
                }
            }
            $y++;
        }
    } else {
        //echo 'error';
    }
    //var_dump($cart);
    return [0 => $cart, 1 => $y];
}

function FormatDate($d) {
    $_MONTH_ = ['January' => '01', 'February' => '02', 'March' => '03', 'April' => '04', 'May' => '05', 'June' => '06', 'July' => '07', 'August' => '08', 'September' => '09', 'October' => '10', 'November' => '11', 'December' => '12'];
    $d = str_replace(' 10:00 AM ~ 08:00 PM', '', $d);
    $d = str_replace(' 06:00 PM ~ 10:00 PM', '', $d);
    $d = explode(', ', $d);
    $Day = explode(' ', $d[1])[1];
    $Month = explode(' ', $d[1])[0];
    
    $Year = str_replace(' ', '', $d[2]);
    return $_MONTH_[$Month] . '/' . $Day . '/' . $Year;
}

/*
Stuard Romero
Descripcion: Verificacion de platos por ordenar para siguiente semana.
20-05-2019
*/
function isSelectMealsbyOrder($id, $user) {

    global $wpdb;
    $table_name = $wpdb->prefix . "orders_in";
    $ID_User = $user;

    $today = date('d-m-Y');


    $date_from = date('Y-m-j', strtotime('next Saturday', strtotime(date('Y-m-j', strtotime($today)))));
    $date_to = date('Y-m-j', strtotime('next Friday', strtotime(date('Y-m-j', strtotime($date_from)))));


    //Se consulta la semana siguiente para verificar si solicito pedido
    $DB = $wpdb->get_results("SELECT * FROM $table_name WHERE orders=" . $id . " AND first_delivery_date BETWEEN '{$date_from}  00:00:00' AND '{$date_to} 23:59:59'");
    //$DB = $wpdb->get_results("SELECT * FROM $table_name WHERE orders=$id AND user=$ID_User");
    if (count($DB) === 0) {
        return false;
    } else {
        return true;
    }
}