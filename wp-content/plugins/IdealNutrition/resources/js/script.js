/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
let _dateOption = {
    Sunday: {
        req: Ajax(DateCJ('Sunday')),
        value: DateCJ('Sunday'),
    },
    Wednesday: {
        req: Ajax(DateCJ('Wednesday')),
        value: DateCJ('Wednesday'),
    },
    Monday: {
        req: Ajax(DateCJ('Monday')),
        value: DateCJ('Monday'),
    },
    Thursday: {
        req: Ajax(DateCJ('Thursday')),
        value: DateCJ('Thursday'),
    },
    run: (req)=>{
        let meday=jQuery('#label_'+req.day);
        if(parseInt(req.counters)===0){
            meday.find('input[type="radio"]').attr('disabled', 'disabled');
            meday.attr('data-av','no');
            jQuery('#'+meday.attr('lbl-br')).find('input[type="radio"]').attr('disabled', 'disabled');
        }else{
            meday.find('input[type="radio"]').removeAttr('disabled', 'disabled');
            meday.attr('data-av','yes');
            if(jQuery('#'+meday.attr('c-id')).attr('data-av')==='yes'){
                jQuery('#'+meday.attr('lbl-br')).find('input[type="radio"]').removeAttr('disabled', 'disabled');
            }else{
                jQuery('#'+meday.attr('lbl-br')).find('input[type="radio"]').attr('disabled', 'disabled');
            }
            //
        }
        meday.find('span.span_'+req.day).html(req.counters + ' Available')
    }
};
var _onErr = null;
var _onSucc = null;
var navbar = null;
var sticky = null;
var zUpdateQty = 0;
var zUpdatePrice = 0;
var ZProgressBar = 10;
var ZPriceMax = 1000;
var FormUniKey = null;
function wp_ajax(f) {
    var Ajax = jQuery(jQuery(f).find('input[name="ajax"]'));
    var Msgs = jQuery(jQuery(f).find('#alert'));
    if (Ajax) {
        jQuery.ajax({
            type: "post",
            url: Ajax.val(), // Pon aquí tu URL
            data: jQuery(f).serialize(),
            error: function (response) {
                console.log(response);
                if (window._onErr !== null) {
                    window._onErr(response);
                }
            },
            success: function (response) {
                console.log(response);
                if (window._onSucc !== null) {
                    window._onSucc(response);
                }
                Msgs.html("");
                Msgs.html(response.message);
            }
        });
    }

}
function zUpdate(e) {
    window.zUpdateQty = 0;
    window.zUpdatePrice = 0;
    var meals = '';
    var additional = '';
    jQuery('input.zUpdate').each(function () {
        if (jQuery(this).attr('step') === '1') {
            window.zUpdateQty = parseInt(window.zUpdateQty) + parseInt(jQuery(this).val());
            if (parseInt(jQuery(this).val()) > 0) {
                meals += jQuery(this).attr('data-id') + ',' + jQuery(this).attr('data-name') + ',' + jQuery(this).val() + '*';
            }
        }
    });
    //
    var isSelect = false;
    jQuery('.Select').each(function () {
        if (jQuery('#Quanty' + jQuery(this).attr('data-id')).attr('step') === '2') {
            window.zUpdatePrice = parseFloat(window.zUpdatePrice) + (parseFloat(jQuery('#Quanty' + jQuery(this).attr('data-id')).val()) * parseFloat(jQuery(this).attr('data-price')));
            if (parseInt(jQuery('#Quanty' + jQuery(this).attr('data-id')).val()) > 0) {
                additional += jQuery(this).attr('data-id') + ',' + jQuery(this).attr('data-name') + ',' + jQuery('#Quanty' + jQuery(this).attr('data-id')).val() + '*';
                isSelect = true;
            }

        }
    });
    ///console.log(jQuery('').attr('data-update'));
    if (parseInt(jQuery('#nextstepu').attr('data-update')) === 1) {
        if (isSelect) {
            // /Original
            /*jQuery('#nextstepu').attr('onclick', 'jQuery("#' + jQuery('#nextstepu').attr('data-key') + '").click();');
            jQuery('#nextstepu').html("Go Checkout");
            jQuery('#zCheckOut' + jQuery('#nextstepu').attr('data-key')).attr("action", jQuery('#zCheckOut' + jQuery('#nextstepu').attr('data-key')).attr("data-action-checkout"));*/
            jQuery('#nextstepu').attr('onclick', 'jQuery("#DeliDates").attr("style","display:table;");');
            jQuery('#nextstepu').html("Go Checkout");
            jQuery('#zCheckOut' + jQuery('#nextstepu').attr('data-key')).attr("action", jQuery('#zCheckOut' + jQuery('#nextstepu').attr('data-key')).attr("data-action-checkout"));
        } else {
            jQuery('#nextstepu').attr('onclick', 'jQuery("#DeliDates").attr("style","display:table;");');
            jQuery('#nextstepu').html("NEXT STEP");
            jQuery('#zCheckOut' + jQuery('#nextstepu').attr('data-key')).attr("action", jQuery('#zCheckOut' + jQuery('#nextstepu').attr('data-key')).attr("data-action-dashboard"));
        }
    }

    var Total = parseFloat(jQuery('#progress_price').attr('data-price-start')) + parseFloat(window.zUpdatePrice);
    var Prj = (Total / window.ZPriceMax) * 100;

    jQuery('.progress_process').attr('style', 'width: ' + Prj + '%;');
    jQuery('#Qty').html(window.zUpdateQty);
    jQuery('#progress_price').html(parseFloat(Total).toFixed(2));
    if (jQuery(e).attr('step') === '1') {
        if (parseInt(jQuery('#MaxQty').html()) <= parseInt(window.zUpdateQty)) {
            jQuery('#zPStep1').attr('class', 'zgrid actived MaxReady');
            //jQuery('#zPfixed').show(100);
            jQuery('#zPfixed').attr('class', 'zPfixed show');
        } else {
            jQuery('#zPStep1').attr('class', 'zgrid actived');
        }
    }

    window.FormUniKey = jQuery('#zCheckOutName').html();
    jQuery('#zCheckOut' + window.FormUniKey).find('input[name="meals"]').val(meals);
    jQuery('#zCheckOut' + window.FormUniKey).find('input[name="additional"]').val(additional);
}
function proccesssubmit(e) {
    jQuery('#message_date_time').html('');
    var proccess = true;
    var DD = jQuery('#DeliDates');
    var PU = jQuery(DD.find('.WinPopUp'));
    PU.attr('style', 'width: 100%;height: 100%;max-height: 260px;margin-top: 0px;padding: 10px 20px;background-color: #00ea00;max-width: 400px;');

    if (jQuery('#mealcity').val().length === 0) {
        jQuery('#message_date_time').append('<p style="text-align:  left;margin: 0px;font-size: 12px;">Delivery County is required</p>');
        PU.attr('style', 'width: 100%;height: 100%;max-height: 290px;margin-top: 0px;padding: 10px 20px;background-color: #00ea00;max-width: 400px;');
        proccess = false;
    }
    if (jQuery('#mealtime').val().length === 0) {
        jQuery('#message_date_time').append('<p style="text-align:  left;margin: 0px;font-size: 12px;">Day of Delivery is required</p>');
        PU.attr('style', 'width: 100%;height: 100%;max-height: 290px;margin-top: 0px;padding: 10px 20px;background-color: #00ea00;max-width: 400px;');
        proccess = false;
    }
    if(proccess){
        var Win = jQuery(jQuery('#DeliDates').find(".WinPopUp"));
        Win.hide(500);
        jQuery('#'+e).click();
    }
}
function ChangeCity(e) {
    var time = jQuery("select#time");
    time.find('option').remove().end().append('<option value="">Day of Delivery</option>').val('');
    jQuery('#mealtime').val("");
    jQuery('#mealcity').val("");
    let disabled = '';
    if (e === "Palm Beach County") {
        time.removeAttr('disabled');
        var ac = 'You will receive half of your meals on: <strong><br>' + _dateOption.Sunday.value + '<br></strong> And the other half on: <strong><br>' + _dateOption.Wednesday.value + '<br></strong>';
        if(zUpdateQty >= 18) {
            
            if(parseInt(_dateOption.Sunday.req.counters) === 0 || parseInt(_dateOption.Wednesday.req.counters) === 0 ){
                disabled = 'disabled';
            }
            time.append('<option value="' + ac + '" '+disabled+'>Recieve Your Order in Two Deliveries</option>');
            disabled = '';
        }
        disabled = parseInt(_dateOption.Sunday.req.counters) === 0 ? 'disabled' : '';
        time.append('<option value="' + _dateOption.Sunday.value + '" '+disabled+'>' + _dateOption.Sunday.value + ' ' +_dateOption.Sunday.req.counters+' Available</option>');
        disabled = parseInt(_dateOption.Wednesday.req.counters) === 0 ? 'disabled' : '';
        time.append('<option value="' + _dateOption.Wednesday.value + '" '+disabled+'>' + _dateOption.Wednesday.value + ' ' +_dateOption.Wednesday.req.counters+' Available</option>');
        jQuery('#mealcity').val(e);
    } else if (e === "Broward and Martin County") {
        time.removeAttr('disabled');
        var ac = 'You will receive half of your meals on: <strong><br>' + _dateOption.Monday.value + '<br></strong> And the other half on: <strong><br>' + _dateOption.Wednesday.value + '<br></strong>';
        if(zUpdateQty >= 18) {
            if(parseInt(_dateOption.Monday.req.counters) === 0 || parseInt(_dateOption.Wednesday.req.counters) === 0 ){
                disabled = 'disabled';
            }
            time.append('<option value="' + ac + '" '+disabled+'>Recieve Your Order in Two Deliveries</option>');
            disabled = '';
        }
        disabled = parseInt(_dateOption.Monday.req.counters) === 0 ? 'disabled' : '';
        time.append('<option value="' + _dateOption.Monday.value + '" '+disabled+'>' + _dateOption.Monday.value + ' ' +_dateOption.Monday.req.counters+' Available</option>');
        disabled = parseInt(_dateOption.Wednesday.req.counters) === 0 ? 'disabled' : '';
        time.append('<option value="' + _dateOption.Wednesday.value + '" '+disabled+'>' + _dateOption.Wednesday.value + ' ' +_dateOption.Wednesday.req.counters+' Available</option>');
        jQuery('#mealcity').val(e);
    }
    else if (e === "Store Pick Up") {
        time.removeAttr('disabled');
        disabled = parseInt(_dateOption.Sunday.req.counters) === 0 ? 'disabled' : '';
        time.append('<option value="' + _dateOption.Sunday.value + '" '+disabled+'>' + _dateOption.Sunday.value + ' ' +_dateOption.Sunday.req.counters+' Available</option>');
        disabled = parseInt(_dateOption.Monday.req.counters) === 0 ? 'disabled' : '';
        time.append('<option value="' + _dateOption.Monday.value + '" '+disabled+'>' + _dateOption.Monday.value + ' ' +_dateOption.Monday.req.counters+' Available</option>');
        disabled = parseInt(_dateOption.Wednesday.req.counters) === 0 ? 'disabled' : '';
        time.append('<option value="' + _dateOption.Wednesday.value + '" '+disabled+'>' + _dateOption.Wednesday.value + ' ' +_dateOption.Wednesday.req.counters+' Available</option>');
        jQuery('#mealcity').val(e);
    }
    else {
        time.attr('disabled', "disabled");
    }
}
function ChangeTime(e) {
  console.log('ChangeTime:');
  console.log(e);
    jQuery('#message_date_time').html("");
    jQuery('#mealtime').val(e);
    var DD = jQuery('#DeliDates');
    var PU = jQuery(DD.find('.WinPopUp'));
    PU.attr('style', 'width: 100%;height: 100%;max-height: 260px;margin-top: 0px;padding: 10px 20px;background-color: #00ea00;max-width: 400px;');
    if (e === 'You will receive half of your meals on: <strong><br>' + DateCJ('Sunday') + '<br></strong> And the other half on: <strong><br>' + DateCJ('Wednesday') + '<br></strong>') {
        jQuery('#message_date_time').append('<p>Meals selected are randomized, first half of your meals will be delivered on <b>' + DateCJ('Sunday') + '</b>, second half on ' +
                '<b>' + DateCJ('Wednesday') + '</b>. You can view this information on your dashboard under the delivery information tab.</p>');
        PU.attr('style', 'width: 100%;height: 100%;max-height: 425px;margin-top: 0px;padding: 10px 20px;background-color: #00ea00;max-width: 400px;');
    }
    if (e === 'You will receive half of your meals on: <strong><br>' + DateCJ('Monday') + '<br></strong> And the other half on: <strong><br>' + DateCJ('Wednesday') + '<br></strong>') {
        jQuery('#message_date_time').append('<p>Meals selected are randomized, first half of your meals will be delivered on <b>' + DateCJ('Monday') + '</b>, second half on ' +
                '<b>' + DateCJ('Wednesday') + '</b>. You can view this information on your dashboard under the delivery information tab.</p>');
        PU.attr('style', 'width: 100%;height: 100%;max-height: 425px;margin-top: 0px;padding: 10px 20px;background-color: #00ea00;max-width: 400px;');
    }
    if (e === DateCJ('Sunday')) {
        jQuery('#message_date_time').append('<p>Your meals will be delivered on <b>' + DateCJ('Sunday') + '</b> between the hours of 10am and 8pm.</p>');
        PU.attr('style', 'width: 100%;height: 100%;max-height: 350px;margin-top: 0px;padding: 10px 20px;background-color: #00ea00;max-width: 400px;');
    }
    if (e === DateCJ('Wednesday')) {
        jQuery('#message_date_time').append('<p>Your meals will be delivered on <b>' + DateCJ('Wednesday') + '</b> between the hours of 10am and 8pm.</p>');
        PU.attr('style', 'width: 100%;height: 100%;max-height: 350px;margin-top: 0px;padding: 10px 20px;background-color: #00ea00;max-width: 400px;');
    }
    if (e === DateCJ('Monday')) {
        jQuery('#message_date_time').append('<p>Your meals will be delivered on <b>' + DateCJ('Monday') + '</b> between the hours of 10am and 8pm.</p>');
        PU.attr('style', 'width: 100%;height: 100%;max-height: 350px;margin-top: 0px;padding: 10px 20px;background-color: #00ea00;max-width: 400px;');
    }
    if (e === DateCJ('Thursday')) {
        jQuery('#message_date_time').append('Your meals will be delivered on <b>' + DateCJ('Thursday') + '</b> between the hours of 10am and 8pm.</p>');
        PU.attr('style', 'width: 100%;height: 100%;max-height: 350px;margin-top: 0px;padding: 10px 20px;background-color: #00ea00;max-width: 400px;');
    }
}
(function ($) {
    $(document).ready(function () {
        $('input.zUpdate').each(function () {
            $(this).change(function () {///console.log(window.zUpdateQty);
                window.zUpdate('#' + $(this).attr("id"));
            });
        });
        window.navbar = document.getElementById('zMoveSticky');
        if (window.navbar) {
            window.sticky = window.navbar.offsetTop;
            window.onscroll = function () {
                //if(window.Screen.width<500){
                //alert(window.Screen.width);
                //}
                //console.log(window.pageYOffset + ' >= ' + window.sticky);
                if (window.pageYOffset >= window.sticky) {
                    window.navbar.classList.add("zMoveSticky");
                    if ($('.zpDashControl').attr('ready') === '1') {
                        $('.zpDashControl').attr('class', 'zpDashControl zpDashControlFixed');
                        $('.zpDashControl').show(500);
                    } else {
                        $('.btnzCupon').show(500);
                    }
                    ;
                } else {
                    window.navbar.classList.remove("zMoveSticky");

                    if ($('.zpDashControl').attr('ready') === '1') {
                        $('.zpDashControl').attr('class', 'zpDashControl');
                    } else {
                        $('.btnzCupon').hide(500);
                    }
                    ;
                }
            };
        }

        $('.btncoupon').each(function () {
            $('#' + $(this).attr('id')).on("click", function () {
                window._onSucc = function (e) {
                    if (parseInt(e.meals) > 0) {
                        $('#MaxQty').html(parseInt($('#MaxQty').html()) + parseInt(e.meals));
                        $('.zUpdate').each(function () {
                            $(this).attr('max', $('#MaxQty').html());
                        });
                    }
                    setTimeout(function () {
                        $('.in-cross').click();
                    }, 2000);
                };
                wp_ajax($('#verycoupon'));
            });
        });
    });
}
)(jQuery);
