<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function _z_add_css_styles() {
    wp_enqueue_style('zStyles', z_PLUGIN_URL . 'resources/css/styles.css?' . z_VERSION);
}

function __zload_CSS() {
    add_action('wp_print_styles', '_z_add_css_styles');
}

function _z_get_javascript() {
    $ROOT = z_PLUGIN_URL . 'resources/js/';
    $N_S = 'zD_';

//wp_enqueue_script('ajax_test');

    wp_register_script($N_S . 'script-ui', $ROOT . 'script.js?' . z_VERSION . uniqid(), array('jquery'), '1', true);
    wp_enqueue_script($N_S . 'script-ui');
}

function __zload_JS() {
    add_action("wp_enqueue_scripts", "_z_get_javascript");
}

function __zload_enabled_empty_cart() {
    $Checked = _zCheckCart();
    $meals = new SelectYourMeals(TRUE);
    $yes = $meals->PopUpLocked();
    if ($yes) {
        ?>
        <script type="text/javascript">
            jQuery(document).ready(function () {
                jQuery('#menu-item-630').remove();
                jQuery('.woocommerce-MyAccount-navigation-link--subscriptions').remove();
                jQuery('.woocommerce-MyAccount-navigation-link--downloads').remove();
            });
        </script>
        <?php
    }
    if ($Checked) {
        $isSubscriptions = $Checked['isSubscriptions'];
        if ($yes) {
            ?>
            <script type="text/javascript">
                jQuery(document).ready(function () {
                    jQuery('#woocommerce-MyAccount-navigation-link--downloads').remove();
                    jQuery('a.reactivate').remove();
                    var body = jQuery('body.woocommerce-cart');
                    if (body.length === 0) {
                        body = jQuery('body.woocommerce-checkout');
                    }
                    if (body.length > 0) {
                        body.find('#content').html('<?= $yes ?>');
                    }
                });
            </script>
            <?php
        }
    } else {
        $isSubscriptions = false;
    }
    ?>
    <style>
        a.button.reactivate{display:none !important;}
        tr.fee td{text-align: right !important;}
        span.pupupcheckout {
            cursor: pointer;
            color: #2b6b2d;
            font-weight: 900;
        }
        span.pupupcheckout:hover {
            color: #0dda15;
        }
        div#popupcheckout {

            position: fixed;
            top: 0px;
            left: 0px;
            bottom: 0px;
            right: 0px;
            z-index: 99999;
            background-color: #2d2d2d91;
            height:100%;
            width:100%;

            overflow:hidden;
            display:none;
        }
        div#popupcheckout.show{transition: all 0.3s ease-out;display:table !important;}
        div#popupcheckout .contenedor{
            display:table-cell;
            width:100%;
            vertical-align:middle;
            text-align:center;
        }
        div#popupcheckout .popupcheckoutwindow {
            background-color: #00ea00;
            border-radius: 5px;
            width: 100%;
            max-width: 465px;
            padding: 30px;
            display: inline-block;
            margin: 0 auto;
        }
        div#popupcheckout .popupcheckoutwindow div{
            max-width: 200px;
            margin: 0 auto;
        }
        div#popupcheckout .popupcheckoutwindow p{display:none;}
        .Palm.All p.Palm.All,.Broward.All p.Broward.All,
        .Sunday .Sunday,
        .Wednesday .Wednesday,
        .Monday .Monday,
        .Thursday .Thursday{display: block !important;}
        a.ywgc-show-giftcard {
            color: #fff;
            background: #1ce934;
            padding: 10px 15px;
            border-radius: 3px;
            white-space: nowrap;
        }
        a.showcoupon {
            background-color: #1ce934;
            padding: 10px 15px;
            margin-left: 10px;
            color: #fff;
            border-radius: 3px;
            white-space: nowrap;
        }
        
    </style>
    <?php if ($Checked) { ?>
        <style>
            .mini-cart-wrap .mini_cart_item .remove {
                display: none;
            }
        </style>
    <?php } ?>
    <script type="text/javascript">
        let Ajax =(datecj)=>{
            return JSON.parse(jQuery.ajax({
                type: "POST", 
                url: "<?= admin_url('admin-ajax.php'); ?>",
                data: {
                    action: 'get_limit_orders',
                    datecj: datecj
                },
                cache: false,
                async: false
            }).responseText);
        }
        var aproved;
        var DateCJ = function (w) {
            //acá con new Date() toma la hora y fecha
            var f = new Date();
            var h = '';
            var m = new Array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");
            //            0         1          2          3             4         5           6
            var k = new Array("Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday");
            var d = 0, a = false, b = false, j = 0, x = 0;
            var u = new Array();
            var t = false;
            if (k.indexOf(w) === -1) {
                return false;
            }
            while (!a && x !== 60 || !b && x !== 60) {
                //console.log(w +'===' +week[j]+' && '+!a);
                if (w === k[j] && !a) {
                    a = j + 1;
                    //1 4
                }
                //console.log(week[f.getDay()]+ '==='+ week[j]);
                if (k[f.getDay()] === k[j] && !b) {
                    //var
                    b = 7 - (j + 1);

                    if (b === 0) {
                        b = 99;
                    }
                }
                j++;
                x++;
            }
            if (a === 1 && b === 99) {
                a = 1;
                b = 7;
            }
            if (b === 99) {
                b = 0;
            }
            d = (a + b);
            if (k[f.getDay()] === 'Saturday') {
                t = true;
            }
            f.setSeconds((d * 86400));
            if (k[f.getDay()] !== 'Sunday' && t) {
                f.setSeconds((7 * 86400));
            }
            if (k[f.getDay()] === 'Wednesday' || k[f.getDay()] === 'Sunday') {
                h = '10:00 AM ~ 08:00 PM';
            }
            if (k[f.getDay()] === 'Thursday' || k[f.getDay()] === 'Monday') {
                h = '06:00 PM ~ 10:00 PM';
            }
            return  k[f.getDay()] + ", " + m[f.getMonth()] + " " + f.getDate() + ", " + f.getFullYear() + ' ' + h;
        };
        let _dates = {
            Sunday: {
                req: Ajax(DateCJ('Sunday')),
                value: DateCJ('Sunday'),
            },
            Wednesday: {
                req: Ajax(DateCJ('Wednesday')),
                value: DateCJ('Wednesday'),
            },
            Monday: {
                req: Ajax(DateCJ('Monday')),
                value: DateCJ('Monday'),
            },
            Thursday: {
                req: Ajax(DateCJ('Thursday')),
                value: DateCJ('Thursday'),
            },
            run: (req)=>{
                let meday=jQuery('#label_'+req.day);
                if(parseInt(req.counters)===0){
                    meday.find('input[type="radio"]').attr('disabled', 'disabled');
                    meday.attr('data-av','no');
                    jQuery('#'+meday.attr('lbl-br')).find('input[type="radio"]').attr('disabled', 'disabled');
                    //jQuery('#'+meday.attr('lbl-br')).find('input[type="radio"]').prop('checked', false);
                }else{
                    meday.find('input[type="radio"]').removeAttr('disabled', 'disabled');
                    meday.attr('data-av','yes');
                    if(jQuery('#'+meday.attr('c-id')).attr('data-av')==='yes'){
                        jQuery('#'+meday.attr('lbl-br')).find('input[type="radio"]').removeAttr('disabled', 'disabled');
                    }else{
                        jQuery('#'+meday.attr('lbl-br')).find('input[type="radio"]').attr('disabled', 'disabled');
                        //jQuery('#'+meday.attr('lbl-br')).find('input[type="radio"]').prop('checked', false);
                    }
                    //
                }
                meday.find('span.span_'+req.day).html(req.counters + ' Available');
            }
        }
        jQuery(document).ready(function () {
            setInterval(function () {
                aproved = true;
                jQuery('abbr.required').each(function () {
                    var id = jQuery(this).parents('.form-row').attr('id').replace('_field', '');
                    if (jQuery('#' + id).val() === '') {
                      aproved = false;
                    }
                });
                if (!jQuery('input[name="billing_myfield18c"]:checked').length) {
                    if(jQuery('#billing_myfield18_field').attr('class')!=='form-row ocultar-campo campo-ciudad'){
                        aproved = false;
                    }
                    
                }

                /*Validacion Campos Shipping*/
                if (!jQuery('input[name="billing_myfield12"]:checked').length) {

                  aproved = false;
                  //Campos Street Address y City
                  //console.log("Street: "+jQuery('input[name="billing_myfield13"]').val().length);
                  //console.log("City: "+jQuery('input[name="billing_myfield16"]').val().length);
                  if(jQuery('input[name="billing_myfield13"]').val() && jQuery('input[name="billing_myfield16"]').val()) {
                    var billing_myfield13 = jQuery('input[name="billing_myfield13"]').val();
                    var billing_myfield16 = jQuery('input[name="billing_myfield16"]').val();
                    if(billing_myfield13.length && billing_myfield16.length) {


                      //console.log("Select: "+jQuery('#select2-billing_myfield16c-container').html());
                      if(jQuery('#select2-billing_myfield16c-container').html() != "Select a state…") {

                        if(jQuery('input[name="billing_myfield17"]').val().length) {
                            aproved = true;
                          //console.log("Ya has llenado todos los campos");
                            if (!jQuery('input[name="billing_myfield18c"]:checked').length) {
                                if(jQuery('#billing_myfield18_field').attr('class')!=='form-row ocultar-campo campo-ciudad'){
                                    aproved = false;
                                }                                
                            }
                        }

                      }
                    }
                  }

                    //aproved = false;
                }
                /*Limpiando los campos*/
                else {
                  jQuery('input[name="billing_myfield13"]').val('');
                  jQuery('input[name="billing_myfield16"]').val('');


                  jQuery('input[name="billing_myfield16c"]').val('');
                  jQuery('#select2-billing_myfield16c-container').html('Select a state…')

                  jQuery('input[name="billing_myfield17"]').val('');
                }


                var btn_place = jQuery('#place_order');
                var older = jQuery('#help_place_older');
                if (older.length === 0) {
                    var anun = jQuery('#payment');
                    anun.append('<div id="help_place_older" style="text-align:  center;">You need to complete all the required fields (*).</div>');
                    older = jQuery('#help_place_older');
                }

                var valDeliveryCounty = jQuery('#billing_myfield18 option:selected').val();

                /*if(valDeliveryCounty == "Store Pick Up"){
                  aproved = true;
                }*/

                if (aproved) {
                    btn_place.removeAttr('disabled');
                    older.hide();
                } else {
                    //console.log(btn_place.html());
                    if (btn_place.html() !== 'Add payment method') {
                        btn_place.attr('disabled', '');
                        older.show();
                    }
                }

            }, 1000);
    <?php if ($Checked) { ?>
                jQuery('.product-remove').each(function () {
                    jQuery(this).remove();
                });
                /*jQuery('a.remove').each(function(){
                 jQuery(this).remove();
                 });*/
    <?php } ?>
            if (jQuery('[for="billing_city"]')) {
                setInterval(function () {
                    jQuery('[for="billing_city"]').html('City <abbr class="required" title="required">*</abbr>');
                }, 500);
            }
            var ca = jQuery('#billing_myfield12_checkbox');
            if (ca) {
                ca.click(function () {
                    if (jQuery(this).attr('checked')) {
                        jQuery('#billing_myfield13_field').hide();
                        jQuery('#billing_myfield14_field').hide();
                        //jQuery('#billing_myfield15_field').hide();
                        jQuery('#billing_myfield16_field').hide();
                        jQuery('#billing_myfield16c_field').hide();
                        jQuery('#billing_myfield17_field').hide();
                    } else {
                        jQuery('#billing_myfield13_field').show();
                        jQuery('#billing_myfield14_field').show();
                        //jQuery('#billing_myfield15_field').show();
                        jQuery('#billing_myfield16_field').show();
                        jQuery('#billing_myfield16c_field').show();
                        jQuery('#billing_myfield17_field').show();
                    }
                });
            }

            jQuery('#billing_myfield18').change(function () {

              var valDeliveryCounty = jQuery('#billing_myfield18 option:selected').val();
              console.log(valDeliveryCounty);
              //alert(btn_place);
              //Cambiar fee delivery en ajax cuando es recogida local
              //if(valDeliveryCounty != "Store Pick Up"){

                var items = '#billing_myfield18c_field';
                //Mostrar campo
                jQuery('#billing_myfield18c_field fieldset').css("display", "block");;

                var fieldset = jQuery(items).find('fieldset');
                var legend = '<legend>Day of Delivery <abbr class="required" title="required">*</abbr></legend>';
                fieldset.html('');
                var item = jQuery('#billing_myfield18c');
                item.empty().trigger("change");
                if (jQuery(this).val() === 'Palm Beach County') {

                    //var ac = DateCJ('Sunday') + ' And ' + DateCJ('Wednesday');
                    var ac = 'You will receive half of your meals on: <strong><br>' + _dates.Sunday.value + '<br></strong> And the other half on: <strong><br>' + _dates.Wednesday.value + '<br></strong>';
                    //var Sunday = new Option(DateCJ('Sunday'), DateCJ('Sunday'), false, false);
                    //var Wednesday = new Option(DateCJ('Wednesday'), DateCJ('Wednesday'), false, false);
                    fieldset.append(legend);
    <?php if (WC()->cart->get_cart_contents_count() >= 18) { ?>
                        //fieldset.append('<label style="font-size: 14px;"><input type="radio" name="billing_myfield18c" value="' + ac + '"> <span onclick="jQuery(\'#popupcheckout\').show();jQuery(\'#popupcheckout\').attr(\'class\',\'show Palm All\');" class="pupupcheckout IdealNutrition in-help"></span> Recieve Your Order in Two Deliveries</label>');
                        fieldset.append('<label id="label_Sunday_and_Wednesday" style="font-size: 14px;"><input type="radio" disabled name="billing_myfield18c" value="' + ac + '"> <span onclick="jQuery(\'#popupcheckout\').show();jQuery(\'#popupcheckout\').attr(\'class\',\'show Palm All\');" class="pupupcheckout IdealNutrition in-help"></span> Recieve Your Order in Two Deliveries</label>');
    <?php }?>
                    //fieldset.append('<label style="font-size: 14px;"><input type="radio" name="billing_myfield18c" value="' + ac + '"> <span onclick="jQuery(\'#popupcheckout\').show();jQuery(\'#popupcheckout\').attr(\'class\',\'show Palm All\');" class="pupupcheckout IdealNutrition in-help"></span> Recieve Your Order in Two Deliveries</label>');
                    //fieldset.append('<label style="font-size: 14px;"><input type="radio" name="billing_myfield18c" value="' + DateCJ('Sunday') + '"> <span onclick="jQuery(\'#popupcheckout\').show();jQuery(\'#popupcheckout\').attr(\'class\',\'show Palm Sunday\');" class="pupupcheckout IdealNutrition in-help"></span>' + DateCJ('Sunday') + '</label>');
                    //fieldset.append('<label style="font-size: 14px;"><input type="radio" name="billing_myfield18c" value="' + DateCJ('Wednesday') + '"> <span onclick="jQuery(\'#popupcheckout\').show();jQuery(\'#popupcheckout\').attr(\'class\',\'show Palm Wednesday\');" class="pupupcheckout IdealNutrition in-help"></span> ' + DateCJ('Wednesday') + '</label>');

                    fieldset.append('<label id="label_Sunday" c-id="label_Wednesday" lbl-br="label_Sunday_and_Wednesday" data-av="no" style="font-size: 14px;"><input type="radio" disabled name="billing_myfield18c" value="' + _dates.Sunday.value + '"> <span onclick="jQuery(\'#popupcheckout\').show();jQuery(\'#popupcheckout\').attr(\'class\',\'show Palm Sunday\');" class="pupupcheckout IdealNutrition in-help"></span>' + _dates.Sunday.value + ' <span class="span_Sunday" style="color: #0dda15;"></span></label>');
                    fieldset.append('<label id="label_Wednesday" c-id="label_Sunday" lbl-br="label_Sunday_and_Wednesday" data-av="no" style="font-size: 14px;"><input type="radio" disabled name="billing_myfield18c" value="' + _dates.Wednesday.value + '"> <span onclick="jQuery(\'#popupcheckout\').show();jQuery(\'#popupcheckout\').attr(\'class\',\'show Palm Wednesday\');" class="pupupcheckout IdealNutrition in-help"></span> ' + _dates.Wednesday.value + ' <span class="span_Wednesday" style="color: #0dda15;"></span></label>');

                    _dates.run(_dates.Sunday.req);
                    _dates.run(_dates.Wednesday.req);
                    //item.append(Sunday).trigger('change');
                    //item.append(Wednesday).trigger('change');
                }
                else if (jQuery(this).val() === 'Broward and Martin County') {
                    var ac = 'You will receive half of your meals on: <strong><br>' + _dates.Monday.value + '<br></strong> And the other half on: <strong><br>' + _dates.Wednesday.value + '<br></strong>';
                    //var Sunday = new Option(DateCJ('Sunday'), DateCJ('Sunday'), false, false);
                    //var Wednesday = new Option(DateCJ('Wednesday'), DateCJ('Wednesday'), false, false);
                    fieldset.append(legend);
    <?php if (WC()->cart->get_cart_contents_count() >= 18) { ?>
                        //fieldset.append('<label style="font-size: 14px;"><input type="radio" name="billing_myfield18c" value="' + ac + '"> <span onclick="jQuery(\'#popupcheckout\').show();jQuery(\'#popupcheckout\').attr(\'class\',\'show Broward All\');" class="pupupcheckout IdealNutrition in-help"></span> Recieve Your Order in Two Deliveries</label>');
                        fieldset.append('<label id="label_Monday_and_Wednesday" style="font-size: 14px;"><input type="radio" disabled name="billing_myfield18c" value="' + ac + '"> <span onclick="jQuery(\'#popupcheckout\').show();jQuery(\'#popupcheckout\').attr(\'class\',\'show Broward All\');" class="pupupcheckout IdealNutrition in-help"></span> Recieve Your Order in Two Deliveries</label>');
                        //fieldset.append('<div class="pupupcheckout" onclick="jQuery(\'#popupcheckout\').show()"><span class="IdealNutrition in-help"></span></div>');
    <?php } ?>
                    //fieldset.append('<label style="font-size: 14px;"><input type="radio" name="billing_myfield18c" value="' + DateCJ('Monday') + '"> <span onclick="jQuery(\'#popupcheckout\').show();jQuery(\'#popupcheckout\').attr(\'class\',\'show Palm Monday\');" class="pupupcheckout IdealNutrition in-help"></span> ' + DateCJ('Monday') + '</label>');
                    //fieldset.append('<label style="font-size: 14px;"><input type="radio" name="billing_myfield18c" value="' + DateCJ('Wednesday') + '"> <span onclick="jQuery(\'#popupcheckout\').show();jQuery(\'#popupcheckout\').attr(\'class\',\'show Palm Wednesday\');" class="pupupcheckout IdealNutrition in-help"></span> ' + DateCJ('Wednesday') + '</label>');
                    fieldset.append('<label id="label_Monday" c-id="label_Wednesday" lbl-br="label_Monday_and_Wednesday" data-av="no" style="font-size: 14px;"><input type="radio" disabled name="billing_myfield18c" value="' + _dates.Monday.value + '"> <span onclick="jQuery(\'#popupcheckout\').show();jQuery(\'#popupcheckout\').attr(\'class\',\'show Palm Monday\');" class="pupupcheckout IdealNutrition in-help"></span> ' + _dates.Monday.value + ' <span class="span_Monday" style="color: #0dda15;"></span></label>');
                    fieldset.append('<label id="label_Wednesday" c-id="label_Monday" lbl-br="label_Monday_and_Wednesday" data-av="no" style="font-size: 14px;"><input type="radio" disabled name="billing_myfield18c" value="' + _dates.Wednesday.value + '"> <span onclick="jQuery(\'#popupcheckout\').show();jQuery(\'#popupcheckout\').attr(\'class\',\'show Palm Wednesday\');" class="pupupcheckout IdealNutrition in-help"></span> ' + _dates.Wednesday.value + ' <span class="span_Wednesday" style="color: #0dda15;"></span></label>');
                        
                    _dates.run(_dates.Monday.req);
                    _dates.run(_dates.Wednesday.req);
                    //var Monday = new Option(DateCJ('Monday'), DateCJ('Monday'), false, false);
                    //var Thursday = new Option(DateCJ('Thursday'), DateCJ('Thursday'), false, false);
                    //item.append(Monday).trigger('change');
                    //item.append(Thursday).trigger('change');
                }
                else if(jQuery(this).val() === 'Store Pick Up')
                {

                  //var ac = 'You will receive half of your meals on: <strong><br>' + DateCJ('Monday') + '<br></strong> And the other half on: <strong><br>' + DateCJ('Thursday') + '<br></strong>';

                  /*fieldset.append('<label style="font-size: 14px;"><input type="radio" name="billing_myfield18c" value="' + DateCJ('Sunday') + '"> <span onclick="jQuery(\'#popupcheckout\').show();jQuery(\'#popupcheckout\').attr(\'class\',\'show Palm Sunday\');" class="pupupcheckout IdealNutrition in-help"></span> ' + DateCJ('Sunday') + '</label>');

                  fieldset.append('<label style="font-size: 14px;"><input type="radio" name="billing_myfield18c" value="' + DateCJ('Monday') + '"> <span onclick="jQuery(\'#popupcheckout\').show();jQuery(\'#popupcheckout\').attr(\'class\',\'show Palm Monday\');" class="pupupcheckout IdealNutrition in-help"></span> ' + DateCJ('Monday') + '</label>');
                  fieldset.append('<label style="font-size: 14px;"><input type="radio" name="billing_myfield18c" value="' + DateCJ('Wednesday') + '"> <span onclick="jQuery(\'#popupcheckout\').show();jQuery(\'#popupcheckout\').attr(\'class\',\'show Palm Wednesday\');" class="pupupcheckout IdealNutrition in-help"></span> ' + DateCJ('Wednesday') + '</label>');
                */
                fieldset.append('<label id="label_Sunday" style="font-size: 14px;"><input type="radio" disabled name="billing_myfield18c" value="' + _dates.Sunday.value + '"> <span onclick="jQuery(\'#popupcheckout\').show();jQuery(\'#popupcheckout\').attr(\'class\',\'show Palm Sunday\');" class="pupupcheckout IdealNutrition in-help"></span> ' + _dates.Sunday.value + ' <span class="span_Sunday" style="color: #0dda15;"></span></label>');
                fieldset.append('<label id="label_Monday" style="font-size: 14px;"><input type="radio" disabled name="billing_myfield18c" value="' + _dates.Monday.value + '"> <span onclick="jQuery(\'#popupcheckout\').show();jQuery(\'#popupcheckout\').attr(\'class\',\'show Palm Monday\');" class="pupupcheckout IdealNutrition in-help"></span> ' + _dates.Monday.value + ' <span class="span_Monday" style="color: #0dda15;"></span></label>');
                fieldset.append('<label id="label_Wednesday" style="font-size: 14px;"><input type="radio" disabled name="billing_myfield18c" value="' + _dates.Wednesday.value + '"> <span onclick="jQuery(\'#popupcheckout\').show();jQuery(\'#popupcheckout\').attr(\'class\',\'show Palm Wednesday\');" class="pupupcheckout IdealNutrition in-help"></span> ' + _dates.Wednesday.value + ' <span class="span_Wednesday" style="color: #0dda15;"></span></label>');
                _dates.run(_dates.Sunday.req);
                _dates.run(_dates.Monday.req);
                _dates.run(_dates.Wednesday.req);
                  //jQuery('#billing_myfield18c_field fieldset').css("display", "none");;
                  //if(valDeliveryCounty == "Store Pick Up"){
                    jQuery.ajax({
                        type: "post",
                        url: "<?= admin_url('admin-ajax.php'); ?>", // Pon aquí tu URL
                        data: {
                            action: 'update_fee',
                            county: valDeliveryCounty
                        },
                        error: function (response) {
                            console.log(response);
                            jQuery('body').trigger('update_checkout');
                        },
                        success: function (response) {
                            console.log(response);
                            jQuery('body').trigger('update_checkout');
                        }
                    });
                  //}

                  var btn_place = jQuery('#place_order');
                  btn_place.removeAttr('disabled');
                }

                <?php
                  if(!isset($_POST['Semana_Siguiente'])) {
                    $_POST['Semana_Siguiente'] = 0;

                  }
                ?>

                /*Validacion para que se actualice el precio la primera vez*/
                jQuery(document).ready(  function () {
                  console.log("resultado post <?php echo  $_POST['Semana_Siguiente']?>");
                    //Variable a pasar al ajax
                    var typeDelivery = jQuery('input[name="billing_myfield18c"]:first').val();

                    jQuery.ajax({
                        type: "post",
                        url: "<?= admin_url('admin-ajax.php'); ?>", // Pon aquí tu URL
                        data: {
                            action: 'update_fee',
                            time: typeDelivery,
                            Semana_Siguiente: '<?php echo $_POST['Semana_Siguiente']?>',
                            county: valDeliveryCounty
                        },
                        error: function (response) {
                            console.log(response);
                            jQuery('body').trigger('update_checkout');
                        },
                        success: function (response) {
                            console.log(response);
                            jQuery('body').trigger('update_checkout');
                        }
                    });

                   });

               jQuery('input[name="billing_myfield18c"]').each(function () {
                    jQuery(this).change(function () {


                        var typeDelivery = jQuery('input[name="billing_myfield18c"]:checked').val();

                        jQuery.ajax({
                            type: "post",
                            url: "<?= admin_url('admin-ajax.php'); ?>", // Pon aquí tu URL
                            data: {
                                action: 'update_fee',
                                time: typeDelivery,
                                Semana_Siguiente: '<?php echo $_POST['Semana_Siguiente']?>',
                                county: valDeliveryCounty
                            },
                            error: function (response) {
                                console.log(response);
                                jQuery('body').trigger('update_checkout');
                            },
                            success: function (response) {
                                console.log(response);
                                jQuery('body').trigger('update_checkout');
                            }
                        });

                    });
                });
              //}//alert(valDeliveryCounty);
                //else {



                //}

            });
        <?php if(isset($_POST['Ciudad_delivery'])) {?>
            jQuery('#billing_myfield18').val("<?php echo $_POST['Ciudad_delivery']?>").trigger('change');
            jQuery('#billing_myfield18').change();
        <?php } else {?>
            jQuery('#billing_myfield18').val('Palm Beach County').trigger('change');
            jQuery('#billing_myfield18').change();
         <?php } ?>

    <?php if ($_SERVER['SCRIPT_URI'] === esc_url(wc_get_checkout_url())) { ?>
                jQuery('body').append('<div id="popupcheckout"><div class="contenedor">' +
                        '<div class="popupcheckoutwindow">' +
                        '<span class="IdealNutrition in-cross" onclick="jQuery(\'#popupcheckout\').attr(\'class\',\'\');jQuery(\'#popupcheckout\').hide(300);" style="position: relative;float: right;bottom: initial;top: 5px;right: 4px;color: #ffffff;cursor: pointer;display:  block;background: transparent;"></span>' +
                        '<h2>DELIVERY</h2>' +
                        '<p class="Palm All">Meals selected are randomized, first half of your meals will be delivered on <b>' + DateCJ('Sunday') + '</b>, second half on ' +
                        '<b>' + DateCJ('Wednesday') + '</b>. You can view this information on your dashboard under the delivery information tab.</p>' +
                        '<p class="Broward All">Meals selected are randomized, first half of your meals will be delivered on <b>' + DateCJ('Monday') + '</b>, second half on ' +
                        '<b>' + DateCJ('Wednesday') + '</b>. You can view this information on your dashboard under the delivery information tab.</p>' +
                        '<p class="Broward Sunday">Your meals will be delivered on <b>' + DateCJ('Sunday') + '</b> between the hours of 10am and 8pm.</p>' +
                        '<p class="Broward Wednesday">Your meals will be delivered on <b>' + DateCJ('Wednesday') + '</b> between the hours of 10am and 8pm.</p>' +
                        '<p class="Broward Monday">Your meals will be delivered on <b>' + DateCJ('Monday') + '</b> between the hours of 10am and 8pm.</p>' +
                        '<p class="Broward Thursday">Your meals will be delivered on <b>' + DateCJ('Thursday') + '</b> between the hours of 10am and 8pm.</p>' +
                        '<div><img src="/wp-content/uploads/2018/07/DELIVERY-640PX.png<?php /* = z_PLUGIN_URL . 'resources/icon/delivery.svg'; */ ?>"><div><div></div></div>');
    <?php } ?>
        });
jQuery(window).on('load',function(){
                    jQuery('.campo-ciudad input').val("<?php echo $_POST['Ciudad_delivery']?>");
                    jQuery('.campo-fecha input').val("<?php echo $_POST['Fecha_delivery']?>");
                    /*Segunda semana*/
                    /*var x = document.getElementsByName("billing_myfield18c");
                    if(x[0]) {
                      x[0].checked = true;
                    }*/


                });
    </script>
    <?php
}
