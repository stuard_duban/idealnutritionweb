<?php

if (!defined('ABSPATH')) {
    die('You are not allowed to call this page directly.');
}

function __zDload_style() {
    $r = '';
    if ($_SERVER['REQUEST_URI'] === '/my-account/') {
        if (!_zisMobile) {
            $r .= '.woocommerce-MyAccount-navigation ul{display:none !important}'
                    . 'li.woocommerce-MyAccount-navigation-link {display: inline-block;background: #fff;padding: 10px 34px;margin-bottom: 8px;}'
                    . '.site-header,.site-footer {display: none;}'
                    . 'body{background: #d3d3d3 !important;}';
        }
    }
    echo '<style>'
    //.$r
    //. '.site-content {background: #d3d3d3 !important;padding-top: 5%;}'
    . '#zGravatares{width: 185px;height: 185px;border-radius: 50%;border: 20px solid #e9e9e9;overflow: hidden;text-align: center;margin-top: 0%;}'
    . '#zGravatares img{max-width: 100%;min-width: 145px;}'
    . ''
    . '</style>';
}

function _z_loading_actions() {
    /* $ROOT = explode(':', $_SERVER['SCRIPT_URI'])[0];//
      foreach (_zDload_my_meals('') as $key => $value) {
      //var_dump(str_replace($ROOT. '://'.$_SERVER['SERVER_NAME'], '',wc_get_account_endpoint_url($key)));
      add_action( 'woocommerce_account_'.$key.'_endpoint', 'gc_custom_endpoint_content' );
      } */
}

/*
  //add_action( 'woocommerce_account_my-stuff-endpoint_endpoint', 'gc_custom_endpoint_content' );
  /**
 * Custom Endpoint content
 */
/* function gc_custom_endpoint_content() {
  echo 'custom end point content goes here';
  } */

function zDClear() {
    $_SESSION[_zDid] .= '<script type="text/javascript">jQuery("body").html("");jQuery(document).ready(function(){setTimeout(function(){jQuery(\'#formilla-frame\').hide();},1000)});</script><style>#formilla-frame{display:none;}</style>';
}

function zDash_INI($id, $class, $data = '') {
    $_SESSION[_zDid] .= _div($id, $class, $data);
}

function zDash_END() {
    _div('/');
}

function _ediv() {
    _div('/');
}

function _div($id = '', $class = '', $data = '') {
    if ($id === '/') {
        $_SESSION[_zDid] .= '</div>';
        return;
    } else {
        if (boolval(strlen(trim($id)))) {
            $id = 'id="' . $id . '"';
        }
        if (boolval(strlen(trim($class)))) {
            $class = 'class="' . $class . '"';
        }
        $_SESSION[_zDid] .= '<div ' . $id . ' ' . $class . ' ' . $data . '>';
    }
}

function zd_shortcode($s) {
    $_SESSION[_zDid] .= do_shortcode($s);
}

function zd_($c) {
    $_SESSION[_zDid] .= $c;
}

function __zSelectMealsbyOrder($id) {
    global $wpdb;
    $table_name = $wpdb->prefix . "orders_in";
    $User = wp_get_current_user();
    $ID_User = $User->ID;
    $DB = $wpdb->get_results("SELECT * FROM $table_name WHERE orders=$id AND user=$ID_User");
    if (count($DB) === 0) {
        return false;
    } else {
        $DB = $DB[0];
    }
    $date = date('d-m-Y');
    $week = ["Sunday" => 1, "Monday" => 2, "Tuesday" => 3, "Wednesday" => 4, "Thursday" => 5, "Friday" => 6, "Saturday" => 7];
    $f = date('l, d-m-Y', strtotime($date));
    $day = $week[explode(',', $f)[0]];
    $count = 0;
    foreach ($week as $n) {
        if ($n > $day) {
            $count++;
        }
    }
    //var_dump(date('d-m-Y', strtotime($DB->date)).' < '.date('d-m-Y', strtotime('+' . ($count + 1) . ' day', strtotime($date))));
    if (date('d-m-Y', strtotime($DB->date)) < date('d-m-Y', strtotime('+' . ($count + 1) . ' day', strtotime($date)))) {
        return false;
    } else {
        return true;
    }
}

function getPlegables($ID_Order, $ID_User) {
    global $wpdb;
    $table_name = $wpdb->prefix . "orders_in";
    $DB = $wpdb->get_results("SELECT * FROM $table_name WHERE orders=$ID_Order AND user=$ID_User");
    if (count($DB) === 0) {
        return false;
    } else {
        $DB = $DB[0];
    }
    return($DB);
}

function __isGreen($id) {
    $subscriptions = wcs_get_users_subscriptions();
    if ($subscriptions) {
        foreach ($subscriptions as $subscription) {
            if (floatval($subscription->get_data()['parent_id']) === floatval($id)) {
                return true;
            }
        }
    }
    return false;
}

function __isRed($id, $user) {
    $r = getPlegables($id, $user);
    if ($r) {
        if (count($r) > 0) {
            return true;
        }
    }
    return false;
}

function _zDisMember() {
    $r = ['title' => '', 'caption' => 'Non-subscribed Member', 'href' => '#', 'isActive' => false, 'isSelect' => false];
    $User = get_currentuserinfo();
    $subscriptions = wcs_get_users_subscriptions();
    if ($subscriptions) {
        foreach ($subscriptions as $subscription) {
            if (esc_attr(wcs_get_subscription_status_name($subscription->get_status())) == 'Active') {
                $order = wc_get_order($subscription->get_data()['parent_id']);
                $order_id = $subscription->get_data()['parent_id'];
                $title = '';
                foreach ($subscription->get_items() as $line_item) {
                    $title = $line_item['name'];
                }
            }
        }
        $r['title'] = str_replace(' meals per week', '', $title);
        $r['href'] = '/see-meal-suscriptions-plans-' . str_replace('', '', str_replace(' meals per week', '', explode('- ', $title)[1])) . '/?order=' . $order_id . '&ini-shopping';
        $r['caption'] = 'Subscribed Member';
        $r['isActive'] = true;
        $r['isSelect'] = __zSelectMealsbyOrder($order_id);
    }
    return (object) $r;
}

function _zDload_my_meals($e) {
    return unserialize(WPMyAccount);
}

function __zDloadDashboard() {

    global $current_user;
    get_currentuserinfo();

    $Member = _zDisMember();
    zDClear();
    zd_('<style>'
            . '.Subscribed .in-check{font-size:  15px;margin-right: 5px;font-weight: 900;}'
            . '.Member-plan.Subscribed {color: #94e934;font-size: 16px;}'
            . 'div#zDHeader {padding-top: 10px;padding-left: 20px;}'
            . '.zDUserName {width: fit-content;display: inline-block;vertical-align: top;padding-top: 20px;font-size: 20px;font-weight: bold;}'
            . '.borderzGrAvatar{display: inline-block;width: 120px;}'
            . '.zGrAvatar{border-radius:  50%;overflow:  hidden;max-width: 100px;border: 1px solid #94e934;}'
            . '.zGrAvatar img{width: 100%;height: 100%;margin: 0px;margin-bottom: -5px;}'
            . 'nav.zDashboard-MyAccount-navigation ul li {list-style: none;border-top: 1px solid #dadbdf;padding: 8px 20px;}'
            . 'nav.zDashboard-MyAccount-navigation ul li a span.label{display:  block;margin: 0px 40px;font-size:  12px;line-height: 4px;}'
            . 'li.zDash-li span.IdealNutrition {color: #94e934;font-size: 24px;margin-right: 10px; font-weight: normal !important;}'
            . 'li.zDash-li a{font-size: calc(2vw + 1.1vh + 1.1vmin);font-weight: 900;color: #333a49;}'
            . 'li.zDash-li:hover {background: #94e934;} li.zDash-li:hover a,li.zDash-li:hover span{color:#fff !important;}'
            . '</style>');
    zDash_INI('zDashBoardMobile', _zDid);

    _div('zDHeader', '');

    _div('', 'borderzGrAvatar');
    _div('', 'zGrAvatar');
    zd_shortcode('[avatar user=' . get_current_user_id() . ']');
    _ediv();
    _ediv();

    _div('', 'zDUserName');
    zd_($current_user->user_firstname . ' ' . $current_user->user_lastname);

    _div('', 'Member-plan ' . $Member->caption);
    if ($Member->isActive) {
        zd_('<span class="IdealNutrition in-check"></span>');
    }
    zd_($Member->caption);
    _ediv();
    _ediv();

    _ediv();

    _div('zDNav');
    zd_('<nav class="zDashboard-MyAccount-navigation">');
    zd_('<ul style="padding: 0px;">');
    $r = '';
    foreach (unserialize(WPMyAccountMobile) as $endpoint => $label) {
        if ($Member->isActive && $endpoint === 'downloads' && $Member->isActive ||
                !$Member->isActive && $endpoint !== 'downloads' ||
                $Member->isActive && $endpoint !== 'downloads') {
                  $accordion = "";
                  if($endpoint == "shop"){
                    $accordion = "accordion";
                  }
            $r .= '<li class="zDash-li">';
            $href = strtolower($endpoint) === 'chat' ? ' onclick="jQuery(\'#formilla-frame\').show();jQuery(jQuery(\'#formilla-frame\').contents().find(\'body\')).find(\'#formilla-chat-button-inner\').click();"' : ' href="' . esc_url(wc_get_account_endpoint_url($endpoint)) . '"';
            $r .= '<a class="zDash-link-' . $endpoint . ' ' . $accordion . '" ' . $href . ' >';
            $r .= '<span class="IdealNutrition ' . $endpoint . '"></span>';
            $r .= '<span class="IdealNutrition in-right" style="color:#d1d3d7 !important;float: right;font-size: 40px;"></span>';
            $r .= $label;

            $r .= '</a>';
            $r .= '<div class="panel">
                    <a href="https://ideal.luxlifeentertainment.com/pay-as-you-go-plans/">- SUBSCRIPTION PLANS</a>
                    <a href="https://ideal.luxlifeentertainment.com/pay-as-you-go/">- PAY AS GO YOU PLANS</a>
                    <a href="https://ideal.luxlifeentertainment.com/individual-meals-menu/">- Individual Meals</a>
                    <a href="https://ideal.luxlifeentertainment.com/by-the-pound/">- By the pound</a>
                    <a href="https://ideal.luxlifeentertainment.com/custom-meal/">- Custom Meal</a>
                    <a href="https://ideal.luxlifeentertainment.com/gift-card/">- Gift Card</a>
                    </div>';
            $r .= '</li>';
        }
    }
    zd_($r);
    zd_('</ul>');
    zd_('</nav>');
    _ediv();
    zDash_END();

    echo $_SESSION[_zDid];
}

function __zDloadOrders() {
    $ID_User = get_current_user_id();
    $customer_orders = get_posts(array(
        'numberposts' => -1,
        'meta_key' => '_customer_user',
        'meta_value' => $ID_User,
        'post_type' => wc_get_order_types(),
        'post_status' => array_keys(wc_get_order_statuses()),
    ));
    zDClear();
    zd_('<style>'
            . '#wpadminbar{display:none}'
            . 'body{background: #eeeff5 !important;}'
            . 'div#zOrdersMobile {padding: 10px;}' . ''
            . '.viewzdOrders {background: #fff;margin-bottom: 10px;box-shadow: 2px 1px 1px 1px #00000070;}'
            . '.zDOrderHeader,.zDHead {padding: 5px 15px;background: #f2f2f242;border-bottom: 1px solid #97989ce3;box-shadow: 0px 3px 4px -4px #00000066;}'
            . 'span.id_order {font-size: 20px;font-weight: 600;}'
            . 'span.id_status {float: right;font-size: 18px;}'
            . '.zDOrderBody,.zDBody {padding: 5px 15px;}'
            . 'span.Total {float: right;font-size: 24px;font-weight: bold;}'
            . '.zDOrderFooter span a {border-radius: 0px;padding: 5px 10px;margin: 5px;}'
            . '.zDOrderFooter .option-l{float:left}'
            . '.zDOrderFooter .option-r{float:right;margin-right: -75px;}'
            . '.zDOrderFooter {padding-bottom: 5px;}'
            . '.clearfix {display: block;content: "";clear: both;}'
            . 'div.zDPopUp {position: fixed;z-index: 9999999;top: 0px;left: 0px;bottom: 0px;right: 0px;background-color: white;}'
            . '.zDPopUp .product, .zDPopUp .product_header{border-bottom: 1px solid #a2a3a7;padding: 5px;}'
            . '.zDPopUp .product_header {background-color: #f1f1f1;border: 1px solid #a2a3a7;font-size: 18px;font-weight: bold;padding-left: 15px;padding-right: 15px;}'
            . '.zDPopUp .option-l{float:left}'
            . '.zDPopUp .option-r{float:right;/*margin-right: -75px;*/}'
            . '.product.zDtotales {font-weight: bold;background-color: #f1f1f1;border-left: 1px solid #a2a3a7;border-right: 1px solid #a2a3a7;}'
            . 'a.close {float: right;height: 16px;width: 16px;cursor: pointer;}'
            . '</style>');
    zDash_INI('zHedaer', _zDid, 'style="position:  fixed;top:  0px;left:  0px;right: 0px;background: #f3f3f6;padding: 10px 15px;text-align: center;font-size: calc(2vw + 1.1vh + 1.1vmin);padding-bottom: 5px;border-bottom: 1px solid #c5c5c5;"');
    $title = "ORDER HISTORY";
    zd_('<a style="float: left;" href="javascript:history.back()"><span class="IdealNutrition in-last"></span></a>' . $title);
    zDash_END();
    zd_('<br><br>');
    zDash_INI('zOrdersMobile', _zDid);
    $r = '';
    foreach ($customer_orders as $customer_order) {
        $order = wc_get_order($customer_order);
        $plgb = getPlegables($order->get_order_number(), $ID_User);
        if (!$plgb) {
            $city = str_replace('$5', '', get_post_meta($order->get_id(), '_billing_myfield18', true));
            $dat = get_post_meta($order->get_id(), '_billing_myfield18c', true);
            if ($dat === "free") {
                $dat = 'You will receive your meals on: <br><strong>' . get_post_meta($order->get_id(), '_billing_myfield16', true) . '</strong>';
            }

        } else {
            $city = $plgb->city;
            $dat = $plgb->tim;
        }
        //echo "fecha: $dat";exit;
        $r .= '<div class="viewzdOrders">';
        $r .= '<div class="zDOrderHeader">';
        $r .= '<span class="id_order"><b>ORDER: </b>#' . $order->get_order_number() . '</span>';
        $r .= '<span class="id_status"><time datetime="' . esc_attr($order->get_date_created()->date('c')) . '">' . esc_html(wc_format_datetime($order->get_date_created())) . '</time></span>'; //esc_html(wc_get_order_status_name($order->get_status()))
        $r .= '</div>';
        $r .= '<div class="zDOrderBody">';
        $r .= '<span class="incity"><b>Delivery info: </b>' . $city . '</span><br>';
        $r .= '<span class="id_time">' . $dat . '</span><hr style="margin-bottom: 4px;">';
        $r .= '<span class="inItems">' . $order->get_item_count() . ' total items</span>';
        $r .= '<span class="Total">' . $order->get_formatted_order_total() . '</span>';
        $r .= '</div>';
        $r .= '<div class="zDOrderFooter">';
        $actions = wc_get_account_orders_actions($order);
        $j = '';
        if (!empty($actions)) {
            foreach ($actions as $key => $action) {

                if (esc_html($action['name']) === 'View') {
                    $onclick = 'onclick="jQuery(\'#PopUp' . $order->get_order_number() . '\').show();" ';
                } else {
                    $onclick = 'href="' . esc_url($action['url']) . '" ';
                }
                $j .= '<a style="width: 100%;min-width: 100px;" ' . $onclick . 'class="btn meal btn-primary btn-xs">' . esc_html($action['name']) . ' details</a>';
            }
        }
        $r .= '<span class="option-r">' . $j . '</span>';
        $j = '';
        if (__isGreen($order->get_order_number())) {
            $j .= '<a href="/' . zpage_by_title(PAGE_VIEW_ORDER_OF_WEEK) . '/?order=' . $order->get_order_number() . '" style="width: 100%;" class="btn meal btn-danger btn-xs">' . esc_html('View Updated Order') . '</a>';
        } else {
            if (__isRed($order->get_order_number(), $ID_User)) {
                $j .= '<a href="/' . zpage_by_title(PAGE_VIEW_MEALS_OF_DELIVERY) . '/?order=' . $order->get_order_number() . '" style="width: 100%;" class="btn meal btn-danger btn-xs">' . esc_html('Delivery Info') . '</a>';
            }
        }
        $r .= '<span class="option-l">' . $j . '</span><div class="clearfix"></div>';
        $r .= '</div>';
        //var_dump($order);
        $r .= '</div>';

        $r .= '<div id="PopUp' . $order->get_order_number() . '" class="zDPopUp" style="display:none;">';
        $r .= '<div id="Head' . $order->get_order_number() . '" class="zDHead">';
        $r .= '<span class="IdealNutrition in-ideal" style="margin-top: -3px;font-size:30px;color:#00f100;float:left;"></span>Details your order #' . $order->get_order_number();
        $r .= '<a class="close" onclick="jQuery(\'#PopUp' . $order->get_order_number() . '\').hide()"><span class="IdealNutrition in-cross"></span></a>';
        $r .= '</div>';
        $r .= '<div id="Body' . $order->get_order_number() . '" class="zDBody">';
        $r .= '<p>' . 'Order #<mark class="order-number">' . $order->get_order_number() . '</mark> was placed on <mark class="order-date">' . wc_format_datetime($order->get_date_created()) . '</mark> and is currently <mark class="order-status">' . wc_get_order_status_name($order->get_status()) . '</mark>.' . '</p>';
        $order_items = $order->get_items(apply_filters('woocommerce_purchase_order_item_types', 'line_item'));
        $show_purchase_note = $order->has_status(apply_filters('woocommerce_purchase_note_order_statuses', array('completed', 'processing')));
        $show_customer_details = is_user_logged_in() && $order->get_user_id() === get_current_user_id();
        $j = '<div class="product_header"><span class="option-l">PRODUCT</span> <span class="option-r">PRICE</span><div class="clearfix"></div></div>';
        foreach ($order_items as $item_id => $item) {
            $product = $item->get_product();
            $j .= '<div class="product">';
            $j .= '<div style="font-size: calc(2vw + 1.1vh + 1.1vmin);">' . $item->get_name() . ' <b>x ' . $item->get_quantity() . '</b></div>';
            $j .= '<span class="option-l">#' . $item_id . '</span> <span class="option-r">' . $order->get_formatted_line_subtotal($item) . '</span>';
            $j .= '<div class="clearfix"></div></div>';
        }
        /* if ($show_purchase_note && $purchase_note) {
          $J .= wpautop(do_shortcode(wp_kses_post($purchase_note)));
          } */
        $r .= $j;
        $j = '';
        foreach ($order->get_order_item_totals() as $key => $total) {
            $j .= '<div class="product zDtotales">';
            $j .= '<span class="option-l">' . $total['label'] . '</span>';
            $j .= '<span class="option-r">' . $total['value'] . '</span>';
            $j .= '<div class="clearfix"></div></div>';
        }
        if ($order->get_customer_note()) {
            $j .= '<h5>Note:</h5>';
            $j .= '<p>' . wptexturize($order->get_customer_note()) . '</p>';
        }
        $r .= $j;
        $r .= '</div>';
        $r .= '</div>';
    }

    zd_($r);
    zd_('<script type="text/javascript"> jQuery(document).ready(function(){jQuery(".zDBody").each(function(){jQuery(this).attr("style","overflow-y: scroll;max-height:"+(screen.availHeight-200)+"px;"+jQuery(this).attr("style"));});});</script>');
    zDash_END();

    echo $_SESSION[_zDid] . zDMyTab();
}

function zDMyTab() {
    $r = '<style>.zDashBoardTabs{
    background:  #f9f9fb;
    position:  fixed;
    bottom:  0px;
    left:  0px;
    right: 0px;
    border-top:  2px solid #e2e2e5;
    box-shadow: -2px -1px 4px 2px #61616142;}
    .zDashBoardTabs .zDBTab {
    padding-top: 10px;
    padding-bottom: 3px;
    display: inline-block;
    width: 25%;
    text-align: center;
}
    .zDashBoardTabs .zDBTab a{
    font-size: 14px;
    font-weight: 900;
    color: #6f6f6f;
    line-height: 16px;
}
.zDashBoardTabs .zDBTab a span {
    display: block;
    font-weight: normal;
    font-size: 20px !important;
}
.zDashBoardTabs .zDBTab:hover{
    background: #94e934;
}
.zDashBoardTabs .zDBTab:hover a,.zDashBoardTabs .zDBTab:hover span{color: #fff !important}
</style>';
    $j = '';
    foreach (unserialize(WPMyAccountTabMobile) as $key => $tab) {
        $href = strtolower($key) === 'home' ? ' href="' . get_site_url() . '"' : ' href="' . esc_url(wc_get_account_endpoint_url($key)) . '"';
        if ($key === 'my-account') {
            $href = str_replace("/my-account/my-account/", "/my-account/", $href);
        }
        $j .= '<div class="zDBTab"><a ' . $href . '><span class="IdealNutrition zd-' . $key . '"></span>' . $tab . '</a></div>';
    }
    $r .= $j;
    $iD = uniqid();
    return '<br><br><br><div id="' . $iD . '" class="zDashBoardTabs">'
            . $r
            . '</div>';
}

function _zDLoadGreen($e = FALSE) {
    $saved = $_SESSION[_zDid];
    $_SESSION[_zDid] = '';
    $iD = uniqid();
    zd_('<style>'
            . 'div.zdHeaderGreen{margin-top: 15px;background: #00e900;text-align: center;padding: 5px;color: #fff;font-weight: bold;font-size: 16px;letter-spacing: 1.2px;}'
            . '.aslowas {    margin-top: 10px;
    font-size: calc(2vw + .6vh + .6vmin);
    text-align: center;}'
            . '.zDPrice {text-align: center;
    margin-top: -18px;
    margin-bottom: -5px;}'
            . '.zDinL {display: inline-block;}'
            . '.zDPriceLow{font-size: calc(6vw + 1.5vh + 2vmin);
    font-weight: 600;}'
            . 'div.zDdecimls,div.zDSymbol {font-size: calc(2vw + 1.5vh + 2vmin);
    font-weight: 600;}'
            . 'div.zDSymbol{vertical-align: super;margin-right: 3px;}'
            . '.zDinL.zDMeal{font-size: calc(1.5vw + .6vh + .6vmin);padding-left: 9px;vertical-align: super;letter-spacing: 1.2px;}'
            . '.zDCaption {text-align: center;
    color: #00e900;
    font-size: calc(1.5vw + .6vh + .6vmin);
    font-weight: 700;
    letter-spacing: 1px;}'
            . '.zDAddCartGreen {font-size: calc(1.5vw + .6vh + .6vmin);background-color: rgba(0,0,0,.5);margin: 0 auto;width: fit-content;width: -moz-fit-content;padding: 5px 25px;color: #fff;border-radius: 50px;margin-top: 5px;cursor: pointer;}'
            . 'select.zDSelect {margin-top: 5px;max-height: 30px;max-width: 230px;margin: 0 auto;}'
            . '.zDBodyGreen {text-align: center;}'
            . '.zDSelectCaretGreen{position: relative;margin: 0 auto;width: fit-content;width: -moz-fit-content;}'
            . '.zDSelectCaretGreen:after{width: 0;height: 0;border-left:6px solid transparent;border-right:6px solid transparent;border-top:6px solid #00e900;position:absolute;top:40%;right:9px;content:"";z-index:98;}'
            . '.zDAddCartGreen:hover{background-color: rgba(0, 0, 0, 0.7);}'
            . '.zGreenMobile {background: #fff;padding-bottom: 15px;margin: 0 15px;}'
            . '</style>');
    zd_('<script type="text/javascript">function Change' . $iD . '(e){'
            . 'jQuery("#zDP' . $iD . '").html(jQuery(e).attr("data-low"));'
            . 'jQuery("#zDD' . $iD . '").html("."+jQuery(e).attr("data-decimal"));'
            . '}</script>');
    zDash_INI('zGreenMobile' . _zDid, 'zGreenMobile');
    _div('zdHeaderGreen' . _zDid, 'zdHeaderGreen');
    zd_('<img style="max-width: 22px;" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADMAAAArCAYAAADVJLDcAAAACXBIWXMAAAsTAAALEwEAmpwYAAAKTWlDQ1BQaG90b3Nob3AgSUNDIHByb2ZpbGUAAHjanVN3WJP3Fj7f92UPVkLY8LGXbIEAIiOsCMgQWaIQkgBhhBASQMWFiApWFBURnEhVxILVCkidiOKgKLhnQYqIWotVXDjuH9yntX167+3t+9f7vOec5/zOec8PgBESJpHmomoAOVKFPDrYH49PSMTJvYACFUjgBCAQ5svCZwXFAADwA3l4fnSwP/wBr28AAgBw1S4kEsfh/4O6UCZXACCRAOAiEucLAZBSAMguVMgUAMgYALBTs2QKAJQAAGx5fEIiAKoNAOz0ST4FANipk9wXANiiHKkIAI0BAJkoRyQCQLsAYFWBUiwCwMIAoKxAIi4EwK4BgFm2MkcCgL0FAHaOWJAPQGAAgJlCLMwAIDgCAEMeE80DIEwDoDDSv+CpX3CFuEgBAMDLlc2XS9IzFLiV0Bp38vDg4iHiwmyxQmEXKRBmCeQinJebIxNI5wNMzgwAABr50cH+OD+Q5+bk4eZm52zv9MWi/mvwbyI+IfHf/ryMAgQAEE7P79pf5eXWA3DHAbB1v2upWwDaVgBo3/ldM9sJoFoK0Hr5i3k4/EAenqFQyDwdHAoLC+0lYqG9MOOLPv8z4W/gi372/EAe/tt68ABxmkCZrcCjg/1xYW52rlKO58sEQjFu9+cj/seFf/2OKdHiNLFcLBWK8ViJuFAiTcd5uVKRRCHJleIS6X8y8R+W/QmTdw0ArIZPwE62B7XLbMB+7gECiw5Y0nYAQH7zLYwaC5EAEGc0Mnn3AACTv/mPQCsBAM2XpOMAALzoGFyolBdMxggAAESggSqwQQcMwRSswA6cwR28wBcCYQZEQAwkwDwQQgbkgBwKoRiWQRlUwDrYBLWwAxqgEZrhELTBMTgN5+ASXIHrcBcGYBiewhi8hgkEQcgIE2EhOogRYo7YIs4IF5mOBCJhSDSSgKQg6YgUUSLFyHKkAqlCapFdSCPyLXIUOY1cQPqQ28ggMor8irxHMZSBslED1AJ1QLmoHxqKxqBz0XQ0D12AlqJr0Rq0Hj2AtqKn0UvodXQAfYqOY4DRMQ5mjNlhXIyHRWCJWBomxxZj5Vg1Vo81Yx1YN3YVG8CeYe8IJAKLgBPsCF6EEMJsgpCQR1hMWEOoJewjtBK6CFcJg4Qxwicik6hPtCV6EvnEeGI6sZBYRqwm7iEeIZ4lXicOE1+TSCQOyZLkTgohJZAySQtJa0jbSC2kU6Q+0hBpnEwm65Btyd7kCLKArCCXkbeQD5BPkvvJw+S3FDrFiOJMCaIkUqSUEko1ZT/lBKWfMkKZoKpRzame1AiqiDqfWkltoHZQL1OHqRM0dZolzZsWQ8ukLaPV0JppZ2n3aC/pdLoJ3YMeRZfQl9Jr6Afp5+mD9HcMDYYNg8dIYigZaxl7GacYtxkvmUymBdOXmchUMNcyG5lnmA+Yb1VYKvYqfBWRyhKVOpVWlX6V56pUVXNVP9V5qgtUq1UPq15WfaZGVbNQ46kJ1Bar1akdVbupNq7OUndSj1DPUV+jvl/9gvpjDbKGhUaghkijVGO3xhmNIRbGMmXxWELWclYD6yxrmE1iW7L57Ex2Bfsbdi97TFNDc6pmrGaRZp3mcc0BDsax4PA52ZxKziHODc57LQMtPy2x1mqtZq1+rTfaetq+2mLtcu0W7eva73VwnUCdLJ31Om0693UJuja6UbqFutt1z+o+02PreekJ9cr1Dund0Uf1bfSj9Rfq79bv0R83MDQINpAZbDE4Y/DMkGPoa5hpuNHwhOGoEctoupHEaKPRSaMnuCbuh2fjNXgXPmasbxxirDTeZdxrPGFiaTLbpMSkxeS+Kc2Ua5pmutG003TMzMgs3KzYrMnsjjnVnGueYb7ZvNv8jYWlRZzFSos2i8eW2pZ8ywWWTZb3rJhWPlZ5VvVW16xJ1lzrLOtt1ldsUBtXmwybOpvLtqitm63Edptt3xTiFI8p0in1U27aMez87ArsmuwG7Tn2YfYl9m32zx3MHBId1jt0O3xydHXMdmxwvOuk4TTDqcSpw+lXZxtnoXOd8zUXpkuQyxKXdpcXU22niqdun3rLleUa7rrStdP1o5u7m9yt2W3U3cw9xX2r+00umxvJXcM970H08PdY4nHM452nm6fC85DnL152Xlle+70eT7OcJp7WMG3I28Rb4L3Le2A6Pj1l+s7pAz7GPgKfep+Hvqa+It89viN+1n6Zfgf8nvs7+sv9j/i/4XnyFvFOBWABwQHlAb2BGoGzA2sDHwSZBKUHNQWNBbsGLww+FUIMCQ1ZH3KTb8AX8hv5YzPcZyya0RXKCJ0VWhv6MMwmTB7WEY6GzwjfEH5vpvlM6cy2CIjgR2yIuB9pGZkX+X0UKSoyqi7qUbRTdHF09yzWrORZ+2e9jvGPqYy5O9tqtnJ2Z6xqbFJsY+ybuIC4qriBeIf4RfGXEnQTJAntieTE2MQ9ieNzAudsmjOc5JpUlnRjruXcorkX5unOy553PFk1WZB8OIWYEpeyP+WDIEJQLxhP5aduTR0T8oSbhU9FvqKNolGxt7hKPJLmnVaV9jjdO31D+miGT0Z1xjMJT1IreZEZkrkj801WRNberM/ZcdktOZSclJyjUg1plrQr1zC3KLdPZisrkw3keeZtyhuTh8r35CP5c/PbFWyFTNGjtFKuUA4WTC+oK3hbGFt4uEi9SFrUM99m/ur5IwuCFny9kLBQuLCz2Lh4WfHgIr9FuxYji1MXdy4xXVK6ZHhp8NJ9y2jLspb9UOJYUlXyannc8o5Sg9KlpUMrglc0lamUycturvRauWMVYZVkVe9ql9VbVn8qF5VfrHCsqK74sEa45uJXTl/VfPV5bdra3kq3yu3rSOuk626s91m/r0q9akHV0IbwDa0b8Y3lG19tSt50oXpq9Y7NtM3KzQM1YTXtW8y2rNvyoTaj9nqdf13LVv2tq7e+2Sba1r/dd3vzDoMdFTve75TsvLUreFdrvUV99W7S7oLdjxpiG7q/5n7duEd3T8Wej3ulewf2Re/ranRvbNyvv7+yCW1SNo0eSDpw5ZuAb9qb7Zp3tXBaKg7CQeXBJ9+mfHvjUOihzsPcw83fmX+39QjrSHkr0jq/dawto22gPaG97+iMo50dXh1Hvrf/fu8x42N1xzWPV56gnSg98fnkgpPjp2Snnp1OPz3Umdx590z8mWtdUV29Z0PPnj8XdO5Mt1/3yfPe549d8Lxw9CL3Ytslt0utPa49R35w/eFIr1tv62X3y+1XPK509E3rO9Hv03/6asDVc9f41y5dn3m978bsG7duJt0cuCW69fh29u0XdwruTNxdeo94r/y+2v3qB/oP6n+0/rFlwG3g+GDAYM/DWQ/vDgmHnv6U/9OH4dJHzEfVI0YjjY+dHx8bDRq98mTOk+GnsqcTz8p+Vv9563Or59/94vtLz1j82PAL+YvPv655qfNy76uprzrHI8cfvM55PfGm/K3O233vuO+638e9H5ko/ED+UPPR+mPHp9BP9z7nfP78L/eE8/sl0p8zAAAAIGNIUk0AAHolAACAgwAA+f8AAIDpAAB1MAAA6mAAADqYAAAXb5JfxUYAAAKTSURBVHja7NpPiI1RGMfx7zNuM1xlNiQlKwuEJaUoU0ZKNpTCkrJkZUqKkkx21MzKRkmklGgGNbGchdUtf8rUdM3KjYY0Qvws5nl15u1e953x3vueuc2pt3ve89zOPZ/3nPO+nfNek0SnpC46KHUs5jWgSI7fwFEzIzyAMnAauAzsDsoBKAWYTV7Rl4IvcA+wHNgcFkrqBZ4CO7zovKQBMxsMv5QckjRZ9FCRdMbbcjGESBr38leSBiX98vNTiSF6TApSkbTGy0942YfEEPUNIDW0JoADZlbz8GPgO7B6MdzNysCTYI7cN7OpADnq82tkMcyZH/5ZDfIDqWH3TtLaxTBnksaul3Q8mPDVMBZ2SFQYSWVJL7wt75PGeuxkGpkeXdFgHDLm7ZiStLFBbA4kOoykbkmPMkAm05CoMA55mAEyJxYdpgmkW9JoM0gUmBSkJmlrFmR0GEnLJD0IINsWCikU45DbGSA1SVsy1tl+TBNIKYBI0qV51NteTAryWdL2BjGllwBRYepAdv6jt24uFNPVhjliwBBwzFex/WY2nkCAWx6bBvqAyv/8WMt6RpJJGs7QI39j9VaahQ+zJhALhlM6FhcmBZmRtDcLMlbM9QDSlxUSHUbS1QyQGUl7su7OFIKRdKEexGNDjWLRYSStkPTVl7f7svRW3pg8nzP7gZXAczN7FkKAc8A34KCZjbXqmVbKsa7DyZaQI7qAKw752WpIbhhJPcAhP/0o6Ybj1jnkSKshefZMP7DK83eD8rfAWTMbacc6KS/MriA/AdwB7plZhTamvDDDwCdgzMxeFrVdlQvGzKrAtaJ3QzvqNWC6ZzZImi64TT15YN4w+yqwN4KLLGbfsc5vuC/9D2AJ0/r0ZwBuSKGRKJfPYgAAAABJRU5ErkJggg==">');
    zd_('SUBSCRIPTION PLANS');
    _ediv();
    _div('', 'zDBodyGreen');
    _div('', 'aslowas');
    zd_('As low as');
    _ediv();
    _div('', 'zDPrice');
    _div('', 'zDinL zDSymbol');
    zd_('$');
    _ediv();
    _div('zDP' . $iD, 'zDinL zDPriceLow');
    zd_('6');
    _ediv();
    _div('zDD' . $iD, 'zDinL zDdecimls');
    zd_('.50');
    _ediv();
    _div('', 'zDinL zDMeal');
    zd_('PER MEAL');
    _ediv();
    _ediv();
    _div('', 'zDCaption');
    zd_('FREE DELIVERY INCLUDED');
    _ediv();
    _div('', 'zDSelectCaretGreen');
    zd_('<select id="zDSelect' . $iD . '" class="zDSelect" name="plans" onchange="Change' . $iD . '(jQuery(this).find(\':selected\'))">'
            . '<option data-low="7" data-decimal="" value="12">12 Meals per week <strong>$84.00</strong></option>'
            . '<option data-low="6" data-decimal="99" alue="13">13 Meals per week <strong>$90.87</strong></option>'
            . '<option data-low="6" data-decimal="95" value="14">14 Meals per week <strong>$97.30</strong></option>'
            . '<option data-low="6" data-decimal="93" value="15">15 Meals per week <strong>$103.95</strong></option>'
            . '<option data-low="6" data-decimal="90" value="16">16 Meals per week <strong>$110.40</strong></option>'
            . '<option data-low="6" data-decimal="88" value="17">17 Meals per week <strong>$116.96</strong></option>'
            . '<option data-low="6" data-decimal="86" value="18">18 Meals per week <strong>$123.48</strong></option>'
            . '<option data-low="6" data-decimal="88" value="19">19 Meals per week <strong>$129.20</strong></option>'
            . '<option data-low="6" data-decimal="78" value="20">20 Meals per week <strong>$135.60</strong></option>'
            . '<option data-low="6" data-decimal="75" value="22">22 Meals per week <strong>$140.50</strong></option>'
            . '<option data-low="6" data-decimal="70" value="26">26 Meals per week <strong>$174.20</strong></option>'
            . '<option data-low="6" data-decimal="65" value="28">28 Meals per week <strong>$186.20</strong></option>'
            . '<option data-low="6" data-decimal="50" value="30">30 Meals per week <strong>$195.00</strong></option>'
            . '</select>');
    _ediv();
    _div('', 'zDAddCartGreen', 'onclick="alert(\'Ailer\')"');
    zd_('Buy Now »');
    _ediv();
    _ediv();
    zDash_END();
    $r = $_SESSION[_zDid];
    $_SESSION[_zDid] = $saved;
    if ($e) {
        echo $r;
    } else {
        return $r;
    }
}

function _zDLoadRed($e = FALSE) {
    $saved = $_SESSION[_zDid];
    $_SESSION[_zDid] = '';
    $iD = uniqid();
    zd_('<style>'
            . 'div.zdHeaderRed{margin-top: 15px;background: #fdcd0b;text-align: center;padding: 5px;color: #fff;font-weight: bold;font-size: 16px;letter-spacing: 1.2px;}'
            . '.aslowas {margin-top: 10px;
    font-size: calc(2vw + .6vh + .6vmin);
    text-align: center;}'
            . '.zDPrice {text-align: center;
    margin-top: -18px;
    margin-bottom: -5px;}'
            . '.zDinL {display: inline-block;}'
            . '.zDPriceLow{font-size: calc(6vw + 1.5vh + 2vmin);
    font-weight: 600;}'
            . 'div.zDdecimls,div.zDSymbol {font-size: calc(2vw + 1.5vh + 2vmin);
    font-weight: 600;}'
            . '.zDSymbol {vertical-align: super;margin-right: 3px;}'
            . '.zDinL.zDMeal{font-size: calc(1.5vw + .6vh + .6vmin);padding-left: 9px;vertical-align: super;letter-spacing: 1.2px;}'
            . '.zDCaption {text-align: center;
    color: #00e900;
    font-size: calc(1.5vw + .6vh + .6vmin);
    font-weight: 700;
    letter-spacing: 1px;}'
            . '.zDAddCart {font-size: calc(1.5vw + .6vh + .6vmin);background-color: #fdcd0b;margin: 0 auto;width: fit-content;width: -moz-fit-content;padding: 5px 25px;color: #fff;border-radius: 50px;margin-top: 5px;cursor: pointer;}'
            . 'select.zDSelect {margin-top: 5px;max-height: 30px;max-width: 230px;margin: 0 auto;}'
            . '.zDBodyRed {text-align: center;}'
            . '.zDSelectCaret{position: relative;margin: 0 auto;width: fit-content;width: -moz-fit-content;}'
            . '.zDSelectCaret:after{width: 0;height: 0;border-left:6px solid transparent;border-right:6px solid transparent;border-top:6px solid #fdcd0b;position:absolute;top:40%;right:9px;content:"";z-index:98;}'
            . '.zDAddCart:hover{background-color: rgb(193, 155, 0);}'
            . '.zRedMobile {background: #fff;padding-bottom: 15px;margin: 0 15px;}'
            . '</style>');
    zd_('<script type="text/javascript">function Change' . $iD . '(e){'
            . 'jQuery("#zDP' . $iD . '").html(jQuery(e).attr("data-low"));'
            . 'jQuery("#zDD' . $iD . '").html("."+jQuery(e).attr("data-decimal"));'
            . '}</script>');
    zDash_INI('zRedMobile' . _zDid, 'zRedMobile');
    _div('zdHeaderRed' . _zDid, 'zdHeaderRed');
    zd_('<img style="max-width: 22px;" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADMAAAArCAYAAADVJLDcAAAACXBIWXMAAAsTAAALEwEAmpwYAAAKTWlDQ1BQaG90b3Nob3AgSUNDIHByb2ZpbGUAAHjanVN3WJP3Fj7f92UPVkLY8LGXbIEAIiOsCMgQWaIQkgBhhBASQMWFiApWFBURnEhVxILVCkidiOKgKLhnQYqIWotVXDjuH9yntX167+3t+9f7vOec5/zOec8PgBESJpHmomoAOVKFPDrYH49PSMTJvYACFUjgBCAQ5svCZwXFAADwA3l4fnSwP/wBr28AAgBw1S4kEsfh/4O6UCZXACCRAOAiEucLAZBSAMguVMgUAMgYALBTs2QKAJQAAGx5fEIiAKoNAOz0ST4FANipk9wXANiiHKkIAI0BAJkoRyQCQLsAYFWBUiwCwMIAoKxAIi4EwK4BgFm2MkcCgL0FAHaOWJAPQGAAgJlCLMwAIDgCAEMeE80DIEwDoDDSv+CpX3CFuEgBAMDLlc2XS9IzFLiV0Bp38vDg4iHiwmyxQmEXKRBmCeQinJebIxNI5wNMzgwAABr50cH+OD+Q5+bk4eZm52zv9MWi/mvwbyI+IfHf/ryMAgQAEE7P79pf5eXWA3DHAbB1v2upWwDaVgBo3/ldM9sJoFoK0Hr5i3k4/EAenqFQyDwdHAoLC+0lYqG9MOOLPv8z4W/gi372/EAe/tt68ABxmkCZrcCjg/1xYW52rlKO58sEQjFu9+cj/seFf/2OKdHiNLFcLBWK8ViJuFAiTcd5uVKRRCHJleIS6X8y8R+W/QmTdw0ArIZPwE62B7XLbMB+7gECiw5Y0nYAQH7zLYwaC5EAEGc0Mnn3AACTv/mPQCsBAM2XpOMAALzoGFyolBdMxggAAESggSqwQQcMwRSswA6cwR28wBcCYQZEQAwkwDwQQgbkgBwKoRiWQRlUwDrYBLWwAxqgEZrhELTBMTgN5+ASXIHrcBcGYBiewhi8hgkEQcgIE2EhOogRYo7YIs4IF5mOBCJhSDSSgKQg6YgUUSLFyHKkAqlCapFdSCPyLXIUOY1cQPqQ28ggMor8irxHMZSBslED1AJ1QLmoHxqKxqBz0XQ0D12AlqJr0Rq0Hj2AtqKn0UvodXQAfYqOY4DRMQ5mjNlhXIyHRWCJWBomxxZj5Vg1Vo81Yx1YN3YVG8CeYe8IJAKLgBPsCF6EEMJsgpCQR1hMWEOoJewjtBK6CFcJg4Qxwicik6hPtCV6EvnEeGI6sZBYRqwm7iEeIZ4lXicOE1+TSCQOyZLkTgohJZAySQtJa0jbSC2kU6Q+0hBpnEwm65Btyd7kCLKArCCXkbeQD5BPkvvJw+S3FDrFiOJMCaIkUqSUEko1ZT/lBKWfMkKZoKpRzame1AiqiDqfWkltoHZQL1OHqRM0dZolzZsWQ8ukLaPV0JppZ2n3aC/pdLoJ3YMeRZfQl9Jr6Afp5+mD9HcMDYYNg8dIYigZaxl7GacYtxkvmUymBdOXmchUMNcyG5lnmA+Yb1VYKvYqfBWRyhKVOpVWlX6V56pUVXNVP9V5qgtUq1UPq15WfaZGVbNQ46kJ1Bar1akdVbupNq7OUndSj1DPUV+jvl/9gvpjDbKGhUaghkijVGO3xhmNIRbGMmXxWELWclYD6yxrmE1iW7L57Ex2Bfsbdi97TFNDc6pmrGaRZp3mcc0BDsax4PA52ZxKziHODc57LQMtPy2x1mqtZq1+rTfaetq+2mLtcu0W7eva73VwnUCdLJ31Om0693UJuja6UbqFutt1z+o+02PreekJ9cr1Dund0Uf1bfSj9Rfq79bv0R83MDQINpAZbDE4Y/DMkGPoa5hpuNHwhOGoEctoupHEaKPRSaMnuCbuh2fjNXgXPmasbxxirDTeZdxrPGFiaTLbpMSkxeS+Kc2Ua5pmutG003TMzMgs3KzYrMnsjjnVnGueYb7ZvNv8jYWlRZzFSos2i8eW2pZ8ywWWTZb3rJhWPlZ5VvVW16xJ1lzrLOtt1ldsUBtXmwybOpvLtqitm63Edptt3xTiFI8p0in1U27aMez87ArsmuwG7Tn2YfYl9m32zx3MHBId1jt0O3xydHXMdmxwvOuk4TTDqcSpw+lXZxtnoXOd8zUXpkuQyxKXdpcXU22niqdun3rLleUa7rrStdP1o5u7m9yt2W3U3cw9xX2r+00umxvJXcM970H08PdY4nHM452nm6fC85DnL152Xlle+70eT7OcJp7WMG3I28Rb4L3Le2A6Pj1l+s7pAz7GPgKfep+Hvqa+It89viN+1n6Zfgf8nvs7+sv9j/i/4XnyFvFOBWABwQHlAb2BGoGzA2sDHwSZBKUHNQWNBbsGLww+FUIMCQ1ZH3KTb8AX8hv5YzPcZyya0RXKCJ0VWhv6MMwmTB7WEY6GzwjfEH5vpvlM6cy2CIjgR2yIuB9pGZkX+X0UKSoyqi7qUbRTdHF09yzWrORZ+2e9jvGPqYy5O9tqtnJ2Z6xqbFJsY+ybuIC4qriBeIf4RfGXEnQTJAntieTE2MQ9ieNzAudsmjOc5JpUlnRjruXcorkX5unOy553PFk1WZB8OIWYEpeyP+WDIEJQLxhP5aduTR0T8oSbhU9FvqKNolGxt7hKPJLmnVaV9jjdO31D+miGT0Z1xjMJT1IreZEZkrkj801WRNberM/ZcdktOZSclJyjUg1plrQr1zC3KLdPZisrkw3keeZtyhuTh8r35CP5c/PbFWyFTNGjtFKuUA4WTC+oK3hbGFt4uEi9SFrUM99m/ur5IwuCFny9kLBQuLCz2Lh4WfHgIr9FuxYji1MXdy4xXVK6ZHhp8NJ9y2jLspb9UOJYUlXyannc8o5Sg9KlpUMrglc0lamUycturvRauWMVYZVkVe9ql9VbVn8qF5VfrHCsqK74sEa45uJXTl/VfPV5bdra3kq3yu3rSOuk626s91m/r0q9akHV0IbwDa0b8Y3lG19tSt50oXpq9Y7NtM3KzQM1YTXtW8y2rNvyoTaj9nqdf13LVv2tq7e+2Sba1r/dd3vzDoMdFTve75TsvLUreFdrvUV99W7S7oLdjxpiG7q/5n7duEd3T8Wej3ulewf2Re/ranRvbNyvv7+yCW1SNo0eSDpw5ZuAb9qb7Zp3tXBaKg7CQeXBJ9+mfHvjUOihzsPcw83fmX+39QjrSHkr0jq/dawto22gPaG97+iMo50dXh1Hvrf/fu8x42N1xzWPV56gnSg98fnkgpPjp2Snnp1OPz3Umdx590z8mWtdUV29Z0PPnj8XdO5Mt1/3yfPe549d8Lxw9CL3Ytslt0utPa49R35w/eFIr1tv62X3y+1XPK509E3rO9Hv03/6asDVc9f41y5dn3m978bsG7duJt0cuCW69fh29u0XdwruTNxdeo94r/y+2v3qB/oP6n+0/rFlwG3g+GDAYM/DWQ/vDgmHnv6U/9OH4dJHzEfVI0YjjY+dHx8bDRq98mTOk+GnsqcTz8p+Vv9563Or59/94vtLz1j82PAL+YvPv655qfNy76uprzrHI8cfvM55PfGm/K3O233vuO+638e9H5ko/ED+UPPR+mPHp9BP9z7nfP78L/eE8/sl0p8zAAAAIGNIUk0AAHolAACAgwAA+f8AAIDpAAB1MAAA6mAAADqYAAAXb5JfxUYAAAKTSURBVHja7NpPiI1RGMfx7zNuM1xlNiQlKwuEJaUoU0ZKNpTCkrJkZUqKkkx21MzKRkmklGgGNbGchdUtf8rUdM3KjYY0Qvws5nl15u1e953x3vueuc2pt3ve89zOPZ/3nPO+nfNek0SnpC46KHUs5jWgSI7fwFEzIzyAMnAauAzsDsoBKAWYTV7Rl4IvcA+wHNgcFkrqBZ4CO7zovKQBMxsMv5QckjRZ9FCRdMbbcjGESBr38leSBiX98vNTiSF6TApSkbTGy0942YfEEPUNIDW0JoADZlbz8GPgO7B6MdzNysCTYI7cN7OpADnq82tkMcyZH/5ZDfIDqWH3TtLaxTBnksaul3Q8mPDVMBZ2SFQYSWVJL7wt75PGeuxkGpkeXdFgHDLm7ZiStLFBbA4kOoykbkmPMkAm05CoMA55mAEyJxYdpgmkW9JoM0gUmBSkJmlrFmR0GEnLJD0IINsWCikU45DbGSA1SVsy1tl+TBNIKYBI0qV51NteTAryWdL2BjGllwBRYepAdv6jt24uFNPVhjliwBBwzFex/WY2nkCAWx6bBvqAyv/8WMt6RpJJGs7QI39j9VaahQ+zJhALhlM6FhcmBZmRtDcLMlbM9QDSlxUSHUbS1QyQGUl7su7OFIKRdKEexGNDjWLRYSStkPTVl7f7svRW3pg8nzP7gZXAczN7FkKAc8A34KCZjbXqmVbKsa7DyZaQI7qAKw752WpIbhhJPcAhP/0o6Ybj1jnkSKshefZMP7DK83eD8rfAWTMbacc6KS/MriA/AdwB7plZhTamvDDDwCdgzMxeFrVdlQvGzKrAtaJ3QzvqNWC6ZzZImi64TT15YN4w+yqwN4KLLGbfsc5vuC/9D2AJ0/r0ZwBuSKGRKJfPYgAAAABJRU5ErkJggg==">');
    zd_('PAY AS GO YOU PLANS');
    _ediv();
    _div('', 'zDBodyRed');
    _div('', 'aslowas');
    zd_('As low as');
    _ediv();
    _div('', 'zDPrice');
    _div('', 'zDinL zDSymbol');
    zd_('$');
    _ediv();
    _div('zDP' . $iD, 'zDinL zDPriceLow');
    zd_('6');
    _ediv();
    _div('zDD' . $iD, 'zDinL zDdecimls');
    zd_('.50');
    _ediv();
    _div('', 'zDinL zDMeal');
    zd_('PER MEAL');
    _ediv();
    _ediv();
    _div('', 'zDCaption', 'style="color: #ffffff;"');
    zd_('FREE DELIVERY INCLUDED');
    _ediv();
    _div('', 'zDSelectCaret');
    zd_('<select id="zDSelect' . $iD . '" class="zDSelect" name="plans" onchange="Change' . $iD . '(jQuery(this).find(\':selected\'))">'
            . '<option data-low="7" data-decimal="" value="12">12 Meals per week <strong>$84.00</strong></option>'
            . '<option data-low="6" data-decimal="99" alue="13">13 Meals per week <strong>$90.87</strong></option>'
            . '<option data-low="6" data-decimal="95" value="14">14 Meals per week <strong>$97.30</strong></option>'
            . '<option data-low="6" data-decimal="93" value="15">15 Meals per week <strong>$103.95</strong></option>'
            . '<option data-low="6" data-decimal="90" value="16">16 Meals per week <strong>$110.40</strong></option>'
            . '<option data-low="6" data-decimal="88" value="17">17 Meals per week <strong>$116.96</strong></option>'
            . '<option data-low="6" data-decimal="86" value="18">18 Meals per week <strong>$123.48</strong></option>'
            . '<option data-low="6" data-decimal="88" value="19">19 Meals per week <strong>$129.20</strong></option>'
            . '<option data-low="6" data-decimal="78" value="20">20 Meals per week <strong>$135.60</strong></option>'
            . '<option data-low="6" data-decimal="75" value="22">22 Meals per week <strong>$140.50</strong></option>'
            . '<option data-low="6" data-decimal="70" value="26">26 Meals per week <strong>$174.20</strong></option>'
            . '<option data-low="6" data-decimal="65" value="28">28 Meals per week <strong>$186.20</strong></option>'
            . '<option data-low="6" data-decimal="50" value="30">30 Meals per week <strong>$195.00</strong></option>'
            . '</select>');
    _ediv();
    _div('', 'zDAddCart', 'onclick="alert(\'Ailer\')"');
    zd_('Buy Now »');
    _ediv();
    _ediv();
    zDash_END();
    $r = $_SESSION[_zDid];
    $_SESSION[_zDid] = $saved;
    if ($e) {
        echo $r;
    } else {
        return $r;
    }
}

function __zDloadShop() {
    $ID_User = get_current_user_id();

    $Member = _zDisMember();


    $customer_orders = get_posts(array(
        'numberposts' => -1,
        'meta_key' => '_customer_user',
        'meta_value' => $ID_User,
        'post_type' => wc_get_order_types(),
        'post_status' => array_keys(wc_get_order_statuses()),
    ));
    zDClear();
    zDash_INI('zHedaer', _zDid, 'style="position:  fixed;top:  0px;left:  0px;right: 0px;background: #f3f3f6;padding: 10px 15px;text-align: center;font-size: calc(2vw + 1.1vh + 1.1vmin);padding-bottom: 5px;border-bottom: 1px solid #c5c5c5;"');
    $title = "SHOP";
    zd_('<a style="float: left;" href="javascript:history.back()"><span class="IdealNutrition in-last"></span></a>' . $title);
    zDash_END();
    zd_('<br><br>');
    if ($Member->isActive) {
        zd_('<div>');
        zd_('<img src="http://vacapp.co/wp-content/uploads/2018/09/fondo.png" style="width: 100%;">');
        zd_('</div>');
    }else{
        zd_(do_shortcode('[shortcode-suscription-plans-mobile]')); //_zDLoadGreen()
        zd_(do_shortcode('[shortcode-pay-as-go-you-plans-mobile]')); //_zDLoadRed()
    }



    zd_('<style>'
            . 'body{background: #eeeff5 !important;}'
            . '.zDwidgetMovil {display: inline-block;width: 25%;margin-top: 5px;padding: 0px 3px;vertical-align: text-top;}'
            . '.prod-destacado-3 a {color: #fff !important;padding: 0px 4px !important;border: 1px solid #5df15d !important;background: #4ff04f !important;white-space: nowrap !important;font-size: 12px !important;border-radius: 7px !important;margin-top: 0 !important;}'
            . '.prod-destacado-3 .icono-feat {text-align: center;margin: 0 auto;padding: 5px 0;max-width: 60px;}'
            . '.prod-destacado-3 h4 {min-height: 38px !important;margin: 0 !important;background: url(/wp-content/uploads/2018/03/planes-down1.png) no-repeat bottom center !important;background-size: 100% 100% !important;padding: 8px 0 6px !important;color: #666666 !important;font-size: calc(1.5vw + .6vh + .6vmin) !important;line-height: 1.2em !important;}'
            . '</style>');
    zDash_INI('zWidgetMobile', _zDid);
    $r = '<div style="margin-top: 10px;padding: 0px 10px;">';

    $r .= '<div class="zDwidgetMovil">';
    $r .= '<div class="prod-destacado-3">';
    $r .= '<p class="see-boton-1"><a href="/individual-meals-menu/">See »</a></p>';
    $r .= '<div class="icono-feat"><img src="/wp-content/uploads/2018/03/icon_Individual-Meals.png" alt="Individual Meals" /></div>';
    $r .= '<h4>Individual Meals</h4>';
    $r .= '</div>';
    $r .= '</div>';

    $r .= '<div class="zDwidgetMovil">';
    $r .= '<div class="prod-destacado-3">';
    $r .= '<p class="see-boton-1"><a href="/by-the-pound/">See »</a></p>';
    $r .= '<div class="icono-feat"><img src="/wp-content/uploads/2018/03/icon_By-the-pound.png" alt="By the pound" /></div>';
    $r .= '<h4>By the pound</h4>';
    $r .= '</div>';
    $r .= '</div>';

    $r .= '<div class="zDwidgetMovil">';
    $r .= '<div class="prod-destacado-3">';
    $r .= '<p class="see-boton-1"><a href="/custom-meal/">See »</a></p>';
    $r .= '<div class="icono-feat"><img src="/wp-content/uploads/2018/03/icon_Custom-Meal.png" alt="Custom Meal" /></div>';
    $r .= '<h4>Custom Meal</h4>';
    $r .= '</div>';
    $r .= '</div>';

    $r .= '<div class="zDwidgetMovil"><div class="prod-destacado-3">';
    $r .= '<p class="see-boton-1"><a href="/gift-card/">See »</a></p>';
    $r .= '<div class="icono-feat"><img src="/wp-content/uploads/2018/03/icon_Gift-Card.png" alt="Gift card" /></div>';
    $r .= '<h4>Gift Card</h4>';
    $r .= '</div>';
    $r .= '</div>';

    $r .= '</div>';
    zd_($r);
    //zd_('<script type="text/javascript"> jQuery(document).ready(function(){jQuery(".zDBody").each(function(){jQuery(this).attr("style","overflow-y: scroll;max-height:"+(screen.availHeight-200)+"px;"+jQuery(this).attr("style"));});});</script>');
    zDash_END();

    echo $_SESSION[_zDid] . zDMyTab();
}

function __zDloadPayment() {
    $saved_methods = wc_get_customer_saved_methods_list(get_current_user_id());
    $has_methods = (bool) $saved_methods;
    $types = wc_get_account_payment_methods_types();

    zDClear();
    zd_('<style>'
            . 'body{background: #eeeff5 !important;}'
            . '.payment {background: #ffff;margin: 10px;padding: 10px;}
                .HeaderPayment { padding-left: 15px;font-size: calc(2vw + 1.1vh + 1.1vmin);padding-bottom: 5px;border-bottom: 1px solid #c5c5c5;background-color: white;}.BodyPayment {padding: 10px;}
                .footerPayment a{color: #ffffff;background-color: rgb(28, 233, 52);border-color: rgb(28, 233, 52);padding: 3px 6px;border-radius: 0px;b8b8ba}'
            . '</style>');
    zDash_INI('zHedaer', _zDid, 'style="position:  fixed;top:  0px;left:  0px;right: 0px;background: #f3f3f6;padding: 10px 15px;text-align: center;font-size: calc(2vw + 1.1vh + 1.1vmin);padding-bottom: 5px;border-bottom: 1px solid #c5c5c5;"');
    $title = "PAYMENT INFORMATION";
    zd_('<a style="float: left;" href="javascript:history.back()"><span class="IdealNutrition in-last"></span></a>' . $title);
    zDash_END();
    zd_('<br><br>');
    zd_('<br><br>');
    zDash_INI('zPayMentMobile', _zDid);
    if ($has_methods) {
        foreach ($saved_methods as $type => $methods) {
            $r = '';
            foreach ($methods as $method) {
                $r .= '<span>';
                if (!empty($method['method']['last4'])) {
                    $r .= esc_html(wc_get_credit_card_type_label($method['method']['brand'])) . ' ending in ' . esc_html($method['method']['last4']);
                } else {
                    $r .= esc_html(wc_get_credit_card_type_label($method['method']['brand']));
                }
                $r .= '</span>';
                $r .= '<span> <b>Expire:</b>';
                $r .= esc_html($method['expires']);
                $r .= '</span>';
                $b = '';
                foreach ($method['actions'] as $key => $action) {
                    $b .= '<a href="' . esc_url($action['url']) . '" class="button ' . sanitize_html_class($key) . '">' . esc_html($action['name']) . '</a>&nbsp;';
                }
                _div('', 'payment');
                _div('', 'HeaderPayment');
                zd_('Payment Methods ' . $type);
                _ediv();
                _div('', 'BodyPayment');
                zd_($r);
                _ediv();
                _div('', 'footerPayment');
                zd_($b);
                _ediv();
                _ediv();
                $r = '';
            }
        }
    }
    zd_($r);
    zDash_END();

    echo $_SESSION[_zDid] . zDMyTab();
}

function __zDloadAccountEdit() {
    $user = get_userdata(get_current_user_id());
    zDClear();
    zd_('<style>'
            . 'body{background: #eeeff5 !important;}'
            . '.edit-account:before {content: "" !important;}'
            . '.zDCard{box-shadow: 2px 1px 1px 1px #00000070;background: #ffff;margin: 10px;padding: 10px;}
                #wpua-remove-button-existing,#wpua-undo-button-existing{margin-bottom: 5px;}
                button,input[type="submit"] {width: fit-content !important;width: -moz-fit-content !important;background: #1ce934 !important;letter-spacing: 1px !important;border-radius: 0px !important;font-size: 12px !important;}
                #zEditAccountMobile .wpua-edit-container h3,#zEditAccountMobile .edit-account h3{font-size: calc(2vw + 1.1vh + 1.1vmin);margin-left: -10px;margin-right: -10px;padding-left: 20px;padding-bottom: 10px;border-bottom: 1px solid #c5c5c5;}
                #zEditAccountMobile label{display: block;font-size: calc(2vw + .6vh + .6vmin);color: #1ce934;font-weight: bold;}
                #zEditAccountMobile input{border-radius: 0px;width: 100%;padding-top: 4px;padding-bottom: 4px;}
                   .HeaderPayment { padding-left: 15px;padding-bottom: 5px;border-bottom: 1px solid #c5c5c5;background-color: white;}
                   .BodyPayment {padding: 10px;}
                   .footerPayment a{color: #ffffff;background-color: rgb(28, 233, 52);border-color: rgb(28, 233, 52);padding: 3px 6px;border-radius: 0px;b8b8ba}
                   .avatar_container [class^=icon-], .avatar_container [class*=" icon-"], .ci_controls [class*=" icon-"], .ci_controls [class^=icon-] {font-family: icomoon !important;}.avatar_container [class^="icon-"]:before, .avatar_container [class*=" icon-"]:before, .ci_controls [class^="icon-"]:before, .ci_controls [class*=" icon-"]:before {font-family: icomoon !important;}'
            . '</style>');
    zDash_INI('zHedaer', _zDid, 'style="position:  fixed;top:  0px;left:  0px;right: 0px;background: #f3f3f6;padding: 10px 15px;text-align: center;font-size: calc(2vw + 1.1vh + 1.1vmin);padding-bottom: 5px;border-bottom: 1px solid #c5c5c5;"');
    $title = "OTHER ACCOUNT DETAIL";
    zd_('<a style="float: left;" href="javascript:history.back()"><span class="IdealNutrition in-last"></span></a>' . $title);
    zDash_END();
    zd_('<br><br>');
    zDash_INI('zEditAccountMobile', _zDid);
    _div('', 'zDCard');
    zd_(do_shortcode('[avatar_upload]'));
    _ediv();
    _div('', 'zDCard');
    zd_('<form class="woocommerce-EditAccountForm edit-account" action="" method="post">');
    zd_('<h3>Edit Account</h3>');
    zd_('<p class="woocommerce-form-row woocommerce-form-row--first form-row form-row-first">');
    zd_('<label for="account_first_name">' . 'First name' . ' <span class="required">*</span></label>');
    zd_('<input type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="account_first_name" id="account_first_name" value="' . esc_attr($user->first_name) . '" />');
    zd_('</p>');
    zd_('<p class="woocommerce-form-row woocommerce-form-row--last form-row form-row-last">');
    zd_('<label for="account_last_name">' . 'Last name' . ' <span class="required">*</span></label>');
    zd_('<input type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="account_last_name" id="account_last_name" value="' . esc_attr($user->last_name) . '" />');
    zd_('</p>');
    zd_('<div class="clear"></div>');

    zd_('<p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">');
    zd_('<label for="account_email">' . 'Email address' . ' <span class="required">*</span></label>');
    zd_('<input type="email" class="woocommerce-Input woocommerce-Input--email input-text" name="account_email" id="account_email" value="' . esc_attr($user->user_email) . '" />');
    zd_('</p>');

    //zd_('<fieldset>');
    zd_('<h3>' . 'Password change' . '</h3>');

    zd_('<p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">');
    zd_('<label for="password_current">' . 'Current password (leave blank to leave unchanged)' . '</label>');
    zd_('<input type="password" class="woocommerce-Input woocommerce-Input--password input-text" name="password_current" id="password_current" />');
    zd_('</p>');
    zd_('<p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">');
    zd_('<label for="password_1">' . 'New password (leave blank to leave unchanged)' . '</label>');
    zd_('<input type="password" class="woocommerce-Input woocommerce-Input--password input-text" name="password_1" id="password_1" />');
    zd_('</p>');
    zd_('<p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">');
    zd_('<label for="password_2">' . 'Confirm new password' . '</label>');
    zd_('<input type="password" class="woocommerce-Input woocommerce-Input--password input-text" name="password_2" id="password_2" />');
    zd_('</p>');
    //zd_('</fieldset>');
    zd_('<div class="clear"></div>');

    zd_('<p>');
    zd_(wp_nonce_field('save_account_details'));
    zd_('<button type="submit" class="woocommerce-Button button" name="save_account_details" value="' . 'Save changes' . '">' . 'Save changes' . '</button>');
    zd_('<input type="hidden" name="action" value="save_account_details" />');
    zd_('</p>');

    zd_('</form>');
    _ediv();
    zDash_END();

    echo $_SESSION[_zDid] . zDMyTab();
    ;
}

function __zDloadAddressEdit() {
    zDClear();
    zd_('<style>'
            . 'body{background: #eeeff5 !important;}'
            . '.edit-account:before {content: "" !important;}'
            . '.zDCard{box-shadow: 2px 1px 1px 1px #00000070;background: #ffff;margin: 10px;padding: 10px;}
                #wpua-remove-button-existing,#wpua-undo-button-existing{margin-bottom: 5px;}
                button,input[type="submit"] {width: fit-content !important;width: -moz-fit-content !important;background: #1ce934 !important;letter-spacing: 1px !important;border-radius: 0px !important;font-size: 12px !important;}
                #zEditAccountMobile h3{font-size: calc(2vw + 1.1vh + 1.1vmin);margin-left: -10px;margin-right: -10px;padding-left: 20px;padding-bottom: 10px;border-bottom: 1px solid #c5c5c5;}
                #zEditAccountMobile label{display: block;font-size: calc(2vw + .6vh + .6vmin);color: #1ce934;font-weight: bold;}
                #zEditAccountMobile input{border-radius: 0px;width: 100%;padding-top: 4px;padding-bottom: 4px;}
                   .HeaderPayment { padding-left: 15px;padding-bottom: 5px;border-bottom: 1px solid #c5c5c5;background-color: white;}
                   .BodyPayment {padding: 10px;}
                   .footerPayment a{color: #ffffff;background-color: rgb(28, 233, 52);border-color: rgb(28, 233, 52);padding: 3px 6px;border-radius: 0px;b8b8ba}'
            . '</style>');
    zDash_INI('zHedaer', _zDid, 'style="position:  fixed;top:  0px;left:  0px;right: 0px;background: #f3f3f6;padding: 10px 15px;text-align: center;font-size: calc(2vw + 1.1vh + 1.1vmin);padding-bottom: 5px;border-bottom: 1px solid #c5c5c5;"');
    $title = "ACCOUNT ADDRESS";
    zd_('<a style="float: left;" href="javascript:history.back()"><span class="IdealNutrition in-last"></span></a>' . $title);
    zDash_END();
    zd_('<br><br>');
    zDash_INI('zEditAccountMobile', _zDid);
    _div('', 'zDCard');
    $customer_id = get_current_user_id();

    if (!wc_ship_to_billing_address_only() && wc_shipping_enabled()) {
        $get_addresses = apply_filters('woocommerce_my_account_get_addresses', array(
            'billing' => 'Billing address',
            'shipping' => 'Shipping address',
                ), $customer_id);
    } else {
        $get_addresses = apply_filters('woocommerce_my_account_get_addresses', array(
            'billing' => 'Billing address',
                ), $customer_id);
    }
    $col = 1;
    zd_('<p>The following addresses will be used on the checkout page by default.</p>');
    $r = '';
    foreach ($get_addresses as $name => $title) {
        $Acr = ( ( $col = $col * -1 ) < 0 ) ? 1 : 2;
        $r .= '<div class="u-column' . $Acr . ' col-' . $Acr . ' woocommerce-Address">';
        $r .= '<header class="woocommerce-Address-title title">';
        $r .= '<h3>' . $title . '</h3>';
        $r .= '<a href="' . esc_url(wc_get_endpoint_url('edit-address', $name)) . '" class="edit">Edit</a>';
        $r .= '</header>';
        $r .= '<address>';
        $address = wc_get_account_formatted_address($name);
        $r .= $address ? wp_kses_post($address) : 'You have not set up this type of address yet.';
        $r .= '</address>';
        $r .= '</div>';
    }
    zd_($r);
    _ediv();
    zDash_END();

    echo $_SESSION[_zDid] . zDMyTab();
}

function __zDloadAddressUpdate() {
    $type = explode("/", $_SERVER['REQUEST_URI']);
    $page_title = ( 'billing' === $type[3] ) ? __('Billing Address', 'woocommerce') : __('Shipping Address', 'woocommerce');
    $current_user = wp_get_current_user();
    $load_address = sanitize_key($type[3]);

    $address = WC()->countries->get_address_fields(get_user_meta(get_current_user_id(), $load_address . '_country', true), $load_address . '_');

    // Enqueue scripts.
    wp_enqueue_script('wc-country-select');
    wp_enqueue_script('wc-address-i18n');

    // Prepare values.
    foreach ($address as $key => $field) {

        $value = get_user_meta(get_current_user_id(), $key, true);

        if (!$value) {
            switch ($key) {
                case 'billing_email':
                case 'shipping_email':
                    $value = $current_user->user_email;
                    break;
                case 'billing_country':
                case 'shipping_country':
                    $value = WC()->countries->get_base_country();
                    break;
                case 'billing_state':
                case 'shipping_state':
                    $value = WC()->countries->get_base_state();
                    break;
            }
        }

        $address[$key]['value'] = apply_filters('woocommerce_my_account_edit_address_field_value', $value, $key, $load_address);
    }
    zDClear();
    zd_('<style>'
            . 'body{background: #eeeff5 !important;}'
            . '.edit-account:before {content: "" !important;}'
            . '.zDCard{box-shadow: 2px 1px 1px 1px #00000070;background: #ffff;margin: 10px;padding: 10px;}
                #wpua-remove-button-existing,#wpua-undo-button-existing{margin-bottom: 5px;}
                button,input[type="submit"] {width: fit-content !important;width: -moz-fit-content !important;background: #1ce934 !important;letter-spacing: 1px !important;border-radius: 0px !important;font-size: 12px !important;}
                #zEditAccountMobile .wpua-edit-container h3,#zEditAccountMobile .edit-account h3{font-size: calc(2vw + 1.1vh + 1.1vmin);margin-left: -10px;margin-right: -10px;padding-left: 20px;padding-bottom: 10px;border-bottom: 1px solid #c5c5c5;}
                #zEditAccountMobile label{display: block;font-size: calc(2vw + .6vh + .6vmin);color: #1ce934;font-weight: bold;}
                #zEditAccountMobile input,#zEditAccountMobile select{border-radius: 0px;width: 100%;padding-top: 4px;padding-bottom: 4px;}
                #zEditAccountMobile .checkbox input[type=checkbox]{float: left;/*margin-top: 20px;*/margin-right: 10px;/*margin-left: -20px;*/display: inline-block;width: fit-content !important;}
                   .HeaderPayment { padding-left: 15px;padding-bottom: 5px;border-bottom: 1px solid #c5c5c5;background-color: white;}
                   .BodyPayment {padding: 10px;}
                   .footerPayment a{color: #ffffff;background-color: rgb(28, 233, 52);border-color: rgb(28, 233, 52);padding: 3px 6px;border-radius: 0px;b8b8ba}'
            . '</style>');
    zDash_INI('zHedaer', _zDid, 'style="position:  fixed;top:  0px;left:  0px;right: 0px;background: #f3f3f6;padding: 10px 15px;text-align: center;font-size: calc(2vw + 1.1vh + 1.1vmin);padding-bottom: 5px;border-bottom: 1px solid #c5c5c5;"');
    $title = strtoupper('Edit ' . $page_title);
    zd_('<a style="float: left;" href="javascript:history.back()"><span class="IdealNutrition in-last"></span></a>' . $title);
    zDash_END();
    zd_('<br><br>');
    zDash_INI('zEditAccountMobile', _zDid);
    _div('', 'zDCard');
    zd_('<form method="post">');
    zd_('<h3>' . $page_title . '</h3>');
    zd_('<div class="woocommerce-address-fields">');
    zd_('<div class="woocommerce-address-fields__field-wrapper">');
    $r = '';
    foreach ($address as $key => $field) {
        if (isset($field['country_field'], $address[$field['country_field']])) {
            $field['country'] = wc_get_post_data_by_key($field['country_field'], $address[$field['country_field']]['value']);
        }$field['return'] = true;
        $r .= woocommerce_form_field($key, $field, wc_get_post_data_by_key($key, $field['value']));
    }
    zd_($r);
    _ediv();
    zd_('<p>');
    zd_('<button type="submit" class="button" name="save_address" value="Save address">Save address</button>');
    zd_(wp_nonce_field('woocommerce-edit_address'));
    zd_('<input type="hidden" name="action" value="edit_address" />');
    zd_('</p>');
    _ediv();
    zd_('</form>');
    _ediv();
    zDash_END();

    echo $_SESSION[_zDid] . zDMyTab();
}

function __zDloadMealsSubscription() {
    $User = get_currentuserinfo();
    $subscriptions = wcs_get_users_subscriptions();
    zDClear();
    zd_('<style>'
            . 'body{background: #eeeff5 !important;}'
            . '.edit-account:before {content: "" !important;}'
            . '.zDCard{box-shadow: 2px 1px 1px 1px #00000070;background: #ffff;margin: 10px;padding: 10px;}
                #wpua-remove-button-existing,#wpua-undo-button-existing{margin-bottom: 5px;}
                button,input[type="submit"] {width: fit-content !important;width: -moz-fit-content !important;background: #1ce934 !important;letter-spacing: 1px !important;border-radius: 0px !important;font-size: 12px !important;}
                .zSubscription h3{font-size: calc(2vw + 1.1vh + 1.1vmin);margin-left: -10px;margin-right: -10px;padding-left: 20px;padding-bottom: 10px;border-bottom: 1px solid #c5c5c5;}
                .zSubscription .position-r{float: right;margin-right: 5px;}.zSubscription .position-l{float: left}
                .clearfix {display: block;content: "";clear: both;}
                #zEditAccountMobile label{display: block;font-size: calc(2vw + .6vh + .6vmin);color: #1ce934;font-weight: bold;}
                #zEditAccountMobile input{border-radius: 0px;width: 100%;padding-top: 4px;padding-bottom: 4px;}
                   .HeaderPayment { padding-left: 15px;padding-bottom: 5px;border-bottom: 1px solid #c5c5c5;background-color: white;}
                   .BodyPayment {padding: 10px;}
                   .footerPayment a{color: #ffffff;background-color: rgb(28, 233, 52);border-color: rgb(28, 233, 52);padding: 3px 6px;border-radius: 0px;b8b8ba}'
            . '</style>');
    zDash_INI('zHedaer', _zDid, 'style="position:  fixed;top:  0px;left:  0px;right: 0px;background: #f3f3f6;padding: 10px 15px;text-align: center;font-size: calc(2vw + 1.1vh + 1.1vmin);padding-bottom: 5px;border-bottom: 1px solid #c5c5c5;"');
    $title = "SUBSCRIPTION MEMBER MENU";
    zd_('<a style="float: left;" href="javascript:history.back()"><span class="IdealNutrition in-last"></span></a>' . $title);
    zDash_END();
    zd_('<br><br>');
    zDash_INI('zEditAccountMobile', _zDid . ' zSubscription');
    $r = '';
    foreach ($subscriptions as $subscription) {
        if (esc_attr(wcs_get_subscription_status_name($subscription->get_status())) == 'Active') {
            $order_id = $subscription->get_data()['parent_id'];
            $r .= '<div class="zDCard">';
            $name = '';
            $j = '';
            foreach ($subscription->get_items() as $line_item) {

                $j = '<h3>' . str_replace(' meals per week', '', $line_item['name']) . '</h3>';
                $name = str_replace('', '', str_replace(' meals per week', '', explode('- ', $line_item['name'])[1]));
            }
            $r .= $j;
            $r .= '<span class="position-l">ORDER: <b>#' . $order_id . '</b></span><span class="position-r">';
            if (!__zSelectMealsbyOrder($order_id)) {
                $r .= 'Meals Selected<br>for the week? <span style="float: right;color: #F44336;font-weight: bold;">NO</span>';
            } else {
                $r .= 'Meals Selected<br>for the week? <span style="float: right;color: #1ce934;font-weight: bold;">YES</span>';
            }
            $r .= '</span><div class="clearfix"></div><hr>';
            if (!SelectMealsbyOrder($order_id)) {
                //Original
                //$r .= '<a style="margin-right: 5px;" href="/see-meal-suscriptions-plans-' . $name . '/?order=' . $order_id . '&ini-shopping" class="btn meal btn-warning btn-xs">See Meals</a>';
                $r .= '<a style="margin-right: 5px;" href="/select-your-meals/?order=' . $order_id . '&amp;plan" class="btn meal btn-warning btn-xs">See Meals</a>';
            }
            $r .= '<a style="margin-right: 5px;" href="/' . zpage_by_title(PAGE_VIEW_ORDER_OF_WEEK) . '/?order=' . $order_id . '" class="btn meal btn-primary btn-xs">View Order</a>';
            $r .= '<a style="margin-right: 5px;" href="/' . zpage_by_title(PAGE_VIEW_MEALS_OF_DELIVERY) . '/?order=' . $order_id . '" class="btn meal btn-danger btn-xs">Delivery Info</a>';
            $r .= '</div>';
        }
    }
    zd_($r);
    zDash_END();

    echo $_SESSION[_zDid] . zDMyTab();
}

function __ccs_mobile() {
    return '<style>'
            . 'div.zdHeaderGreen{margin-top: 15px;background: #00e900;text-align: center;padding: 5px;color: #fff;font-weight: bold;font-size: 16px;letter-spacing: 1.2px;}'
            . '.aslowas {    margin-top: 10px;
    font-size: calc(2vw + .6vh + .6vmin);
    text-align: center;}'
            . '.zDPrice {text-align: center;
    margin-top: -18px;
    margin-bottom: -5px;}'
            . '.zDinL {display: inline-block;}'
            . '.zDPriceLow{font-size: calc(6vw + 1.5vh + 2vmin);
    font-weight: 600;}'
            . 'div.zDdecimls,div.zDSymbol {font-size: calc(2vw + 1.5vh + 2vmin);
    font-weight: 600;}'
            . 'div.zDSymbol{vertical-align: super;margin-right: 3px;}'
            . '.zDinL.zDMeal{font-size: calc(1.5vw + .6vh + .6vmin);padding-left: 9px;vertical-align: super;letter-spacing: 1.2px;}'
            . '.zDCaption {text-align: center;
    color: #00e900;
    font-size: calc(1.5vw + .6vh + .6vmin);
    font-weight: 700;
    letter-spacing: 1px;}'
            . '.zDAddCartGreen {font-size: calc(1.5vw + .6vh + .6vmin);background-color: rgba(0,0,0,.5);margin: 0 auto;width: fit-content;width: -moz-fit-content;padding: 5px 25px;color: #fff;border-radius: 50px;margin-top: 5px;cursor: pointer;}'
            . 'select.zDSelect {margin-top: 5px;max-height: 30px;max-width: 230px;margin: 0 auto;}'
            . '.zDBodyGreen {text-align: center;}'
            . '.zDSelectCaretGreen{position: relative;margin: 0 auto;width: fit-content;width: -moz-fit-content;}'
            . '.zDSelectCaretGreen:after{width: 0;height: 0;border-left:6px solid transparent;border-right:6px solid transparent;border-top:6px solid #00e900;position:absolute;top:40%;right:9px;content:"";z-index:98;}'
            . '.zDAddCartGreen:hover{background-color: rgba(0, 0, 0, 0.7);}'
            . '.zGreenMobile {background: #fff;padding-bottom: 15px;margin: 0 15px;}'
            . '</style>';
}
