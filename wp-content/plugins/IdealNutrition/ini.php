<?php

/*
  Plugin Name: Product List Subscription
  Description: DProduct List Subscription require Woocommerce
  Version: 5.3.4.0
  Author: JB
  License: Free or later
 */

if (!defined('ABSPATH')) {
    die('You are not allowed to call this page directly.');
}

require_once plugin_dir_path(__FILE__) . 'libs/zGlobals.php';

class IdealNutrition {

    protected $globlals = null;
    protected $admin_menu = null;
    protected $plans = null;
    protected $loads = array('ajax', 'functions', 'zMenu', 'zPlans', 'html', 'zinstaller', 'SelectYourMeals', 'zCuponez', 'resources', 'myAccount', 'zDashboard', 'report', 'zEmail');
    protected $ajax = array('notify_button_click', 'update_fee', 'get_limit_orders');
    protected $Dashboard;

    function __construct() {
        $this->globlals = new zGlobals(array(dirname(__FILE__), plugin_dir_path(__FILE__), $this->loads));
        $this->init();
    }

    function init() {
        $this->globlals->init();
        $this->globlals->loadAjax($this->ajax);
        new zinstaller();
        $this->Dashboard = new zDashboard();
        $this->Dashboard->run();
        $this->request();

        $this->zPlans = new zPlans();
        $this->zPlans->init();

        $this->admin_menu = new zMenu();
        $this->admin_menu->_Tab();
    }

    function request() {
        add_action('woocommerce_payment_complete', 'z_woocommerce_payment_complete', 10, 1);
        add_filter('woocommerce_add_to_cart_validation', 'so_validate_add_cart_item', 10, 5);
        add_action('init', 'clear_cart');
        if (isset($_REQUEST['subscription_action'])) {
            add_action('init', 'zRequestMove');
        }
        if (isset($_REQUEST['post_type'])) {
            if ($_REQUEST['post_type'] === 'shop_subscription') {
                if (isset($_REQUEST['marked_on-hold'])) {
                    if (intval($_REQUEST['marked_on-hold']) === 1) {
                        if (isset($_REQUEST['post']) && isset($_REQUEST['action'])) {
                            if ($_REQUEST['action'] === 'on-hold') {
                                if(get_post($_REQUEST['post'])){
                                    add_action('init', 'subs_supend');
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    public function getSubscriptionStatus( $user_id, $status ) {
        return wcs_user_has_subscription($user_id, '', $status);
    }

}

New IdealNutrition();
