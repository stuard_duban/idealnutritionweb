<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of zCuponez
 *
 * @author Windows
 */
class zCuponez {

    //put your code here
    public function __construct() {
        
    }

    public function init($e = false) {
        $saved = $_SESSION[_zPid];
        $_SESSION[_zPid] = '';
        $iD = uniqid();
        zp_('<style>'
                . 'div.zCupon {
    transition: all 0.3s ease-out;
    position: fixed;
    z-index: 9;
    left: 0px;
    top: 0px;
    right: 0px;
    bottom: 0px;
    background-color: #716d6d96;
}
div.zpform {
    position: absolute;
    top: 50%;
    left: 50%;
    margin-top: -112.5px;
    margin-left: -150px;
    height: 225px;
    width: 300px;
    border-radius: 5px;
    background-color: white;
    box-shadow: -1px 1px 3px 1px black;
}
div.zpHeaderCupon {
    padding: 10px 40px;
    border-bottom: 1px solid #1b921b;
    background-color: #01c309;
    color: #ffffff;
    font-weight: bold;
}
div.zpHeaderCupon .in-ideal:before {color: #ffffff;}
a.btncoupon {
    padding: 10px 15px;
    background-color: #00f100;
    color: white;
    margin-bottom: 20px;
    margin-right: 20px;
    right: 0px;
    bottom: 0px;
    position: absolute;
    box-shadow: 0.3px 0.2px 3px 0px #00000096;
}
.btnzCupon {
    transition: all 0.3s ease-out;
    position: fixed;
    top: 113px;
    right: 22px;
    cursor: pointer;
    text-transform: uppercase;
    margin-top: 3px;
    display: block;
    background-color: rgb(1, 195, 9);
    color: #fff !important;
    font-size: 12px;
    text-align: center;
    padding: 7px 15px;
    border-radius: 4px;
    border-bottom: 3px solid #086709;
    width: fit-content;
    width: -moz-fit-content;
}
.btnzCupon a{color: #fff !important;}'
                . '</style>');
        zp_('<script type="text/javascript">'
                . ''
                . '</script>');

        _zdiv('btnzCupon' . $iD, 'btnzCupon');
        zp_('<a href="javascript:void(0)" onclick="jQuery(\'#zCupon' . $iD . '\').show();">Enter Coupon Code</a>');
        _ezdiv();

        zPash_INI('zCupon' . $iD, 'zCupon', 'style="display:none;"');
        _zdiv('zpform' . $iD, 'zpform');
        _zdiv('zpHeaderCupon' . $iD, 'zpHeaderCupon');
        zp_('<span class="IdealNutrition in-ideal" style="font-size: 40px;position: absolute;left: 0px;top: 0px;"></span>');
        zp_('Enter your promotion coupon');
        zp_('<span class="IdealNutrition in-cross" onclick="jQuery(\'#zCupon' . $iD . '\').hide(500);" style="position: absolute;bottom: initial;top: 5px;right: 4px;color: #F44336;cursor: pointer;display:  block;background: transparent;"></span>');
        _ezdiv();
        _zdiv('', 'zPBodyCupon');

        zp_('<form id="verycoupon" method="POST" enctype="multipart/form-data">');
        zp_('<input type="hidden" name="ajax" value="' . admin_url('admin-ajax.php') . '">');
        zp_('<input type="hidden" name="action" value="notify_button_click">');
        zp_('<input type="text" name="coupon" placeholder="Enter Coupon Code" id="coupon' . $iD . '" value="" style="display: block;margin: 20px;border-radius: 0px;width: 100%;max-width: 260px;">');
        zp_('<div id="alert" style="font-size: 14px;padding: 0px 25px;text-align: left;margin-bottom: 12px;color: #F44336;"></div>');
        zp_('<a id="btncoupon' . $iD . '" href="javascript:void(0)" class="btncoupon">Apply coupon</a>');
        zp_('<div style="display: block;content: "";clear: both;"></div>');
        zp_('</form>');
        _ezdiv();
        _ezdiv();
        zPash_END();
        $r = $_SESSION[_zPid];
        $_SESSION[_zPid] = $saved;
        if ($e) {
            echo $r;
        } else {
            return $r;
        }
    }

    public function addCoupon() {
        $RS = strtoupper(uniqid());
        ?>
        <form method = "POST" action = "" enctype = "multipart/form-data">
            <input type = "hidden" name = "subscription_action" value = "zcoupon">
            <input type="hidden" name="coupon" value="<?= $RS; ?>">
            <label for="codecoupon">Coupon code:</label>
            <input id="codecoupon" style="max-width: 125px;background-color: white;color:  red;font-weight: bold;" disabled type="text" value="<?= $RS; ?>">
            <label for="meals">Quantity Meals:</label>
            <input required="" id="meals" type="number" style="max-width: 50px;" name="meals" min="1" max="60" value="1">
            <label for="dateexpire">Expiration date</label>
            <input required="" id="dateexpire" style="max-width: 150px;" type="date" name="dateexpire">
            <input class="" type="submit" value="Generate" />
        </form>
        <?php
    }

    public function AllList() {
        global $wpdb;
        $table_name = $wpdb->prefix . WP_TB_Coupon;
        $DB = $wpdb->get_results("SELECT * FROM $table_name ORDER BY id ASC");
        $r = '';
        ?>
        <table class="wp-list-table widefat fixed striped tags ui-sortable">
            <thead>
                <tr>
                    <td id="cb" class="manage-column column-cb check-column">
                        <label class="screen-reader-text" for="cb-select-all-1">Select All</label>
                        <input id="cb-select-all-1" type="checkbox">
                    </td>
                    <!--th scope="col" id="thumb" class="manage-column column-thumb">Image</th-->
                    <th scope="col" id="name" class="manage-column column-slug column-primary sortable desc">
                        <span style="line-height: 50px;">Coupon</span>
                    </th>
                    <th scope="col" id="slow" class="manage-column column-slug column-primary sortable desc">
                        <span style="line-height: 50px;">Meals</span>
                    </th>
                    <th scope="col" id="date" class="manage-column column-slug column-primary sortable desc">
                        <span style="line-height: 50px;">Status</span>
                    </th>
                    <th scope="col" id="description" class="manage-column column-slug sortable desc">
                        <span>Expiration date</span>
                    </th>

                    <th scope="col" id="handle" class="manage-column column-handle" style="display: table-cell;"></th>
                </tr>
            </thead>
            <tbody id="the-list" data-wp-lists="list:tag">
                <?php if (count($DB) !== 0) {
                    ?>
                    <?php
                    foreach ($DB as $key => $item) {
                        ?>
                        <tr id="tag-24">
                            <th scope="row" class="check-column">
                                <label class="screen-reader-text" for="cb-select-<?= $item->id; ?>"><?= $item->coupon; ?></label>
                                <input type="checkbox" name="delete_tags[]" value="<?= $item->id; ?>" id="cb-select-<?= $item->id; ?>">
                            </th>
                            <td class="name column-slug has-row-actions column-primary" data-colname="Name">
                                <strong>
                                    <a class="row-title" href="#" aria-label="“<?= $item->coupon ?>” (Edit)"><?= $item->coupon; ?></a>
                                </strong>
                                <br>
                                <div class="hidden" id="inline_24">
                                    <div class="name"><?= $item->coupon; ?></div>
                                    <div class="slug"><?= $item->meals; ?></div>
                                    <div class="parent"><?= $item->status; ?></div>
                                </div>
                                <div class="row-actions">
                                    <span class="edit">
                                        <a href="javascript:void(0)" onclick="jQuery('#PopUp_Update_<?= $item->id ?>').show()" aria-label="Edit “<?= $item->coupon; ?>”">
                                            Edit
                                        </a> | 
                                    </span>
                                    <!--span class="delete">
                                        <a href="#" class="delete-tag aria-button-if-js" aria-label="Delete “<?/*; */?>”" role="button">
                                            Delete
                                        </a> | 
                                    </span-->
                                </div>
                            </td>
                            <td class="slug column-slug" data-colname="Slug"><?= $item->meals; ?></td>
                            <td class="slug column-slug" data-colname="Slug"><?= $item->status;                             ?></td>
                            <td class="description column-slug" data-colname="Description">
                                <span aria-hidden="true"><?= $item->dateexpire; ?></span>
                                <span class="screen-reader-text">shortcode-<?= $item->dateexpire; ?></span>
                            </td>

                            <td class="handle column-handle ui-sortable-handle" data-colname="" style="display: table-cell;">
                                <input type="hidden" name="term_id" value="<?= $item->id; ?>">
                            </td>
                        </tr>
                        <?php
                        $r .= '<div id="PopUp_Update_' . $item->id . '" style="display: none">
        <div></div>
        <div>
            <form method = "POST" action = "" enctype = "multipart/form-data">
            <input type = "hidden" name = "subscription_action" value = "zcoupon">
            <input type="hidden" name="id" value="' . $item->id . '">
            <label for="codecoupon">Coupon code:</label>
            <input id="codecoupon" style="max-width: 125px;background-color: white;color:  red;font-weight: bold;" disabled type="text" value="' . $item->coupon . '">
            <label for="meals">Quantity Meals:</label>
            <input required="" id="meals" type="number" style="max-width: 50px;" name="meals" min="1" max="60" value="' . $item->meals . '">
            <label for="dateexpire">Expiration date</label>
            <input required="" id="dateexpire" style="max-width: 150px;" type="date" name="dateexpire" value="' . $item->dateexpire . '">
            <label for="status">Status Coupon</label>
            <select required="" id="status" name="status">
                <option value="Available">Available</option>
                <option value="Disabled">Disabled</option>
                <option value="Locked">Locked</option>
                <option value="Used">Used</option>
            </select>
            <input class="" type="submit" value="Generate" />
        </form>
        </div>
    </div> <script type="text/javascript">
    jQuery(document).ready(function(){
    jQuery("#status").val("' . $item->status . '")
});
</script>';
                    }
                }
                ?>
            </tbody>
            <tfoot>
                <tr>
                    <td class="manage-column column-cb check-column">
                        <label class="screen-reader-text" for="cb-select-all-2">Select All</label>
                        <input id="cb-select-all-2" type="checkbox">
                    </td>
                    <th scope="col" class="manage-column column-slug column-primary sortable desc">
                        <span>Coupon</span>
                    </th>
                    <th scope="col" id="slow" class="manage-column column-slug column-primary sortable desc">
                        <span style="line-height: 50px;">Meals</span>
                    </th>
                    <th scope="col" id="date" class="manage-column column-slug column-primary sortable desc">
                        <span style="line-height: 50px;">Status</span>
                    </th>
                    <th scope="col" class="manage-column column-slug sortable desc">
                        <span>Expiration date</span>
                    </th>

                    <th scope="col" class="manage-column column-handle" style="display: table-cell;"></th>
                </tr>
            </tfoot>
        </table>    
        <?php
        echo $r;
    }

    public function save() {
        global $wpdb;
        $table_name = $wpdb->prefix . WP_TB_Coupon;
        $id = isset($_REQUEST['id']) ? $_REQUEST['id'] : false;
        //var_dump($_REQUEST);
        if ($id) {
            $wpdb->show_errors(); 
            $DB = $wpdb->get_results("SELECT * FROM $table_name WHERE id=" . $id . " ORDER BY id ASC");
            if ($DB !== 0) {
                $wpdb->update($table_name, array(
                    'meals' => isset($_REQUEST['meals']) ? $_REQUEST['meals'] : 1,
                    'status' => isset($_REQUEST['status']) ? $_REQUEST['status'] : 'Locked',
                    'dateexpire' => isset($_REQUEST['dateexpire']) ? date('Y-m-d', strtotime($_REQUEST['dateexpire'])) : date('Y-m-d')
                        ), array('id' => $id));
            }
        } else {
            
            $wpdb->insert($table_name, array(
                "users" => 0,
                "ip" => '0.0.0.0',
                "coupon" => isset($_REQUEST['coupon']) ? $_REQUEST['coupon']: strtoupper(uniqid()),
                "meals" => isset($_REQUEST['meals']) ? $_REQUEST['meals']: 1,
                "status" => isset($_REQUEST['status']) ? $_REQUEST['status']: 'Available',
                "dates" => date('Y-m-d'),
                "dateused" => '0000-00-00',
                "dateexpire" => isset($_REQUEST['dateexpire']) ? $_REQUEST['dateexpire']: date('Y-m-d'),
            ));
        }
    }
    
    public function getcoupon() {
        global $wpdb;
        $table_name = $wpdb->prefix . WP_TB_Coupon;
        $id = isset($_REQUEST['coupon']) ? $_REQUEST['coupon'] : false;
        if ($id) {
            $DB = $wpdb->get_results("SELECT * FROM $table_name WHERE coupon='" . $id . "'");
            if ($DB !== 0) {
                $DB = $DB[0];
                return $DB;
            }
        }else{
            return false;
        }
    }

}
