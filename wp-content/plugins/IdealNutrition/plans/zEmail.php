<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of zEmail
 *
 * @author JB
 */
class zEmail {
    //put your code here
    private function mailHeader() {
        return '<table style="margin: 0 auto;width: 100%;">
        <tr>
            <td>
                <p style="text-align: right;">
                    <span><a target="_blank" href="#"><img src="http://vacapp.co/wp-content/plugins/IdealNutrition/email/resource/instagram.png" alt="Instagram" srcset=""></a></span>
                    <span> <a target="_blank" href="https://facebook.com/idealnutritionnow"><img src="http://vacapp.co/wp-content/plugins/IdealNutrition/email/resource/facebook.png" alt="Facebook" srcset=""></a></span>
                    <span><a target="_blank" href="mailto:info@idealnutritionsofla.com?subject=Contact Web"><img src="http://vacapp.co/wp-content/plugins/IdealNutrition/email/resource/mail.png" alt="mail" srcset=""></a></span>
                </p>
            </td>
        </tr>
        <tr>
            <td style="text-align:center;"><img style="margin-bottom:30px" src="http://vacapp.co/wp-content/uploads/2018/03/logo_idealbig.png" alt="" srcset=""></td>
        </tr>
        <tr>
            <td style="text-align:center;background-color: red;padding: 20px 0px;font-size: 24px;font-family: Arial;color: #fff;">
                Order Update
            </td>
        </tr>
    </table>';
    }

    private function mailBody($id, $order) {
        $r = '<table style="margin: 0 auto;width: 100%;background-color: #f4f4f4;">
        <tr>
            <td>
                <table style="margin: 0px auto;">
                    <tr>
                        <td><img style="margin-bottom:30px" src="http://vacapp.co/wp-content/plugins/IdealNutrition/email/resource/alertc.png" alt="" srcset=""></td>
                        <td style="    vertical-align: text-top;">
                            <p style="font-family: Arial;font-size: 18px;">We noticed you’ve made changes to your order. <br>
                                    Here is a list of your updated meals</p>
                        </td>
                    </tr>
                </table>
                <p style="text-align: center;">Your order has been received, and is now being processed. Order #' . $id . '</p>
                <table style="width:100%;margin: 0 auto;">';
        $j = '';
        foreach ($order as $item) {
            $j .= '<tr>
                        <td style="width:10%;text-align: center;"><p style="font-family: Arial;font-size: 18px;">
                        ' . $item['n'] . '
                        </p></td>
                        <td style="width:80%"><p style="font-family: Arial;font-size: 18px;">
                        ' . $item['product'] . '
                        </p></td>
                        <td style="width:10%"><p style="font-family: Arial;font-size: 18px;">
                        x <strong>' . $item['qty'] . '</strong>
                        </p></td>
                    </tr>';
        }

        return $r. $j . '</table>
            </td>
        </tr>
    </table>';
    }

    private function mailFooter() {
        return '<table style="margin: 0 auto;width: 100%;">
        <tr>
            <td style="background-color: #fff;">
                <p style="text-align:center;font-family: Arial;"><a style="color: #fff;text-decoration: none;padding: 7px 29px;background-color: #01f11a;border-radius: 19px;" href="http://vacapp.co/log-in/" target="_blank" rel="noopener noreferrer">Go to your account</a></p>    
                <br><br><br>
            </td>
        </tr>
        <tr><td style="background-color: #01f11a;padding:5px"></td></tr>
        <tr>
            <td style="text-align:center;">
                <img src="http://vacapp.co/wp-content/plugins/IdealNutrition/email/resource/card.PNG" alt="card" srcset="">
            </td>
        </tr>
        <tr>
            <td style="background-color: #3a403f;">
                <p style="font-family: Arial;text-align:center;color: #fff;">Central - Copyright © 2018 mediosefectivos.co . All Rights Reserved.</p>
            </td>
        </tr>
    </table>';
    }

    public function send($id, $order) {
        global $current_user;
        get_currentuserinfo();

        add_filter('wp_mail_content_type', 'wpdocs_set_html_mail_content_type');
        $to = $current_user->user_email;

        $body = $this->mailHeader() . $this->mailBody($id, $order) . $this->mailFooter();
        $headers = array('Content-Type: text/html; charset=UTF-8');
        //$headers[] = 'From: '.PAGE_CHECKING_BOOKING_PLUGIN.' <no-reply@webmaster.com>';
        $headers[] = 'Cc: ' . $current_user->user_firstname.' '.$current_user->user_lastname . ' <' . $to . '>';
        $headers[] = 'Cc: ' . $to; // note you can just use a simple email address

        wp_mail($to, 'We noticed you’ve made changes to your order.', $body, $headers);

        remove_filter('wp_mail_content_type', 'wpdocs_set_html_mail_content_type');
    }
    
    public function Mail_Susp() {
        return '<table style="margin: 0 auto;width: 100%;max-width: 740px;">
        <tr>
            <td>
                <p style="text-align: right;">
                    <span><a target="_blank" href="#"><img src="http://vacapp.co/wp-content/plugins/IdealNutrition/email/resource/instagram.png" alt="Instagram" srcset=""></a></span>
                    <span> <a target="_blank" href="https://facebook.com/idealnutritionnow"><img src="http://vacapp.co/wp-content/plugins/IdealNutrition/email/resource/facebook.png" alt="Facebook" srcset=""></a></span>
                    <span><a target="_blank" href="mailto:info@idealnutritionsofla.com?subject=Contact Web"><img src="http://vacapp.co/wp-content/plugins/IdealNutrition/email/resource/mail.png" alt="mail" srcset=""></a></span>
                </p>
            </td>
        </tr>
        <tr>
            <td style="text-align:center;"><img style="margin-bottom:30px" src="http://vacapp.co/wp-content/uploads/2018/03/logo_idealbig.png" alt="" srcset=""></td>
        </tr>
    </table>
    <table style="margin: 0 auto;width: 100%;background-color: #f4f4f4;max-width: 740px;">
        <tr>
            <td>
                <table style="margin: 50px auto;">
                    <tr>
                        <td><img style="margin-bottom:30px" src="http://vacapp.co/wp-content/plugins/IdealNutrition/email/resource/alert.png" alt="" srcset=""></td>
                        <td style="    vertical-align: text-top;">
                            <p style="font-family: Arial;font-size: 22px;"><h2 style="font-family: Arial;color:red;font-family: Arial;color:red;margin-bottom: 0px;">Your account is suspended</h2>. <br>
                                In order to reactivate your <br>
account, you must contact us</p><hr style="border-color: #00f100;">
<a style="font-family: Arial;font-size: 14px;" target="_blank" href="mailto:info@idealnutritionsofla.com?subject=Contact Web">info@idealnutritionsofla.com</a>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <table style="margin: 0 auto;width: 100%;max-width: 740px;">
        <tr>
            <td style="background-color: #fff;">
                <p style="text-align:center;font-family: Arial;"><a style="color: #fff;text-decoration: none;padding: 7px 29px;background-color: #01f11a;border-radius: 19px;" href="http://vacapp.co/log-in/" target="_blank" rel="noopener noreferrer">Go to your account</a></p>    
                <br><br><br>
            </td>
        </tr>
        <tr><td style="background-color: #01f11a;padding:5px"></td></tr>
        <tr>
            <td style="text-align:center;">
                <img src="http://vacapp.co/wp-content/plugins/IdealNutrition/email/resource/card.PNG" alt="card" srcset="">
            </td>
        </tr>
        <tr>
            <td style="background-color: #3a403f;">
                <p style="font-family: Arial;text-align:center;color: #fff;">Central - Copyright © 2018 mediosefectivos.co . All Rights Reserved.</p>
            </td>
        </tr>
    </table>';
    }
    
    public function wp_send($current_user) {
        add_filter('wp_mail_content_type', 'wpdocs_set_html_mail_content_type');
        $to = $current_user->user_email;

        $body = $this->Mail_Susp();
        $headers = array('Content-Type: text/html; charset=UTF-8');
        //$headers[] = 'From: '.PAGE_CHECKING_BOOKING_PLUGIN.' <no-reply@webmaster.com>';
        $headers[] = 'Cc: ' . $current_user->user_firstname.' '.$current_user->user_lastname . ' <' . $to . '>';
        $headers[] = 'Cc: ' . $to; // note you can just use a simple email address

        wp_mail($to, 'IDEAL NUTRITION Your account is suspended.', $body, $headers);

        remove_filter('wp_mail_content_type', 'wpdocs_set_html_mail_content_type');
    }
}
