<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of SelectYourMeals
 *
 * @author Windows
 */
class SelectYourMeals {

    protected $id;
    protected $variable;
    protected $key = false;
    protected $step = null;
    protected $category = 0;
    protected $cupon = null;
    //put your code here
    protected $products;
    protected $products_html;
    protected $index;
    protected $zSymbol;
    protected $title;
    protected $Meals;
    protected $price;
    protected $subcategory = 0;
    protected $products_sub;
    protected $products_sub_html;
    protected $categoty_index;
    protected $categoty_product_index;
    //form
    protected $uniq;
    protected $isSeeMeal;
    protected $isEdit;
    protected $Order;
    protected $CategoryStep = null;
    protected $user;
    protected $token;
    protected $locked;
    protected $UserInfo;

    protected $plan;

    public function __construct($Nothing = false) {
        $this->UserInfo = false;
        $this->locked = false;
        $this->uniq = uniqid();
        $this->zSymbol = get_woocommerce_currency_symbol();
        $this->id = isset($_REQUEST['add-to-cart']) ? $_REQUEST['add-to-cart'] : 0;
        $this->variable = isset($_REQUEST['variation_id']) ? $_REQUEST['variation_id'] : 0;

        $this->plan = $_REQUEST['plan'];


        $this->key = false;
        $this->category = 0;
        $this->products = array();
        $this->products_html = array();
        $this->index = 0;
        $this->subcategory = array();

        $this->products_sub = array();
        $this->products_sub_html = array();
        $this->step = array();
        $this->CategoryStep = array();
        $this->categoty_index = 0;
        $this->categoty_product_index = 0;
        $this->isEdit = false;
        $this->isSeeMeal = false;

        $this->user = 0;
        $this->token = false;

        global $woocommerce;
        global $wpdb;
        if (isset($_REQUEST['order'])) {
            $this->Order = $_REQUEST['order'];
            $jOrder = new WC_Order($this->Order);
            $items = $jOrder->get_items();
            $table_name = $wpdb->prefix . 'termmeta';
            foreach ($items as $key => $product) {
                if ($this->id === 0 && $this->variable === 0) {
                    $res = get_post_meta($product->get_product_id());
                    $attr = (unserialize($res['_product_attributes'][0]));
                    if (count($attr) > 0) {
                        foreach ($attr as $key => $value) {
                            if ($this->id === 0 && $this->variable === 0) {
                                if (strlen(trim($key)) > 8) {
                                    $RD = $wpdb->get_results("SELECT * FROM $table_name WHERE meta_key='plans' AND meta_value = '" . $key . "'");
                                    if (count($RD) !== 0) {
                                        $this->step = $wpdb->get_results("SELECT * FROM $table_name WHERE meta_key='step1' AND term_id=" . $RD[0]->term_id);
                                        $this->id = $product->get_product_id();
                                        $this->variable = $product->get_variation_id();
                                        $this->key = $key;
                                        $this->category = $RD[0]->term_id;
                                        $this->isEdit = true;
                                        if (isset($_REQUEST['plan'])) {
                                            $this->isEdit = false;
                                            $this->isSeeMeal = true;
                                            if (isset($_REQUEST['user'])) {
                                                $this->user = $_REQUEST['user'];
                                                $this->token = isset($_REQUEST['token']) ? $_REQUEST['token'] : false;
                                                if ($this->token !== hash('sha256', $this->user)) {
                                                    $this->token = false;
                                                }
                                            }
                                        }
                                    }
                                }
                            } else {
                                break;
                            }
                        }
                    }
                } else {
                    break;
                }
            }
        } else {
            if (!$Nothing) {
                $table_name = $wpdb->prefix . "orders_in";
                $woocommerce->cart->empty_cart();
                if ($this->variable) {
                    $woocommerce->cart->add_to_cart($this->variable);
                    $wpdb->delete($table_name, array('ip' => zAddressIPs(), 'orders' => '-'));
                }
            }
        }

        $this->cupon = new zCuponez();

        $Plans = wc_get_product($this->variable);
        $this->title = $Plans->name;
        $this->price = $Plans->price;
        if (class_exists('WC_Subscriptions_Product') && WC_Subscriptions_Product::is_subscription($this->id)) {
            $this->Meals = floatval(str_replace('Subscription Plans - ', '', str_replace(' meals per week', '', $this->title)));
        } else {
            $this->Meals = floatval(str_replace('Pay As Go You Plans - ', '', str_replace(' meals per week', '', $this->title)));
        }
        //var_dump($this->title);
        if (!$this->key && $this->category === 0) {
            $res = get_post_meta($this->id);
            $attr = (unserialize($res['_product_attributes'][0]));
            $table_name = $wpdb->prefix . 'termmeta';
            foreach ($attr as $key => $att) {
                if (strlen(trim($key)) > 8) {
                    $RD = $wpdb->get_results("SELECT * FROM $table_name WHERE meta_key='plans' AND meta_value = '" . $key . "'");
                    if (count($RD) !== 0) {
                        $this->key = $key;
                        $this->category = $RD[0]->term_id;
                        $this->step = $wpdb->get_results("SELECT * FROM $table_name WHERE meta_key='step1' AND term_id=" . $RD[0]->term_id);
                    }
                }
            }
        }

        $products = $wpdb->get_results("SELECT object_id FROM " . $wpdb->prefix . "term_relationships WHERE term_taxonomy_id = " . $this->category);
        if (!empty($products)) {
            foreach ($products as $product) {
                $this->products[] = $product->object_id;
            }
        }
        if (count($this->step) !== 0) {
            $this->step = json_decode($this->step[0]->meta_value);
            foreach ($this->step as $key => $category) {
                $products = $wpdb->get_results("SELECT object_id FROM " . $wpdb->prefix . "term_relationships WHERE term_taxonomy_id = " . $category);
                $Cat = $wpdb->get_results("SELECT * FROM " . $wpdb->prefix . "terms WHERE term_id=" . $category);
                //var_dump($Cat);                exit();
                if (!empty($products)) {
                    $this->CategoryStep[$Cat[0]->term_id] = array(
                        'id' => $Cat[0]->term_id,
                        'name' => $Cat[0]->name,
                        'count' => 0//(count($products))
                    );
                    foreach ($products as $product) {
                        $this->products[] = $product->object_id;
                    }
                }
            }
        }
        //var_dump($this->products);

        foreach ($this->products as $key => $id) {
            $Product = wc_get_product($id);
            $access = $Product->get_data();
            $children = $Product->get_children();
            if (count($children) > 0) {
                foreach ($children as $product_children_id) {
                    $product_children = wc_get_product($product_children_id);
                    $data = $product_children->get_data();
                    if (isset($data["attributes"])) {
                        //var_dump($access["attributes"]["date"]["options"]);
                        $rang = explode('~', $access["attributes"]["date"]["options"][0]);
                        date_default_timezone_set("America/New_York");
                        $now = date('Y-m-d');
                        //var_dump(date('Y-m-d H:m:s'));
                        $start = date('Y-m-d', strtotime($rang[0]));
                        $end = date('Y-m-d', strtotime($rang[1]));
                        $category_id = -1;
                        $terms = get_the_terms($id, 'product_cat');
                        //var_dump($this->CategoryStep);
                        foreach ($terms as $key => $cats) {
                            //var_dump($cats);
                            if (isset($this->CategoryStep[$cats->term_id])) {
                                $category_id = $cats->term_id;
                            }
                        }
                        foreach ($data["attributes"] as $key => $mode) {
                            if ($key === 'mode') {
                                if ($mode === 'Subscription' && strtotime($end . '') >= strtotime($now . '') && strtotime($now . '') >= strtotime($start . '')) {
                                    //echo $mode . '===Subscription' . ' && ' . $end . ' >= ' . $now . ' && ' . $now . ' >= ' . $start . '<br>';  //                              exit();
                                    $this->products[$key] = $product_children_id;
                                    $this->products_html[] = array(
                                        'id' => $product_children_id,
                                        'name' => $product_children->name,
                                        'slug' => $product_children->slug,
                                        'sku' => $product_children->sku,
                                        'category' => $category_id,
                                        'start' => $start,
                                        'end' => $end,
                                        'purchasable' => $product_children->is_purchasable() ? 'purchasable' : 'nopurchasable',
                                        'description' => $Product->description,
                                        'picture' => get_the_post_thumbnail($product_children->id, 'thumbnail', array('class' => 'aligncenter')),
                                        'price' => floatval($product_children->price),
                                        'price_html' => $this->zSymbol . number_format(floatval($product_children->price), 2)
                                    );
                                    $Update = intval($this->CategoryStep[$category_id]['count']);
                                    $Update++;
                                    $this->CategoryStep[$category_id]['count'] = $Update;
                                } else {
                                    if ($mode === 'Subscription') {
                                        unset($this->products[$key]);
                                    }
                                }
                            }
                        }
                    }
                }
                if ($this->products[$key] === $id) {
                    unset($this->products[$key]);
                }
            } else {
                //echo $key;
                unset($this->products[$key]);
            }
        }

        /* ORDER BY SKU PRODUCTS */
        if (is_array($this->products_html)) {
            $order = [];
            for ($idx = 1; $idx <= count($this->products_html); $idx++) {
                foreach ($this->products_html as $item) {
                    if (intval($item['sku']) === intval($idx)) {
                        $order[] = $item;
                    }
                }
            }
            $this->products_html = $order;
        }
//var_dump($this->products_html);
        //echo '<pre>';
        //var_dump($this->products);
        $categories = get_terms('product_cat', array('child_of' => $this->category));
        foreach ($categories as $key => $val) {
            $this->subcategory[$val->term_id] = array(
                'id' => $val->term_id,
                'name' => $val->name,
                'slug' => $val->slug
            );
        }
        foreach ($this->subcategory as $sc) {
            $products = $wpdb->get_results("SELECT object_id FROM " . $wpdb->prefix . "term_relationships WHERE term_taxonomy_id = " . $sc['id']);
            if (!empty($products)) {
                $this->products_sub[$sc['id']] = array();
                $this->products_sub_html[$sc['id']] = array();
                foreach ($products as $product) {
                    $this->products_sub[$sc['id']][] = $product->object_id;
                    $Product = wc_get_product($product->object_id);
                    $this->products_sub_html[$sc['id']][] = array(
                        'id' => $Product->id,
                        'name' => $Product->name,
                        'slug' => $Product->slug,
                        'sku' => $Product->sku,
                        'purchasable' => $Product->is_purchasable() ? 'purchasable' : 'nopurchasable',
                        'description' => $Product->description,
                        'picture' => get_the_post_thumbnail($Product->id, 'thumbnail', array('class' => 'aligncenter')),
                        'price' => floatval($Product->price),
                        'price_html' => $this->zSymbol . number_format(floatval($Product->price), 2)
                    );
                }
            }
        }
        if (is_user_logged_in()) {
            $this->UserInfo = wp_get_current_user();

            $subscriptions = get_posts(array(
                'numberposts' => -1,
                'post_type' => 'shop_subscription', // Subscription post type
                'post_status' => 'wc-on-hold', // Active subscription
                'order' => 'ASC',
                'meta_key' => '_customer_user',
                'meta_value' => $this->UserInfo->ID,
            ));
            //($subscriptions);
            if (count($subscriptions) !== 0) {
                $this->locked = true;
            }
        }
    }

    public function init() {

        $r = $this->styles();
        if (!$this->locked) {
            $r .= $this->header();
            $r .= $this->progress();
            if (!$this->isEdit) {
                if (!$this->isSeeMeal) {
                    //$r .= $this->cupon->init();
                }
                $r .= '<div class="zpDashControl" style="display:none;" ready="0">';
                $r .= '<a class="btnFixed" onclick="jQuery(\'.zpDashControl\').hide();'
                        . 'jQuery(\'.zpDashControl\').attr(\'ready\',\'0\');'
                        . 'jQuery(\'#zMoveStickyCont\').attr(\'style\',\'text-align: center;margin-top: 30px;opacity: 1;display:block\');'
                        . 'jQuery(\'#zPfixed\').attr(\'class\',\'zPfixed show\');jQuery(\'.btnzCupon\').show();'
                        . 'jQuery(\'#zPStep2\').attr(\'class\',\'zgrid\');'
                        . 'jQuery(\'#zPStep1\').attr(\'class\',\'zgrid actived MaxReady\');">PREVIOUS STEP</a>';
                if ($this->isSeeMeal) {
                    $r .= '<a id="nextstepu" class="btnFixed" data-key="' . $this->uniq . '" data-update="1" onclick="jQuery(\'#DeliDates\').show();">NEXT STEP</a>';
                } else {
                    $r .= '<a id="nextstepu" class="btnFixed" data-key="' . $this->uniq . '" data-update="0" onclick="jQuery(\'#' . $this->uniq . '\').click();">Go Checkout</a>';
                }
                $r .= '</div>';
            }
            if ($this->user !== 0 && !$this->token) {
                $r .= '<h2>Your token is incorrect, you do not have permission to access here</h2>';
            } else {
                $r .= $this->grid('1');
                if (!$this->isEdit) {
                    $r .= $this->grid('2');
                }
                if ($this->isSeeMeal) {
                    $r .= $this->PopUpSeeMeal();
                }
                $r .= $this->PopUp();

                $r .= $this->form_proccess_checkout();

               // echo "</br>entro a pago";
                //var_dump($r);
                //echo "</pre>";
                // /exit;
            }
        } else {
            $j = $this->PopUpLocked();
            if ($j) {
                $r .= $j;
            }
        }
        return $r . '<br><br><br><br>';
    }

    public function styles() {
        $r = '<style>';
        $r .= 'h1 {
                text-align: center;
            }';
        $r .= '.zMoveSticky{
                    transition: all 0.3s ease-out;z-index: 9999;position: fixed !important;top: 65px !important;font-size: 20px !important;left: initial !important;right: 20px !important;width:  fit-content;padding:  5px 20px !important;color:  #fff;border-radius:  5px;background-color: #01c309;
               }';
        $r .= 'div.ZPProduct {
                    max-width: 160px;margin: 0 auto;
                }';
        $r .= '.ZPControlAdd {
                    text-align: center;
                    min-height: 24px;
                    margin-bottom: 10px;
               }';
        $r .= '.ZPControlAdd span {
                    color: #fff;
                    display: none;
                    background-color: #01c309;
                    border-radius: 50px;
                    padding: 7px;
                    font-size: 9px;
                    margin-left: 2.5px;
                    margin-right: 2.5px;
                    cursor: pointer;
                }';
        $r .= '.ZPQuanty {
                    position: absolute;
                    display: none;
                    color: #fff;
                    background: #01c309;
                    width: 30px;
                    height: 30px;
                    font-size: 13px;
                    padding: 6px;
                    text-align: center;
                    font-weight: 900;
                    border-radius: 22px;
                    top: -10px;
                    left: -10px;
                }';
        $r .= '.ZPPicture {
                position: relative;cursor: pointer;min-height: 160px;
            }';
        $r .= '.BtnAddSelect {
                    background-color: #01c309;
                    color: #fff;
                    border-radius: 5px;
                    width: 76px;min-width: 76px;white-space: nowrap;
                    font-size: 14px;
                    font-weight: bold;
                    text-align: center;
                    padding: 2px 10px;
                    cursor: pointer;
                    position: absolute;
                    z-index: 1;
                    bottom: -5px;
                    right: 0px;
                }';
        $r .= '.BtnAddSelect:hover, .ZPControlAdd span:hover {background-color: rgb(108, 185, 18);}';
        $r .= 'span.IdealNutrition.in-cross {
                    display: none;
                    padding: 10px;
                    font-size: 18px;
                    background-color: #f1f1f1;
                    color: #F44336;
                    border-radius: 50%;
                    cursor: pointer;
                    position: absolute;
                    right: -10px;
                    bottom: -10px;
                }';
        $r .= 'span.IdealNutrition.in-cross:hover {background-color: #F44336;color: #f1f1f1;}';
        $r .= '.ZPPrice {
                position: absolute;
                display:none;
                top: 0px;
                right: 0px;
                color: #fff;
                background-color: #34495ed1;
                padding: 2px 10px;
                font-size: 14px;
            }';
        $r .= '.ZPDescription {
                    cursor: default;
                    position: absolute;
                    display: none;
                    margin-left: -15px;
                    padding: 7px;
                    margin-top: -25px;
                    border-radius: 5px;
                    max-width: 190px;
                    background-color: #34495e;
                    color: #ffffff;
                    text-align: center;
                    z-index: 99999;
                    line-height: 16px;
                }';
        $r .= '.ZPDescription:before {
                    content: "";
                    position: absolute;
                    top: -12px;
                    left: 50%;
                    margin-left: -25px;
                    border-bottom: 12px solid #34495e;
                    border-top-color: inherit;
                    border-left: 25px solid #2d2d2d00;
                    border-right: 25px solid transparent;
                }';
        $r .= '.ZPDescription span {cursor: default;color: #fff !important}';
        $r .= 'div.ZPProduct:hover .ZPPrice,div.ZPProduct:hover .ZPDescription {
                display: block;
            }';
        $r .= '.ZPName {
                cursor: default;
                text-align: center;
                margin-top: 16px!important;
                font-size: 12px;
                line-height: 14px;
                margin-left: -25px;
                margin-right: -25px;
                border-radius: 5px;
                padding: 10px;
                background-color: rgb(52, 73, 94);
                text-overflow: ellipsis;
                overflow: hidden;
                white-space: nowrap;
                color: #fff;
            }';
        $r .= '.ZPSelectItem {
                position: absolute;
                display: none;
                top: 0px;
                left: 4px;
                right: 4px;
                bottom: 0px;
                /*background-color: #7b7b7bbd;*/
            }';
        $r .= '.MaxReady .ZPProduct {opacity: 0.3;}';
        $r .= '.Select .ZPControlAdd span, .Select .ZPQuanty, .Select .ZPSelectItem{display:initial;position: absolute;}';
        $r .= '.Select .BtnAddSelect{display: none}';
        $r .= '.Select {opacity: 1 !important;}';
        $r .= '.MaxReady span.in-plus {background-color: #9E9E9E !important;cursor: not-allowed !important;display:none;}';
        $r .= '.MaxReady .disabled {
                position: absolute;
                top: 27px;
                height: 173px;
                width: 100%;
                cursor: not-allowed;
            }';
        $r .= '.Select .disabled {
                display: none;
            }';
        $r .= '.MaxReady .ZPDescription,.MaxReady .ZPPrice,.MaxReady .BtnAddSelect {display: none !important;}';
        $r .= 'div.Select:hover .ZPPrice,div.Select:hover .ZPDescription {
                display: block !important;
            }';
        $r .= '.Select .ZPPicture {
                border: 5px solid #01c309;
                border-radius: 5px;
            }';
        $r .= '.Select span.IdealNutrition.in-cross {display: block;}';
        $r .= '.progress{
                position: relative;
                border-radius: 40px;
                max-width: 80%;
                margin: 10px auto;
                min-height: 30px;
                max-height: 30px;
                background-color: #dedede;
            }';
        $r .= '.progress_process {
                position: relative;
                height: 30px;
                border-radius: 40px;
                background-color: #01c309;
            }';
        $r .= '.progress_price_container {
                position: absolute;
                top: 50%;
                right: 0px;
                margin-top: -30px;
                border-radius: 50%;
                background-color: #01c309;
                min-width: 60px;
                min-height: 60px;
                height: 60px;
                max-width: 60px;
                font-size: 14px;
                color: #fff;
                text-align: center;
                font-weight: bold;
                border-bottom: 4px solid #385f0a;
            }';
        $r .= '.progress_symbol {
                display: inline-block;
                margin-top: 18px;
                max-width: 10px;
            }';
        $r .= '.progress_price {
                display: inline-block;
            }';
        $r .= '.zpcol {
                display: inline-block;
            }';
        $r .= '.zpcol-1,.zpcol-2,.zpcol-3,.zpcol-4 {
                width: 25%;
                text-align: center;
            }';
        $r .= '.zprow {position: relative;}';
        $r .= '.zgrid {
                max-width: 865px;
                margin: 0 auto;
            }';
        $r .= '.zPfixed{'
                . 'position:  fixed;
            top: 0px;
            left:  0px;
            right:  0px;
            bottom:  0px;
            background-color: #505050d1;
            z-index: 99;
            height:100%;
            width:100%;
            overflow:hidden;
            display:none;
            transition: all 0.3s ease-out;
        }';
        $r .= '.show{transition: all 0.3s ease-out;display:table !important;}';
        $r .= '.zPfixed .contenedor{
            display:table-cell;
            width:100%;
            vertical-align:middle;
            text-align:center;
        }';
        $r .= '.WinPopUp {
            display: inline-block;
            margin: 0 auto;
            width: 265px;
            border-radius: 3px;
            height: 200px;
            background-color: white;
        }';
        $r .= '.Header {
            padding: 7px;
            font-weight: bold;
            font-size: 16px;
            background-color: #00a70100;
            color: #00a701;
            text-align: center;
            letter-spacing: 1px;
            text-transform: uppercase;
        }';
        $r .= '.Boddy {
                padding: 10px;
            }';
        $r .= 'a.here{cursor: pointer;
            display: block;
            background-color: #F44336;
            color: #fff;
            text-align: center;
            padding: 7px;
            border-radius: 4px;
            border-bottom: 3px solid #B71C1C;
            }';
        $r .= 'a.additional{
            cursor: pointer;
            display: block;
            margin-top: 3px;
            background-color: #2196F3;
            color: #fff;
            text-align: center;
            padding: 7px;
            border-radius: 4px;
            border-bottom: 3px solid #0D47A1;
            }';
        $r .= 'a.checkout{
            cursor: pointer;
            display: block;
            margin-top: 3px;
            background-color: #FFC107;
            color: #fff;
            text-align: center;
            padding: 7px;
            border-radius: 4px;
            border-bottom: 3px solid #c59503;
            }';
        $r .= 'a.green{'
                . 'cursor: pointer;
        text-transform: uppercase;
        margin-top: 3px;
        display: block;
        background-color: rgba(0, 167, 1, 1);
        color: #fff;
        text-align: center;
        padding: 7px;
        border-radius: 4px;
        border-bottom: 3px solid #086709;'
                . '}';
        $r .= 'a.green:hover {background-color: rgb(18, 208, 26);}';
        $r .= '.additionalTitle {
                    text-align: center;
                    margin-top: 30px;
                    font-weight: bold;
                }';
        $r .= '.zgrid{display:none;} .actived{display:block;}';

        $r .= '.zpDashControl{position: relative;top: 0px;max-width: 225px;margin: -45px auto 0px;display: block;}';
        $r .= 'a.btnFixed{
                white-space: nowrap;
                cursor: pointer;
                text-transform: uppercase;
                margin-top: 3px;
                display: block;
                background-color: rgb(1, 195, 9);
                color: #fff !important;
                font-size: 12px;
                text-align: center;
                padding: 7px 15px;
                border-radius: 4px;
                border-bottom: 3px solid #086709;
                width: 100%;
                    }';
        $r .= '.btnzCupon{z-index:99}';
        $r .= '.nopurchasable{position:relative;opacity: .8;}';
        $r .= '.nopurchasable span.IdealNutrition.in-error {font-size: 30px;line-height: 50px;}';
        $r .= '.nopurchasablealert {position: absolute;cursor: not-allowed;width: 100%;height: 171px;text-align: center;top: 23px;padding-top: 40px;font-size: 20px;line-height: 22px;font-weight: bold;color: #ffffff;background-color: #000000ba;}';
        $r .= '.nopurchasable .ZPDescription,.nopurchasable .ZPControlSelect{display:none !important;}';
        $r .= '.zpDashControlFixed{position: fixed;top: 100px;right: 22px;min-width: 141px;z-index: 9999;}';
        $r .= '.zpDashControl a.btnFixed{display: inline-block;width: 107px;margin-left: 2.5px;margin-right: 2.5px;}';
        $r .= '.zpDashControlFixed a.btnFixed{display: block !important;width: 100%!important;margin-left: 0px!important; margin-right: 0px!important;}';
        $r .= $this->responsive();
        $r .= '</style>';
        return $r;
    }

    public function responsive() {
        $r = '';
        $r .= '
            @media screen and (max-width: 998px) {
                .zpcol-1, .zpcol-2, .zpcol-3, .zpcol-4 {width: 49%;}
            }
            @media screen and (max-width: 400px) {
                .ZPName {margin-left: 0px;margin-right: 0px;}
                h1 {text-align: center;font-size: 28px;}
                .zMoveSticky {top: 0px !important;right: 0px !important;}
                .btnzCupon {top: 40px !important;right: 0px !important;}
            }';
        return $r;
    }

    public function header() {
        if($this->plan == "pag") {

          $r = '<h1>Order Meals for Your Pay as you go Plan</h1>';
        }
        else {
          $r = '<h1>Order Meals for Your Subscription Plan</h1>';
        }

        $r .= '<div style="text-align:  center;margin-top: -25px;font-size: 80px;line-height: 30px;"><span class="IdealNutrition in-ideal" style=""></span></div>';
        return $r;
    }

//
    public function progress() {
        $r = '<div class="progress">';
        $r .= '<div class="progress_process" style="width: 10%;">';
        $r .= '<div class="progress_price_container">';
        $r .= '<div class="progress_symbol">';
        $r .= $this->zSymbol;
        $r .= '</div>';
        $r .= '<div id="progress_price" class="progress_price" data-price-start="' . number_format($this->price, 2) . '">';
        $r .= number_format($this->price, 2);
        $r .= '</div>';
        $r .= '</div>';
        $r .= '</div>';
        $r .= '</div>';
        $r .= '<div id="zMoveStickyCont" style="text-align: center;margin-top: 30px;"><h2 id="zMoveSticky" class="">Selected <span id="Qty">0</span>/<span id="MaxQty">' . $this->Meals . '</span> Meals</h2></div>';
        return $r;
    }

    public function view($step = '1') {
        if ($step === '1') {
            $product = $this->products_html[$this->index];
            $MaxQ = $this->Meals;
            //var_dump($product);
        }
        if ($step === '2') {
            $product = $this->products_sub_html[$this->categoty_index][$this->categoty_product_index];
            $MaxQ = 99;
        }
        $r = ''; //
        $r .= '<div select="0" id="ZPProduct' . $product['id'] . '" data-id="' . $product['id'] . '" data-sku="' . $product['sku'] . '" data-symbol="' . $this->zSymbol . '" data-name="' . $product['name'] . '" data-price="' . $product['price'] . '" data-price-html="' . $product['price_html'] . '" class="ZPProduct Product' . $product['id'] . ' ' . $product['purchasable'] . '" data-unique="' . uniqid() . '">';
        $r .= '<div class="ZPControlAdd">'
                . '<span style="margin-left: -28px;" id="negative' . $product['id'] . '" class="IdealNutrition in-negative" onclick="if(parseInt(jQuery(\'#Quanty' . $product['id'] . '\').val())<=parseInt(jQuery(\'#Quanty' . $product['id'] . '\').attr(\'min\'))){return}jQuery(\'#Quanty' . $product['id'] . '\').val(parseInt(jQuery(\'#Quanty' . $product['id'] . '\').val())-1);jQuery(\'#vQuanty' . $product['id'] . '\').html(jQuery(\'#Quanty' . $product['id'] . '\').val());jQuery(\'#Quanty' . $product['id'] . '\').change()"></span>'
                . '<span style="margin-left: 5px;" id="plus' . $product['id'] . '" class="IdealNutrition in-plus" onclick="if(parseInt(jQuery(\'#Quanty' . $product['id'] . '\').val())>=parseInt(jQuery(\'#Quanty' . $product['id'] . '\').attr(\'max\'))){return}jQuery(\'#Quanty' . $product['id'] . '\').val(parseInt(jQuery(\'#Quanty' . $product['id'] . '\').val())+1);jQuery(\'#vQuanty' . $product['id'] . '\').html(jQuery(\'#Quanty' . $product['id'] . '\').val());jQuery(\'#Quanty' . $product['id'] . '\').change()"></span>'
                . '</div>';

        $r .= '<div class="ZPPicture" onclick="if(jQuery(\'#ZPProduct' . $product['id'] . '\').attr(\'select\')===\'0\'){jQuery(\'#ZPProduct' . $product['id'] . '\').attr(\'class\',\'ZPProduct Select\');jQuery(\'#ZPProduct' . $product['id'] . '\').attr(\'select\',\'1\');jQuery(\'#plus' . $product['id'] . '\').click();}else{jQuery(\'#Quanty' . $product['id'] . '\').val(\'0\');jQuery(\'#vQuanty' . $product['id'] . '\').html(\'0\');jQuery(\'#Quanty' . $product['id'] . '\').change();jQuery(\'#ZPProduct' . $product['id'] . '\').attr(\'class\',\'ZPProduct\');jQuery(\'#ZPProduct' . $product['id'] . '\').attr(\'select\',\'0\');}">';
        $r .= $product['picture'];
        $r .= '<div class="ZPSelectItem"></div>';
        $r .= '<div id="vQuanty' . $product['id'] . '" class="ZPQuanty">0</div>';
        $r .= '<input id="Quanty' . $product['id'] . '" class="zUpdate" min="1" data-id="' . $product['id'] . '" data-name="' . $product['name'] . '" max="' . $MaxQ . '" type="hidden" step="' . $step . '" change="zUpdate(\'#Quanty' . $product['id'] . '\');" value="0">';
        if (floatval($product['price']) > 0) {
            $r .= '<div class="ZPPrice">' . $product['price_html'] . '</div>';
        }
        $r .= '<div class="ZPControlSelect">'
                . '<div class="BtnAddSelect"><span class="IdealNutrition in-checkmark"></span> Select</div>'
                . '<span class="IdealNutrition in-cross"></span>'
                . '</div>';
        $r .= '</div>';
        $r .= '<div class="disabled"></div>';
        if ($product['purchasable'] === 'nopurchasable') {
            $r .= '<div class="nopurchasablealert">'
                    . '<div>Product<br>not available</div>'
                    . '<span class="IdealNutrition in-error"></span>'
                    . '</div>';
        }
        $r .= '<div class="ZPName">' . str_replace(' - Subscription', '', $product['name']) . '</div>';
        $r .= '<div class="ZPDescription">' . $product['description'] . '</div>';
        $r .= '</div>';
        return $r;
    }

    public function grid($step = '1') {
        $count = 1;
        $Max = 4;
        $j = '';
        $this->index = 0;
        if ($step === '1') {
            $_products_ = $this->CategoryStep; //$this->products_html
            $start = 0;
        }
        if ($step === '2') {
            $_products_ = $this->products_sub_html;
            $start = 1;
        }
        foreach ($_products_ as $category => $item) {
            if ($step === '1') {
                $count = 1;
                $j .= '<div class="additionalTitle"><h2 style="text-transform: uppercase;">' . $item['name'] . '</h2></div>';
                for ($index1 = $start; $index1 < intval($item['count']); $index1++) {
                    if ($count === 1) {
                        $r = '<div class="zprow">';
                    }
                    $r .= '<div class="zpcol-' . $count . ' zpcol">';
                    $r .= $this->view();
                    $this->index += 1;
                    $r .= '</div>';
                    if ($count === $Max) {
                        $r .= '</div>';
                        $j .= $r;
                        $r = '';
                        $count = 0;
                    }
                    $count++;
                }

                if (strlen(trim($r)) > 0) {
                    $r .= '</div>';
                    $j .= $r;
                    $r = '';
                }
            }
            if ($step === '2') {
                $this->categoty_index = $category;
                $this->categoty_product_index = 0;
                $j .= '<div class="additionalTitle"><h2>' . $this->subcategory[$category]['name'] . '</h2></div>';
                foreach ($item as $more) {
                    //echo count($more['id']).'<br>';
                    if ($count === 1) {
                        $r = '<div class="zprow">';
                    }
                    $r .= '<div class="zpcol-' . $count . ' zpcol">';

                    $r .= $this->view($step);
                    $this->categoty_product_index += 1;
                    $r .= '</div>';
                    if ($count === $Max) {
                        $r .= '</div>';
                        $j .= $r;
                        $r = '';
                        $count = 0;
                    }
                    $count++;
                }
                if (strlen(trim($r)) > 0) {
                    $r .= '</div>';
                    $j .= $r;
                    $r = '';
                }
            }
        }
        if (strlen(trim($r)) > 0) {
            $r .= '</div>';
            $j .= $r;
        }
        $class = $step === '1' ? 'actived' : '';
        return '<div id="zPStep' . $step . '" class="zgrid ' . $class . '">' . $j . '</div>';
    }

    public function PopUpSeeMeal() {
        $r = '';
        $r = '<div id="DeliDates" class="zPfixed" style="display:none;">'
                . '<div class="contenedor">'
                . '<div class="WinPopUp" style="width: 100%;height: 100%;max-height: 260px;margin-top: 0px;padding: 10px 20px;background-color: #00ea00;max-width: 400px;">' . '<div class="Header" style="color:#fff!important">';
        $r .= '<span class="IdealNutrition in-cross" onclick="jQuery(\'#DeliDates\').hide(300);" style="position: relative;float: right;bottom: initial;top: 5px;right: 4px;color: #ffffff;cursor: pointer;display:  block;background: transparent;"></span>';
        $r .= 'DELIVERY';
        $r .= '</div>'//zPStep
                . '<div class="Boddy">';
        $r .= '<label for="city" class="" style="float:  left;">
                                Delivery County *
                           </label>
                            <select onchange="ChangeCity(jQuery(\'#city option:selected\').val());" class="country_select select2-hidden-accessible" data-placeholder="Delivery County" name="city" id="city" tabindex="-1" aria-hidden="true">
                                    <option value="">Delivery County</option>
                                    <option value="Palm Beach County">Palm Beach County</option>
                                    <option value="Broward and Martin County">Broward and Martin County</option>
                                    <option value="Store Pick Up">Store Pick Up</option>
                            </select>';
        $r .= '<label for="time" class="" style="float:  left;">
                        Day of Delivery *
                   </label>
		<select disabled onchange="ChangeTime(jQuery(\'#time option:selected\').val());" class="country_select select2-hidden-accessible" data-placeholder="Delivery County" name="time" id="time" tabindex="-1" aria-hidden="true">
			<option value="">Day of Delivery</option>
		</select>';
        $r .= '<div id="message_date_time" style="margin-top: 15px;"></div>';
        $r .= '<a onclick="proccesssubmit(\'' . $this->uniq . '\')" id="btn btn-submit-order" style="background: #303530;color: #fff;float: right;font-size: 14px;padding: 7px 22px;border-radius:  4px;cursor:  pointer;">Submit Order</a>';
        /* . '<a class="green" onclick="jQuery(\'#zPfixed\').hide(100);jQuery(\'#zPfixed\').attr(\'class\',\'zPfixed\');">Edit Meal Selection</a>';
          if (!$this->isEdit) {
          $r .= '<a class="green" onclick="jQuery(\'.btnzCupon\').hide();jQuery(\'.zpDashControl\').attr(\'ready\',\'1\');jQuery(\'.zpDashControl\').show(500);jQuery(\'#zMoveStickyCont\').attr(\'style\',\'text-align: center;margin-top: 30px;opacity: 0;display:none\');jQuery(\'#zPfixed\').hide(100);jQuery(\'#zPfixed\').attr(\'class\',\'zPfixed\');jQuery(\'#zPStep1\').attr(\'class\',\'zgrid\');jQuery(\'#zPStep2\').attr(\'class\',\'zgrid actived\');">NEXT STEP</a>';
          //. '<a class="green" onclick="jQuery(\'#' . $this->uniq . '\').click();">Go Checkout</a>';
          } else {
          $r .= '<a class="green" onclick="jQuery(\'#' . $this->uniq . '\').click();">Save Selected Items</a>';
          } */
        $r .= '</div>'
                . '</div>'
                . '</div></div>';
        return $r;
    }

    public function PopUp() {
        $r = '';
        $r = '<div id="zPfixed" class="zPfixed" style="display:none;">'
                . '<div class="contenedor"><div class="WinPopUp"';
        //if ($this->isEdit) {
        $r .= ' style="height: 150px;margin-top: -75px;"';
        //}
        $r .= '>' . '<div class="Header" style="color:#fff!important">';
        //if (!$this->isEdit) {
        $r .= '_';
        //} else {
        //   $r .= strtoupper('Save Meals Selected');
        //}
        $r .= '</div>'//zPStep
                . '<div class="Boddy">'
                . '<a class="green" onclick="jQuery(\'#zPfixed\').hide(100);jQuery(\'#zPfixed\').attr(\'class\',\'zPfixed\');">Edit Meal Selection</a>';
        if (!$this->isEdit) {
            if ($this->user === 0 && !$this->token) {
                $r .= '<a class="green" onclick="jQuery(\'html, body\').animate({ scrollTop: 0 }, \'slow\');jQuery(\'.btnzCupon\').hide();jQuery(\'.zpDashControl\').attr(\'ready\',\'1\');jQuery(\'.zpDashControl\').show(500);jQuery(\'#zMoveStickyCont\').attr(\'style\',\'text-align: center;margin-top: 30px;opacity: 0;display:none\');jQuery(\'#zPfixed\').hide(100);jQuery(\'#zPfixed\').attr(\'class\',\'zPfixed\');jQuery(\'#zPStep1\').attr(\'class\',\'zgrid\');jQuery(\'#zPStep2\').attr(\'class\',\'zgrid actived\');">NEXT STEP</a>';
            } else {
                $r .= '<a class="green" onclick="jQuery(\'html, body\').animate({ scrollTop: 0 }, \'slow\');jQuery(\'#zPfixed\').hide(100);jQuery(\'#zPfixed\').attr(\'class\',\'zPfixed\');jQuery(\'#DeliDates\').attr(\'style\',\'display:table;\');">NEXT STEP</a>';
            }
            //
        } else {
            $r .= '<a class="green" onclick="jQuery(\'#' . $this->uniq . '\').click();">Save Selected Items</a>';
        }
        $r .= '</div>'
                . '</div>'
                . '</div></div>';
        return $r;
    }

    public function form_proccess_checkout() {
        if ($this->isEdit) {

            //Original
            $uri = zUriMyAccount();
            $action = 'zEditOut_Proccess';
            //echo "Entro a isEdit";
            //exit;
            /*$uri = esc_url(wc_get_checkout_url());

            $action = 'zCheckOut_Proccess';*/
        } else {
            //echo "Entro a no isEdit";
            //exit;
            if (!$this->isSeeMeal) {
                //echo "Entro a no isSeeMeal";
                //exit;
                $uri = esc_url(wc_get_checkout_url());
                $action = 'zCheckOut_Proccess';
            } else {
                //echo "Entro a isSeeMeal";
                //exit;
                $action = 'zSeeMeals_Proccess';
                $uri = zUriMyAccount() . '" data-action-dashboard="' . zUriMyAccount() . '" data-action-checkout="' . esc_url(wc_get_checkout_url());
            }
        }
        $r = '<form id="zCheckOut' . $this->uniq . '" action="' . $uri . '" style="position: fixed;" method="POST" enctype="multipart/form-data">';

        $r .= '<input type="text" name="subscription_action" value="' . $action . '">';
        $r .= '<input type="text" name="type" value="add">';
        if ($this->isEdit) {
            $r .= '<input type="text" name="order" value="' . $this->Order . '">';
        } else {
            if ($this->isSeeMeal) {
                $r .= '<input type="text" name="order" value="' . $this->Order . '">';
                $r .= '<input id="mealcity" type="text" name="city" value="">';
                $r .= '<input id="mealtime" type="text" name="tim" value="">';
                if ($this->user !== 0 && strlen(trim($this->token)) > 0) {
                    $r .= '<input type="text" name="user" value="' . $this->user . '">';
                    $r .= '<input type="text" name="token" value="' . $this->token . '">';
                }
            }
        }
        $r .= '<input type="text" name="uniq" value="' . $this->uniq . '">';
        $r .= '<input type="text" name="product_id" value="' . $this->id . '">';
        $r .= '<input type="text" name="variation_id" value="' . $this->variable . '">';
        $r .= '<input type="text" name="date" value="' . date('d-m-Y') . '">';
        $r .= '<input type="text" name="meals" value="">';
        $r .= '<input type="text" name="additional" value="">';
        $r .= '<input type="text" name="max" value="' . $this->Meals . '">';
        $r .= '<input id="' . $this->uniq . '" type="submit" value="send">';
        $r .= '</form>';
        $r .= '<div id="zCheckOutName" style="position: fixed;">' . $this->uniq . '</div>';
        //echo "<pre>";
        //var_dump($r);
        //exit;
        return $r;
    }

    public function PopUpLocked() {
        global $woocommerce;
        if ($this->locked) {
            $woocommerce->cart->empty_cart();
            $r = '';
            $r .= '<div>'
                    . '<div style="border: 2px solid #d7d7d7;margin: 40px auto;width: fit-content;width: -moz-fit-content;">'
                    . '<div style="display: inline-block;text-align:  center;max-width: 200px;"><img src="' . z_SITE_URL . z_PLUGIN_URL . 'resources/image/alert.png"></div>'
                    . '<div style="padding: 30px 30px;display: inline-block;vertical-align: top;"><h2>Your account is suspended</h2><div style="font-size: 18px;font-weight:  bold;text-transform: uppercase;">' . $this->UserInfo->user_firstname . ' ' . $this->UserInfo->user_lastname . '</div><div style="font-size: 18px;font-weight: bold;color: #ff4600;">' . $this->UserInfo->user_email . '</div><span>in order to reactivate your account, you must</span><div style="margin-top:20px"><a style="padding: 5px 10px;background: #ff4600;color:  #fff;" href="/contact-us/">Contact us</a></div></div>'
                    . '</div>'
                    . '</div>';
            return $r;
        }
        return false;
    }

}
