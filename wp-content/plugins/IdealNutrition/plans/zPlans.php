<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class zPlans {
    protected $plans ;
    
    function __construct() {
        $this->plans = array();
        foreach (z_Plans() as $plans) {
            $this->plans[] = array(
                'id' => $plans->term_id,
                'shortcode' => 'shortcode-'.$plans->slug,
                'name' => $plans->name,
                'link' => $plans->meta_value
            );
        }
    }
    
    function init() {
        foreach ($this->plans as $e) {
            $function = str_replace('-', '_', 'function-'.$e['shortcode']);
            if(!function_exists($function)){
                $r = "function ".$function."(){ echo _zPLoadPlegablePC('".$e['link']."','".$e['id']."','".$e['name']."'); }";
                eval($r);
            }
            $function = str_replace('-', '_', 'function-'.$e['shortcode'].'-mobile');
            if(!function_exists($function)){
                $r = "function ".$function."(){ return _zPLoadPlegablePC('".$e['link']."','".$e['id']."','".$e['name']."'); }";
                eval($r);
            }
            add_shortcode($e['shortcode'].'-mobile', $function);
            add_shortcode($e['shortcode'], $function);
        }
    }

}
