<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of functions
 *
 * @author JB
 */
function wpdocs_set_html_mail_content_type() {
    return 'text/html';
}

function so_validate_add_cart_item($passed, $product_id, $quantity, $variation_id = '', $variations = '') {

    // do your validation, if not met switch $passed to false
    /* if ( 1 != 2 ){
      $passed = false;
      wc_add_notice( __( 'You can not do that', 'textdomain' ), 'error' );
      } */
    return $passed;
}

function z_Plans() {
    global $wpdb;
    $table_name = $wpdb->prefix . 'termmeta,' . $wpdb->prefix . 'terms';
    $table = explode(',', $table_name);
    return $wpdb->get_results("SELECT * FROM $table_name WHERE $table[0].meta_key='plans' AND $table[1].term_id=$table[0].term_id");
}

function zALLPlans() {
    global $wpdb;
    $table_name = $wpdb->prefix . 'termmeta,' . $wpdb->prefix . 'terms';
    $table = explode(',', $table_name);
    $DB = $wpdb->get_results("SELECT * FROM $table_name WHERE $table[0].meta_key='plans' AND $table[1].term_id=$table[0].term_id ORDER BY $table[0].term_id ASC");
    $DB_PRICE = $wpdb->get_results("SELECT * FROM $table_name WHERE $table[0].meta_key='price' AND $table[1].term_id=$table[0].term_id ORDER BY $table[0].term_id ASC");
    $DB_STEP1 = $wpdb->get_results("SELECT * FROM $table_name WHERE $table[0].meta_key='step1' AND $table[1].term_id=$table[0].term_id ORDER BY $table[0].term_id ASC");
    //$DB_END = $wpdb->get_results("SELECT * FROM $table_name WHERE $table[0].meta_key='end' AND $table[1].term_id=$table[0].term_id ORDER BY $table[0].term_id ASC");
    $DB_COLOR = $wpdb->get_results("SELECT * FROM $table_name WHERE $table[0].meta_key='color' AND $table[1].term_id=$table[0].term_id ORDER BY $table[0].term_id ASC");
    $r = '';
    ?>
    <table class="wp-list-table widefat fixed striped tags ui-sortable">
        <thead>
            <tr>
                <td id="cb" class="manage-column column-cb check-column">
                    <label class="screen-reader-text" for="cb-select-all-1">Select All</label>
                    <input id="cb-select-all-1" type="checkbox">
                </td>
                <!--th scope="col" id="thumb" class="manage-column column-thumb">Image</th-->
                <th scope="col" id="name" class="manage-column column-slug column-primary sortable desc">
                    <span style="line-height: 50px;">Name</span>
                </th>
                <th scope="col" id="slow" class="manage-column column-slug column-primary sortable desc">
                    <span style="line-height: 50px;">Price slow</span>
                </th>
                <th scope="col" id="date" class="manage-column column-slug column-primary sortable desc">
                    <span style="line-height: 50px;">Step 1</span>
                </th>
                <th scope="col" id="description" class="manage-column column-slug sortable desc">
                    <span>Shortcode</span>
                </th>
                <th scope="col" id="slug" class="manage-column column-slug sortable desc">
                    <span>Slug</span>
                </th>
                <th scope="col" id="posts" class="manage-column column-slug sortable desc">
                    <span>Link</span>
                </th>
                <th scope="col" id="color" class="manage-column column-slug sortable desc">
                    <span>Color</span>
                </th>
                <th scope="col" id="handle" class="manage-column column-handle" style="display: table-cell;"></th>
            </tr>
        </thead>
        <tbody id="the-list" data-wp-lists="list:tag">
            <?php if (count($DB) !== 0) {
                ?>
                <?php
                foreach ($DB as $key => $item) {
                    ?>
                    <tr id="tag-24">
                        <th scope="row" class="check-column">
                            <label class="screen-reader-text" for="cb-select-<?= $item->term_id; ?>"><?= $item->name; ?></label>
                            <input type="checkbox" name="delete_tags[]" value="<?= $item->term_id; ?>" id="cb-select-<?= $item->term_id; ?>">
                        </th>
                        <td class="name column-slug has-row-actions column-primary" data-colname="Name">
                            <strong>
                                <a class="row-title" href="#" aria-label="“By the pound” (Edit)"><?= $item->name; ?></a>
                            </strong>
                            <br>
                            <div class="hidden" id="inline_24">
                                <div class="name"><?= $item->name; ?></div>
                                <div class="slug"><?= $item->slug; ?></div>
                                <div class="parent"><?= $item->term_group; ?></div>
                            </div>
                            <div class="row-actions">
                                <span class="edit">
                                    <a href="javascript:void(0)" onclick="jQuery('#PopUp_Update_<?= $item->term_id ?>').show()" aria-label="Edit “<?= $item->name; ?>”">
                                        Edit
                                    </a> |
                                </span>
                                <span class="delete">
                                    <a href="#" class="delete-tag aria-button-if-js" aria-label="Delete “<?= $item->name; ?>”" role="button">
                                        Delete
                                    </a> |
                                </span>
                            </div>
                        </td>
                        <td class="slug column-slug" data-colname="Slug"><?= $DB_PRICE[$key]->meta_value; ?></td>
                        <td class="slug column-slug" data-colname="Slug"><?= viewCategorysArray($DB_STEP1[$key]->meta_value); ?></td>
                        <td class="description column-slug" data-colname="Description">
                            <span aria-hidden="true">[shortcode-<?= $item->slug; ?>]</span>
                            <span class="screen-reader-text">shortcode-<?= $item->slug; ?></span>
                        </td>
                        <td class="slug column-slug" data-colname="Slug"><?= $item->slug; ?></td>
                        <td class="posts column-slug" data-colname="Count">
                            <a href="#"><?= $item->meta_value; ?></a>
                        </td>
                        <th scope="col" id="color" class="manage-column column-slug sortable desc">
                            <div style="background-color: <?= $DB_COLOR[$key]->meta_value ?>; width:15px; height: 15px"></div>
                        </th>
                        <td class="handle column-handle ui-sortable-handle" data-colname="" style="display: table-cell;">
                            <input type="hidden" name="term_id" value="<?= $item->term_group; ?>">
                        </td>
                    </tr>
                    <?php
                    $r .= '<div id="PopUp_Update_' . $item->term_id . '" style="display: none">
        <div></div>
        <div>
            <form method="POST" action="" enctype="multipart/form-data">
                <input type="hidden" name="subscription_action" value="updatesubscription">
                <input type="hidden" name="terms" value="' . $item->term_id . '">
                <input type="text" disabled value="#' . $item->term_id . '">
                <input type="text" disabled value="' . $item->name . '">
                <input type="text" name="price" value="' . $DB_PRICE[$key]->meta_value . '">';
                    $r .= zALLCategorys('step1' . $item->term_id, true, true,str_replace(']', '', str_replace('[', '', str_replace('"', '', $DB_STEP1[$key]->meta_value))));
                    /* <input type="date" name="start" value="' . $DB_START[$key]->meta_value . '">
                      <input type="date" name="end" value="' . $DB_END[$key]->meta_value . '"> */
                    $r .= '<input type="text" name="color" value="' . $DB_COLOR[$key]->meta_value . '">
                <input class="" type="submit" value="Udapte" />
            </form>
        </div>
    </div>';
                }
            }
            ?>
        </tbody>
        <tfoot>
            <tr>
                <td class="manage-column column-cb check-column">
                    <label class="screen-reader-text" for="cb-select-all-2">Select All</label>
                    <input id="cb-select-all-2" type="checkbox">
                </td>
                <th scope="col" class="manage-column column-slug column-primary sortable desc">
                    <span>Name</span>
                </th>
                <th scope="col" id="slow" class="manage-column column-slug column-primary sortable desc">
                    <span style="line-height: 50px;">Price slow</span>
                </th>
                <th scope="col" id="date" class="manage-column column-slug column-primary sortable desc">
                    <span style="line-height: 50px;">Step 1</span>
                </th>
                <th scope="col" class="manage-column column-slug sortable desc">
                    <span>Shortcode</span>
                </th>
                <th scope="col" class="manage-column column-slug sortable desc">
                    <span>Slug</span>
                </th>
                <th scope="col" class="manage-column column-slug sortable desc">
                    <span>Link</span>
                </th>
                <th scope="col" class="manage-column column-slug sortable desc">
                    <span>Color</span>
                </th>
                <th scope="col" class="manage-column column-handle" style="display: table-cell;"></th>
            </tr>
        </tfoot>
    </table>
    <?php
    echo $r;
}

function viewCategorysArray($arg) {
    global $wpdb;
    $table_name = $wpdb->prefix . 'term_taxonomy,' . $wpdb->prefix . 'terms';
    $table = explode(',', $table_name);
    $DB = $wpdb->get_results("SELECT * FROM $table_name WHERE $table[0].taxonomy='product_cat' AND $table[1].term_id=$table[0].term_id");
    $array = json_decode($arg);
    //var_dump($array);    exit();
    $r = '';
    if (!is_array($array)) {
        return $r;
    }

    if (count($DB) !== 0) {
        foreach ($DB as $key => $item) {
            $table_name = $wpdb->prefix . 'termmeta';
            $DB_i = $wpdb->get_results("SELECT * FROM $table_name WHERE term_id=" . $item->term_id . " AND meta_key='plans'");
            if (count($DB_i) === 0) {
                //$j .= '<option value="' . $item->term_id . '">' .  . '</option>';
                foreach ($array as $key => $id) {
                    if ($item->term_id === $id) {
                        $r .= $item->name . ', ';
                    }
                }
            }
        }
    }
    return $r;
}

function zALLCategorys($id = 'terms', $multiple = false, $return = false,$vorder="") {
    global $wpdb;
    $table_name = $wpdb->prefix . 'term_taxonomy,' . $wpdb->prefix . 'terms';
    $table = explode(',', $table_name);
    $DB = $wpdb->get_results("SELECT * FROM $table_name WHERE $table[0].taxonomy='product_cat' AND $table[1].term_id=$table[0].term_id");
    if (count($DB) !== 0) {
        $r = '';
        if ($multiple && !$return) {
            $r .= '<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css">
            <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/css/bootstrap-select.css" />
            <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
            <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.bundle.min.js"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/js/bootstrap-select.min.js"></script>';
        }
        $name = $multiple == true ? '[]' : '';
        $multiple = $multiple == true ? 'multiple="multiple" class="selectpicker" onclick="SaveOrder(this.value)"' : '';
        $r .= '<select id="' . $id . '" style="max-width:130px"' . $multiple . ' required name="' . $id . $name . '">';
        $j = '';
        foreach ($DB as $key => $item) {
            $table_name = $wpdb->prefix . 'termmeta';
            $DB_i = $wpdb->get_results("SELECT * FROM $table_name WHERE term_id=" . $item->term_id . " AND meta_key='plans' ORDER BY term_id ASC");
            if (count($DB_i) === 0) {
                if ($name === '[]') {
                    $order = 'ID: ' . $item->term_id . ' ';
                }
                $j .= '<option value="' . $item->term_id . '">' . $order . $item->name . '</option>';
                //
            }
        }
        $r .= $j;
        $r .= '</select>';
        if ($name === '[]') {
            //$r .= '<script>function set_order(val){var order_val = document.getElementById("order_val").value;if(order_val==""){order_val = val;}else{order_val = order_val+","+val;}document.getElementById("order_val").value = order_val;}</script>'
            $r .= '<input type="text" name="order' . $id . '" id="order' . $id . '" value="'.$vorder.'">';
        }
        if (!$return) {
            echo $r;
        } else {
            return $r;
        }
    }
}

function addSubscription() {
    ?>
    <form method="POST" action="" enctype="multipart/form-data">
        <input type="hidden" name="subscription_action" value="addsubscription">
        <label for="terms">Select category</label>
        <?php zALLCategorys(); ?>
        <label for="terms">Price slow</label>
        <input style="max-width:60px" id="price" required type="text" name="price" value="0.00">
        <label for="step1">Add Category First Step</label>
        <?php zALLCategorys('step1', true); ?>
        <!--label for="start">Date start</label>
        <input style="max-width:150px" id="start" required type="date" name="start" value="">
        <label for="end">Date end</label>
        <input style="max-width:150px" id="end" required type="date" name="end" value=""-->
        <label for="color">Color</label>
        <input style="max-width:60px" id="color" required type="text" name="color" value="">
        <input  class="" type="submit" value="Generate" />
    </form>
    <script type="text/javascript">
        jQuery(document).ready(function () {
            jQuery('[type="date"]').each(function () {
                //jQuery(this).datepicker();
            });
        });
    </script>
    <?php
}

function addsubscription_request() {
    $Cat = $_REQUEST['step1'];
    $prepare = [];
    foreach (explode(',', $_REQUEST['orderstep1']) as $key => $order) {
        foreach ($Cat as $key => $item) {
            if ($item === $order) {
                $prepare[] = $item;
                unset($Cat[$key]);
            }
        }
    }
    foreach ($Cat as $key => $item) {
        if ($item === $order) {
            $prepare[] = $item;
        }
    }
    add_term_meta(absint($_REQUEST['terms']), 'plans', uniqid(), true);
    add_term_meta(absint($_REQUEST['terms']), 'price', $_REQUEST['price']);
    add_term_meta(absint($_REQUEST['terms']), 'step1', json_encode($prepare));
    //add_term_meta(absint($_REQUEST['terms']), 'end', $_REQUEST['end']);
    add_term_meta(absint($_REQUEST['terms']), 'color', $_REQUEST['color']);
}

function updatesubscription_request() {
    $Cat = $_REQUEST['step1' . $_REQUEST['terms']];
    $prepare = [];
    foreach (explode(',', $_REQUEST['orderstep1' . $_REQUEST['terms']]) as $key => $order) {
        foreach ($Cat as $key => $item) {
            if ($item === $order) {
                $prepare[] = $item;
                unset($Cat[$key]);
            }
        }
    }
    foreach ($Cat as $key => $item) {
        if ($item === $order) {
            $prepare[] = $item;
        }
    }
    update_term_meta($_REQUEST['terms'], 'price', $_REQUEST['price']);
    update_term_meta($_REQUEST['terms'], 'step1', json_encode($prepare));
    //update_term_meta($_REQUEST['terms'], 'end', $_REQUEST['end']);
    update_term_meta($_REQUEST['terms'], 'color', $_REQUEST['color']);
}

function _zPLoadPlegablePC($KEY, $ID, $name, $e = FALSE) {
    global $wpdb;
    $zSymbol = get_woocommerce_currency_symbol();
    $table_name = $wpdb->prefix . 'postmeta';
    $RD = $wpdb->get_results("SELECT * FROM $table_name WHERE meta_key='_product_attributes' AND meta_value LIKE '%" . $KEY . "%'");
    $prepare = array();
    $isSubscriptions = FALSE;
    if (count($RD) !== 0) {
        $RD = $RD[0];
        $Product = wc_get_product($RD->post_id);
        if (class_exists('WC_Subscriptions_Product') && WC_Subscriptions_Product::is_subscription($RD->post_id)) {
            $isSubscriptions = TRUE;
        }
        foreach ($Product->get_available_variations() as $variant) {
            $prepare[] = array(
                'product_id' => $RD->post_id,
                'variation_id' => $variant['variation_id'],
                'attributes' => $variant['attributes']["attribute_meals"],
                'price' => $zSymbol . number_format(floatval($variant['display_price']), 2),
                'slow' => $zSymbol . number_format(floatval($variant['display_price']) / floatval(explode(' ', $variant['attributes']["attribute_meals"])[0]), 2)
            );
        }
    }
    //
    if (!$isSubscriptions) {
        $src = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADsAAAAjCAYAAAAqoHJFAAAACXBIWXMAAAsTAAALEwEAmpwYAAAKTWlDQ1BQaG90b3Nob3AgSUNDIHByb2ZpbGUAAHjanVN3WJP3Fj7f92UPVkLY8LGXbIEAIiOsCMgQWaIQkgBhhBASQMWFiApWFBURnEhVxILVCkidiOKgKLhnQYqIWotVXDjuH9yntX167+3t+9f7vOec5/zOec8PgBESJpHmomoAOVKFPDrYH49PSMTJvYACFUjgBCAQ5svCZwXFAADwA3l4fnSwP/wBr28AAgBw1S4kEsfh/4O6UCZXACCRAOAiEucLAZBSAMguVMgUAMgYALBTs2QKAJQAAGx5fEIiAKoNAOz0ST4FANipk9wXANiiHKkIAI0BAJkoRyQCQLsAYFWBUiwCwMIAoKxAIi4EwK4BgFm2MkcCgL0FAHaOWJAPQGAAgJlCLMwAIDgCAEMeE80DIEwDoDDSv+CpX3CFuEgBAMDLlc2XS9IzFLiV0Bp38vDg4iHiwmyxQmEXKRBmCeQinJebIxNI5wNMzgwAABr50cH+OD+Q5+bk4eZm52zv9MWi/mvwbyI+IfHf/ryMAgQAEE7P79pf5eXWA3DHAbB1v2upWwDaVgBo3/ldM9sJoFoK0Hr5i3k4/EAenqFQyDwdHAoLC+0lYqG9MOOLPv8z4W/gi372/EAe/tt68ABxmkCZrcCjg/1xYW52rlKO58sEQjFu9+cj/seFf/2OKdHiNLFcLBWK8ViJuFAiTcd5uVKRRCHJleIS6X8y8R+W/QmTdw0ArIZPwE62B7XLbMB+7gECiw5Y0nYAQH7zLYwaC5EAEGc0Mnn3AACTv/mPQCsBAM2XpOMAALzoGFyolBdMxggAAESggSqwQQcMwRSswA6cwR28wBcCYQZEQAwkwDwQQgbkgBwKoRiWQRlUwDrYBLWwAxqgEZrhELTBMTgN5+ASXIHrcBcGYBiewhi8hgkEQcgIE2EhOogRYo7YIs4IF5mOBCJhSDSSgKQg6YgUUSLFyHKkAqlCapFdSCPyLXIUOY1cQPqQ28ggMor8irxHMZSBslED1AJ1QLmoHxqKxqBz0XQ0D12AlqJr0Rq0Hj2AtqKn0UvodXQAfYqOY4DRMQ5mjNlhXIyHRWCJWBomxxZj5Vg1Vo81Yx1YN3YVG8CeYe8IJAKLgBPsCF6EEMJsgpCQR1hMWEOoJewjtBK6CFcJg4Qxwicik6hPtCV6EvnEeGI6sZBYRqwm7iEeIZ4lXicOE1+TSCQOyZLkTgohJZAySQtJa0jbSC2kU6Q+0hBpnEwm65Btyd7kCLKArCCXkbeQD5BPkvvJw+S3FDrFiOJMCaIkUqSUEko1ZT/lBKWfMkKZoKpRzame1AiqiDqfWkltoHZQL1OHqRM0dZolzZsWQ8ukLaPV0JppZ2n3aC/pdLoJ3YMeRZfQl9Jr6Afp5+mD9HcMDYYNg8dIYigZaxl7GacYtxkvmUymBdOXmchUMNcyG5lnmA+Yb1VYKvYqfBWRyhKVOpVWlX6V56pUVXNVP9V5qgtUq1UPq15WfaZGVbNQ46kJ1Bar1akdVbupNq7OUndSj1DPUV+jvl/9gvpjDbKGhUaghkijVGO3xhmNIRbGMmXxWELWclYD6yxrmE1iW7L57Ex2Bfsbdi97TFNDc6pmrGaRZp3mcc0BDsax4PA52ZxKziHODc57LQMtPy2x1mqtZq1+rTfaetq+2mLtcu0W7eva73VwnUCdLJ31Om0693UJuja6UbqFutt1z+o+02PreekJ9cr1Dund0Uf1bfSj9Rfq79bv0R83MDQINpAZbDE4Y/DMkGPoa5hpuNHwhOGoEctoupHEaKPRSaMnuCbuh2fjNXgXPmasbxxirDTeZdxrPGFiaTLbpMSkxeS+Kc2Ua5pmutG003TMzMgs3KzYrMnsjjnVnGueYb7ZvNv8jYWlRZzFSos2i8eW2pZ8ywWWTZb3rJhWPlZ5VvVW16xJ1lzrLOtt1ldsUBtXmwybOpvLtqitm63Edptt3xTiFI8p0in1U27aMez87ArsmuwG7Tn2YfYl9m32zx3MHBId1jt0O3xydHXMdmxwvOuk4TTDqcSpw+lXZxtnoXOd8zUXpkuQyxKXdpcXU22niqdun3rLleUa7rrStdP1o5u7m9yt2W3U3cw9xX2r+00umxvJXcM970H08PdY4nHM452nm6fC85DnL152Xlle+70eT7OcJp7WMG3I28Rb4L3Le2A6Pj1l+s7pAz7GPgKfep+Hvqa+It89viN+1n6Zfgf8nvs7+sv9j/i/4XnyFvFOBWABwQHlAb2BGoGzA2sDHwSZBKUHNQWNBbsGLww+FUIMCQ1ZH3KTb8AX8hv5YzPcZyya0RXKCJ0VWhv6MMwmTB7WEY6GzwjfEH5vpvlM6cy2CIjgR2yIuB9pGZkX+X0UKSoyqi7qUbRTdHF09yzWrORZ+2e9jvGPqYy5O9tqtnJ2Z6xqbFJsY+ybuIC4qriBeIf4RfGXEnQTJAntieTE2MQ9ieNzAudsmjOc5JpUlnRjruXcorkX5unOy553PFk1WZB8OIWYEpeyP+WDIEJQLxhP5aduTR0T8oSbhU9FvqKNolGxt7hKPJLmnVaV9jjdO31D+miGT0Z1xjMJT1IreZEZkrkj801WRNberM/ZcdktOZSclJyjUg1plrQr1zC3KLdPZisrkw3keeZtyhuTh8r35CP5c/PbFWyFTNGjtFKuUA4WTC+oK3hbGFt4uEi9SFrUM99m/ur5IwuCFny9kLBQuLCz2Lh4WfHgIr9FuxYji1MXdy4xXVK6ZHhp8NJ9y2jLspb9UOJYUlXyannc8o5Sg9KlpUMrglc0lamUycturvRauWMVYZVkVe9ql9VbVn8qF5VfrHCsqK74sEa45uJXTl/VfPV5bdra3kq3yu3rSOuk626s91m/r0q9akHV0IbwDa0b8Y3lG19tSt50oXpq9Y7NtM3KzQM1YTXtW8y2rNvyoTaj9nqdf13LVv2tq7e+2Sba1r/dd3vzDoMdFTve75TsvLUreFdrvUV99W7S7oLdjxpiG7q/5n7duEd3T8Wej3ulewf2Re/ranRvbNyvv7+yCW1SNo0eSDpw5ZuAb9qb7Zp3tXBaKg7CQeXBJ9+mfHvjUOihzsPcw83fmX+39QjrSHkr0jq/dawto22gPaG97+iMo50dXh1Hvrf/fu8x42N1xzWPV56gnSg98fnkgpPjp2Snnp1OPz3Umdx590z8mWtdUV29Z0PPnj8XdO5Mt1/3yfPe549d8Lxw9CL3Ytslt0utPa49R35w/eFIr1tv62X3y+1XPK509E3rO9Hv03/6asDVc9f41y5dn3m978bsG7duJt0cuCW69fh29u0XdwruTNxdeo94r/y+2v3qB/oP6n+0/rFlwG3g+GDAYM/DWQ/vDgmHnv6U/9OH4dJHzEfVI0YjjY+dHx8bDRq98mTOk+GnsqcTz8p+Vv9563Or59/94vtLz1j82PAL+YvPv655qfNy76uprzrHI8cfvM55PfGm/K3O233vuO+638e9H5ko/ED+UPPR+mPHp9BP9z7nfP78L/eE8/sl0p8zAAAAIGNIUk0AAHolAACAgwAA+f8AAIDpAAB1MAAA6mAAADqYAAAXb5JfxUYAAAQxSURBVHja7JlriFVVFMf/q3n00JkGQqVpohkoVAKdMnoYPRjRmvqSJoUfsg9R0APKD4KFmBjZB0MkgtGiHIpgoqKYSQqVaErBgiyUZgh7TCEpRY46ZWrO/PrQOrrndO65jznXazkLNvfstddj//dee+3HNUBnC52js4jGwf7vwZpZyUXSfZKGJXGGlZ3lmNlZZ2iUXBVWqjM2vlHSqqC+RdLlkp6U1FUmQLsk1UlaLOlT5zUF32UDe9jMBoB6SW0OVJI2mdlAOZACOyTNlTTHzF53Xk5hjXWvBdbxD20DNgMnGE07gLvz2DDgSuB24HpgYoG+F7qPEeAu5zUHvk9hDCorgI9zlA1AdQ5nVcB7/JuOAkMx3ifArAQb9wM/JOi/CkwpYJDed52/gOeAm/KBzUdbHVS8fBnIDAIdwI3R4ADTgE4f+WgGOoFG7+iGmJ99wJGgvheYmgfwhcBHOfqdCnYV8ERQCqFh4Jm00AOuBnoDnT+AnqD+EtDksjXAAmC/t/UBtXkAVwGPAHuKAdscMxLRemBlULYFbQuLWN8LgO8SosYSZKf5oAA8UISP68YKtjW2Tr53/oslJLRaYClwMLD/OTA7QfYFb+8pwn5zlmBbg/XXlMNhu6/nJSnJbZKv8TB7vxn2AbjX+f1jBVvqqWeG/35tZntzyMyW1CppraSvgFviAmb2q5k9LGmmpM3OvkdSv2fVOkn1zh/KYlMuZWYfc153it3zgOdja/MNoDFFpx3oD+T3A7ujBFapML4IeDbk5XB6LnAoBnjI12tNDp1q4FHgt5je4gKBXgOszgxskZHT5fp7YgeHPqAtRa8BWAscDwZpeor8dGB7sfts1mCjBHMQqAeeBv4M7HYBl6ToTw1C+52UreZQkDR3VgpsHXDMbcxzXgvQXURoz3e5wwlt9cDPUcYGZmS6ZksAvMltvBbj3xk7YPQDcxP027z9WELbcm/7Bbg48wRVAthFbuMEMCcha6+IhfZbwKXefgHwgfN7E2zv8rZl+bKxBde76KMlvHtySuBmSbsDP0fM7HiBYKv8Mn2D++nxS/dwINbizzvRsfGopC8kXSFpsvM+lPRZzPxySVX+cPCT8xokPR5t5yf39QSwMwMlSRrMgWFEUp+kdyV1mNm+PIAnSXrbB+10UirYUuh3SUvNbH2+u6ekOyS1S2rM+N1qgkfOBK9v9VPZmqQT1DcpV7gDvg4avEwBrgWeAgYCudWVfF3zPkaHkYdi+EZValw4qZyf51j4cgD4wQoDfsX70ZETbAZOOoOL/Mq0ASoz2C3ejzVxsCfXrD92j8VJraRuSbc564Anr+2SfvREN1hGnHWSFvmzrSTdama9oyYyq5kNnkaWxS7llaCNCTkpu5mNH+EkzZc0z7eyyyRNPA1R/K2kDknrzGwk/oZs4//PjoP979PfAwBTrsrRmYGOwgAAAABJRU5ErkJggg==';
        $maxW = 'max-width: 28px;';
    } else {
        $src = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADMAAAArCAYAAADVJLDcAAAACXBIWXMAAAsTAAALEwEAmpwYAAAKTWlDQ1BQaG90b3Nob3AgSUNDIHByb2ZpbGUAAHjanVN3WJP3Fj7f92UPVkLY8LGXbIEAIiOsCMgQWaIQkgBhhBASQMWFiApWFBURnEhVxILVCkidiOKgKLhnQYqIWotVXDjuH9yntX167+3t+9f7vOec5/zOec8PgBESJpHmomoAOVKFPDrYH49PSMTJvYACFUjgBCAQ5svCZwXFAADwA3l4fnSwP/wBr28AAgBw1S4kEsfh/4O6UCZXACCRAOAiEucLAZBSAMguVMgUAMgYALBTs2QKAJQAAGx5fEIiAKoNAOz0ST4FANipk9wXANiiHKkIAI0BAJkoRyQCQLsAYFWBUiwCwMIAoKxAIi4EwK4BgFm2MkcCgL0FAHaOWJAPQGAAgJlCLMwAIDgCAEMeE80DIEwDoDDSv+CpX3CFuEgBAMDLlc2XS9IzFLiV0Bp38vDg4iHiwmyxQmEXKRBmCeQinJebIxNI5wNMzgwAABr50cH+OD+Q5+bk4eZm52zv9MWi/mvwbyI+IfHf/ryMAgQAEE7P79pf5eXWA3DHAbB1v2upWwDaVgBo3/ldM9sJoFoK0Hr5i3k4/EAenqFQyDwdHAoLC+0lYqG9MOOLPv8z4W/gi372/EAe/tt68ABxmkCZrcCjg/1xYW52rlKO58sEQjFu9+cj/seFf/2OKdHiNLFcLBWK8ViJuFAiTcd5uVKRRCHJleIS6X8y8R+W/QmTdw0ArIZPwE62B7XLbMB+7gECiw5Y0nYAQH7zLYwaC5EAEGc0Mnn3AACTv/mPQCsBAM2XpOMAALzoGFyolBdMxggAAESggSqwQQcMwRSswA6cwR28wBcCYQZEQAwkwDwQQgbkgBwKoRiWQRlUwDrYBLWwAxqgEZrhELTBMTgN5+ASXIHrcBcGYBiewhi8hgkEQcgIE2EhOogRYo7YIs4IF5mOBCJhSDSSgKQg6YgUUSLFyHKkAqlCapFdSCPyLXIUOY1cQPqQ28ggMor8irxHMZSBslED1AJ1QLmoHxqKxqBz0XQ0D12AlqJr0Rq0Hj2AtqKn0UvodXQAfYqOY4DRMQ5mjNlhXIyHRWCJWBomxxZj5Vg1Vo81Yx1YN3YVG8CeYe8IJAKLgBPsCF6EEMJsgpCQR1hMWEOoJewjtBK6CFcJg4Qxwicik6hPtCV6EvnEeGI6sZBYRqwm7iEeIZ4lXicOE1+TSCQOyZLkTgohJZAySQtJa0jbSC2kU6Q+0hBpnEwm65Btyd7kCLKArCCXkbeQD5BPkvvJw+S3FDrFiOJMCaIkUqSUEko1ZT/lBKWfMkKZoKpRzame1AiqiDqfWkltoHZQL1OHqRM0dZolzZsWQ8ukLaPV0JppZ2n3aC/pdLoJ3YMeRZfQl9Jr6Afp5+mD9HcMDYYNg8dIYigZaxl7GacYtxkvmUymBdOXmchUMNcyG5lnmA+Yb1VYKvYqfBWRyhKVOpVWlX6V56pUVXNVP9V5qgtUq1UPq15WfaZGVbNQ46kJ1Bar1akdVbupNq7OUndSj1DPUV+jvl/9gvpjDbKGhUaghkijVGO3xhmNIRbGMmXxWELWclYD6yxrmE1iW7L57Ex2Bfsbdi97TFNDc6pmrGaRZp3mcc0BDsax4PA52ZxKziHODc57LQMtPy2x1mqtZq1+rTfaetq+2mLtcu0W7eva73VwnUCdLJ31Om0693UJuja6UbqFutt1z+o+02PreekJ9cr1Dund0Uf1bfSj9Rfq79bv0R83MDQINpAZbDE4Y/DMkGPoa5hpuNHwhOGoEctoupHEaKPRSaMnuCbuh2fjNXgXPmasbxxirDTeZdxrPGFiaTLbpMSkxeS+Kc2Ua5pmutG003TMzMgs3KzYrMnsjjnVnGueYb7ZvNv8jYWlRZzFSos2i8eW2pZ8ywWWTZb3rJhWPlZ5VvVW16xJ1lzrLOtt1ldsUBtXmwybOpvLtqitm63Edptt3xTiFI8p0in1U27aMez87ArsmuwG7Tn2YfYl9m32zx3MHBId1jt0O3xydHXMdmxwvOuk4TTDqcSpw+lXZxtnoXOd8zUXpkuQyxKXdpcXU22niqdun3rLleUa7rrStdP1o5u7m9yt2W3U3cw9xX2r+00umxvJXcM970H08PdY4nHM452nm6fC85DnL152Xlle+70eT7OcJp7WMG3I28Rb4L3Le2A6Pj1l+s7pAz7GPgKfep+Hvqa+It89viN+1n6Zfgf8nvs7+sv9j/i/4XnyFvFOBWABwQHlAb2BGoGzA2sDHwSZBKUHNQWNBbsGLww+FUIMCQ1ZH3KTb8AX8hv5YzPcZyya0RXKCJ0VWhv6MMwmTB7WEY6GzwjfEH5vpvlM6cy2CIjgR2yIuB9pGZkX+X0UKSoyqi7qUbRTdHF09yzWrORZ+2e9jvGPqYy5O9tqtnJ2Z6xqbFJsY+ybuIC4qriBeIf4RfGXEnQTJAntieTE2MQ9ieNzAudsmjOc5JpUlnRjruXcorkX5unOy553PFk1WZB8OIWYEpeyP+WDIEJQLxhP5aduTR0T8oSbhU9FvqKNolGxt7hKPJLmnVaV9jjdO31D+miGT0Z1xjMJT1IreZEZkrkj801WRNberM/ZcdktOZSclJyjUg1plrQr1zC3KLdPZisrkw3keeZtyhuTh8r35CP5c/PbFWyFTNGjtFKuUA4WTC+oK3hbGFt4uEi9SFrUM99m/ur5IwuCFny9kLBQuLCz2Lh4WfHgIr9FuxYji1MXdy4xXVK6ZHhp8NJ9y2jLspb9UOJYUlXyannc8o5Sg9KlpUMrglc0lamUycturvRauWMVYZVkVe9ql9VbVn8qF5VfrHCsqK74sEa45uJXTl/VfPV5bdra3kq3yu3rSOuk626s91m/r0q9akHV0IbwDa0b8Y3lG19tSt50oXpq9Y7NtM3KzQM1YTXtW8y2rNvyoTaj9nqdf13LVv2tq7e+2Sba1r/dd3vzPoMdFTve75TsvLUreFdrvUV99W7S7oLdjxpiG7q/5n7duEd3T8Wej3ulewf2Re/ranRvbNyvv7+yCW1SNo0eSDpw5ZuAb9qb7Zp3tXBaKg7CQeXBJ9+mfHvjUOihzsPcw83fmX+39QjrSHkr0jq/dawto22gPaG97+iMo50dXh1Hvrf/fu8x42N1xzWPV56gnSg98fnkgpPjp2Snnp1OPz3Umdx590z8mWtdUV29Z0PPnj8XdO5Mt1/3yfPe549d8Lxw9CL3Ytslt0utPa49R35w/eFIr1tv62X3y+1XPK509E3rO9Hv03/6asDVc9f41y5dn3m978bsG7duJt0cuCW69fh29u0XdwruTNxdeo94r/y+2v3qB/oP6n+0/rFlwG3g+GDAYM/DWQ/vDgmHnv6U/9OH4dJHzEfVI0YjjY+dHx8bDRq98mTOk+GnsqcTz8p+Vv9563Or59/94vtLz1j82PAL+YvPv655qfNy76uprzrHI8cfvM55PfGm/K3O233vuO+638e9H5ko/ED+UPPR+mPHp9BP9z7nfP78L/eE8/sl0p8zAAAAIGNIUk0AAHolAACAgwAA+f8AAIDpAAB1MAAA6mAAADqYAAAXb5JfxUYAAAKTSURBVHja7NpPiI1RGMfx7zNuM1xlNiQlKwuEJaUoU0ZKNpTCkrJkZUqKkkx21MzKRkmklGgGNbGchdUtf8rUdM3KjYY0Qvws5nl15u1e953x3vueuc2pt3ve89zOPZ/3nPO+nfNek0SnpC46KHUs5jWgSI7fwFEzIzyAMnAauAzsDsoBKAWYTV7Rl4IvcA+wHNgcFkrqBZ4CO7zovKQBMxsMv5QckjRZ9FCRdMbbcjGESBr38leSBiX98vNTiSF6TApSkbTGy0942YfEEPUNIDW0JoADZlbz8GPgO7B6MdzNysCTYI7cN7OpADnq82tkMcyZH/5ZDfIDqWH3TtLaxTBnksaul3Q8mPDVMBZ2SFQYSWVJL7wt75PGeuxkGpkeXdFgHDLm7ZiStLFBbA4kOoykbkmPMkAm05CoMA55mAEyJxYdpgmkW9JoM0gUmBSkJmlrFmR0GEnLJD0IINsWCikU45DbGSA1SVsy1tl+TBNIKYBI0qV51NteTAryWdL2BjGllwBRYepAdv6jt24uFNPVhjliwBBwzFex/WY2nkCAWx6bBvqAyv/8WMt6RpJJGs7QI39j9VaahQ+zJhALhlM6FhcmBZmRtDcLMlbM9QDSlxUSHUbS1QyQGUl7su7OFIKRdKEexGNDjWLRYSStkPTVl7f7svRW3pg8nzP7gZXAczN7FkKAc8A34KCZjbXqmVbKsa7DyZaQI7qAKw752WpIbhhJPcAhP/0o6Ybj1jnkSKshefZMP7DK83eD8rfAWTMbacc6KS/MriA/AdwB7plZhTamvDDDwCdgzMxeFrVdlQvGzKrAtaJ3QzvqNWC6ZzZImi64TT15YN4w+yqwN4KLLGbfsc5vuC/9D2AJ0/r0ZwBuSKGRKJfPYgAAAABJRU5ErkJggg==';
        $maxW = 'max-width: 22px;';
    }
    $SLow = get_term_meta($ID, 'price')[0];
    $Color = get_term_meta($ID, 'color')[0];

    $saved = $_SESSION[_zPid];
    $_SESSION[_zPid] = '';
    $iD = uniqid();
    $uniq = $iD;

    zp_('<style>'
            . 'div.zpHeaderGreen' . $uniq . '{
                margin-top: 15px;
                background: ' . $Color . ';
                text-align: center;
                padding: 10px;
                color: #fff;
                font-weight: bold;
                font-size: 18px;
                letter-spacing: 0;
                }'
            . '.aslowas {
                margin-top: 20px;
                font-size: 24px;
                text-align: center;
                }'
            . '.zPPrice {
                text-align: center;
                margin-top: 0px;/*30px*/
                margin-bottom: 0px;/*25px*/
                }'
            . '.zPinL {display: inline-block;}'
            . '.zPPriceLow{
                font-size: 80px;
                font-weight: 600;
                }'
            . 'div.zPdecimls,div.zPSymbol {
                font-size: 55px;
                font-weight: 600;
                }'
            . 'div.zPSymbol{vertical-align: super;margin-right: 3px;}'
            . '.zPinL.zPMeal{'
            . ' font-size: 20px;
                padding-left: 9px;
                vertical-align: super;
                letter-spacing: 0px;'
            . '}'
            . '.zPCaption' . $uniq . ' {
                text-align: center;
                color: ' . $Color . ';
                font-size: 16px;
                font-weight: 700;
                margin-bottom: 5px;
                letter-spacing: 0px;
                }'
            . '.zPAddCartGreen {'
            . 'font-size: 18px;
                background-color: rgba(0,0,0,.5);
                margin: 0 auto 20px;
                width: fit-content;
                width: -moz-fit-content;
                padding: 10px 25px;
                color: #fff;
                border-radius: 50px;
                margin-top: 15px;
                cursor: pointer;'
            . '}'
            . 'select.zPSelect' . $uniq . ' {'
            . ' font-size: 14px;
                margin-top: 5px;
                min-height: 38px;
                max-height: 46px;
                padding: 5px;
                max-width: 100%;
                margin: 5px auto;'
            . '}'
            . 'select.zPSelect' . $uniq . ':focus{
                    border-color: ' . $Color . ';
                    box-shadow: 0 0 2px rgba(0, 233, 0, 0.67);
                }'
            . '.zPBodyGreen {text-align: center;}'
            . '.zPSelectCaretGreen' . $uniq . '{position: relative;margin: 0 auto 20px;width: 100%;width: fit-content;width: -moz-fit-content;max-width: 243px;}'
            . '.zPSelectCaretGreen' . $uniq . ':after{    width: 0;
                height: 0;
                border-left: 6px solid transparent;
                border-right: 6px solid transparent;
                border-top: 6px solid ' . $Color . ';
                position: absolute;
                top: 43.5%;
                right: 4px;
                content: "";
                z-index: 98;
                }'
            . '.zPAddCartGreen:hover{background-color: rgba(0, 0, 0, 0.7);}'
            . '.zGreenMobile {
                background: #fff;
                padding-bottom: 30px;
                width: fit-content;
                margin: 0 auto;
                min-width: 340px;
                box-shadow: 1px 2px 9px #00000066;
                }'
            . '</style>');
    //Variable para cambiar tipo de plan si  es pas as you go o subscription
    $subsjs = ($isSubscriptions ? "sub" : "pag");

    zp_('<script type="text/javascript">function Change' . $iD . '(e){'
            . 'jQuery("#zPP' . $iD . '").html(jQuery(e).attr("data-low"));'
            . 'jQuery("#zPD' . $iD . '").html("."+jQuery(e).attr("data-decimal"));'
            . 'jQuery("#zPAddCartGreen"+jQuery(e).attr("data-unique")).attr("href","' . z_SITE_URL . DIRECTORY_SEPARATOR . Page_Exist(PAGE_SELECT_PRODUCTS, true) . DIRECTORY_SEPARATOR . '?add-to-cart="+jQuery(e).attr("data-id")+"&variation_id="+jQuery(e).val()+"&plan='.$subsjs.'");'
            . '}</script>');
    zPash_INI('zGreenMobile' . _zPid, 'zGreenMobile');
    _zdiv('zpHeaderGreen' . _zPid, 'zpHeaderGreen' . $uniq);
    zp_('<img style="' . $maxW . ';margin-right: 5px;" src="' . $src . '">');
    zp_(strtoupper($name));
    _ezdiv();
    _zdiv('', 'zPBodyGreen');
    _zdiv('', 'aslowas');
    zp_('As low as');
    _ezdiv();
    _zdiv('', 'zPPrice');
    _zdiv('', 'zPinL zPSymbol');
    zp_($zSymbol);
    _ezdiv();
    _zdiv('zPP' . $iD, 'zPinL zPPriceLow');
    zp_(str_replace($zSymbol, "", explode('.', $SLow)[0])); //$prepare[0]['slow']
    _ezdiv();
    _zdiv('zPD' . $iD, 'zPinL zPdecimls');
    zp_('.' . explode('.', $SLow)[1]); //$prepare[0]['slow']
    _ezdiv();
    _zdiv('', 'zPinL zPMeal');
    zp_('PER MEAL');
    _ezdiv();
    _ezdiv();
    _zdiv('', 'zPCaption' . $uniq);
    if ($isSubscriptions) {
        zp_('FREE DELIVERY INCLUDED');
    } else {
        zp_('DELIVERY FEES MAY APPLY');
    }
    _ezdiv();
    _zdiv('', 'zPSelectCaretGreen' . $uniq);
    $xZ = '';
    foreach ($prepare as $key => $variant) {
        $xZ .= '<option data-unique="' . $iD . '" data-id="' . $variant['product_id'] . '" data-low="' . str_replace($zSymbol, "", explode('.', $variant['slow'])[0]) . '" data-decimal="' . explode('.', $variant['slow'])[1] . '" value="' . $variant['variation_id'] . '">' . $variant['attributes'] . ' <strong>' . $variant['price'] . ' (' . $variant['slow'] . ' each)</strong></option>'; //
    }
    zp_('<select id="zPSelect' . $iD . '" class="zPSelect' . $uniq . '" name="plans" onchange="Change' . $iD . '(jQuery(this).find(\':selected\'))">' . $xZ . '</select>');
    _ezdiv();
    //_zdiv('', '', 'onclick="alert(\'Ailer\')"');
    if($isSubscriptions) {
      $pagina_actual = $_SERVER['REQUEST_URI'];

      //Get parameter to change order
      if($pagina_actual == '/change-order-subscription/') {
        zp_('<a id="zPAddCartGreen' . $iD . '" class="zPAddCartGreen" href="' . z_SITE_URL . DIRECTORY_SEPARATOR . Page_Exist(PAGE_SELECT_PRODUCTS, true) . DIRECTORY_SEPARATOR . '?add-to-cart=' . $prepare[0]['product_id'] . '&variation_id=' . $prepare[0]['variation_id'] . '&plan=sub&type=change_subscription" style="color: #fff !important;text-decoration: none;padding: 7px 25px;">Buy Now »</a>');
      }
      else {
        zp_('<a id="zPAddCartGreen' . $iD . '" class="zPAddCartGreen" href="' . z_SITE_URL . DIRECTORY_SEPARATOR . Page_Exist(PAGE_SELECT_PRODUCTS, true) . DIRECTORY_SEPARATOR . '?add-to-cart=' . $prepare[0]['product_id'] . '&variation_id=' . $prepare[0]['variation_id'] . '&plan=sub" style="color: #fff !important;text-decoration: none;padding: 7px 25px;">Buy Now »</a>');
      }

    }
    else {
      zp_('<a id="zPAddCartGreen' . $iD . '" class="zPAddCartGreen" href="' . z_SITE_URL . DIRECTORY_SEPARATOR . Page_Exist(PAGE_SELECT_PRODUCTS, true) . DIRECTORY_SEPARATOR . '?add-to-cart=' . $prepare[0]['product_id'] . '&variation_id=' . $prepare[0]['variation_id'] . '&plan=pag" style="color: #fff !important;text-decoration: none;padding: 7px 25px;">Buy Now »</a>');
    }

    //_ezdiv();
    _ezdiv();
    zPash_END();
    $r = $_SESSION[_zPid];
    $_SESSION[_zPid] = $saved;
    if ($e) {
        echo $r;
    } else {
        return $r;
    }
}

function select_your_meals() {
    $SYM = new SelectYourMeals();
    echo $SYM->init();
}

function meals_orders_view() {
    $MY = new myAccount();
    echo $MY->viewMealsOrders();
}

function view_meals_delivery() {
    $MY = new myAccount();
    echo $MY->ViewMealsDelivery();
}

function zEditOut_Proccess() {

    if (!isset($_REQUEST['order'])) {
        return;
    } else {
        global $woocommerce;
        global $wpdb;
        $User = wp_get_current_user();
        $ID_Order = $_REQUEST['order'];
        $table_name = $wpdb->prefix . "orders_in";
        $DB = $wpdb->get_results("SELECT * FROM $table_name WHERE orders=$ID_Order AND user=$User->ID");
        if (count($DB) === 0) {
            return;
        }
        $DB = $DB[0];
        $_Additional = '';
        $products = explode('*', $DB->products);
        foreach ($products as $item) {
            if (strlen(trim($item)) > 0) {
                if (!strpos($item, 'Subscription')) {
                    $_Additional .= $item . '*';
                }
            }
        }
        $_Subscription = isset($_REQUEST['meals']) ? $_REQUEST['meals'] : '';
        $_Products = $_Subscription . $_Additional;
        /* $products = explode('*', $_Products);
          $Cart = [];
          $index = 1;
          $plan = 0;
          foreach ($products as $product) {
          $product = explode(',', $product);
          if (isset($product[0]) && isset($product[1]) && isset($product[2])) {
          $Is = true;
          if (intval($product[2]) > 1) {
          for ($idx = 1; $idx <= intval($product[2]); $idx++) {
          if (strpos($product[1], 'Subscription') === false) {
          $Is = false;
          } else {
          $plan++;
          }
          $Cart[$index . $product[0]] = $Is;
          $index++;
          }
          } else {
          if (strpos($product[1], 'Subscription') === false) {
          $Is = false;
          } else {
          $plan++;
          }
          $Cart[$index . $product[0]] = $Is;
          $index++;
          }
          }
          }
          if ($plan % 2 != 0) {
          $plan = intval($plan / 2) + 1;
          } else {
          $plan = intval($plan / 2);
          }
          $rcox = (explode('And', $DB->tim));
          if (count($rcox) > 1) {
          $rcox[0] = str_replace('You will receive half of your meals on: ', '', $rcox[0]);
          $rcox[0] = str_replace(' ', '', $rcox[0]);
          $rcox[0] = explode(',', $rcox[0])[0];
          $rcox[1] = str_replace(' the other half on: ', '', $rcox[1]);
          //$rcox[1] = str_replace('', '', $rcox[1]);
          $rcox[1] = explode(',', $rcox[1])[0];
          } else {
          $rcox[0] = explode(',', $rcox[0])[0];
          }
          $mwk = '';
          $index = 1;
          foreach ($Cart as $key => $value) {
          if (count($rcox) > 1) {
          if ($value) {
          if ($index > $plan) {
          $mwk .= $key . '=' . $rcox[1] . ',';
          } else {
          $mwk .= $key . '=' . $rcox[0] . ',';
          }
          $index++;
          } else {
          $mwk .= $key . '=' . $rcox[0] . ',';
          }
          } else {
          $mwk .= $key . '=' . $rcox[0] . ',';
          }
          } */
        $mwk = shuffle_delivery($_Products, $DB->tim);
        /*Original*/
        $wpdb->update($table_name, array(
            'products' => $_Products,
            'delivery' => $mwk
                ), array(
            'id' => $DB->id
        ));

        /*$result = $wpdb->insert($wpdb->prefix . 'orders_in', array(
            'id' => $DB->id+1,
            'products' => $_Products,
            'delivery' => $mwk
        ));
        var_dump($result);
        exit();*/
        $order=array();
        $j=0;
        foreach (explode('*', $_Products) as $item) {
            if(strlen(trim($item))>0){
                $j++;
                $to = explode(',', $item);
                $order[]=array(
                    'n'=>$j,
                    'product'=>$to[1],
                    'qty'=>$to[2]
                );
            }
        }
        $mail = new zEmail();
        $mail->send($ID_Order, $order);
    }
}

function shuffle_delivery($Products, $Time) {
    $products = explode('*', $Products);
    $Cart = [];
    $index = 1;
    $plan = 0;
    foreach ($products as $product) {
        $product = explode(',', $product);
        if (isset($product[0]) && isset($product[1]) && isset($product[2])) {
            $Is = true;
            if (intval($product[2]) > 1) {
                for ($idx = 1; $idx <= intval($product[2]); $idx++) {
                    if (strpos($product[1], 'Subscription') === false) {
                        $Is = false;
                    } else {
                        $plan++;
                    }
                    $Cart[$index . $product[0]] = array('key' => $index . $product[0], 'type' => $Is);
                    $index++;
                }
            } else {
                if (strpos($product[1], 'Subscription') === false) {
                    $Is = false;
                } else {
                    $plan++;
                }
                $Cart[$index . $product[0]] = array('key' => $index . $product[0], 'type' => $Is);
                $index++;
            }
        }
    }
    if ($plan % 2 != 0) {
        $plan = intval($plan / 2) + 1;
    } else {
        $plan = intval($plan / 2);
    }
    $rcox = (explode('And', str_replace('<strong>', '', str_replace('</strong>', '', str_replace('<br>', '', $Time)))));
    if (count($rcox) > 1) {
        $rcox[0] = str_replace('You will receive half of your meals on: ', '', $rcox[0]);
        $rcox[0] = str_replace(' ', '', $rcox[0]);
        $rcox[0] = explode(',', $rcox[0])[0];
        $rcox[1] = str_replace(' the other half on: ', '', $rcox[1]);
        //$rcox[1] = str_replace('', '', $rcox[1]);
        $rcox[1] = explode(',', $rcox[1])[0];
    } else {
        $rcox[0] = explode(',', $rcox[0])[0];
    }
    $mwk = '';
    $index = 1;
    shuffle($Cart);
    shuffle($Cart);
    foreach ($Cart as $key => $value) {
        if (count($rcox) > 1) {
            if ($value['type']) {
                if ($index > $plan) {
                    $mwk .= $value['key'] . '=' . $rcox[1] . ',';
                } else {
                    $mwk .= $value['key'] . '=' . $rcox[0] . ',';
                }
                $index++;
            } else {
                $mwk .= $value['key'] . '=' . $rcox[0] . ',';
            }
        } else {
            $mwk .= $value['key'] . '=' . $rcox[0] . ',';
        }
    }

    return $mwk;
}
/*Actualizacion*/
function z_woocommerce_payment_complete($order_id) {
    global $wpdb;

    $table_name = $wpdb->prefix . "orders_in";
    $User = wp_get_current_user();

    $ID_User = $User->ID;
    $ID_IP = zAddressIPs();

    $date = date('d-m-Y');
    $week = ["Sunday" => 1, "Monday" => 2, "Tuesday" => 3, "Wednesday" => 4, "Thursday" => 5, "Friday" => 6, "Saturday" => 7];
    $f = date('l, d-m-Y', strtotime($date));
    $day = $week[explode(',', $f)[0]];
    $count = 0;
    foreach ($week as $n) {
        if ($n > $day) {
            $count++;
        }
    }
    $date = date('d-m-Y', strtotime('+' . ($count + 1) . ' day', strtotime($date)));
    $tim = (get_post_meta($order_id, '_billing_myfield18c', true));
    $_me_date_time = (get_post_meta($order_id, '_billing_myfield18c', true));
    $tz = 'America/Bogota';
    $timestamp = time();
    $dt = new DateTime("now", new DateTimeZone($tz)); //first argument "must" be a string
    $dt->setTimestamp($timestamp); //adjust the object to correct timestamp
    $today_date = $dt->format('Y-m-d H:i:s');

    $order_date = $today_date;

    //You will receive half of your meals on: Sunday, April 7, 2019 10:00 AM ~ 08:00 PM And the other half on: Wednesday, April 10, 2019 10:00 AM ~ 08:00 PM
    $deliveryDates = (explode('And', $tim));

    $firstDeliDate = "0000-00-00 00:00:00";
    $secondDeliDate = "0000-00-00 00:00:00";
    /*Si hay dos fechas de entregas*/
    if (count($deliveryDates) > 1) {
        $deliveryDates[0] = str_replace('You will receive half of your meals on: ', '', $deliveryDates[0]);
        $firstDeliDate = str_replace('<br></strong> ', '', $deliveryDates[0]);
        $firstDeliDate = FormatDate($firstDeliDate);
        //Se convierte a timestamp
        $auxFirst = strtotime($firstDeliDate);
        $firstDeliDate = date("Y-m-d H:i:s", $auxFirst);

        /*Conversion de segunda fecha de entrega*/
        $deliveryDates[1] = str_replace(' the other half on: ', '', $deliveryDates[1]);
        $secondDeliDate = str_replace('<br></strong> ', '', $deliveryDates[1]);
        $secondDeliDate = FormatDate($secondDeliDate);
        $auxSecond = strtotime($secondDeliDate);
        $secondDeliDate = date("Y-m-d H:i:s", $auxSecond);

        $_me_dates_all = (explode(' And ', $_me_date_time));
        $_me_dates_all_1 = str_replace('You will receive half of your meals on: ', '', $_me_dates_all[0]);
        $_me_dates_all_2 = str_replace('the other half on: ', '', $_me_dates_all[1]);


        $reg = $wpdb->get_results( "SELECT * FROM {$wpdb->prefix}orders_reg WHERE dates = '{$_me_dates_all_1}'");
        $reg = isset($reg[0]) ? $reg[0]: false;
        if($reg){
            $wpdb->update($wpdb->prefix.'orders_reg', array(
                'counters'=>(intval($reg->counters)-1)
            ), array(
                'id' => $reg->id
            ));
        }

        $reg = $wpdb->get_results( "SELECT * FROM {$wpdb->prefix}orders_reg WHERE dates = '{$_me_dates_all_2}'");
        $reg = isset($reg[0]) ? $reg[0]: false;
        if($reg){
            $wpdb->update($wpdb->prefix.'orders_reg', array(
                'counters'=>(intval($reg->counters)-1)
            ), array(
                'id' => $reg->id
            ));
        }

    }else {/*Una fecha de entrega*/
        $reg = $wpdb->get_results( "SELECT * FROM {$wpdb->prefix}orders_reg WHERE dates = '{$_me_date_time}'");
        $reg = isset($reg[0]) ? $reg[0]: false;
        if($reg){
            $wpdb->update($wpdb->prefix.'orders_reg', array(
                'counters'=>(intval($reg->counters)-1)
            ), array(
                'id' => $reg->id
            ));
        }

        $firstDeliDate = $deliveryDates[0];

        $firstDeliDate = FormatDate($firstDeliDate);
        //Se convierte a timestamp
        $auxFirst = strtotime($firstDeliDate);

        $firstDeliDate = date("Y-m-d H:i:s", $auxFirst);

    }


    $city = str_replace(' $5', '', get_post_meta($order_id, '_billing_myfield18', true));

    $DB = WC_Order_in(array('ip' => $ID_IP, 'orders' => '-'));
    if (count($DB) !== 0) {
        $DB = $DB[0]->products;
        $mwk = shuffle_delivery($DB, $tim);

        //echo "Variables que se pasan: ". $mwk . " "- $order_id . " " . $firstDeliDate;
        $wpdb->query($wpdb->prepare("UPDATE $table_name SET delivery=%s, orders=%d, tim=%s, city=%s, date=%s, user=%d, status=%d, order_date=%s, first_delivery_date=%s, second_delivery_date=%s WHERE ip = %s AND orders=%s AND status=%d", $mwk, $order_id, $tim, $city, $date, $ID_User, 1, $order_date, $firstDeliDate, $secondDeliDate, $ID_IP, '-', 0));


    }
}
/*Posible actualiza*/
function UpdateDeliveryDate() {
    //var_dump($_REQUEST);
    $rs = WC_Order_in(array('orders' => $_REQUEST["order"], 'user' => $_REQUEST['user']));
    if (count($rs) !== 0) {
        global $wpdb;
        $table_name = $wpdb->prefix . "orders_in";
        $rs = $rs[0];
        unset($_REQUEST['subscription_action']);
        unset($_REQUEST['order']);
        unset($_REQUEST['user']);
        $mwk = '';
        foreach ($_REQUEST as $key => $day) {
            $mwk .= str_replace('time', '', $key) . '=' . $day . ',';
        }
        /*Original*/
        $wpdb->query($wpdb->prepare("UPDATE $table_name SET delivery='$mwk' WHERE orders = '" . $rs->orders . "' AND user='" . $rs->user . "' AND id=" . $rs->id));

    }
}
/*Funcion para modificar pedidos y segunda semana*/
function zSeeMeals_Proccess() {
    global $woocommerce;

    $OrderID = isset($_REQUEST['order']) ? $_REQUEST['order'] : '0';
    $Order = WC_Order_in(array('orders' => $OrderID));
    $_POST['Semana_Siguiente'] = true;
    if (count($Order) !== 0) {
        $Order = $Order[0];
        global $wpdb;
        $_Subscription = isset($_REQUEST['meals']) ? $_REQUEST['meals'] : '';
        $_Additional = isset($_REQUEST['additional']) ? $_REQUEST['additional'] : '';

        //echo "_Aditional: <pre>";
        //var_dump($_Additional);
        //exit;
        $products = $_Subscription . $_Additional;
        $tim = isset($_REQUEST['tim']) ? $_REQUEST['tim'] : '-';
        $_me_delivery_date=$tim;
        $_POST['Fecha_delivery'] = $tim;
        if ($tim !== '-') {
            $tim = str_replace('<strong>', '', str_replace('</strong>', '', str_replace('<br>', '', $tim)));
            $mwk = shuffle_delivery($products, $tim);
        }
        $date = isset($_REQUEST['date']) ? $_REQUEST['date'] : date('d-m-Y');
        $week = ["Sunday" => 1, "Monday" => 2, "Tuesday" => 3, "Wednesday" => 4, "Thursday" => 5, "Friday" => 6, "Saturday" => 7];
        $f = date('l, d-m-Y', strtotime($date));
        $day = $week[explode(',', $f)[0]];
        $count = 0;
        foreach ($week as $n) {
            if ($n > $day) {
                $count++;
            }
        }


        $_POST['Ciudad_delivery'] = $_REQUEST['city'];


        $date = date('d-m-Y', strtotime('+' . ($count + 1) . ' day', strtotime($date)));


        $deliveryDates = (explode('And', $tim));

        $firstDeliDate = "0000-00-00 00:00:00";
        $secondDeliDate = "0000-00-00 00:00:00";

        /*Si hay dos fechas de entregas*/
        if (count($deliveryDates) > 1) {
            $deliveryDates[0] = str_replace('You will receive half of your meals on: ', '', $deliveryDates[0]);


            $firstDeliDate = str_replace('<br></strong> ', '', $deliveryDates[0]);

            $firstDeliDate = FormatDate($firstDeliDate);
            //Se convierte a timestamp
            $auxFirst = strtotime($firstDeliDate);

            $firstDeliDate = date("Y-m-d H:i:s", $auxFirst);


            /*Conversion de segunda fecha de entrega*/
            $deliveryDates[1] = str_replace(' the other half on: ', '', $deliveryDates[1]);

            $secondDeliDate = str_replace('<br></strong> ', '', $deliveryDates[1]);

            $secondDeliDate = FormatDate($secondDeliDate);

            $auxSecond = strtotime($secondDeliDate);

            $secondDeliDate = date("Y-m-d H:i:s", $auxSecond);

            $_me_dates_all = (explode(' And ', $_me_delivery_date));
            $_me_dates_all_1 = str_replace('You will receive half of your meals on: ', '', $_me_dates_all[0]);
            $_me_dates_all_2 = str_replace('the other half on: ', '', $_me_dates_all[1]);


            $reg = $wpdb->get_results( "SELECT * FROM {$wpdb->prefix}orders_reg WHERE dates = '{$_me_dates_all_1}'");
            $reg = isset($reg[0]) ? $reg[0]: false;
            if($reg){
                $wpdb->update($wpdb->prefix.'orders_reg', array(
                    'counters'=>(intval($reg->counters)-1)
                ), array(
                    'id' => $reg->id
                ));
            }

            $reg = $wpdb->get_results( "SELECT * FROM {$wpdb->prefix}orders_reg WHERE dates = '{$_me_dates_all_2}'");
            $reg = isset($reg[0]) ? $reg[0]: false;
            if($reg){
                $wpdb->update($wpdb->prefix.'orders_reg', array(
                    'counters'=>(intval($reg->counters)-1)
                ), array(
                    'id' => $reg->id
                ));
            }

        }else {/*Una fecha de entrega*/
            $firstDeliDate = str_replace('<br></strong> ', '', $deliveryDates[0]);

            $firstDeliDate = FormatDate($firstDeliDate);
            //Se convierte a timestamp
            $auxFirst = strtotime($firstDeliDate);

            $firstDeliDate = date("Y-m-d H:i:s", $auxFirst);

            $reg = $wpdb->get_results( "SELECT * FROM {$wpdb->prefix}orders_reg WHERE dates = '{$_me_delivery_date}'");
            $reg = isset($reg[0]) ? $reg[0]: false;
            if($reg){
                $wpdb->update($wpdb->prefix.'orders_reg', array(
                    'counters'=>(intval($reg->counters)-1)
                ), array(
                    'id' => $reg->id
                ));
            }
        }

        /*Actualizacion tabla post wordpress*/
        $todayUpdateGMT = date("Y-m-d H:i:s");

        $tz = 'America/Bogota';
        $timestamp = time();
        $dt = new DateTime("now", new DateTimeZone($tz)); //first argument "must" be a string
        $dt->setTimestamp($timestamp); //adjust the object to correct timestamp
        $todayUpdate = $dt->format('Y-m-d H:i:s');

        $order_date = $todayUpdate;


        $actWoo = $wpdb->update($wpdb->prefix . 'posts', array(
            'post_date' => $todayUpdate,
            'post_date_gmt' => $todayUpdateGMT
            ), array(
            'id' => $OrderID
        ));


        $table_name = $wpdb->prefix . 'orders_in';
        $old_order = $wpdb->get_results("SELECT * FROM $table_name WHERE id=$Order->id",ARRAY_A);

        /*Original*/
        $wpdb->update($wpdb->prefix . 'orders_in', array(
            'ip' => zAddressIPs(),
            'products' => $products,
            'city' => isset($_REQUEST['city']) ? $_REQUEST['city'] : '-',
            'tim' => $tim,
            'date' => $date,
            'delivery' => $mwk,
            'order_date' => $order_date,
            'first_delivery_date' => $firstDeliDate,
            'second_delivery_date' => $secondDeliDate
                ), array(
            'id' => $Order->id
        ));


        array_shift($old_order[0]);


        $countOrdenes = count($old_order);
        for($i = 0; $i < $countOrdenes; $i++) {
          $wpdb->insert($wpdb->prefix . 'orders_in', $old_order[$i]);
        }

        $rs = (explode('*', $_Additional));
        if (is_array($rs)) {
            if (count($rs) > 0) {
                $woocommerce->cart->empty_cart();
                foreach ($rs as $prod) {
                    if (strlen(trim($prod)) > 0) {
                        $Product = explode(',', $prod);
                        if (isset($Product[0]) && isset($Product[1]) && isset($Product[2])) {
                            $parent_id = wp_get_post_parent_id($Product[0]);
                            $productData = array();
                            $productData['lfbRef'] = sanitize_text_field($Product[0]);

                            if (intval($parent_id) > 0) {
                                $productWoo = new WC_Product_Variation($Product[0]);
                                $attributes = $productWoo->get_variation_attributes();
                                //var_dump($attributes);exit();
                                $woocommerce->cart->add_to_cart($parent_id, $Product[2], $Product[0], $attributes, $productData);
                            } else {
                                $woocommerce->cart->add_to_cart($Product[0], $Product[2], 0, array(), $productData);
                            }
                        }
                    }
                }
            }
        }
        $order=array();
        $j=0;
        foreach (explode('*', $products) as $item) {
            if(strlen(trim($item))>0){
                $j++;
                $to = explode(',', $item);
                $order[]=array(
                    'n'=>$j,
                    'product'=>$to[1],
                    'qty'=>$to[2]
                );
            }
        }
        $mail = new zEmail();
        $mail->send($Order->id, $order);
    }
}
/*Inserccion de pedidos de usuarios nuevos*/
function zCheckOut_Proccess() {
    global $woocommerce;
    global $wpdb;

    $pay_go_order = false;
    $User = wp_get_current_user();
    $_Subscription = isset($_REQUEST['meals']) ? $_REQUEST['meals'] : '';
    $_Additional = isset($_REQUEST['additional']) ? $_REQUEST['additional'] : '';

    $cart_items = $woocommerce->cart->get_cart();


    foreach($cart_items as $item => $values) {
        $_product =  wc_get_product( $values['data']->get_id());
        //echo "<b>".$_product->get_title();
        /*Si es un pedido pay as you go retorna sin guardar*/
        if($_product->get_title() == "Pay As Go You Plans") {
          $pay_go_order = true;
          break;
        }

    }

    $products = $_Subscription . $_Additional;
    $save = array(
        'user' => $User->ID,
        'orders' => '-',
        'ip' => zAddressIPs(),
        'ref' => isset($_REQUEST['uniq']) ? $_REQUEST['uniq'] : uniqid(),
        'products' => $products,
        'city' => '-',
        'tim' => '-',
        'date' => isset($_REQUEST['date']) ? $_REQUEST['date'] : date('d-m-Y'),
        'meal' => isset($_REQUEST['product_id']) ? $_REQUEST['product_id'] : 0,
        'delivery' => '-',
        'status' => 0,
        'stype' => isset($_REQUEST['variation_id']) ? $_REQUEST['variation_id'] : 0,
    ); /**/
    $rs = (explode('*', $save['products']));
    $woocommerce->cart->empty_cart();
    $woocommerce->cart->add_to_cart($_REQUEST['product_id'], 1, $_REQUEST['variation_id']);
    foreach ($rs as $prod) {
        if (strlen(trim($prod)) > 0) {
            $Product = explode(',', $prod);
            if (isset($Product[0]) && isset($Product[1]) && isset($Product[2])) {
                $parent_id = wp_get_post_parent_id($Product[0]);
                $productData = array();
                $productData['lfbRef'] = sanitize_text_field($Product[0]);

                if (intval($parent_id) > 0) {
                    $productWoo = new WC_Product_Variation($Product[0]);
                    $attributes = $productWoo->get_variation_attributes();
                    //var_dump($attributes);exit();
                    $woocommerce->cart->add_to_cart($parent_id, $Product[2], $Product[0], $attributes, $productData).'<br>';
                } else {
                    $woocommerce->cart->add_to_cart($Product[0], $Product[2], 0, array(), $productData).'<br>';
                }
            }
        }
    }

    if (!session_id()) {
        @session_start();
    }
    $_SESSION['_ref_'] = $save['ref'];
    $filter = array(
        'user' => $save['user'],
        'ip' => $save['ip'],
        'orders' => $save['orders'],
        'status' => $save['status']
    );
    if (count(WC_Order_in($filter)) !== 0) {
        $wpdb->delete($wpdb->prefix . 'orders_in', $filter);
    }
    if(!$pay_go_order) {
      $wpdb->insert($wpdb->prefix . 'orders_in', $save);
    }
}
